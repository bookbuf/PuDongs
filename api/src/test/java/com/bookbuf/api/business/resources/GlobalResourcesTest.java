package com.bookbuf.api.business.resources;

import com.bookbuf.api.apis.ClerkAPI;
import com.bookbuf.api.apis.MeasureAPI;
import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.clients.resources.APIResources;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.customer.CreateUserResponse;
import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.bookbuf.api.responses.impl.global.SplashScreenResponse;
import com.bookbuf.api.responses.parsers.impl.global.CheckUserByQueryResponseJSONImpl;
import com.ipudong.core.Result;
import com.ipudong.core.network.RestServiceFactory;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class GlobalResourcesTest extends AbstractBusinessTest {

    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void fetch_splash_screen_mock() throws Exception {

        Result<SplashScreenResponse> result =
                withSession(true).globalResources().querySplashScreen();
        Assert.assertNotNull(result);
        SplashScreenResponse response = result.getModel();
        Assert.assertNotNull(response);
        Assert.assertEquals(1484720921, response.ui().start());
        Assert.assertEquals(1485720921, response.ui().end());
        Assert.assertEquals("http://uploads.yjbys.com/allimg/201701/46-1F114102Z3-50.jpg", response.ui().background());

        Assert.assertEquals("open_discover", response.target().type());
        Assert.assertEquals("splashscreen", response.type());
        Assert.assertEquals(2, response.id());
        Assert.assertEquals(-1, response.vendor());
        Assert.assertNotNull(response.extras());
        Assert.assertNotNull(response.extras().getJSONObject("discover"));
        Assert.assertEquals("http://www.163.com/", response.extras().getJSONObject("discover").getString("url"));


    }

    @Test
    public void fetch_splash_screen() throws Exception {
        Result<SplashScreenResponse> result =
                withSession().globalResources().querySplashScreen();
    }

    @Test
    public void checkUserByQuery() {
        Result<LongResponse> result = withSession().globalResources().checkUserByQuery("18668247775");
        Assert.assertTrue(result.getModel() instanceof CheckUserByQueryResponseJSONImpl);
        Assert.assertEquals(1832065, result.getModel().get());
    }

    @Test
    public void query_role_0() {
        Result<RoleResponse> result = withSession(true).globalResources().queryRole("18668247775");
        RoleResponse response = result.getModel();
        Assert.assertEquals(RoleResponse.ROLE_CLERK, response.id());
        Assert.assertEquals(false, response.isVirtual());
        Assert.assertEquals("店员", response.description());
    }

    @Test
    public void fetchAppUpgrade() {
        withSession().globalResources().fetchAppUpgrade("7");
    }

    @Test
    public void test_checkVerifyCode() {
        withSession().globalResources().checkVerifyCode("18268810820", "clerk", "1234");
    }

    @Test
    public void test_queryProfile() {
        ClerkAPI api = RestServiceFactory.getService("http://172.16.8.194/mapi", ClerkAPI.class);
        api.queryProfile(
                "pudong*mp",
                APIResources.B.CLERK_QUERY_PROFILE.method(),
                APIResources.B.CLERK_QUERY_PROFILE.version(),
                "77abee7defdd438db7bedbce142eca6e",
                7,
                "3.5.0",
                System.currentTimeMillis(),
                "x||101.69.252.3||1920*1080||LeEco||Le X528||1||null",
                "close"
        );
    }

    @Test
    public void test_login() {
        ClerkAPI api = RestServiceFactory.getService("http://172.16.8.194/mapi", ClerkAPI.class);
        api.login(
                "pudong*mp",
                APIResources.Global.CLERK_LOGIN.method(),
                APIResources.Global.CLERK_LOGIN.version(),
                7,
                "3.5.0",
                System.currentTimeMillis(),
                "x||101.69.252.3||1920*1080||LeEco||Le X528||1||null",
                "13655812763",
                "812763",
                "pushId"
        );
    }

    @Test
    public void test_latestForB() {
        final MeasureAPI api = RestServiceFactory.getService("http://172.16.8.194/mapi", MeasureAPI.class);
        api.listLatestRecord(
                "pudong*mp",
                APIResources.B.MEASURE_LATEST_FOR_B.method(),
                APIResources.B.MEASURE_LATEST_FOR_B.version(),
                "7dd07b9e46164bea8bbc5424bd50159e",
                7,
                "3.5.0",
                1480660442993l,
                "862533031361709||101.69.252.3||1920*1080||LeEco||Le X528||1||null",
                1261822);
    }

    @Test
    public void test_createUser() {
        Result<CreateUserResponse> result = withSession().globalResources().createUser(null,null,"057186622158","340300199211238883",null,"",null,"");
        Assert.assertEquals("183",result.getModel().uid());
    }

}