package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class DiscoverResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void test_cancelFavorite() {
        withSession().discoverResources().cancelStoreArticle(39);
    }

    @Test
    public void test_fetchDiscoverQuestion() {
        withSession().discoverResources().fetchDiscoverQuestion(38);
    }

    @Test
    public void answerQuestionDiscovery() {
        withSession().discoverResources().answerQuestionDiscovery(38, "[]");
    }

    @Test
    public void storeArticle() {
        withSession().discoverResources().storeArticle(38);
    }
}
