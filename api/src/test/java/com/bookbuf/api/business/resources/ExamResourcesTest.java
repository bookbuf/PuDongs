package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExamResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }
    @Test
    public void fetchExamHistory()  {
        withSession().examResources().fetchExamHistory(1);
    }

    @Test
    public void getExamStatus()  {
        withSession().examResources().getExamStatus(1);
    }

    @Test
    public void beginExam()  {
        withSession().examResources().beginExam(1);
    }

    @Test
    public void handleInExam()  {
        withSession().examResources().handleInExam(1, "A");
    }

    @Test
    public void terminateExam()  {
        withSession().examResources().terminateExam(1);
    }


}