package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class QuestionResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }
    @Test
    public void fetchQuestion() {
        withSession().questionResources().fetchQuestion(58);
    }

    @Test
    public void answerQuestion() {
        withSession().questionResources().answerQuestion(41, "A");
    }

}