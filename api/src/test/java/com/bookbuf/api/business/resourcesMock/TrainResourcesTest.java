package com.bookbuf.api.business.resourcesMock;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.clients.b.BusinessApi;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class TrainResourcesTest extends AbstractBusinessTest {


    static {
        isMock = true;
    }

    @Test
    public void test_fetchCareerMap() throws Exception {

        BusinessApi resources = withSession();

        Assert.assertNotNull(resources);

        Result<FetchCareerMapResponse> result =
                withSession().achievementResources().fetchCareerMap();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        Assert.assertEquals(100, result.getModel().type());
        Assert.assertEquals(100, result.getModel().type());
        Assert.assertEquals(false, result.getModel().isHot());
        Assert.assertEquals(3, result.getModel().achievements().size());
        Assert.assertEquals(101, result.getModel().achievements().get(0).id());
        Assert.assertEquals("实习店员", result.getModel().achievements().get(0).title());
        Assert.assertEquals("实习店员是迈向人生巅峰的第一步啊", result.getModel().achievements().get(0).description());
        Assert.assertEquals(2, result.getModel().achievements().get(0).status());
        Assert.assertEquals(10, result.getModel().achievements().get(0).passedCount());
        Assert.assertEquals(10, result.getModel().achievements().get(0).totalCount());

    }

    @Test
    public void test_fetchAchievement() throws Exception {
        BusinessApi resources = withSession();

        Assert.assertNotNull(resources);

        Result<FetchAchievementDetailResponse> result =
                resources.achievementResources().fetchAchievement(1);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        FetchAchievementDetailResponse response = result.getModel();

        Assert.assertEquals(false, response.events().get(0).online());
        Assert.assertEquals(true, response.events().get(1).online());

    }

    @Test
    public void test_fetchTrain() throws Exception {
        BusinessApi resources = withSession();


        Result<FetchTrainResponse> result =
                resources.trainResources().fetchTrain(1, 1);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        FetchTrainResponse response = result.getModel();

        Assert.assertEquals(false, response.isSignUp());
        Assert.assertEquals(10012, response.course().id());
        Assert.assertEquals("北京暂不启动公交地铁票价调整", response.course().title());
        Assert.assertEquals(false, response.course().online());
        Assert.assertEquals(0, response.course().status());
        Assert.assertEquals(3, response.course().passedCount());
        Assert.assertEquals(6, response.course().totalCount());

        Assert.assertNotNull(response.offline());
        Assert.assertEquals("王老师", response.offline().teacher());
        Assert.assertEquals("60分钟", response.offline().period());
        Assert.assertEquals("2016年12月12日 14:00", response.offline().time());
        Assert.assertEquals("文一西路998号海创园18幢701", response.offline().address());
    }


    @Test
    public void test_sign_up_train() throws Exception {
        BusinessApi resources = withSession();


        Result<BooleanResponse> result =
                resources.trainResources().signUpTrain(1);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertTrue(result.getModel().get());
    }

    @Test
    public void test_fetch_sign_up_train() throws Exception {
        BusinessApi resources = withSession();


        Result<FetchSignUpCourseResponse> result =
                resources.trainResources().fetchSignUpCourse();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        FetchSignUpCourseResponse response = result.getModel();

        Assert.assertEquals("热门培训", response.achievement().title());
        Assert.assertEquals("完成了这些培训，你往高富帅又踏进了一步！", response.achievement().description());
        Assert.assertEquals(5, response.achievement().passedCount());
        Assert.assertEquals(6, response.achievement().totalCount());

        Assert.assertEquals(2, response.courses().size());

        Assert.assertEquals(10011, response.courses().get(0).id());
        Assert.assertEquals("思维与心态", response.courses().get(0).title());
        Assert.assertEquals(0, response.courses().get(0).status());

        Assert.assertEquals(10031, response.courses().get(1).id());
        Assert.assertEquals("销售话术与技巧", response.courses().get(1).title());
        Assert.assertEquals(2, response.courses().get(1).status());

    }

    @Test
    public void test_fetch_lesson() throws Exception {

        BusinessApi resources = withSession();


        Result<FetchLessonResponse> result =
                resources.trainResources().fetchLesson(1, 1);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        FetchLessonResponse response = result.getModel();


        Assert.assertEquals(10011, response.course().id());
        Assert.assertEquals(1, response.course().status());
        Assert.assertEquals(2, response.course().passedCount());
        Assert.assertEquals(3, response.course().totalCount());
        Assert.assertEquals("储备组长：销售话术和技巧", response.course().title());
        Assert.assertEquals("恭喜你正式迈向人生巅峰", response.course().description());

        Assert.assertEquals(3, response.lessons().size());
        Assert.assertEquals(true, response.lessons().get(0).online());
        Assert.assertEquals(true, response.lessons().get(1).online());
        Assert.assertEquals(true, response.lessons().get(2).online());
    }
}