package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponNewResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class CouponResourcesTest extends AbstractBusinessTest {

    static {
//        username = "15657179100";
//        password = "179100";
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    // ------------------------------------------------------------
    // fetch
    // ------------------------------------------------------------
    @Test
    public void test_fetch_coupon() throws Exception {

        Result<FetchCouponStatusResponse> result =
                withSession().couponResources().fetchCoupon("3664353035393631613166643466323761393436326135333333336665666335");

        Assert.assertNotNull(result);
        Assert.assertEquals("无效的优惠券", result.getMsgContent());
        Assert.assertEquals(103, result.getMsgCode());

    }
    // ------------------------------------------------------------
    // verify
    // ------------------------------------------------------------
    @Test
    public void test_verify_coupon_used() throws Exception {

        Result<VerifyCouponResponse> result =
                withSession().couponResources().verifyCoupon("3664353035393631613166643466323761393436326135333333336665666335");

        Assert.assertNotNull(result);
        Assert.assertEquals("无效的优惠券", result.getMsgContent());
        Assert.assertEquals(103, result.getMsgCode());

    }

    @Test
    public void test_verified_coupon() throws Exception {
        Result<CouponNewResponse> result =
                withSession().couponResources().getVerfiedCoupon(1L,1L);
    }

    @Test
    public void test_verify_coupon_out_service() throws Exception {

        Result<VerifyCouponResponse> result =
                withSession().couponResources().verifyCoupon("3239326665343632653631363438363038383361353539306262646364306339");

        Assert.assertNotNull(result);
        Assert.assertEquals("无效的优惠券", result.getMsgContent());
        Assert.assertEquals(103, result.getMsgCode());

    }

    @Test
    public void test_verify_coupon_invalid() throws Exception {

        Result<VerifyCouponResponse> result =
                withSession().couponResources().verifyCoupon("3239326665343632653631363438363038383361353539332262646364306339");

        Assert.assertNotNull(result);
        Assert.assertEquals("无效的优惠券", result.getMsgContent());
        Assert.assertEquals(103, result.getMsgCode());

    }

    @Test
    public void test_verify_coupon_out_range() throws Exception {

        Result<VerifyCouponResponse> result =
                withSession().couponResources().verifyCoupon("6439616236373530663562373435383961323438646335363635636634666634");

        Assert.assertNotNull(result);
        Assert.assertEquals("无效的优惠券", result.getMsgContent());
        Assert.assertEquals(103, result.getMsgCode());
    }

    @Test
    public void test_coupon_code_List() throws Exception {
        Result<CouponAliasListResponse> result =
                withSession().couponResources().searchCouponCodeList();

        Assert.assertNotNull(result);
        Assert.assertEquals("领取优惠码了", result.getModel().couponAliases().get(0).title());

    }

    @Test
    public void test_fetchCouponAlias() throws Exception {

        Result<CouponAliasResponse> result =
                withSession().couponResources().fetchCouponAlias(46);

        Assert.assertNotNull(result);
        Assert.assertEquals("jb8f5", result.getModel().alias());

    }
}