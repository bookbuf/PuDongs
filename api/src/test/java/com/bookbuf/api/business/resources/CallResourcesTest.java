package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class CallResourcesTest extends AbstractBusinessTest {

    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void test_call_cancel() {
        //有数据返回
        withSession().callResources().callCancel("18268810820", 1);
    }

    @Test
    public void test_call() {
        withSession().callResources().call("18668247775");
    }
}
