package com.bookbuf.api.business.resourcesMock;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class CouponResourcesTest extends AbstractBusinessTest {


    static {
        isMock = true;
    }

    @Test
    public void test_verify_coupon() throws Exception {

        Result<VerifyCouponResponse> result =
                withSession().couponResources().verifyCoupon(null);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        VerifyCouponResponse response = result.getModel();
        Assert.assertEquals(true, response.get());

    }

    @Test
    public void test_fetch_coupon() throws Exception {

        Result<FetchCouponStatusResponse> result =
                withSession(true).couponResources().fetchCoupon(null);

        FetchCouponStatusResponse response = result.getModel();

        Assert.assertEquals("2017.01.18", response.beginDate());
        Assert.assertEquals("2017.01.22", response.endDate());
        Assert.assertEquals(3, response.goods().size());


        Assert.assertEquals(1, response.type());
        Assert.assertEquals("奶粉类商品", response.description());

        Assert.assertTrue(response.isOutService());
        Assert.assertEquals(FetchCouponStatusResponse.TimeLimit.NORMAL, response.timeLimit());

    }

    @Test
    public void test_fetch_coupon_alias() throws Exception {

        Result<CouponAliasResponse> result =
                withSession(true).couponResources().fetchCouponAlias(36);


    }

    @Test
    public void test_code_list() throws Exception {

        Result<CouponAliasListResponse> result =
                withSession(true).couponResources().searchCouponCodeList();


    }
}