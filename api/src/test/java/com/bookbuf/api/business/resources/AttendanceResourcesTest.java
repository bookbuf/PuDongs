package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.components.AttendanceMonthRecordResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class AttendanceResourcesTest extends AbstractBusinessTest {

    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void preference() throws Exception {
        withSession().attendanceResource().preference();
    }

    @Test
    public void month_2016_01() throws Exception {
        Result<AttendanceMonthResponse> result = withSession().attendanceResource().month(2016, 1);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        AttendanceMonthResponse model = result.getModel();

        Assert.assertEquals(2016, model.year());
        Assert.assertEquals(1, model.month());
        Assert.assertEquals(31, model.records().size());


        AttendanceMonthRecordResponse recordResponse = model.records().get(0);

        Assert.assertEquals(1, recordResponse.day());
        Assert.assertEquals(0, recordResponse.status());
        Assert.assertEquals(0, recordResponse.signInCount());

    }

    @Test
    public void month_2016_10() throws Exception {
        Result<AttendanceMonthResponse> result = withSession().attendanceResource().month(2016, 10);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        AttendanceMonthResponse model = result.getModel();

        Assert.assertEquals(2016, model.year());
        Assert.assertEquals(10, model.month());
        Assert.assertEquals(31, model.records().size());


        AttendanceMonthRecordResponse recordResponse = model.records().get(0);

        Assert.assertEquals(1, recordResponse.day());
        Assert.assertEquals(0, recordResponse.status());
        Assert.assertEquals(0, recordResponse.signInCount());

    }

    @Test
    public void day() throws Exception {
        withSession().attendanceResource().day(2016, 1, 1);
    }

    @Test
    public void context() throws Exception {
        withSession().attendanceResource().today();
    }

    @Test
    public void apply_success_1() throws Exception {
        //  正常情况签到
        withSession().attendanceResource().apply(0, 100, 28, "设备mac地址", null);
    }

    @Test
    public void apply_success_2() throws Exception {
        //  正常情况签退
        withSession().attendanceResource().apply(1, 100, 28, "设备mac地址", null);
    }

    @Test
    public void apply_fail_3() throws Exception {
        //  异常签卡类型
        withSession().attendanceResource().apply(2, 100, 28, "设备mac地址", null);
    }

    @Test
    public void apply_fail_4() throws Exception {
        // 您马上到达工作地点, 再走几步吧!
        withSession().attendanceResource().apply(0, 100, 28.003, null, "异常说明");
    }

    @Test
    public void apply_fail_5() throws Exception {
        // 您的当前位置不属于工作范围内
        withSession().attendanceResource().apply(0, 100, 28.0044915765, "设备mac地址", "异常说明");
    }

    @Test
    public void apply_success_6() throws Exception {
        withSession().attendanceResource().apply(0, 100, 28.001, null, null);
    }

    @Test
    public void apply_success_7() throws Exception {
        withSession().attendanceResource().apply(0, 100, 28.001, "设备mac地址", null);
    }

    @Test
    public void apply_fail_8() throws Exception {
        withSession().attendanceResource().apply(0, -1, -1, null, null);
    }

    @Test
    public void apply_success_9() throws Exception {
        withSession().attendanceResource().apply(0, -1, -1, null, "异常说明");
    }

    @Test
    public void apply_fail_10() throws Exception {
        withSession().attendanceResource().apply(0, -1, -1, "设备mac地址", null);
    }

    @Test
    public void apply_success_11() throws Exception {
        withSession().attendanceResource().apply(0, -1, -1, "设备mac地址", "异常说明");
    }

    @Test
    public void status() throws Exception {
        // 若有经纬度，才会调用检测status接口
        // 若没有经纬度，则点击签卡直接跳转到补充说明界面
        // withSession().attendanceResource().status(0, -1, -1);
        withSession().attendanceResource().status(0, 100, 28);
    }
}