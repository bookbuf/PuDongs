package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class MessageResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }
    @Test
    public void fetchMessageCenter() {
        withSession().messageResources().fetchMessageCenter();
    }

    @Test
    public void fetchSystemMessages() {
        withSession().messageResources().fetchSystemMessages();
    }

    @Test
    public void fetchDetailMessage() {
        withSession().messageResources().fetchDetailMessage(1);
    }

}