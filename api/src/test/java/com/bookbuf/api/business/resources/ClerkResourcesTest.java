package com.bookbuf.api.business.resources;

import com.bookbuf.api.apis.GlobalAPI;
import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.BannerResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.customer.BindCardListResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.impl.user.AddressResponse;
import com.bookbuf.api.responses.impl.user.ClerkResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.FetchClerkProfileResponse;
import com.bookbuf.api.responses.impl.user.ProfileResponse;
import com.bookbuf.api.responses.impl.user.VendorResponse;
import com.bookbuf.api.responses.parsers.impl.BannerResponseJSONImpl;
import com.ipudong.core.Result;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ClerkResourcesTest extends AbstractBusinessTest {
    static {
        username = "17757110004";
        password = "110004";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void test_loginCustomer_to_supplement() {
        Result<CustomerResponse> result =
                withSession().clerkResources().queryCustomer("13575481106", "mobile");

        CustomerResponse response = result.getModel();
        Assert.assertNotNull(response);
        Assert.assertEquals(38023, response.profile().userId());
        Assert.assertEquals("李丽丽", response.profile().realname());
        Assert.assertEquals("13575481106", response.profile().mobile());
        Assert.assertEquals("男", response.profile().gender());
        Assert.assertEquals(47, response.profile().age());
        Assert.assertEquals("", response.profile().idcard());
        Assert.assertEquals("", response.profile().phone());
        Assert.assertEquals(-1, response.profile().height());
        Assert.assertEquals(-1, response.profile().weight());
        Assert.assertNotNull(response.profile().address());

        Assert.assertEquals("", response.profile().address().province());
        Assert.assertEquals("", response.profile().address().city());
        Assert.assertEquals("", response.profile().address().district());
        Assert.assertEquals("", response.profile().address().detail());
        Assert.assertEquals("", response.profile().address().fullAddress());
    }


    @Test
    public void test_createCustomer() {
        Result<LongResponse> result =
                withSession().clerkResources().createCustomer(null,"13388881212", null, "张三", "130105198408212182", "浙江省杭州市");
//        Assert.assertTrue(1832309 == result.getModel().get());
    }

    @Test
    public void test_resetPassword() {
        //返回值为布尔型
        withSession().clerkResources().resetPassword("18268810820", "k97531", "318308", "1");
    }

    @Test
    public void test_updateCustomerProfile() {
        //布尔型返回  连接超时
        withSession().clerkResources().updateCustomerProfileBasic(1831819, "魏波", "18268810820", "330108199403230913", "3.23", "男","057187727139", "蛤");
    }

    @Test
    public void test_queryCustomerProfile() {
        Result<CustomerResponse> result =
                withSession().clerkResources().queryCustomerProfile(906278);
        CustomerResponse response = result.getModel();

        Assert.assertNotNull(response);
        Assert.assertEquals("userId", 906278, response.profile().userId());

    }

    @Test
    public void test_queryClerkProfile_unbind_vendor() {
        withSession().clerkResources().queryClerkProfile();
    }

    @Test
    public void test_queryClerkProfile() {
        Result<ClerkResponse> result = withSession(true).clerkResources().queryClerkProfile();
        Assert.assertNotNull(result);

        ClerkResponse response = result.getModel();

        VendorResponse vendorResponse = response.vendor();
        Assert.assertEquals(2, vendorResponse.id());
        Assert.assertEquals(28, vendorResponse.shopId());
        Assert.assertEquals(1, vendorResponse.position());
        Assert.assertEquals("dsl", vendorResponse.code());
        Assert.assertEquals("总经办", vendorResponse.name());
        Assert.assertEquals("大参林医药", vendorResponse.department());
        Assert.assertEquals("http://s1.healthbok.com/logo/6/36775/145wrhe97b.jpg", vendorResponse.logo());

        ProfileResponse profileResponse = response.profile();

        Assert.assertEquals("1974-04-14", profileResponse.birthday());
        Assert.assertEquals("status", 1, profileResponse.status());
        Assert.assertEquals("女", profileResponse.gender());
        Assert.assertEquals("13555555555", profileResponse.mobile());
        Assert.assertEquals("532628197404143101", profileResponse.idcard());
        Assert.assertEquals("小白龙", profileResponse.realname());
        Assert.assertEquals("", profileResponse.email());
        Assert.assertEquals(38254, profileResponse.userId());

        AddressResponse addressResponse = profileResponse.address();

        // FIXME "full_address": "55555555",
        // Assert.assertEquals("fullAddress", "55555555", addressResponse.fullAddress());
        Assert.assertEquals("55555555", addressResponse.detail());

    }

    @Test
    public void test_queryClerkProfile_2017_01_22() {
        Result<FetchClerkProfileResponse> result = withSession(true).clerkResources().queryClerkProfile_2017_01_22();
        Assert.assertNotNull(result);

        FetchClerkProfileResponse response = result.getModel();

        Assert.assertEquals(28, response.vendorShopId());
        Assert.assertEquals(1, response.vendorPosition());
        Assert.assertEquals("dsl", response.vendorCode());
        Assert.assertEquals("总经办", response.vendorShopName());
        Assert.assertEquals("大参林医药", response.vendorName());
        Assert.assertEquals("http://s1.healthbok.com/logo/6/36775/145wrhe97b.jpg", response.vendorLogoUrl());

        Assert.assertEquals("1974-04-14", response.birthday());
        Assert.assertEquals("status", 1, response.status());
        Assert.assertEquals("女", response.gender());
        Assert.assertEquals("13555555555", response.mobile());
        Assert.assertEquals("532628197404143101", response.idCard());
        Assert.assertEquals("小白龙", response.realname());
        Assert.assertEquals("", response.email());
        Assert.assertEquals(38254, response.userId());

        Assert.assertEquals(0, response.clerkStatus());
        Assert.assertEquals("", response.note());
        Assert.assertEquals("003", response.clerkNum());
        Assert.assertEquals("clerk", response.roleName());


    }

    @Test
    public void test_logout() {
        withSession().clerkResources().logout();
    }

    @Test
    public void test_loginCustomer_18668247775() {
        Result<CustomerResponse>
                result = withSession().clerkResources().queryCustomer("18668247775", "mobile");
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
    }

    @Test
    public void test_loginCustomer() {
        Result<CustomerResponse> result = withSession().clerkResources().queryCustomer("18268810820", "mobile");
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals("10",result.getModel().profile().response().contactFlag());
    }

    @Test
    public void test_updatePassword() {
        withSession().clerkResources().updatePassword("666666", "555555");
    }

    @Test
    public void test_register() {
        withSession().clerkResources().register("13852883893", "230102197910058424", "你好", "123456");
    }

    @Test
    public void test_register_mock_success() {
        withoutSession(true).clerkResources().register("13852883893", "230102197910058424", "你好", "123456");
    }

    @Test
    public void test_updateProfile() {
        withSession().clerkResources().updateProfileBasic(1831819, "魏波", "13268810820", "371083197903103471", "3.23", null, "haha");
    }

    @Test
    public void test_login() {
        Result<CredentialResponse> result =
                withoutSession().clerkResources().login("18668247777", "123456", "12");
        // Result{msgContent='null', msgCode='-1', isSuccess=false, count=-1, model=null}
        // Result{msgContent='系统异常', msgCode='1', isSuccess=false, count=-1, model=com.bookbuf.api.responses.parsers.impl.user.CredentialResponseJSONImpl@4c12331b}
        // Result{msgContent='服务器处于不可达或者其他异常状态，无法正确处理发出的请求。', msgCode='10003', isSuccess=false, count=0, model=com.bookbuf.api.responses.parsers.impl.user.CredentialResponseJSONImpl@459e9125}
        Assert.assertNotNull(result);
    }


    @Test
    public void test_profile() {
        Result<ClerkResponse> result = withSession().clerkResources().queryClerkProfile();
        Assert.assertNotNull(result);
    }


    @Test
    public void test_bindVendor() throws Exception {
        withSession().bindVendor("13003600500", "6", "1234");
    }

    @Test
    public void test_sendClerkCode() throws Exception {
        withSession().sendClerkVerifyCode("18877776666", GlobalAPI.Sign.clerk);
    }

    @Test
    public void test_fetch_banner() {
        Result<BannerResponse> result =
                withSession(true).clerkResources().fetchBannerConfiguration();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        BannerResponse response = result.getModel();

        try {
            BannerResponseJSONImpl bannerResponseJSON = new BannerResponseJSONImpl(new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"sum\":0,\"totalPage\":0,\"currentPage\":0,\"model\":\"\"}"));
            Assert.assertNotNull(bannerResponseJSON.banners());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(6, response.banners().size());
        Assert.assertEquals("http://s1.healthbok.com/article/9/6/783ce083bf111b07.jpg", response.banners().get(0).intent());
        Assert.assertEquals("www.healthbok.com", response.banners().get(0).url());

    }

    @Test
    public void test_get_permission() {
        Result<PermissionResponse> result = withSession().getPermission();
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getModel().geneEnable(),true);
        Assert.assertEquals(result.getModel().hospitalEnable(),false);
    }

    @Test
    public void test_userLogin(){
        Result<CustomerLoginResponse> result = withSession().clerkResources().userLogin("18268810820","161838","");
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        CustomerLoginResponse response = result.getModel();
        Assert.assertEquals(1831820,response.loginId());
    }

    @Test
    public void test_bindCard(){
        Result<BindCardListResponse> result = withSession().clerkResources().bindCard("506","2","1234");
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        BindCardListResponse response = result.getModel();
        Assert.assertEquals("test:1510801487813",response.list().get(0).card());
    }
}