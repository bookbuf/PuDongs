package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * Created by bo.wei on 2016/11/24.
 */

public class CheckVerifyCodeResourceTest extends AbstractBusinessTest {

    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void test_checkVerifyCodeResource() {
        withSession().globalResources().checkVerifyCode("18268810820", "1", "279838");
    }
}
