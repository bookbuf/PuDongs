package com.bookbuf.api.business.resources;

import android.util.Log;

import com.bookbuf.api.apis.MayoAPI;
import com.bookbuf.api.apis.MonitorAPI;
import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.clients.resources.impl.MonitorResources;
import com.bookbuf.api.responses.impl.monitor.MonitorSelectAllResponse;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorCountByIdResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorCountResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorCountTypeListResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorCountTypeResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorPassengerByDateAllResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorSelectAllResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorSiteListResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.monitor.MonitorTypeDataResponseJSONImpl;
import com.ipudong.core.network.RestServiceFactory;
import com.ipudong.core.network.response.impl.HealthbokResponse;

import junit.framework.Assert;

import org.json.JSONException;

import org.json.JSONObject;
import org.junit.Test;

/**
 * Created by bo.wei on 2017/11/28.
 */

public class MonitorResourcesTest extends AbstractBusinessTest{
    @Test
    public void monitorCountTest(){
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);
        String s = "{\"alarmDateEnd\":\"2017-12-24 06:00:00\",\"alarmDateStart\":\"2000-10-24 06:00:00\",\"chainPharmacyId\":4}";

        try {
            JSONObject result = new JSONObject(s);
            HealthbokResponse response = api.selectCount(result.toString());
            MonitorCountResponseJSONImpl responseJSON = new MonitorCountResponseJSONImpl(response.getResponse());
            Assert.assertEquals("5839",responseJSON.data());
            System.out.print(response.getResponse().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test_MonitorCountType ()  {
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);
        String s = "{\"alarmDateEnd\":\"2017-12-06 06:00:00\",\"alarmDateStart\":\"2017-12-04 18:00:00\",\"storeNumber\":\"1159\",\"chainPharmacyId\":1}";

        try {
            JSONObject result = new JSONObject(s);
            HealthbokResponse response = api.selectCountType(result.toString());
            MonitorCountTypeListResponseJSONImpl responseJSON = new MonitorCountTypeListResponseJSONImpl(response.getResponse());
            Assert.assertEquals("5839",responseJSON.data().alarmData().get(0).alarmType());
            System.out.print(response.getResponse().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_MonitorCountById()  {
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);
        String s = "{\"startTime\":\"2017-12-21 16:00:00\",\"endTime\":\"2017-02-21 11:00:00\",\"id\":4}";

        try {
            JSONObject result = new JSONObject(s);
            HealthbokResponse response = api.selectCountBychainPharmacy(result.toString());
            MonitorCountByIdResponseJSONImpl responseJSON = new MonitorCountByIdResponseJSONImpl(response.getResponse());
            Assert.assertEquals(0,responseJSON.data().passengerOff());
            Assert.assertEquals(4,responseJSON.data().id());
            System.out.print(response.getResponse().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_monitorSite(){
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);

        try {
            JSONObject result = new JSONObject();
            result.put("chainPharmacyId",1);
            HealthbokResponse response = api.selectAllSite(result.toString());
            MonitorSiteListResponseJSONImpl responseJSON = new MonitorSiteListResponseJSONImpl(response.getResponse());
            Assert.assertEquals("绵阳市高兴区温州商贸",responseJSON.list().get(0).city());
            System.out.print(response.getResponse().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_monitorType(){
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);

        JSONObject result = new JSONObject();
        HealthbokResponse response = api.getType(result.toString());
        MonitorTypeDataResponseJSONImpl responseJSON = new MonitorTypeDataResponseJSONImpl(response.getResponse());
        Assert.assertEquals("绵阳市高兴区温州商贸",responseJSON.data().list().get(0).typeDesc());
        System.out.print(response.getResponse().toString());
    }

    @Test
    public void test_monitor_select(){
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);

        JSONObject result = new JSONObject();
        try {
            result.put("pageNum","1");
            result.put("pageSize","12");
            result.put("alarmDateEnd","2017-12-24 07:00:00");
            result.put("alarmDateStart","2017-10-24 07:00:00");
            result.put("storeNumber","1159");
            result.put("alarmType","39");
            result.put("chainPharmacyId","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HealthbokResponse response = api.select(result.toString());
        MonitorSelectAllResponseJSONImpl responseJSON = new MonitorSelectAllResponseJSONImpl(response.getResponse());
        Assert.assertEquals("绵阳市高新区温州商贸广场",responseJSON.data().alarmData().list().get(0).alarmSite());
    }

    @Test
    public void test_monitor_selectByDate(){
        MonitorAPI api = RestServiceFactory.getService ("", MonitorAPI.class);

        JSONObject result = new JSONObject();
        try {
            result.put("startTime","2017-02-21 11:00:00");
            result.put("endTime","2017-12-21 11:00:00");
            result.put("storeNumber","1159");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HealthbokResponse response = api.passengerActionSelectByDateTime(result.toString());
        MonitorPassengerByDateAllResponseJSONImpl responseJSON = new MonitorPassengerByDateAllResponseJSONImpl(response.getResponse());
        Assert.assertEquals(0,responseJSON.data().passengerGet());
    }
}
