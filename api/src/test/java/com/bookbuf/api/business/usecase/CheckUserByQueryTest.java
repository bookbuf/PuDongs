package com.bookbuf.api.business.usecase;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.exceptions.PuDongException;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class CheckUserByQueryTest extends AbstractBusinessTest {

    @Test
    public void test_NULL() throws PuDongException {
        withSession().globalResources().checkUserByQuery(null);
    }

    @Test
    public void test_Illegal() throws PuDongException {
        withSession().globalResources().checkUserByQuery("138");
    }

    @Test
    public void test_Mobile() throws PuDongException {
        withSession().globalResources().checkUserByQuery("18668247775");
    }

    @Test
    public void test_IDCard() throws PuDongException {
        withSession().globalResources().checkUserByQuery("321282199206113230");
    }

    @Test
    public void test_Telephone() throws PuDongException {
        withSession().globalResources().checkUserByQuery("05714524365");
    }

    @Test
    public void test_Empty() throws PuDongException {
        withSession().globalResources().checkUserByQuery("");
    }
}