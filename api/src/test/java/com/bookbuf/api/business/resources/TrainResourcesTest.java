package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.clients.b.BusinessApi;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class TrainResourcesTest extends AbstractBusinessTest {

    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    @Test
    public void test_fetchCareerMap() throws Exception {

        BusinessApi resources = withSession();

        Assert.assertNotNull(resources);

        Result<FetchCareerMapResponse> result =
                withSession().achievementResources().fetchCareerMap();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

    }

    @Test
    public void test_fetchAchievement() throws Exception {
        BusinessApi resources = withSession();

        Assert.assertNotNull(resources);

        Result<FetchAchievementDetailResponse> result =
                resources.achievementResources().fetchAchievement(1);

    }

    @Test
    public void test_fetchTrain() throws Exception {
        BusinessApi resources = withSession();


        Result<FetchTrainResponse> result =
                resources.trainResources().fetchTrain(12, 1);


        FetchTrainResponse response = result.getModel();

    }


    @Test
    public void test_sign_up_train() throws Exception {
        BusinessApi resources = withSession();


        Result<BooleanResponse> result =
                resources.trainResources().signUpTrain(1);
    }

    @Test
    public void test_fetch_sign_up_train() throws Exception {
        BusinessApi resources = withSession();


        Result<FetchSignUpCourseResponse> result =
                resources.trainResources().fetchSignUpCourse();

    }

    @Test
    public void test_fetch_lesson() throws Exception {

        BusinessApi resources = withSession();


        Result<FetchLessonResponse> result =
                resources.trainResources().fetchLesson(1, 1);
    }

    @Test
    public void test_fetch_lesson_un() throws Exception {

        BusinessApi resources = withSession();


        Result<FetchLessonResponse> result =
                resources.trainResources().fetchLesson(1);
    }
}