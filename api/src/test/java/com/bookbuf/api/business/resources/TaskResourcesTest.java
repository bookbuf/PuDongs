package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.bookbuf.api.responses.impl.task.TaskResponse;
import com.bookbuf.api.responses.parsers.impl.task.TaskResponseJSONImpl;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/21.
 */

public class TaskResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }
    @Test
    public void test_fetchTaskList() {
        withSession().taskResources().fetchTaskList();
    }


    @Test
    public void test_fetchTaskList_mock() {
        Result<TaskContextResponse> result =
                withSession(true).taskResources().fetchTaskList();
        TaskContextResponse response = result.getModel();
        Assert.assertNotNull(response);
    }

    @Test
    public void test_TaskResponseJSONImpl() throws Exception {
        // [fieldName = id]
        JSONObject jsonObject = new JSONObject("{\n" +
                "    \"stat\": {\n" +
                "        \"performNum\": 0,\n" +
                "        \"totalNum\": 1\n" +
                "    },\n" +
                "    \"ui\": {},\n" +
                "    \"extras\": {\n" +
                "        \"examPaper\": {\n" +
                "            \"duration\": 60,\n" +
                "            \"summary\": \"1\",\n" +
                "            \"id\": 30,\n" +
                "            \"title\": \"1\"\n" +
                "        }\n" +
                "    },\n" +
                "    \"id\": \"\",\n" +
                "    \"type\": \"leaf\",\n" +
                "    \"intent\": {},\n" +
                "    \"target\": {\n" +
                "        \"type\": \"exam\"\n" +
                "    },\n" +
                "    \"desc\": {\n" +
                "        \"summary\": \"去考试\",\n" +
                "        \"title\": \"1\"\n" +
                "    }\n" +
                "}");
        TaskResponse response = new TaskResponseJSONImpl(jsonObject);
        Assert.assertTrue(-1 == response.id());
    }
}
