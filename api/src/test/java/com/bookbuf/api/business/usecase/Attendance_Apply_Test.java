package com.bookbuf.api.business.usecase;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class Attendance_Apply_Test extends AbstractBusinessTest {

    @Test
    public void test_NULL_1()  {
        //定位不到签到
        withSession().attendanceResource().apply(0, -1, -1, null, "异常说明");
    }
    @Test
    public void test_NULL_2()  {
        //定位不到，没有填写说明无法签到
        withSession().attendanceResource().apply(0, -1, -1, null, null);
    }
    @Test
    public void test_success_1()  {
        //  正常情况签到
        withSession().attendanceResource().apply(0, 100, 28, "设备mac地址", null);
    }
    @Test
    public void test_success_2()  {
            //  200米内签到
        withSession().attendanceResource().apply(0, 100, 28.002, "设备mac地址", null);
    }
    @Test
    public void test_success_3()  {
        //  正常情况签退
        withSession().attendanceResource().apply(1, 100, 28, "设备mac地址", null);
    }
    @Test
    public void test_fail_1()  {
        // 您马上到达工作地点, 再走几步吧!
        withSession().attendanceResource().apply(0, 100, 28.003, null, null);
    }
    @Test
    public void test_fail_2()  {
        // 您的当前位置不属于工作范围内
        withSession().attendanceResource().apply(0, 100, 28.0044915765, "设备mac地址", "异常说明");
    }

}