package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExamPaperResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }
    @Test
    public void fetchExamPaperWithExam()  {
        withSession().examPaperResources().fetchExamPaperWithExam();
    }

    @Test
    public void answerQuestion()  {
        withSession().examPaperResources().fetchExamPaper(1);
    }

}