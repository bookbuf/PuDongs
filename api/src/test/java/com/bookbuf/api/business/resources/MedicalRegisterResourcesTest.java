package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.medical.DepartmentResponse;
import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.bookbuf.api.responses.parsers.InternalFactoryImpl;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class MedicalRegisterResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }

    private InternalFactoryImpl get() {
        return new InternalFactoryImpl();
    }

    @Test
    public void sendSMS() throws Exception {
        /*JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"remainNum\":2, \"totalNum\":3 } }");
        Result<MedicalRegisterResponse> result = get().createMedicalRegisterResponse(jsonObject);
        MedicalRegisterResponse response = result.getModel();*/
        Result<List<MedicalRegisterResponse>> result = withSession().medicalRegisterResources().listMedicalRegisters("18268810820");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getModel().get(0).id(),-1);
    }

    @Test
    public void parse_list_departments() throws Exception {

        Result<List<DepartmentResponse>> result =
                withSession(true).medicalRegisterResources().listDepartments(2);
        Assert.assertNotNull(result);
    }
}
