package com.bookbuf.api.business.resources;

import com.bookbuf.api.business.AbstractBusinessTest;

import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/30.
 */

public class MeasureResourcesTest extends AbstractBusinessTest {
    static {
        username = "18668247775";
        password = "123456";
        isMock = false;
        isDevMode = true;
    }
    @Test
    public void test_listLatestMeasureRecord() throws Exception {
        withSession().measureResources().listLatestMeasureRecord(1832065);
    }

}
