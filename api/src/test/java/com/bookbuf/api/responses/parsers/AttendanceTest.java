package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceContextResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceDayResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceStatusResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class AttendanceTest extends AbstractBusinessTest {

    @Test
    public void preference() throws Exception {

        // 测试输入
        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":true}");

        // 开发执行过程
        Result<BooleanResponse> result = new InternalFactoryImpl().createBooleanResponse(jsonObject);

        // 测试的验证过程
        // 验证包裹的正确性
        Assert.assertNotNull(result);// result
        Assert.assertEquals(0, result.getCount());  // count
        Assert.assertEquals(0, result.getMsgCode());// msgcode
        Assert.assertEquals(null, result.getMsgContent());// msgcontent
        Assert.assertNotNull(result.getModel());// model

        // 验证包裹里的东西的正确性
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void month() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"year\":2016,\"month\":11,\"records\":[{\"day\":1,\"isExceptionStatus\":1,\"signInCount\":0},{\"day\":2,\"isExceptionStatus\":1,\"signInCount\":2},{\"day\":3,\"isExceptionStatus\":0,\"signInCount\":2},{\"day\":4,\"isExceptionStatus\":0,\"signInCount\":0}]}}");

        Result<AttendanceMonthResponse> result = new InternalFactoryImpl().createAttendanceMonthResponse(jsonObject);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        Assert.assertEquals(2016, result.getModel().year());
        Assert.assertEquals(11, result.getModel().month());
        Assert.assertEquals(4, result.getModel().records().size());
        Assert.assertEquals(1, result.getModel().records().get(0).day());
        Assert.assertEquals(1, result.getModel().records().get(0).status());
        Assert.assertEquals(0, result.getModel().records().get(0).signInCount());
    }

    @Test
    public void day() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"year\":2016,\"month\":11,\"records\":[{\"id\":99,\"title\":\"上班签到\",\"status\":2,\"tag\":\"旷工\",\"time\":\"14: 12\",\"address\":\"杭州市余杭区文一西路998号\",\"remark\":\"事由：忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了。\"},{\"id\":100,\"title\":\"下班签退\",\"status\":2,\"tag\":\"正常\",\"time\":\"18: 12\",\"address\":\"杭州市余杭区文一西路998号\",\"remark\":null}]}}");
        Result<AttendanceDayResponse> result = new InternalFactoryImpl().createAttendanceDayResponse(jsonObject);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(2, result.getModel().records().size());
//        Assert.assertEquals(99, result.getModel().records().get(0).id());
        Assert.assertEquals("上班签到", result.getModel().records().get(0).title());
        Assert.assertEquals(2, result.getModel().records().get(0).status());
        Assert.assertEquals("旷工", result.getModel().records().get(0).tag());
        Assert.assertEquals("14: 12", result.getModel().records().get(0).time());
        Assert.assertEquals("杭州市余杭区文一西路998号", result.getModel().records().get(0).address());
        Assert.assertEquals("事由：忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了。", result.getModel().records().get(0).remark());
    }

    @Test
    public void context() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"date\":\"2016年11月29日\",\"description\":\"出勤 10:00-18:00\",\"records\":[{\"id\":99,\"title\":\"上班签到\",\"status\":2,\"tag\":\"旷工\",\"time\":\"14: 12\",\"address\":\"杭州市余杭区文一西路998号\",\"remark\":\"事由：忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了。\"}]}}");

        Result<AttendanceContextResponse> result = new InternalFactoryImpl().createAttendanceContextResponse(jsonObject);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        Assert.assertEquals("2016年11月29日", result.getModel().date());
        Assert.assertEquals("出勤 10:00-18:00", result.getModel().description());
        Assert.assertEquals(1, result.getModel().records().size());
//        Assert.assertEquals(99, result.getModel().records().get(0).id());
        Assert.assertEquals("上班签到", result.getModel().records().get(0).title());
        Assert.assertEquals(2, result.getModel().records().get(0).status());
        Assert.assertEquals("旷工", result.getModel().records().get(0).tag());
        Assert.assertEquals("14: 12", result.getModel().records().get(0).time());
        Assert.assertEquals("杭州市余杭区文一西路998号", result.getModel().records().get(0).address());
        Assert.assertEquals("事由：忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了今天要上班了，忘了。", result.getModel().records().get(0).remark());


    }

    @Test
    public void apply() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":true}");

        Result<BooleanResponse> result = new InternalFactoryImpl().createBooleanResponse(jsonObject);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void status() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"id\":99,\"distance\":300,\"date\":\"2016年11月14日 14:20\",\"status\":0}}");

        Result<AttendanceStatusResponse> result = new InternalFactoryImpl().createAttendanceStatusResponse(jsonObject);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        Assert.assertEquals(300, result.getModel().distance(), 0.001);
        Assert.assertEquals("2016年11月14日 14:20", result.getModel().date());
        Assert.assertEquals(0, result.getModel().status());
    }

}