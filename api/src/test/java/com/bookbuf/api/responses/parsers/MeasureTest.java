package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.measure.add.AddRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.LatestMeasureResponse;
import com.bookbuf.api.responses.impl.measure.list.ListRecordResponse;
import com.bookbuf.api.responses.parsers.impl.measure.add.AddRecordResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.measure.latest.LatestMeasureResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.measure.list.ListRecordResponseJSONImpl;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class MeasureTest {

	@Test
	public void test_addRecordResponseJSONImpl () throws Exception {

		JSONObject jsonObject = new JSONObject ("{\"id\":\"bp\",\"unit_name\":\"\",\"indicator_name\":\"血压\",\"group_name\":\"基础指标\",\"take_time\":1478663259,\"time\":\"2016-11-09 11:47:39\",\"status\":3,\"indicator_fields\":[{\"id\":\"sbp\",\"name\":\"收缩压\",\"value\":\"137\",\"status\":2},{\"id\":\"dbp\",\"name\":\"舒张压\",\"value\":\"85\",\"status\":3},{\"id\":\"hr\",\"name\":\"心率\",\"value\":\"64\",\"status\":1}],\"suggestion\":\"指标解读：您的检测结果已超过正常值\"}");

		AddRecordResponse response = new AddRecordResponseJSONImpl (jsonObject);


		Assert.assertEquals ("bp", response.record ().id ());
		Assert.assertEquals ("", response.record ().unit ());
		Assert.assertEquals ("血压", response.record ().indicatorName ());
		Assert.assertEquals ("基础指标", response.record ().groupName ());
		Assert.assertEquals (1478663259, response.record ().takeTime ());
		Assert.assertEquals ("2016-11-09 11:47:39", response.record ().timeFormat ());
		Assert.assertEquals (3, response.record ().status ());
		Assert.assertEquals ("指标解读：您的检测结果已超过正常值", response.suggestion ());

		Assert.assertEquals (3, response.record ().fields ().size ());
		Assert.assertEquals ("sbp", response.record ().fields ().get (0).id ());
		Assert.assertEquals ("收缩压", response.record ().fields ().get (0).name ());
		Assert.assertEquals ("137", response.record ().fields ().get (0).value ());
		Assert.assertEquals (2, response.record ().fields ().get (0).status ());
	}

	@Test
	public void test_LatestMeasureResponseJSONImpl () throws Exception {
		JSONObject jsonObject = new JSONObject ("{\"key1\":[{\"text\":\"您还从未检测过血压\",\"jump_id\":1,\"jump_action_desc\":\"立即检测\",\"color\":\"#666666\"},{\"text\":\"您还从未检测过血糖\",\"jump_id\":2,\"jump_action_desc\":\"立即检测\",\"color\":\"#666666\"},{\"text\":\"您的体质指数偏低\",\"color\":\"#5ad1ea\",\"jump_id\":4,\"jump_action_desc\":\"再次检测\",\"last_time\":\"21天前\",\"id\":\"bmi\",\"unit_name\":\"\",\"name\":\"体质指数\",\"group_name\":\"基础指标\",\"indicator_name\":\"体质指数\",\"take_time\":1478663259,\"time\":\"2016-11-09 11:47:39\",\"status\":4,\"indicator_fields\":[{\"id\":\"bmi\",\"name\":\"体质指数\",\"value\":\"16.6\",\"status\":4}]}],\"key2\":[{\"id\":\"bmi\",\"unit_name\":\"\",\"indicator_name\":\"体质指数\",\"group_name\":\"基础指标\",\"take_time\":1478663259,\"time\":\"2016-11-09 11:47:39\",\"status\":4,\"indicator_fields\":[{\"id\":\"sbp\",\"name\":\"体质指数\",\"value\":\"16.6\",\"status\":4}]}]}");
		LatestMeasureResponse response = new LatestMeasureResponseJSONImpl (jsonObject);

		Assert.assertEquals (1, response.records ().size ());
		Assert.assertEquals (4, response.records ().get (0).status ());
		Assert.assertEquals ("bmi", response.records ().get (0).id ());
		Assert.assertEquals ("基础指标", response.records ().get (0).groupName ());
		Assert.assertEquals ("体质指数", response.records ().get (0).indicatorName ());
		Assert.assertEquals ("", response.records ().get (0).unit ());
		Assert.assertEquals (1478663259, response.records ().get (0).takeTime ());
		Assert.assertEquals ("2016-11-09 11:47:39", response.records ().get (0).timeFormat ());

		Assert.assertEquals ("sbp", response.records ().get (0).fields ().get (0).id ());
		Assert.assertEquals ("体质指数", response.records ().get (0).fields ().get (0).name ());
		Assert.assertEquals ("16.6", response.records ().get (0).fields ().get (0).value ());
		Assert.assertEquals (4, response.records ().get (0).fields ().get (0).status ());


		Assert.assertEquals (3, response.tasks ().size ());
		Assert.assertEquals ("您的体质指数偏低", response.tasks ().get (2).title ());
		Assert.assertEquals ("再次检测", response.tasks ().get (2).action ());
		Assert.assertEquals (4, response.tasks ().get (2).jumpId ());
		Assert.assertEquals ("#5ad1ea", response.tasks ().get (2).color ());


	}

	@Test
	public void test_ListRecordResponseJSONImpl () throws Exception {
		JSONObject jsonObject = new JSONObject ("{\"name\":\"收缩压/舒张压\",\"value_list\":[{\"value\":\"111/69\",\"date\":\"2016-10-18 17:03\",\"status\":1},{\"value\":\"111/69\",\"date\":\"2016-10-18 17:03\",\"status\":1},{\"value\":\"111/95\",\"date\":\"2015-12-25 11:08\",\"status\":3}]}");
		ListRecordResponse response = new ListRecordResponseJSONImpl (jsonObject);


		Assert.assertEquals ("收缩压/舒张压", response.name ());
		Assert.assertEquals (3, response.records ().size ());
		Assert.assertEquals ("111/69", response.records ().get (0).valueFormat ());
		Assert.assertEquals ("2016-10-18 17:03", response.records ().get (0).dateFormat ());
		Assert.assertEquals (1, response.records ().get (0).status ());
	}

}
