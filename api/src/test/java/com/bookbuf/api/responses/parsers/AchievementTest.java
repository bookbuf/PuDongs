package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.parsers.impl.achievement.FetchAchievementDetailResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.achievement.FetchCareerMapResponseJSONImpl;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public class AchievementTest {


    @Test
    public void test_pd_achievement_fetch() throws Exception {

        String json = "{\n" +
                "    \"type\": 100,\n" +
                "    \"achievements\": [\n" +
                "        {\n" +
                "            \"id\": 101,\n" +
                "            \"title\": \"实习店员\",\n" +
                "            \"description\": \"实习店员是迈向人生巅峰的第一步啊\",\n" +
                "            \"status\": 2,\n" +
                "            \"passedCount\": 10,\n" +
                "            \"totalCount\": 10\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 102,\n" +
                "            \"title\": \"正式店员\",\n" +
                "            \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                "            \"status\": 1,\n" +
                "            \"passedCount\": 5,\n" +
                "            \"totalCount\": 6\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 103,\n" +
                "            \"title\": \"储备组长\",\n" +
                "            \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                "            \"status\": 0,\n" +
                "            \"passedCount\": 0,\n" +
                "            \"totalCount\": 5\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        JSONObject jsonObject = new JSONObject(json);
        FetchCareerMapResponse response = new FetchCareerMapResponseJSONImpl(jsonObject);

        Assert.assertEquals(100, response.type());
        Assert.assertEquals(3, response.achievements().size());
        Assert.assertEquals(101, response.achievements().get(0).id());
        Assert.assertEquals("实习店员", response.achievements().get(0).title());
        Assert.assertEquals("实习店员是迈向人生巅峰的第一步啊", response.achievements().get(0).description());
        Assert.assertEquals(2, response.achievements().get(0).status());
        Assert.assertEquals(10, response.achievements().get(0).passedCount());
        Assert.assertEquals(10, response.achievements().get(0).totalCount());
    }


    @Test
    public void test_pd_achievement_detail() throws Exception {

        String json = "{\n" +
                "    \"id\": 1001,\n" +
                "    \"title\": \"储备组长\",\n" +
                "    \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                "    \"status\": 1,\n" +
                "    \"passedCount\": 5,\n" +
                "    \"totalCount\": 6,\n" +
                "    \"events\": [\n" +
                "        {\n" +
                "            \"id\": 10011,\n" +
                "            \"title\": \"思维与心态\",\n" +
                "            \"online\": false,\n" +
                "            \"status\": 0\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 10031,\n" +
                "            \"title\": \"销售话术与技巧\",\n" +
                "            \"status\": 2,\n" +
                "            \"online\": true\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONObject jsonObject = new JSONObject(json);
        FetchAchievementDetailResponse response = new FetchAchievementDetailResponseJSONImpl(jsonObject);

        Assert.assertEquals(1001, response.achievement().id());
        Assert.assertEquals(1, response.achievement().status());
        Assert.assertEquals(5, response.achievement().passedCount());
        Assert.assertEquals(6, response.achievement().totalCount());
        Assert.assertNotNull(response.events());
        Assert.assertEquals(2, response.events().size());
        Assert.assertEquals(10011, response.events().get(0).id());
        Assert.assertEquals(0, response.events().get(0).status());
        Assert.assertEquals("思维与心态", response.events().get(0).title());
        Assert.assertEquals("储备组长", response.achievement().title());
        Assert.assertEquals("恭喜你正式迈向人生巅峰", response.achievement().description());
        Assert.assertEquals(false, response.events().get(0).online());

    }
}
