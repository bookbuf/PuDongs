package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.bookbuf.api.responses.parsers.impl.train.FetchLessonResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.train.FetchSignUpCourseResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.train.FetchTrainResponseJSONImpl;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public class TrainTest {


    @Test
    public void test_pd_train_detail_2() throws Exception {

        String string = "{\n" +
                "    \"success\": true,\n" +
                "    \"msgCode\": null,\n" +
                "    \"msgInfo\": null,\n" +
                "    \"count\": 0,\n" +
                "    \"model\": {\n" +
                "        \"course\": {\n" +
                "            \"id\": 113,\n" +
                "            \"title\": \"lv1:y56\",\n" +
                "            \"description\": \"sdfg\",\n" +
                "            \"online\": true,\n" +
                "            \"status\": 0,\n" +
                "            \"passedCount\": 0,\n" +
                "            \"totalCount\": 2\n" +
                "        },\n" +
                "        \"offLine\": {\n" +
                "            \"teacher\": \"sdfg\",\n" +
                "            \"time\": \"2016-12-24 16:55\",\n" +
                "            \"address\": \"fghj\"\n" +
                "        },\n" +
                "        \"isSignUp\": true\n" +
                "    }\n" +
                "}";
        JSONObject jsonObject = new JSONObject(string);


        Result<FetchTrainResponse> result =
                new InternalFactoryImpl().createFetchTrainResponse(jsonObject);
        FetchTrainResponse response = result.getModel();

        Assert.assertEquals(true, response.isSignUp());
        Assert.assertEquals(113, response.course().id());
        Assert.assertEquals("lv1:y56", response.course().title());
        Assert.assertEquals(2, response.course().totalCount());

        Assert.assertEquals("sdfg", response.offline().teacher());
        Assert.assertEquals("2016-12-24 16:55", response.offline().time());
        Assert.assertEquals("fghj", response.offline().address());
    }

    @Test
    public void test_pd_train_detail() throws Exception {

        String json = "{\n" +
                "        \"course\": {\n" +
                "            \"id\": 113,\n" +
                "            \"title\": \"lv1:y56\",\n" +
                "            \"description\": \"sdfg\",\n" +
                "            \"online\": true,\n" +
                "            \"status\": 0,\n" +
                "            \"passedCount\": 0,\n" +
                "            \"totalCount\": 2\n" +
                "        },\n" +
                "        \"offLine\": {\n" +
                "            \"teacher\": \"sdfg\",\n" +
                "            \"time\": \"2016-12-24 16:55\",\n" +
                "            \"address\": \"fghj\"\n" +
                "        },\n" +
                "        \"isSignUp\": true\n" +
                "    }";
        JSONObject jsonObject = new JSONObject(json);
        FetchTrainResponse response = new FetchTrainResponseJSONImpl(jsonObject);

        Assert.assertEquals(true, response.isSignUp());
        Assert.assertEquals(113, response.course().id());
        Assert.assertEquals("lv1:y56", response.course().title());
        Assert.assertEquals(2, response.course().totalCount());

        Assert.assertEquals("sdfg", response.offline().teacher());
        Assert.assertEquals("2016-12-24 16:55", response.offline().time());
        Assert.assertEquals("fghj", response.offline().address());
    }


    @Test
    public void test_pd_course_fetch() throws Exception {

        String json = "{\n" +
                "    \"title\": \"热门培训\",\n" +
                "    \"description\": \"完成了这些培训，你往高富帅又踏进了一步！\",\n" +
                "    \"passedCount\": 5,\n" +
                "    \"totalCount\": 6,\n" +
                "    \"courses\": [\n" +
                "        {\n" +
                "            \"id\": 10011,\n" +
                "            \"title\": \"思维与心态\",\n" +
                "            \"online\": false,\n" +
                "            \"status\": 0\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 10031,\n" +
                "            \"title\": \"销售话术与技巧\",\n" +
                "            \"online\": true,\n" +
                "            \"status\": 2\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        JSONObject jsonObject = new JSONObject(json);
        FetchSignUpCourseResponse response = new FetchSignUpCourseResponseJSONImpl(jsonObject);

        Assert.assertEquals("热门培训", response.achievement().title());
        Assert.assertEquals("完成了这些培训，你往高富帅又踏进了一步！", response.achievement().description());
        Assert.assertEquals(5, response.achievement().passedCount());
        Assert.assertEquals(6, response.achievement().totalCount());
        Assert.assertNotNull(response.courses());
        Assert.assertEquals(2, response.courses().size());
        Assert.assertEquals(10011, response.courses().get(0).id());
        Assert.assertEquals("思维与心态", response.courses().get(0).title());
        Assert.assertEquals(0, response.courses().get(0).status());
        Assert.assertEquals(false, response.courses().get(0).online());
        Assert.assertEquals(true, response.courses().get(1).online());
    }

    @Test
    public void test_pd_lesson_fetch() throws Exception {

        String json = "{\n" +
                "    \"id\": 10011,\n" +
                "    \"title\": \"储备组长：销售话术和技巧\",\n" +
                "    \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                "    \"status\": 1,\n" +
                "    \"passedCount\": 2,\n" +
                "    \"totalCount\": 3,\n" +
                "    \"lessons\": [\n" +
                "        {\n" +
                "            \"title\": \"课时1：如何判断顾客类型\",\n" +
                "            \"status\": 1,\n" +
                "            \"redirect\": \"http://www.healthbok-inc.com/course/100111/\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"title\": \"课时2：如何描述产品卖点\",\n" +
                "            \"status\": 0,\n" +
                "            \"redirect\": \"http://www.healthbok-inc.com/course/100211/\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"title\": \"课时3：如何正确的给顾客推荐药品\",\n" +
                "            \"status\": 2,\n" +
                "            \"redirect\": \"http://www.healthbok-inc.com/course/100311/\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        JSONObject jsonObject = new JSONObject(json);
        FetchLessonResponse response = new FetchLessonResponseJSONImpl(jsonObject);

        Assert.assertEquals(10011, response.course().id());
        Assert.assertEquals("储备组长：销售话术和技巧", response.course().title());
        Assert.assertEquals("恭喜你正式迈向人生巅峰", response.course().description());
        Assert.assertEquals(1, response.course().status());
        Assert.assertEquals(2, response.course().passedCount());
        Assert.assertEquals(3, response.course().totalCount());
        Assert.assertEquals(3, response.lessons().size());
        Assert.assertEquals("课时1：如何判断顾客类型", response.lessons().get(0).title());
        Assert.assertEquals(1, response.lessons().get(0).status());
        Assert.assertEquals("http://www.healthbok-inc.com/course/100111/", response.lessons().get(0).redirect());

    }

}
