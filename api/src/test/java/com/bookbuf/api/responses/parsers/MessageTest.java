package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.message.MessageResponse;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class MessageTest {

    private InternalFactoryImpl get() {
        return new InternalFactoryImpl();
    }

    @Test
    public void fetchSystemMessages() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":[{\"id\":342,\"title\":\"li测试\",\"time\":\"2016-11-08 13:43:17\",\"status\":1,\"class\":\"com.ipudong.pdserver.client.pojo.msg.MessageListPOJO\"},{\"id\":340,\"title\":\"测试\",\"time\":\"2016-11-08 13:30:13\",\"status\":1,\"class\":\"com.ipudong.pdserver.client.pojo.msg.MessageListPOJO\"}]}");
        List<MessageResponse> list = get().createMessageListResponse(jsonObject).getModel();

        Assert.assertEquals(2, list.size());

        MessageResponse messageResponse = list.get(0);

        Assert.assertEquals(342, messageResponse.id());
        Assert.assertEquals("li测试", messageResponse.title());
        Assert.assertEquals("2016-11-08 13:43:17", messageResponse.time());
        Assert.assertEquals(1, messageResponse.status());
    }

    @Test
    public void fetchDetailMessage() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"id\":342,\"content\":\"测试\",\"title\":\"li测试\",\"time\":\"2016-11-08 13:43:17\",\"class\":\"com.ipudong.pdserver.client.pojo.msg.MessagePOJO\"}}");
        MessageResponse messageResponse = get().createMessageResponse(jsonObject).getModel();
        Assert.assertEquals(342, messageResponse.id());
        Assert.assertEquals("li测试", messageResponse.title());
        Assert.assertEquals("测试", messageResponse.content());
        Assert.assertEquals("2016-11-08 13:43:17", messageResponse.time());

    }

    @Test
    public void fetchMessageCenter() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"news\":13 } }");
        LongResponse messageResponse = get().createFetchNewsMessageResponse(jsonObject).getModel();

        Assert.assertEquals(13, messageResponse.get());
    }
}
