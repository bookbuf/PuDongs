package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by bo.wei on 2016/11/24.
 */

public class BooleanParserTest {

    @Test
    public void test_int_1() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":1}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createBooleanResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void test_string_1() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":\"1\"}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createBooleanResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void test_string_Y() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":\"Y\"}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createBooleanResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void test_boolean_true() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":true}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createBooleanResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void test_string_true() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":\"true\"}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createBooleanResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void test_api_create_discover_record() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":{\"is_answer_right\":true}}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createCreateExaminationRecordResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }

    @Test
    public void test_api_create_discover_record_string() throws Exception {

        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":null,\"model\":{\"is_answer_right\":\"true\"}}";
        JSONObject jsonObject = new JSONObject(json);

        Result<BooleanResponse> result =
                new InternalFactoryImpl().createCreateExaminationRecordResponse(jsonObject);


        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());
        Assert.assertEquals(true, result.getModel().get());
    }
}
