package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.examination.FetchDiscoverQuestionResponse;
import com.bookbuf.api.responses.parsers.impl.question.FetchDiscoverQuestionResponseJSONImpl;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by bo.wei on 2016/11/24.
 */

public class DiscoverTest {
    @Test
    public void fetch_question() throws Exception{
        JSONObject jsonObject = new JSONObject("{\"examId\":22,\"question\":\"在做电话回访前，以下哪些准备是不需要的？\",\"options\":[\"了解顾客的基本信息、健康检测和购药情况\",\"根据顾客年龄、特点安排回访时间\",\"准备标准的自我介绍话术\",\"发短信确认顾客是否方便通话\"]}");
        FetchDiscoverQuestionResponse response = new FetchDiscoverQuestionResponseJSONImpl(jsonObject);

        Assert.assertEquals(22,response.id());
        Assert.assertEquals("在做电话回访前，以下哪些准备是不需要的？",response.title());
        Assert.assertEquals("了解顾客的基本信息、健康检测和购药情况",response.choiceList().get(0));
    }
}
