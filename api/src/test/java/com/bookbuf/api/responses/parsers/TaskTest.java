package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.bookbuf.api.responses.impl.task.TaskDescResponse;
import com.bookbuf.api.responses.impl.task.TaskResponse;
import com.bookbuf.api.responses.impl.task.TaskStatResponse;
import com.bookbuf.api.responses.impl.task.TaskUIResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class TaskTest {

	@Test
	public void fetchTaskList () throws Exception {
//
		JSONObject jsonObject = new JSONObject ("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"progress\":0,\"tasks\":[{\"ui\":{\"background\":\"http://s1.healthbok.com/task/9/6/3aaf1dcf183c287e.png\"},\"target\":{\"type\":\"card\"},\"stat\":{\"totalNum\":5,\"performNum\":0},\"desc\":{\"title\":\"开卡任务\",\"summary\":\"开卡任务任务完成0%\"},\"intent\":{},\"extras\":{},\"type\":\"limb\",\"id\":1476168268694,\"tasks\":[{\"ui\":{},\"target\":{\"type\":\"card\"},\"stat\":{\"totalNum\":1,\"performNum\":0},\"desc\":{\"title\":\"今日第一卡\",\"summary\":\"未完成\"},\"intent\":{},\"extras\":{},\"type\":\"leaf\",\"id\":1476168283044},{\"ui\":{},\"target\":{\"type\":\"card\"},\"stat\":{\"totalNum\":1,\"performNum\":0},\"desc\":{\"title\":\"今日第二卡\",\"summary\":\"未完成\"},\"intent\":{},\"extras\":{},\"type\":\"leaf\",\"id\":1476168283044},{\"ui\":{},\"target\":{\"type\":\"card\"},\"stat\":{\"totalNum\":1,\"performNum\":0},\"desc\":{\"title\":\"今日第三卡\",\"summary\":\"未完成\"},\"intent\":{},\"extras\":{},\"type\":\"leaf\",\"id\":1476168283044},{\"ui\":{},\"target\":{\"type\":\"card\"},\"stat\":{\"totalNum\":1,\"performNum\":0},\"desc\":{\"title\":\"今日第四卡\",\"summary\":\"未完成\"},\"intent\":{},\"extras\":{},\"type\":\"leaf\",\"id\":1476168283044},{\"ui\":{},\"target\":{\"type\":\"card\"},\"stat\":{\"totalNum\":1,\"performNum\":0},\"desc\":{\"title\":\"今日第五卡\",\"summary\":\"未完成\"},\"intent\":{},\"extras\":{},\"type\":\"leaf\",\"id\":1476168283044}]},{\"ui\":{},\"target\":{\"type\":\"detect\"},\"stat\":{\"totalNum\":5,\"performNum\":0},\"desc\":{\"title\":\"检测任务\",\"summary\":\"检测任务任务完成0%\"},\"intent\":{},\"extras\":{},\"type\":\"limb\",\"id\":1479118242041,\"tasks\":[{\"ui\":{},\"target\":{\"type\":\"bp\"},\"stat\":{\"totalNum\":5,\"performNum\":0},\"desc\":{\"title\":\"检测血压5次\",\"summary\":\"\"},\"intent\":{},\"extras\":{},\"type\":\"leaf\",\"id\":1479118256666}]}]}}");
		Result<TaskContextResponse> result = new InternalFactoryImpl ().createTaskResponseList (jsonObject);

		TaskContextResponse response = result.getModel ();

		Assert.assertEquals (0, response.progress ());
		Assert.assertEquals (2, response.tasks ().size ());

		List<TaskResponse> list = response.tasks ();

		TaskResponse task = list.get (0);

		TaskUIResponse uiResponse = task.ui ();
		Assert.assertEquals ("http://s1.healthbok.com/task/9/6/3aaf1dcf183c287e.png", uiResponse.background ());

		TaskResponse.TaskTarget target = task.target ();
		Assert.assertEquals ("card", target.type ());

		TaskStatResponse statResponse = task.stat ();
		Assert.assertEquals (5, statResponse.totalNum ());
		Assert.assertEquals (0, statResponse.performNum ());

		TaskDescResponse descResponse = task.desc ();
		Assert.assertEquals ("开卡任务", descResponse.title ());
		Assert.assertEquals ("开卡任务任务完成0%", descResponse.summary ());

		Assert.assertEquals ("limb", task.type ());
		
	}
}
