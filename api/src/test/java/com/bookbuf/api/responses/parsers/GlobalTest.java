package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class GlobalTest {

    @Test
    public void fetchAppUpgrade() throws Exception {

        Result<AppUpgradeResponse> result = new InternalFactoryImpl().createAppUpgradeResponse(
                new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"appName\":\"7\",\"id\":11,\"content\":null,\"serialId\":\"3.5.0\",\"class\":\"com.ipudong.pdserver.client.domain.update.UpdateDO\",\"vendorId\":-1,\"channel\":\"pudong\",\"url\":\"http://s1.healthbok.com/app/9/6/4945e22ccded083a.apk\",\"version\":\"1000\",\"isUpdate\":1}}"));
        AppUpgradeResponse response = result.getModel();

        Assert.assertEquals("7", response.platform());
        Assert.assertEquals(null, response.content());
        Assert.assertEquals("3.5.0", response.serialId());
        Assert.assertEquals(-1, response.vendorId());
        Assert.assertEquals("pudong", response.apkChannel());
        Assert.assertEquals("http://s1.healthbok.com/app/9/6/4945e22ccded083a.apk", response.apkUrl());
        Assert.assertEquals(1000, response.apkVersion());
        Assert.assertEquals(1, response.updateStrategy());
    }

    @Test
    public void checkUserByQuery() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"uid\":1261822 } }");
        InternalFactoryImpl factory = new InternalFactoryImpl();
        Result<LongResponse> result = factory.createCheckUserByQueryResponse(jsonObject);

        LongResponse response = result.getModel();
        Assert.assertNotNull(response);
        Assert.assertEquals("uid = ", 1261822, response.get());
    }

    @Test
    public void sendVerifyCode() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":true }");
        Result<BooleanResponse> result = new InternalFactoryImpl().createBooleanResponse(jsonObject);
        BooleanResponse response = result.getModel();

        Assert.assertEquals(true, response.get());
    }

    @Test
    public void sendClerkVerifyCode() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":true }");
        Result<BooleanResponse> result = new InternalFactoryImpl().createBooleanResponse(jsonObject);
        BooleanResponse response = result.getModel();

        Assert.assertEquals(true, response.get());
    }

    @Test
    public void queryRole() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"role\":2 } }");
        Result<RoleResponse> result = new InternalFactoryImpl().createRoleResponse(jsonObject);
        RoleResponse response = result.getModel();

        Assert.assertEquals(2, response.id());
    }
}
