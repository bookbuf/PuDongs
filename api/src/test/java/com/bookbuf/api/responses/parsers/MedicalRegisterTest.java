package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class MedicalRegisterTest {
    private InternalFactoryImpl get() {
        return new InternalFactoryImpl();
    }

    @Test
    public void sendSMS() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"remainNum\":2, \"totalNum\":3 } }");
        Result<MedicalRegisterResponse> result = get().createMedicalRegisterResponse(jsonObject);
        MedicalRegisterResponse response = result.getModel();

        Assert.assertEquals(2, response.remainNum());
        Assert.assertEquals(3, response.remainMaxNum());
    }

}
