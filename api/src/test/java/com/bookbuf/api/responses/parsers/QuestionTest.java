package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class QuestionTest {

    private InternalFactoryImpl get() {
        return new InternalFactoryImpl();
    }

    @Test
    public void fetchQuestion() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"id\":19,\"title\":\"1+1=?\",\"choices\":[{\"key\":\"A\",\"value\":\"1\"},{\"key\":\"B\",\"value\":\"2\"},{\"key\":\"C\",\"value\":\"3\"},{\"key\":\"D\",\"value\":\"4\"}],\"choicesNum\":1}}");
        Result<QuestionResponse> result = get().createQuestionResponse(jsonObject);

        QuestionResponse response = result.getModel();
        Assert.assertEquals(19, response.id());
        Assert.assertEquals("1+1=?", response.title());
        Assert.assertEquals(1, response.choicesNum());
        Assert.assertEquals(4, response.choices().size());

        QuestionResponse.ChoiceResponse choiceResponse = response.choices().get(0);
        Assert.assertEquals("A", choiceResponse.option());
        Assert.assertEquals("1", choiceResponse.description());
    }

    @Test
    public void answerQuestion() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"isCorrected\":true } }");
        Result<BooleanResponse> result = get().createQuestionAnswerBooleanResponse(jsonObject);
        BooleanResponse response = result.getModel();
        Assert.assertEquals(true, response.get());
    }

    @Test
    public void answerQuestionDiscovery() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"result\":1581, \"is_answer_right\":true } }");
        Result<BooleanResponse> result = get().createCreateExaminationRecordResponse(jsonObject);
        BooleanResponse response = result.getModel();
        Assert.assertEquals(true, response.get());
    }

    @Test
    public void storeArticle() throws Exception {
        JSONObject jsonObject = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":true }");
        Result<BooleanResponse> result = get().createBooleanResponse(jsonObject);
        BooleanResponse response = result.getModel();
        Assert.assertEquals(true, response.get());
    }
}
