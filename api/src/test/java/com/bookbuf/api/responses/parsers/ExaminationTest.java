package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.business.AbstractBusinessTest;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperWithExamResponse;
import com.bookbuf.api.responses.impl.examination.ExamRankResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.bookbuf.api.responses.impl.question.AnswerResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.parsers.impl.examination.ExamPaperResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.examination.ExamResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.examination.ExamTerminateBooleanResponseJSONImpl;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/24.
 */

public class ExaminationTest extends AbstractBusinessTest {

    @Test
    public void test_beginExam() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"root\":{\"id\":562,\"state\":1,\"remainTime\":1200,\"examPaper\":{\"id\":1,\"title\":\"时候\",\"questions\":[{\"id\":46,\"title\":\"请问你是男生还是女生\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"男生\"},{\"key\":\"B\",\"value\":\"女生\"},{\"key\":\"C\",\"value\":\"不是男生\"},{\"key\":\"D\",\"value\":\"不是女生\"}]},{\"id\":76,\"title\":\"哪个国家摊上大事了？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"中国\"},{\"key\":\"B\",\"value\":\"美国\"},{\"key\":\"C\",\"value\":\"沙特\"},{\"key\":\"D\",\"value\":\"阿拉伯\"}]},{\"id\":77,\"title\":\"哪个国家摊上大事了？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"中国\"},{\"key\":\"B\",\"value\":\"美国\"},{\"key\":\"C\",\"value\":\"沙特\"},{\"key\":\"D\",\"value\":\"阿拉伯\"}]},{\"id\":78,\"title\":\"中国又叫什么名字？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"China\"},{\"key\":\"B\",\"value\":\"Chinese\"},{\"key\":\"C\",\"value\":\"BaiChi\"},{\"key\":\"D\",\"value\":\"Haha\"}]},{\"id\":85,\"title\":\"请问你的年龄范围是？\",\"choicesNum\":4,\"choices\":[{\"key\":\"A\",\"value\":\"20岁以内\"},{\"key\":\"B\",\"value\":\"20~40岁\"},{\"key\":\"C\",\"value\":\"40~60岁\"},{\"key\":\"D\",\"value\":\"60岁以上\"}]}]}}}");
        ExamResponse response = new ExamResponseJSONImpl(jsonObject);

        Assert.assertEquals(562, response.id());
        Assert.assertEquals(1, response.status());
        Assert.assertEquals(1200, response.remainTime());

        ExamPaperResponse paper = response.paper();

        Assert.assertNotNull(paper);

        Assert.assertEquals(1, paper.id());
        Assert.assertEquals("时候", paper.title());

        List<QuestionResponse> questions = paper.questions();
        Assert.assertNotNull(questions);
        Assert.assertEquals(5, questions.size());

        QuestionResponse first = questions.get(0);
        Assert.assertEquals(46, first.id());
        Assert.assertEquals("请问你是男生还是女生", first.title());
        Assert.assertEquals(1, first.choicesNum());
        Assert.assertEquals(4, first.choices().size());

        QuestionResponse.ChoiceResponse choice = first.choices().get(0);

        Assert.assertEquals("A", choice.option());
        Assert.assertEquals("男生", choice.description());
    }

    @Test
    public void test_getExamStatus() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"root\":{\"id\":562,\"state\":1,\"remainTime\":837,\"examPaper\":{\"id\":1,\"title\":\"时候\",\"questions\":[{\"id\":46,\"title\":\"请问你是男生还是女生\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"男生\"},{\"key\":\"B\",\"value\":\"女生\"},{\"key\":\"C\",\"value\":\"不是男生\"},{\"key\":\"D\",\"value\":\"不是女生\"}]},{\"id\":76,\"title\":\"哪个国家摊上大事了？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"中国\"},{\"key\":\"B\",\"value\":\"美国\"},{\"key\":\"C\",\"value\":\"沙特\"},{\"key\":\"D\",\"value\":\"阿拉伯\"}]},{\"id\":77,\"title\":\"哪个国家摊上大事了？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"中国\"},{\"key\":\"B\",\"value\":\"美国\"},{\"key\":\"C\",\"value\":\"沙特\"},{\"key\":\"D\",\"value\":\"阿拉伯\"}]},{\"id\":78,\"title\":\"中国又叫什么名字？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"China\"},{\"key\":\"B\",\"value\":\"Chinese\"},{\"key\":\"C\",\"value\":\"BaiChi\"},{\"key\":\"D\",\"value\":\"Haha\"}]},{\"id\":85,\"title\":\"请问你的年龄范围是？\",\"choicesNum\":4,\"choices\":[{\"key\":\"A\",\"value\":\"20岁以内\"},{\"key\":\"B\",\"value\":\"20~40岁\"},{\"key\":\"C\",\"value\":\"40~60岁\"},{\"key\":\"D\",\"value\":\"60岁以上\"}]}]}}}");
        ExamResponse response = new ExamResponseJSONImpl(jsonObject);

        Assert.assertEquals(562, response.id());
        Assert.assertEquals(1, response.status());
        Assert.assertEquals(837, response.remainTime());
    }

    @Test
    public void test_terminateExam() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"root\":{ \"isTerminated\":true } }");
        BooleanResponse response = new ExamTerminateBooleanResponseJSONImpl(jsonObject);

        Assert.assertEquals(true, response.get());
    }

    @Test
    public void test_handleInExam() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"root\":{\"id\":563,\"level\":4,\"summary\":\"差一点合格，加油！\",\"myRank\":{\"rank\":1,\"name\":\"陈俊棋\",\"score\":0,\"avatar\":\"http://s1.healthbok.com/C/Android/followRelationLogo/pd_relation_other_manage.png\"},\"rankList\":[{\"rank\":1,\"name\":\"陈俊棋\",\"score\":0,\"avatar\":\"http://s1.healthbok.com/C/Android/followRelationLogo/pd_relation_other_manage.png\"}],\"answers\":[{\"answers\":\"\",\"questionId\":46,\"correctAnswers\":\"A\"},{\"answers\":\"\",\"questionId\":76,\"correctAnswers\":\"B\"},{\"answers\":\"\",\"questionId\":77,\"correctAnswers\":\"B\"},{\"answers\":\"\",\"questionId\":78,\"correctAnswers\":\"A\"},{\"answers\":\"\",\"questionId\":85,\"correctAnswers\":\"ABCD\"}]} }");
        ExamResponse response = new ExamResponseJSONImpl(jsonObject);

        Assert.assertEquals(563, response.id());
        Assert.assertEquals(4, response.medal());
        Assert.assertEquals("差一点合格，加油！", response.evaluate());

        ExamRankResponse rank = response.mineRank();

        Assert.assertEquals(1, rank.rank());
        Assert.assertEquals(0, rank.score());
        Assert.assertEquals("陈俊棋", rank.name());
        Assert.assertEquals("http://s1.healthbok.com/C/Android/followRelationLogo/pd_relation_other_manage.png", rank.avatar());


        List<ExamRankResponse> ranks = response.ranks();
        Assert.assertEquals(1, ranks.size());


        List<AnswerResponse> answers = response.answers();
        Assert.assertEquals(5, answers.size());

        AnswerResponse first = answers.get(0);
        Assert.assertEquals("", first.answer());
        Assert.assertEquals(46, first.questionId());
        Assert.assertEquals("A", first.correctAnswer());
    }

    @Test
    public void test_fetchExamHistory() throws Exception {
        // ignore ...
    }

    @Test
    public void test_fetchExamPaper() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"root\":{\"id\":1,\"title\":\"时候\",\"duration\":1200,\"summary\":\"三十年\",\"goToReview\":\"http://172.16.8.191:82/pdh5/view/src/index.html?id=16\"} }");
        ExamPaperResponse response = new ExamPaperResponseJSONImpl(jsonObject);

        Assert.assertEquals(1, response.id());
        Assert.assertEquals("时候", response.title());
        Assert.assertEquals(1200, response.duration());
        Assert.assertEquals("三十年", response.summary());
        Assert.assertEquals("http://172.16.8.191:82/pdh5/view/src/index.html?id=16", response.advice());


    }


    @Test
    public void test_fetchExamPaperWithExam() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":[{\"examHighest\":{},\"examPaper\":{\"id\":6,\"duration\":1800,\"goToReview\":\"http://h5.iputong.com/pdh5/view/src/index.html\",\"title\":\"【采血检测操作视频】考试\",\"summary\":\"考查采血检测的操作要点\",\"questions\":[{\"id\":43,\"title\":\"关于采血检测操作过程，以下哪项说法是错误的？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"为了保证检测结果的准确性，必须擦去第一滴血\"},{\"key\":\"B\",\"value\":\"试纸代码和分析仪显示代码不一致的情况下仍可测量\"},{\"key\":\"C\",\"value\":\"按C键在分析仪显示代码值时进行代码调节\"},{\"key\":\"D\",\"value\":\"为减少顾客的痛感，可选择无名指指尖两侧进行采血\"}]},{\"id\":62,\"title\":\"新的血糖试纸开启后，需要在多久时间内使用？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"两个月\"},{\"key\":\"B\",\"value\":\"三个月\"},{\"key\":\"C\",\"value\":\"半年\"},{\"key\":\"D\",\"value\":\"没具体规定\"}]},{\"id\":64,\"title\":\"检测前需要用75%的酒精棉签消毒采血位置，以什么方式消毒比较合适？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"以采血位置为圆心，由周围向中心消毒\"},{\"key\":\"B\",\"value\":\"由采血位置开始，整个指尖消毒\"},{\"key\":\"C\",\"value\":\"以采血位置为中心向四周消毒指甲盖大小区域\"},{\"key\":\"D\",\"value\":\"涂抹过采血位置都可以\"}]},{\"id\":67,\"title\":\"关于采血检测操作，以下说法错误的是：\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"可以先试纸条取血后再插入分析仪检测\"},{\"key\":\"B\",\"value\":\"试纸虹吸口不能紧贴皮肤\"},{\"key\":\"C\",\"value\":\"对于指尖皮肤冰凉的顾客，让顾客搓热双手，有利于提高采血检测的成功率\"},{\"key\":\"D\",\"value\":\"采血时要绷紧手指皮肤\"}]},{\"id\":68,\"title\":\"分析仪插入试纸条，提示“Er2”，请问是由于以下哪项不当操作引起的？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"采血血量不足\"},{\"key\":\"B\",\"value\":\"未等滴样等待符号出现就进行检测\"},{\"key\":\"C\",\"value\":\"血样吸入试纸条后，才将试纸条插入分析仪\"},{\"key\":\"D\",\"value\":\"分析仪程序死机\"}]},{\"id\":69,\"title\":\"关于多参数分析仪，以下说法正确的是\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"任何品牌的血糖试纸都可以用于多参数分析仪 \"},{\"key\":\"B\",\"value\":\"插入试纸条，代码与试纸瓶上不一致不需要调节\"},{\"key\":\"C\",\"value\":\"分析仪代码调适按侧面M健\"},{\"key\":\"D\",\"value\":\"退去试纸，可以按分析仪上方的退纸键\"}]},{\"id\":70,\"title\":\"如果分析仪插入试纸条显示的代码值与试纸瓶上的代码值不一致，怎么调节？\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"按C键顺序调节代码值，1到49循环进行，无需按确认键\"},{\"key\":\"B\",\"value\":\"按M键顺序调节代码值，1到49循环进行，无需按确认键\"},{\"key\":\"C\",\"value\":\"先按C键，再按M键，1到49循环进行，无需按确认键\"},{\"key\":\"D\",\"value\":\"不用调节\"}]},{\"id\":71,\"title\":\"测胆固醇，尿酸，血糖三个项目，检测的顺序依次是\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"胆固醇、血糖、尿酸 \"},{\"key\":\"B\",\"value\":\"血糖、尿酸、胆固醇\"},{\"key\":\"C\",\"value\":\"胆固醇、尿酸、血糖 \"},{\"key\":\"D\",\"value\":\"无论怎么测可以\"}]},{\"id\":72,\"title\":\"以下不适合作为采血检测的部位的是\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"选择五指指肚位置采血\"},{\"key\":\"B\",\"value\":\"避开有老茧的部位采血 \"},{\"key\":\"C\",\"value\":\"避开有伤口的部位采血\"},{\"key\":\"D\",\"value\":\"选择无名指以及中指指尖两侧采血 \"}]},{\"id\":73,\"title\":\"分析仪插入试纸开机，左下角出现温度计，屏幕显示“Lo”，请问是什么原因\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"试纸条过期\"},{\"key\":\"B\",\"value\":\"测试数值低于分析仪的测量范围\"},{\"key\":\"C\",\"value\":\"电池电量不足\"},{\"key\":\"D\",\"value\":\"测试环境温度过低\"}]},{\"id\":74,\"title\":\"检测后，分析仪屏幕显示“Er3”，以下解决方法正确的是\",\"choicesNum\":1,\"choices\":[{\"key\":\"A\",\"value\":\"分析仪已损毁，联系扑咚健康进行维修\"},{\"key\":\"B\",\"value\":\"检测吸血时，试纸吸样口应该紧贴皮肤\"},{\"key\":\"C\",\"value\":\"血样加于错误位置\"},{\"key\":\"D\",\"value\":\"更换新的试纸条，并且在出现滴样符号时进行吸血 \"}]}]}}]}");
        InternalFactoryImpl factory = new InternalFactoryImpl();
        Result<List<ExamPaperWithExamResponse>> result = factory.createExamPaperWithExamResponseList(jsonObject);
        Assert.assertNotNull(result);
        List<ExamPaperWithExamResponse> responses = result.getModel();
        Assert.assertNotNull(responses);

        Assert.assertEquals(1, responses.size());

        ExamPaperWithExamResponse response = responses.get(0);
        ExamPaperResponse paperResponse = response.paper();

        Assert.assertEquals(6, paperResponse.id());
        Assert.assertEquals(1800, paperResponse.duration());
        Assert.assertEquals("http://h5.iputong.com/pdh5/view/src/index.html", paperResponse.advice());
        Assert.assertEquals("【采血检测操作视频】考试", paperResponse.title());
        Assert.assertEquals("考查采血检测的操作要点", paperResponse.summary());


        List<QuestionResponse> questionResponses = paperResponse.questions();
        Assert.assertEquals(11, questionResponses.size());

    }
}
