package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.ProfileResponse;
import com.bookbuf.api.responses.parsers.impl.user.CustomerResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.user.ProfileResponseJSONImpl;

import junit.framework.Assert;

import org.json.JSONObject;
import org.junit.Test;

/**
 * Created by bo.wei on 2016/11/24.
 */

public class ClerkTest {

    @Test
    public void test_updateProfile() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"user_id\":\"1831819\",\"realname\":\"魏波\",\"mobile\":\"18268810820\"," +
                "\"idCard\":\"330108199403230913\",\"phone\":\"\",\"gender\":\"男\",\"body_height\":\"\",\"address\":{\"province\":\"\",\"city\":\"\",\"distinct\":\"\",\"detail\":\"啊\",\"fullAddress\":\"啊\"},\"age\":22,\"body_weight\":\"\"}}");
        CustomerResponse response = new CustomerResponseJSONImpl(jsonObject);

        Assert.assertEquals(1831819,response.profile().userId());
        Assert.assertEquals("魏波",response.profile().realname());
        Assert.assertEquals("18268810820",response.profile().mobile());
        Assert.assertEquals("330108199403230913",response.profile().idcard());
        Assert.assertEquals("",response.profile().phone());
        Assert.assertEquals(-1,response.profile().height());
        Assert.assertEquals("",response.profile().address().province());
        Assert.assertEquals("啊",response.profile().address().detail());
        Assert.assertEquals(22,response.profile().age());
        Assert.assertEquals(-1,response.profile().weight());
        Assert.assertEquals("男",response.profile().gender());
    }

    @Test
    public void test_resetPassword() throws Exception {

    }

    @Test
    public void test_queryProfile() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"phone\":\"057187727139\",\"workUnit\":null,\"originArea\":null,\"permanentType\":null,\"creater\":6,\"password\":null,\"race\":null,\"distinct\":null,\"city\":null,\"drugRecord\":\"[]\",\"medicalPayment\":null,\"rHBloodType\":null,\"username\":null,\"toiletType\":null,\"userId\":1831819,\"age\":\"22\",\"province\":null,\"gender\":\"男\",\"kitchenExhaust\":null,\"selfHistory\":\"[]\",\"openid\":null,\"exposureHistory\":null,\"bodyWeight\":null,\"limit\":1,\"platform\":\"onetree\",\"occupation\":null,\"class\":\"com.ipudong.pdserver.client.pojo.user.UserPOJO\",\"avatar\":null,\"vendorMemberId\":\"202000000000050\",\"drinkingStartYear\":null,\"bloodType\":null,\"lastLoginDeviceId\":null,\"bodyHeight\":null,\"fuelType\":null,\"emergencyContacts\":null,\"email\":null,\"openCardClerkName\":null,\"operationExperience\":null,\"historyDisease\":null,\"eatingHabits\":null,\"drinkingWater\":null,\"openCardShopName\":null,\"storeId\":2,\"mobile\":\"18268810820\",\"region\":null,\"birthday\":\"1994-03-23\",\"appId\":null,\"maritalStatus\":null,\"lastLoginIp\":null,\"pushId\":null,\"accountStatus\":0,\"smokingStatus\":null,\"disability\":null,\"edoc\":null,\"offset\":0,\"updatedTime\":1479975048000,\"lastLoginTime\":null,\"vendorLogoUrl\":null,\"animal\":null,\"app\":\"4\",\"idCard\":\"330108199403230913\",\"medicareCardNumber\":null,\"storeName\":null,\"fullAddress\":\"蛤\",\"originProvince\":null,\"openCardShopNumber\":null,\"vendorId\":1,\"smokingStartYear\":null,\"familyHistory\":\"[]\",\"bloodTransfusionExperience\":null,\"address\":\"{\\\"province\\\":\\\"\\\",\\\"city\\\":\\\"\\\",\\\"distinct\\\":\\\"\\\",\\\"detail\\\":\\\"蛤\\\",\\\"fullAddress\\\":\\\"蛤\\\"}\",\"drinkingStatus\":null,\"drugAllergy\":null,\"realname\":\"魏波\"}");
        ProfileResponse response = new ProfileResponseJSONImpl(jsonObject);
        Assert.assertEquals("魏波",response.realname());
        Assert.assertEquals("18268810820",response.mobile());
        Assert.assertEquals("330108199403230913",response.idcard());
        Assert.assertEquals("057187727139",response.phone());
        Assert.assertEquals(-1,response.height());
        Assert.assertEquals("",response.address().province());
        Assert.assertEquals("蛤",response.address().detail());
        Assert.assertEquals(22,response.age());
        Assert.assertEquals(-1,response.weight());
        Assert.assertEquals("男",response.gender());
    }
}
