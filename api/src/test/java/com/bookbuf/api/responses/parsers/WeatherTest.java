package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.weather.WeatherAQIResponse;
import com.bookbuf.api.responses.impl.weather.WeatherAlarmResponse;
import com.bookbuf.api.responses.impl.weather.WeatherBasicResponse;
import com.bookbuf.api.responses.impl.weather.WeatherDailyResponse;
import com.bookbuf.api.responses.impl.weather.WeatherHourlyResponse;
import com.bookbuf.api.responses.impl.weather.WeatherNowResponse;
import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.bookbuf.api.responses.impl.weather.WeatherSuggestionResponse;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherAQIResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherAlarmResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherBasicResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherDailyResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherHourlyResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherNowResponseJSONImpl;
import com.bookbuf.api.responses.parsers.impl.weather.WeatherResponseJSONImpl;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  16/11/22.
 */

public class WeatherTest {

    @Test
    public void test_alarm() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"level\":\"蓝色\",\"stat\":\"预警中\",\"title\":\"山东省青岛市气象台发布大风蓝色预警\",\"txt\":\"青岛市气象台2016年08月29日15时24分继续发布大风蓝色预警信号：预计今天下午到明天，我市北风风力海上6到7级阵风9级，陆地4到5阵风7级，请注意防范。\",\"type\":\"大风\"}");
        WeatherAlarmResponse response = new WeatherAlarmResponseJSONImpl(jsonObject);

        Assert.assertEquals("蓝色", response.level());
        Assert.assertEquals("预警中", response.stat());
        Assert.assertEquals("山东省青岛市气象台发布大风蓝色预警", response.title());
        Assert.assertEquals("大风", response.type());
        Assert.assertEquals("青岛市气象台2016年08月29日15时24分继续发布大风蓝色预警信号：预计今天下午到明天，我市北风风力海上6到7级阵风9级，陆地4到5阵风7级，请注意防范。", response.txt());
    }

    @Test
    public void test_aqi() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"aqi\":\"60\",\"co\":\"0\",\"no2\":\"14\",\"o3\":\"95\",\"pm10\":\"67\",\"pm25\":\"15\",\"qlty\":\"良\",\"so2\":\"10\"}");
        WeatherAQIResponse response = new WeatherAQIResponseJSONImpl(jsonObject);

        Assert.assertEquals("60", response.aqi());
        Assert.assertEquals("0", response.co());
        Assert.assertEquals("14", response.no2());
        Assert.assertEquals("95", response.o3());
        Assert.assertEquals("67", response.pm10());
        Assert.assertEquals("15", response.pm25());
        Assert.assertEquals("良", response.qlty());
        Assert.assertEquals("10", response.so2());
    }

    @Test
    public void test_basic() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"city\":\"青岛\",\"cnty\":\"中国\",\"id\":\"CN101120201\",\"lat\":\"36.088000\",\"lon\":\"120.343000\",\"prov\":\"山东\",\"update\":{\"loc\":\"2016-08-30 11:52\",\"utc\":\"2016-08-30 03:52\"}}");
        WeatherBasicResponse response = new WeatherBasicResponseJSONImpl(jsonObject);

        Assert.assertEquals("青岛", response.city());
        Assert.assertEquals("中国", response.cnty());
        Assert.assertEquals("CN101120201", response.id());
        Assert.assertEquals("36.088000", response.lat());
        Assert.assertEquals("120.343000", response.lon());
        Assert.assertEquals("山东", response.prov());
        Assert.assertEquals("2016-08-30 11:52", response.timeArea().loc());
        Assert.assertEquals("2016-08-30 03:52", response.timeArea().utc());

    }

    @Test
    public void test_daily() throws Exception {
        JSONObject jsonObject = new JSONObject("{\"astro\":{\"mr\":\"03:09\",\"ms\":\"17:06\",\"sr\":\"05:28\",\"ss\":\"18:29\"},\"cond\":{\"code_d\":\"100\",\"code_n\":\"100\",\"txt_d\":\"晴\",\"txt_n\":\"晴\"},\"date\":\"2016-08-30\",\"hum\":\"45\",\"pcpn\":\"0.0\",\"pop\":\"8\",\"pres\":\"1005\",\"tmp\":{\"max\":\"29\",\"min\":\"22\"},\"vis\":\"10\",\"wind\":{\"deg\":\"339\",\"dir\":\"北风\",\"sc\":\"4-5\",\"spd\":\"24\"}}");
        WeatherDailyResponse response = new WeatherDailyResponseJSONImpl(jsonObject);

        Assert.assertEquals("03:09", response.astro().mr());
        Assert.assertEquals("17:06", response.astro().ms());
        Assert.assertEquals("05:28", response.astro().sr());
        Assert.assertEquals("18:29", response.astro().ss());

        Assert.assertEquals("100", response.cond().codeDaily());
        Assert.assertEquals("100", response.cond().codeNight());
        Assert.assertEquals("晴", response.cond().txtDaily());
        Assert.assertEquals("晴", response.cond().txtNight());
        Assert.assertEquals("10", response.vis());

        Assert.assertEquals("2016-08-30", response.date());
        Assert.assertEquals("45", response.hum());
        Assert.assertEquals("0.0", response.pcpn());
        Assert.assertEquals("8", response.pop());
        Assert.assertEquals("1005", response.pres());
        Assert.assertEquals("29", response.tmp().max());
        Assert.assertEquals("22", response.tmp().min());

        Assert.assertEquals("339", response.wind().deg());
        Assert.assertEquals("北风", response.wind().dir());
        Assert.assertEquals("4-5", response.wind().sc());
        Assert.assertEquals("24", response.wind().spd());
    }

    @Test
    public void test_hourly() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"cond\":{\"code\":\"100\",\"txt\":\"晴\"},\"date\":\"2016-08-30 12:00\",\"hum\":\"47\",\"pop\":\"0\",\"pres\":\"1006\",\"tmp\":\"29\",\"wind\":{\"deg\":\"335\",\"dir\":\"西北风\",\"sc\":\"4-5\",\"spd\":\"36\"}}");
        WeatherHourlyResponse response = new WeatherHourlyResponseJSONImpl(jsonObject);

        Assert.assertEquals("335", response.wind().deg());
        Assert.assertEquals("西北风", response.wind().dir());
        Assert.assertEquals("4-5", response.wind().sc());
        Assert.assertEquals("36", response.wind().spd());

        Assert.assertEquals("29", response.tmp());
        Assert.assertEquals("1006", response.pres());
        Assert.assertEquals("0", response.pop());
        Assert.assertEquals("47", response.hum());
        Assert.assertEquals("2016-08-30 12:00", response.date());

        Assert.assertEquals("100", response.cond().code());
        Assert.assertEquals("晴", response.cond().txt());
    }

    @Test
    public void test_now() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"cond\":{\"code\":\"100\",\"txt\":\"晴\"},\"fl\":\"28\",\"hum\":\"41\",\"pcpn\":\"0\",\"pres\":\"1005\",\"tmp\":\"26\",\"vis\":\"10\",\"wind\":{\"deg\":\"330\",\"dir\":\"西北风\",\"sc\":\"6-7\",\"spd\":\"34\"}}");
        WeatherNowResponse response = new WeatherNowResponseJSONImpl(jsonObject);

        Assert.assertEquals("330", response.wind().deg());
        Assert.assertEquals("西北风", response.wind().dir());
        Assert.assertEquals("6-7", response.wind().sc());
        Assert.assertEquals("34", response.wind().spd());

        Assert.assertEquals("100", response.cond().code());
        Assert.assertEquals("晴", response.cond().txt());

        Assert.assertEquals("28", response.fl());
        Assert.assertEquals("41", response.hum());
        Assert.assertEquals("0", response.pcpn());
        Assert.assertEquals("1005", response.pres());
        Assert.assertEquals("26", response.tmp());
        Assert.assertEquals("10", response.vis());


    }

    @Test
    public void test_weather() throws Exception {

        JSONObject jsonObject = new JSONObject("{\"alarms\":[{\"level\":\"蓝色\",\"stat\":\"预警中\",\"title\":\"山东省青岛市气象台发布大风蓝色预警\",\"txt\":\"青岛市气象台2016年08月29日15时24分继续发布大风蓝色预警信号：预计今天下午到明天，我市北风风力海上6到7级阵风9级，陆地4到5阵风7级，请注意防范。\",\"type\":\"大风\"}],\"aqi\":{\"city\":{\"aqi\":\"60\",\"co\":\"0\",\"no2\":\"14\",\"o3\":\"95\",\"pm10\":\"67\",\"pm25\":\"15\",\"qlty\":\"良\",\"so2\":\"10\"}},\"basic\":{\"city\":\"青岛\",\"cnty\":\"中国\",\"id\":\"CN101120201\",\"lat\":\"36.088000\",\"lon\":\"120.343000\",\"prov\":\"山东\",\"update\":{\"loc\":\"2016-08-30 11:52\",\"utc\":\"2016-08-30 03:52\"}},\"daily_forecast\":[{\"astro\":{\"mr\":\"03:09\",\"ms\":\"17:06\",\"sr\":\"05:28\",\"ss\":\"18:29\"},\"cond\":{\"code_d\":\"100\",\"code_n\":\"100\",\"txt_d\":\"晴\",\"txt_n\":\"晴\"},\"date\":\"2016-08-30\",\"hum\":\"45\",\"pcpn\":\"0.0\",\"pop\":\"8\",\"pres\":\"1005\",\"tmp\":{\"max\":\"29\",\"min\":\"22\"},\"vis\":\"10\",\"wind\":{\"deg\":\"339\",\"dir\":\"北风\",\"sc\":\"4-5\",\"spd\":\"24\"}}],\"hourly_forecast\":[{\"cond\":{\"code\":\"100\",\"txt\":\"晴\"},\"date\":\"2016-08-30 12:00\",\"hum\":\"47\",\"pop\":\"0\",\"pres\":\"1006\",\"tmp\":\"29\",\"wind\":{\"deg\":\"335\",\"dir\":\"西北风\",\"sc\":\"4-5\",\"spd\":\"36\"}}],\"now\":{\"cond\":{\"code\":\"100\",\"txt\":\"晴\"},\"fl\":\"28\",\"hum\":\"41\",\"pcpn\":\"0\",\"pres\":\"1005\",\"tmp\":\"26\",\"vis\":\"10\",\"wind\":{\"deg\":\"330\",\"dir\":\"西北风\",\"sc\":\"6-7\",\"spd\":\"34\"}},\"status\":\"ok\",\"suggestion\":{\"comf\":{\"brf\":\"较舒适\",\"txt\":\"白天天气晴好，您在这种天气条件下，会感觉早晚凉爽、舒适，午后偏热。\"},\"cw\":{\"brf\":\"较不宜\",\"txt\":\"较不宜洗车，未来一天无雨，风力较大，如果执意擦洗汽车，要做好蒙上污垢的心理准备。\"},\"drsg\":{\"brf\":\"热\",\"txt\":\"天气热，建议着短裙、短裤、短薄外套、T恤等夏季服装。\"},\"flu\":{\"brf\":\"较易发\",\"txt\":\"虽然温度适宜但风力较大，仍较易发生感冒，体质较弱的朋友请注意适当防护。\"},\"sport\":{\"brf\":\"较适宜\",\"txt\":\"天气较好，但风力较大，推荐您进行室内运动，若在户外运动请注意防风。\"},\"trav\":{\"brf\":\"适宜\",\"txt\":\"天气较好，风稍大，但温度适宜，是个好天气哦。适宜旅游，您可以尽情地享受大自然的无限风光。\"},\"uv\":{\"brf\":\"强\",\"txt\":\"紫外线辐射强，建议涂擦SPF20左右、PA++的防晒护肤品。避免在10点至14点暴露于日光下。\"}}}");
        WeatherResponse response = new WeatherResponseJSONImpl(jsonObject);

        WeatherAlarmResponse alarm = response.alarms().get(0);
        Assert.assertEquals("蓝色", alarm.level());
        Assert.assertEquals("预警中", alarm.stat());
        Assert.assertEquals("山东省青岛市气象台发布大风蓝色预警", alarm.title());
        Assert.assertEquals("青岛市气象台2016年08月29日15时24分继续发布大风蓝色预警信号：预计今天下午到明天，我市北风风力海上6到7级阵风9级，陆地4到5阵风7级，请注意防范。", alarm.txt());
        Assert.assertEquals("大风", alarm.type());


        WeatherAQIResponse aqi = response.cityAQI();
        Assert.assertEquals("60", aqi.aqi());
        Assert.assertEquals("0", aqi.co());
        Assert.assertEquals("14", aqi.no2());
        Assert.assertEquals("95", aqi.o3());
        Assert.assertEquals("67", aqi.pm10());
        Assert.assertEquals("15", aqi.pm25());
        Assert.assertEquals("良", aqi.qlty());
        Assert.assertEquals("10", aqi.so2());


        WeatherBasicResponse basic = response.basic();
        Assert.assertEquals("青岛", basic.city());
        Assert.assertEquals("中国", basic.cnty());
        Assert.assertEquals("CN101120201", basic.id());
        Assert.assertEquals("36.088000", basic.lat());
        Assert.assertEquals("120.343000", basic.lon());
        Assert.assertEquals("山东", basic.prov());
        Assert.assertEquals("2016-08-30 11:52", basic.timeArea().loc());
        Assert.assertEquals("2016-08-30 03:52", basic.timeArea().utc());


        WeatherDailyResponse daily = response.daily().get(0);
        Assert.assertEquals("2016-08-30", daily.date());
        Assert.assertEquals("45", daily.hum());
        Assert.assertEquals("0.0", daily.pcpn());
        Assert.assertEquals("8", daily.pop());
        Assert.assertEquals("1005", daily.pres());
        Assert.assertEquals("29", daily.tmp().max());
        Assert.assertEquals("22", daily.tmp().min());
        Assert.assertEquals("10", daily.vis());

        Assert.assertEquals("339", daily.wind().deg());
        Assert.assertEquals("北风", daily.wind().dir());
        Assert.assertEquals("4-5", daily.wind().sc());
        Assert.assertEquals("24", daily.wind().spd());

        Assert.assertEquals("100", daily.cond().codeDaily());
        Assert.assertEquals("100", daily.cond().codeNight());
        Assert.assertEquals("晴", daily.cond().txtDaily());
        Assert.assertEquals("晴", daily.cond().txtNight());

        Assert.assertEquals("03:09", daily.astro().mr());
        Assert.assertEquals("17:06", daily.astro().ms());
        Assert.assertEquals("05:28", daily.astro().sr());
        Assert.assertEquals("18:29", daily.astro().ss());


        WeatherHourlyResponse hourly = response.hourly().get(0);
        Assert.assertEquals("100", hourly.cond().code());
        Assert.assertEquals("晴", hourly.cond().txt());
        Assert.assertEquals("2016-08-30 12:00", hourly.date());
        Assert.assertEquals("47", hourly.hum());
        Assert.assertEquals("0", hourly.pop());
        Assert.assertEquals("1006", hourly.pres());
        Assert.assertEquals("29", hourly.tmp());

        Assert.assertEquals("335", hourly.wind().deg());
        Assert.assertEquals("西北风", hourly.wind().dir());
        Assert.assertEquals("4-5", hourly.wind().sc());
        Assert.assertEquals("36", hourly.wind().spd());

        WeatherNowResponse now = response.now();

        Assert.assertEquals("100", now.cond().code());
        Assert.assertEquals("晴", now.cond().txt());

        Assert.assertEquals("41", now.hum());
        Assert.assertEquals("0", now.pcpn());
        Assert.assertEquals("28", now.fl());
        Assert.assertEquals("10", now.vis());
        Assert.assertEquals("1005", now.pres());
        Assert.assertEquals("26", now.tmp());

        Assert.assertEquals("330", now.wind().deg());
        Assert.assertEquals("西北风", now.wind().dir());
        Assert.assertEquals("6-7", now.wind().sc());
        Assert.assertEquals("34", now.wind().spd());


        Assert.assertEquals("ok", response.status());


        WeatherSuggestionResponse suggestion = response.suggestion();

        Assert.assertEquals("较舒适", suggestion.comf().brf());
        Assert.assertEquals("白天天气晴好，您在这种天气条件下，会感觉早晚凉爽、舒适，午后偏热。", suggestion.comf().txt());


        Assert.assertEquals("较不宜", suggestion.cw().brf());
        Assert.assertEquals("较不宜洗车，未来一天无雨，风力较大，如果执意擦洗汽车，要做好蒙上污垢的心理准备。", suggestion.cw().txt());

        Assert.assertEquals("热", suggestion.drsg().brf());
        Assert.assertEquals("天气热，建议着短裙、短裤、短薄外套、T恤等夏季服装。", suggestion.drsg().txt());


        Assert.assertEquals("较易发", suggestion.flu().brf());
        Assert.assertEquals("虽然温度适宜但风力较大，仍较易发生感冒，体质较弱的朋友请注意适当防护。", suggestion.flu().txt());

        Assert.assertEquals("较适宜", suggestion.sport().brf());
        Assert.assertEquals("天气较好，但风力较大，推荐您进行室内运动，若在户外运动请注意防风。", suggestion.sport().txt());

        Assert.assertEquals("适宜", suggestion.trav().brf());
        Assert.assertEquals("天气较好，风稍大，但温度适宜，是个好天气哦。适宜旅游，您可以尽情地享受大自然的无限风光。", suggestion.trav().txt());

        Assert.assertEquals("强", suggestion.uv().brf());
        Assert.assertEquals("紫外线辐射强，建议涂擦SPF20左右、PA++的防晒护肤品。避免在10点至14点暴露于日光下。", suggestion.uv().txt());


    }


}
