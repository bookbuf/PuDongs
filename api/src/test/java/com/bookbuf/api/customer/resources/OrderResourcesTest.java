package com.bookbuf.api.customer.resources;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public class OrderResourcesTest extends AbstractCustomerTest {

    {
        username = "13655812763";
        password = "812763";
    }

    @Test
    public void orderPay() throws Exception {
        withSession().orderResources().orderPay("168");
    }

    @Test
    public void orderResult() throws Exception {
        withSession().orderResources().orderResult("","","");
    }
}
