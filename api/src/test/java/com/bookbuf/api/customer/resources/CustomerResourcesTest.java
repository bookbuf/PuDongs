package com.bookbuf.api.customer.resources;

import com.bookbuf.api.apis.GlobalAPI;
import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public class CustomerResourcesTest extends AbstractCustomerTest {
    {
        username = "18268810820";
        password = "810820";
    }
    @Test
    public void test_send_code() throws Exception {
        withoutSession().globalResources().sendNotice("18668247775", GlobalAPI.Sign.login,"sms");
    }

    @Test
    public void test_check_code() throws Exception {
        withoutSession().globalResources().checkVerifyCode("18668247775", GlobalAPI.Sign.login, "710224");
    }

    @Test
    public void test_login() throws Exception {
        withoutSession().customerResources().login("18668247775", "710224", "");
    }

    @Test
    public void test_logout() throws Exception {
        withSession().customerResources().logout();
    }

    @Test
    public void test_fetchProfile() throws Exception {
        withSession().customerResources().fetchProfile();
    }

    @Test
    public void test_modifyProfile() throws Exception {
        withSession().customerResources().modifyProfile("1","1","1","1","1","1","1","1");
    }

    @Test
    public void test_detection() throws Exception {
        Result<DetectionListResponse> result = withSession().fetchDetectionListByType("bmi",1,100);
        Assert.assertEquals(null,result.getModel().list().get(0).description());
    }

    @Test
    public void test_profile() throws Exception {
        Result<ProfileResponse> result = withSession().fetchProfile();
        Assert.assertEquals("",result.getModel());
    }

    @Test
    public void test_globalLogin() throws Exception {
        Result<CustomerLoginResponse> result = withSession().globalLogin("18268810820",null,"810820");
        Assert.assertEquals("a7e83b25d25649f0a595687c99553c24",result.getModel().sessionId());
    }

    @Test
    public void test_resetPassword() throws Exception {
        Result<BooleanResponse> result = withSession().resetPassword("18268810820","535108","810820", GlobalAPI.Sign.register);
        Assert.assertEquals(true,result.getModel().get());
    }

    @Test
    public void test_thirtyLogin() throws Exception {
        Result<CustomerLoginResponse> result = withSession().thirtyLogin("aaa","wechat","a");
        Assert.assertEquals("",result.getModel().sessionId());
    }

    @Test
    public void test_thirtyBind() throws Exception {
        Result<BooleanResponse> result = withSession().thirtyBind("18268810820","aaa","578378","wechat");
        Assert.assertEquals(true,result.getModel());
    }

    @Test
    public void test_sendVerify() throws Exception {
        Result<BooleanResponse> result = withSession().sendVerifyCode("18268810820",GlobalAPI.Sign.login);
    }

    @Test
    public void test_sendNotice() throws Exception {
        Result<BooleanResponse> result = withSession().sendNotice("18268810820",GlobalAPI.Sign.login,"voice");
        Assert.assertEquals(true,result.getModel().get());
    }

    @Test
    public void test_updatePwd() throws Exception {
        Result<BooleanResponse> result = withSession().updatePassword("810820","810820");
        Assert.assertEquals(true,result.getModel().get());
    }

    @Test
    public void test_createInvite() throws Exception {
        Result<BooleanResponse> result = withSession().createInvite(2L,1,"0000_test",20L);
        Assert.assertEquals(true,result.getModel().get());
    }

}
