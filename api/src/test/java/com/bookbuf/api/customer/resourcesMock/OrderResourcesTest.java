package com.bookbuf.api.customer.resourcesMock;

import com.bookbuf.api.customer.AbstractCustomerTest;

import org.junit.Test;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public class OrderResourcesTest extends AbstractCustomerTest {

    static {
        isMock = true;
    }

    @Test
    public void orderPay() throws Exception {
        withSession(true).orderResources().orderPay("168");
    }
}
