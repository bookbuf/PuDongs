package com.bookbuf.api.customer.resourcesMock;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.CategoryResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.HealthLiveResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public class HealthLiveResourcesTest extends AbstractCustomerTest {

    @Test
    public void test_fetch_category() throws Exception {

        Result<CategoryListResponse> result
                = withSession(true)
                .healthLiveResources()
                .fetchCategory();
        Assert.assertNotNull(result);
        CategoryListResponse response = result.getModel();
        Assert.assertNotNull(response);

        Assert.assertEquals(3, response.categorys().size());

        CategoryResponse response1 = response.categorys().get(0);
        Assert.assertEquals(49, response1.id());
        Assert.assertEquals("名医讲坛", response1.title());
        Assert.assertEquals("http://s1.healthbok.com/article/9/6/6b9ed1358003b32e.png", response1.background());
    }

    @Test
    public void test_fetch_category_list() throws Exception {
        Result<FetchHealthLiveListResponse> result
                = withSession(true)
                .healthLiveResources()
                .fetchHealthLiveListByCategoryId();

        Assert.assertNotNull(result);
        FetchHealthLiveListResponse response = result.getModel();
        Assert.assertNotNull(response);

        Assert.assertEquals(2, response.lists().size());

        HealthLiveResponse h = response.lists().get(0);

        Assert.assertEquals(1, h.liveId());
        Assert.assertEquals("4.5", h.rate());
        Assert.assertEquals(234, h.visits());
        Assert.assertEquals("...", h.background());


    }

    @Test
    public void test_fetch_health_live_detail() throws Exception {
        Result<FetchRelatedHealthLiveListResponse> result
                = withSession(true)
                .healthLiveResources()
                .fetchHealthLiveListByLiveId(99);

        Assert.assertNotNull(result);
        FetchRelatedHealthLiveListResponse response = result.getModel();
        Assert.assertNotNull(response);

        HealthLiveResponse header = response.main();
        Assert.assertNotNull(header);

        Assert.assertEquals("9.0", header.rate());
        Assert.assertEquals(10000, header.visits());
        Assert.assertEquals("http://s1.healthbok.com/article/9/6/6b9ed1358003b32e.png", header.background());
        Assert.assertEquals("我是中科院生物学博士李雷。主要从事健康长寿方面的研究。老年痴呆是一种常见的疾病，也是我研究的重要领域。", header.summary());

        Assert.assertEquals(5, header.knowledge().size());
        Assert.assertEquals("老年痴呆症的基本情况？", header.knowledge().get(0));
        Assert.assertEquals("http://s1.healthbok.com/null", header.url());

        List<HealthLiveResponse> content = response.lists();
        Assert.assertNotNull(content);
        Assert.assertEquals(2, content.size());

        HealthLiveResponse first = content.get(0);

        Assert.assertEquals(6, first.liveId());
        Assert.assertEquals("5.0", first.rate());
        Assert.assertEquals(5000, first.visits());
        Assert.assertEquals(5, first.knowledge().size());
        Assert.assertEquals("http://s1.healthbok.com/null", first.url());

    }
}
