package com.bookbuf.api.customer.resourcesMock;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class CouponResourcesTest extends AbstractCustomerTest {


    static {
        isMock = false;
    }

    @Test
    public void test_fetchCouponList() throws Exception {
        Result<CouponListResponse> result =
                withSession(true).couponResources().fetchCouponList(null);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        CouponListResponse response = result.getModel();

        Assert.assertEquals(4, response.coupons().size());

        CouponResponse coupon = response.coupons().get(0);

        Assert.assertEquals("2017.03.21", coupon.startTime());
        Assert.assertEquals(3599, coupon.id());

    }

    @Test
    public void test_fetchCouponList_without_login() throws Exception {
        Result<CouponListResponse> result =
                withSession(true).couponResources().fetchCouponListWithoutLogin(null);

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        CouponListResponse response = result.getModel();

        Assert.assertEquals(3, response.coupons().size());

        CouponResponse coupon = response.coupons().get(0);

        Assert.assertEquals("2017.03.21", coupon.startTime());
        Assert.assertEquals(-1, coupon.id());/*没有 couponId 会传-1*/

    }

    @Test
    public void test_searhCoupon() throws Exception {

        Result<CouponResponse> result =
                withSession(true).couponResources().searhCoupon(3,1L);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        CouponResponse response = result.getModel();

        Assert.assertEquals(6, response.rule().size());
        Assert.assertEquals("2", response.isChecked());
        Assert.assertEquals("http://s1.healthbok.com/qrCode/5/15/f28188918b0fba9d.jpg", response.pic());

        Assert.assertEquals(3, response.status());
        Assert.assertEquals("2017-02-24", response.endTime());
        Assert.assertEquals("2017-02-21", response.startTime());
        Assert.assertEquals("满￥999可用", response.detail());
        Assert.assertEquals("满￥999减￥200", response.title());
        Assert.assertEquals(3, response.id());


    }

    @Test
    public void test_searhCoupon_without_login() throws Exception {

        Result<CouponResponse> result =
                withSession(true).couponResources().searhCouponWithoutLogin(3,1L);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        CouponResponse response = result.getModel();

        Assert.assertEquals(6, response.rule().size());
        Assert.assertEquals("2", response.isChecked());
        Assert.assertEquals("http://s1.healthbok.com/qrCode/5/15/f28188918b0fba9d.jpg", response.pic());

        Assert.assertEquals(1, response.status());
        Assert.assertEquals("2017-02-24", response.endTime());
        Assert.assertEquals("2017-02-21", response.startTime());
        Assert.assertEquals("满￥999可用", response.detail());
        Assert.assertEquals("满￥999减￥200", response.title());
        Assert.assertEquals(3, response.id());



    }

    @Test
    public void test_gainCoupon() throws Exception {
        Result<CouponResponse> result =
                withSession().couponResources().gainCoupon(120);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getModel());

        CouponResponse response = result.getModel();
        Assert.assertEquals(120L, response.id());
    }

}