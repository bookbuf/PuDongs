package com.bookbuf.api.customer.resourcesMock;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.DetectionItemResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.UserDocumentResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class CustomerResourcesTest extends AbstractCustomerTest {


    static {
        isMock = true;
    }

    @Test
    public void test_login() throws Exception {

        Result<CustomerLoginResponse> result =
                withSession(true).customerResources().login("", "", "");

        CustomerLoginResponse response = result.getModel();

        Assert.assertEquals(123, response.loginId());
        Assert.assertEquals("abc", response.sessionId());
    }

    @Test
    public void test_wx_login() throws Exception {

        Result<CustomerLoginResponse> result =
                withSession(true).customerResources().wxBindLogin("", "", "", "");

        CustomerLoginResponse response = result.getModel();

        Assert.assertEquals(123, response.loginId());
        Assert.assertEquals("abc", response.sessionId());
    }

    @Test
    public void test_detection() throws Exception {

        Result<DetectionListResponse> result =
                withoutSession(true)
                        .customerResources()
                        .fetchDetectionList(2);

        DetectionListResponse response = result.getModel();
        Assert.assertNotNull(response);
        DetectionItemResponse first = response.list().get(0);

        Assert.assertEquals("体质指数", first.name());
        Assert.assertEquals("70/110", first.value());
        Assert.assertEquals("mmHg", first.unit());
        Assert.assertEquals("2016-8-22 18:02", first.takeTime());
        Assert.assertEquals(0, first.status());

    }


    @Test
    public void test_detection_list() throws Exception {
        Result<DetectionListResponse> result = withSession(true)
                .customerResources()
                .fetchDetectionListByType(null, 1, 1);
        DetectionListResponse response = result.getModel();

        Assert.assertNotNull(response);

        Assert.assertEquals(1, response.list().size());


        DetectionItemResponse first = response.list().get(0);

        Assert.assertEquals("体质指数", first.name());
        Assert.assertEquals("70/110", first.value());
        Assert.assertEquals("mmHg", first.unit());
        Assert.assertEquals("2016-8-22 18:02", first.takeTime());
        Assert.assertEquals(0, first.status());
    }

    @Test
    public void test_detection_user_document() throws Exception {

        Result<UserDocumentResponse> result =
                withSession(true)
                        .customerResources()
                        .fetchUserDocument();

        UserDocumentResponse response = result.getModel();
        Assert.assertNotNull(response);

        Assert.assertEquals("陶青", response.name());
        Assert.assertEquals("1987-10-25", response.birthday());
        Assert.assertEquals("15757118194", response.mobile());
        Assert.assertEquals("0", response.height());
        Assert.assertEquals("0", response.weight());
    }

}