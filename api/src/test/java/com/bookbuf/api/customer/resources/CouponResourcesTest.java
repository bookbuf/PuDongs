package com.bookbuf.api.customer.resources;

import android.widget.EditText;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeResponse;
import com.bookbuf.api.responses.impl.global.StringResponse;
import com.bookbuf.api.responses.impl.medical.SectionResponse;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public class CouponResourcesTest extends AbstractCustomerTest {

    {
//        username = "13655812763";
//        password = "812763";
        username = "18268810820";
        password = "810820";
    }

    @Test
    public void fetchCouponList() throws Exception {
        withSession().couponResources().fetchCouponList(null);
    }

    @Test
    public void fetchCouponListWithoutLogin() throws Exception {
        withSession().couponResources().fetchCouponListWithoutLogin(null);
    }

    @Test
    public void searhCoupon() throws Exception {
        Result<CouponResponse> result = withSession().couponResources().searhCoupon(10,1L);
        CouponResponse response = result.getModel();

        Assert.assertNotNull(response);

        Assert.assertEquals(6, response.rule().size());
        Assert.assertEquals(10, response.id());
        Assert.assertEquals("2017-02-16", response.startTime());
        Assert.assertEquals("2017-04-01", response.endTime());
        Assert.assertEquals("满￥199减￥50", response.title());
        Assert.assertEquals("满￥199可用", response.detail());
        Assert.assertEquals("健康体检",response.categoryName());
        Assert.assertEquals(2, response.status());
    }

    @Test
    public void searhCouponWithoutLogin() throws Exception {
        Result<CouponResponse> result = withSession().couponResources().searhCouponWithoutLogin(10,1L);

        CouponResponse response = result.getModel();

        Assert.assertNotNull(response);

        Assert.assertEquals(6, response.rule().size());
        Assert.assertEquals(10, response.id());
        Assert.assertEquals("2017-02-16", response.startTime());
        Assert.assertEquals("2017-04-01", response.endTime());
        Assert.assertEquals("满￥199减￥50", response.title());
        Assert.assertEquals("满￥199可用", response.detail());
        Assert.assertEquals(1, response.status());
    }

    @Test
    public void gainCoupon() throws Exception {
        withSession().couponResources().gainCoupon(11);
    }

    @Test
    public void gainCouponByAlias() throws Exception {
        withSession().couponResources().gainCouponByAlias("1234");
    }

    @Test
    public void searhCouponByAlias() throws Exception {
        Result<CouponResponse> result = withSession().couponResources().searchCouponByAlias("89z8",null);
        CouponResponse response = result.getModel();

        Assert.assertNotNull(response);

        Assert.assertEquals(6, response.rule().size());
        Assert.assertEquals(10, response.id());
        Assert.assertEquals("2017-02-16", response.startTime());
        Assert.assertEquals("2017-04-01", response.endTime());
        Assert.assertEquals("满￥199减￥50", response.title());
        Assert.assertEquals("满￥199可用", response.detail());
        Assert.assertEquals(2, response.status());
    }

    @Test
    public void searhMyCoupon() throws Exception {
        Result<CouponListResponse> result = withSession().couponResources().searchMyCoupon();

    }

    @Test
    public void searhMyCouponAlias() throws Exception {
        Result<CouponAliasListResponse> result = withSession().couponResources().searchMyCouponAlias();
        CouponAliasResponse response = result.getModel().couponAliases().get(0);
        Assert.assertNotNull(response);

        Assert.assertEquals("优惠码标题 title",response.title());
        Assert.assertEquals("http://s1.healthbok.com/coupon/9/1/c05575557297df73.png",response.couponIcon());
    }

    @Test
    public void searhCouponProduct() throws Exception {
        Result<CouponProductResponse> result = withSession().couponResources().fetchProduct(104,-1L);
        CouponProductResponse response = result.getModel();
        Assert.assertNotNull(response);
        Assert.assertEquals("http://s1.healthbok.com/coupon/9/1/238803fba35c3ac3.png",response.productImgs().get(0));
        Assert.assertEquals("适用于补品",response.className());
    }

    @Test
    public void generateOrder() throws Exception {
        Result<OrderGenerateResponse> result = withSession().orderResources().orderGenerateResponse("aa","ss","1","230204785867153",4);
        OrderGenerateResponse response = result.getModel();

        Assert.assertNotNull(response);
        Assert.assertEquals("app_id=2016080600183259&sign_type=RSA2&sign=MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCzf+ovewX8FaPN76PZYyg+DefaVqAU8P2XM7C0v7iWYQgNxbUzwy+fXKLQ+2xWngLHs0yNJW1UgbdmflYIvEbXAgJ0YgfmGF/kppYCLrdxP/NDul7+wAOWG2uwWNCghjx2rDjSmhpUgyzuGczaTXmvJkdFvKhcjUfYWi2lbjOhIpgos+07dalVrdpJeJJx9bUi7dBDJtZQyO0xCjtKE1kEMJMI2+Tp2YGr1YEuWWReF1GtueLds9pqGhhoLHVNplJABxiqoeVYo/5xWigk2JjvZMPLchnGtQzu2NL7dMvGYEcUkusynb+XDs8umeN1DD/ha1Aot1f+M4Rk3OwlXT+hAgMBAAECggEATB3vIelKVWabysVXNuUMeJwO+JCJW7gB4W9i/JLcZLCK6MQxHd0kQbDXvkUP4P8qcKLA0I3Reedz7Tufoh6s75oDjnTPoqwtZdmafRxwX87dipfqlp64JpxEI0jWV2v1EeHgSrZcegASOnmoW95Evpg92v2wa2bh04YSGzfLHwXEMSDZnaDnPcDzbaxhhMcGEJ8ExMLI1yFVbu1kWtMHh8QpxgkIM41QxydrnP3Inrw76rebKI9/1rtv2z+xcHPY2b4kY50PSUsnhBgZXToe2APxoa1OvVZDCSjv3ezRvwEhDxAq/Zig8snF/xp52mgm9RNiKtAOHlTftQHv1jVqLQKBgQDzsSiq//XJnendJeRJqbK28HRLJi3dMBQwQSevZTj1PG2T45nATv0FI1zJeWtTxU8R3x3D0OAHBpqo3b7meWDeonRAAo0gbK4YeyNHQ02iSqC+K9yQHJdNMpiovgVuARvPsjHDmBATBjrM8ZFPlTbIw8XY84WGmvGZ5YK7mIdSCwKBgQC8kMYybhp0iN4TDo9hx/icUVxHWfBrUy8eTSnSxnzxocHlP/byRsg3xfUO4baRhI4LVDXWL0gznG6n6zDf6u3iGnlWkCxAJGoaxQl/cAOwstV0HsT7okbwm7mEwSxYebu83aD7QtzCjynV2UkpIZh0Euljzmsm0men1+NvWClMgwKBgQCEbEe6LJ9GsU4tQ3L7aIDaEm5Pi+6uXHsMAcvr7uh1syGklLCvJP0vdpjsTtO40OZ3wrOs/etPRhJzIxSir2CpesQLKJfB/+zXFssD5ZnhUAZrcPBJS5AYfh8gyymuLmILijyqmg5aGC+JwNeC1IXRsTjo/OQZUkdppqNVB7y5IwKBgGTu+ag012RZfsWknYsxhxnlUwe+VLqeJDKMCz8PkAGnHCqiOoUk7jcsdygoGZl64q2dvdOQK420xmaLtfWlwv+sPBbhxU3wL/t0TMkyJEeI8kjdz4MCMgbI1F0DKcGSJ7PIzkqwV4qtJy+Yx1iE6x32oYcTDrgRF/r3zH3lKvYRAoGARKm24E5ctr2AhhaLcxhIi+pgDjlyq8NGXMKMb5urlFXaxvwy4BTnjcTHWl9idiEPWKevnqpwjBIjDnyknjZaLzA5WXnajb+GrYM0hosFPyI6oiicTfavZr53fHeEP4h8visbt9bzs6iIP7UmNb1KvjQ6QrdFLb98CaaYHylfqvM=&version=1.0&notify_url=http://172.16.8.103:8080/callback/aliPay&out_trade_no=229789511939140&subject=香蕉&total_amount=100&product_code=QUICK_WAP_PAY",response.payResult().alipayRequest());
        Assert.assertEquals("229789511939140",response.orderRequestId());
        Assert.assertEquals("1",response.payType());
    }

    @Test
    public void OrderDetail() throws Exception {
        Result<OrderDetailResponse> result = withSession().orderResources().orderDetail("229789511939140");
        OrderDetailResponse response = result.getModel();

        Assert.assertNotNull(response);

        Assert.assertEquals("100",response.order().totalPrice());
        Assert.assertEquals("229789511939140",response.orderRequestId());
    }

    @Test
    public void getVerifyCode() throws Exception {
        Result<String> result = withSession().getVerifyCode("18268810820","login");

        Assert.assertEquals("111000",result.getModel());
    }

    @Test
    public void fetchRecom() throws Exception {
        Result<CouponListResponse> result = withSession().fetchRecomCoupon();
        Assert.assertNotNull(result.getModel());
    }

    @Test
    public void getCouponType() throws Exception {
        Result<CouponTypeListResponse> result = withSession().getCouponType();
        Assert.assertNotNull(result.getModel());
    }
}
