package com.bookbuf.api.customer.resources;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.integral.IntegralListResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by bo.wei on 2017/6/13.
 */

public class IntegralResourceTest extends AbstractCustomerTest {
    {
        username = "13655812763";
        password = "812763";
    }

    @Test
    public void fetchIntegralList(){
        Result<IntegralListResponse> result = withSession().fetchIntegral();
        Assert.assertEquals("一树药业", result.getModel().list().get(0).vendorName());

    }

    @Test
    public void cutIntegral(){
        Result<BooleanResponse> result = withSession().cutIntegral(11L);
        Assert.assertEquals(false, result.getModel().get());

    }
}
