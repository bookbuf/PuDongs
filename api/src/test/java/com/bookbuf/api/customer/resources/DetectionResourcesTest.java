package com.bookbuf.api.customer.resources;

import com.bookbuf.api.customer.AbstractCustomerTest;

import org.junit.Test;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public class DetectionResourcesTest extends AbstractCustomerTest {

    @Test
    public void fetchDetectionList() throws Exception {
        withSession().customerResources().fetchDetectionList(1);
    }

    @Test
    public void fetchDetectionListByType() throws Exception {
        withSession().customerResources().fetchDetectionListByType("bmi", 1, 1);
    }

    @Test
    public void fetchUserDocument() throws Exception {
        withSession().customerResources().fetchUserDocument();
    }
}
