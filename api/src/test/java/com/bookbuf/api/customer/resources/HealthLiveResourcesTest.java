package com.bookbuf.api.customer.resources;

import com.bookbuf.api.customer.AbstractCustomerTest;
import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.CategoryResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.ipudong.core.Result;

import org.junit.Assert;
import org.junit.Test;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public class HealthLiveResourcesTest extends AbstractCustomerTest {


    @Test
    public void fetchCategory() throws Exception {
        Result<CategoryListResponse> result = withSession().healthLiveResources().fetchCategory();
        CategoryListResponse response = result.getModel();

        Assert.assertNotNull(response);
        Assert.assertEquals(3, response.categorys().size());

        CategoryResponse categoryResponse = response.categorys().get(0);
        Assert.assertEquals(49, categoryResponse.id());
        Assert.assertEquals("名医讲坛", categoryResponse.title());
        Assert.assertEquals("http://s1.healthbok.com/article/9/6/6b9ed1358003b32e.png", categoryResponse.background());

    }

    @Test
    public void fetchHealthLiveListByCategoryId() throws Exception {
        Result<FetchHealthLiveListResponse>
                result = withSession().healthLiveResources().fetchHealthLiveListByCategoryId();
        FetchHealthLiveListResponse response = result.getModel();

        Assert.assertEquals("5000人看过", response.lists().get(0).visits());
        Assert.assertEquals("5.0", response.lists().get(0).rate());
        Assert.assertEquals("http://s1.healthbok.com/img/img.png", response.lists().get(0).background());
        Assert.assertEquals(6, response.lists().get(0).liveId());
        Assert.assertEquals("原价¥99.9", response.lists().get(0).originalCost());
        Assert.assertEquals("¥68.8", response.lists().get(0).currentCoast());
    }

    @Test
    public void fetchHealthLiveListByLiveId() throws Exception {
        Result<FetchRelatedHealthLiveListResponse> result = withSession(true).healthLiveResources().fetchHealthLiveListByLiveId(6);
        FetchRelatedHealthLiveListResponse response = result.getModel();

        Assert.assertEquals(4, response.lists().size());

        Assert.assertEquals("5000人看过", response.lists().get(0).visits());
        Assert.assertEquals(5, response.lists().get(0).liveId());
        Assert.assertEquals("5.0", response.lists().get(0).rate());
        Assert.assertEquals("更新5期,每期¥9.9", response.lists().get(0).description());
        Assert.assertEquals("http://s1.healthbok.com/img/img.png", response.lists().get(0).background());
        Assert.assertEquals("原价¥99.9", response.lists().get(0).originalCost());
        Assert.assertEquals("¥68.8", response.lists().get(0).currentCoast());

        Assert.assertEquals("我是简介", response.main().summary());
        Assert.assertEquals(3, response.main().knowledge().size());
        Assert.assertEquals("我是知识1", response.main().knowledge().get(0));
        Assert.assertEquals("http://s1.healthbok.com/video/video.mp4", response.main().url());

    }
}
