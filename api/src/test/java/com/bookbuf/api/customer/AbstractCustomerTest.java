package com.bookbuf.api.customer;

import com.bookbuf.api.clients.c.CustomerApi;
import com.bookbuf.api.clients.c.CustomerFactoryImpl;
import com.bookbuf.api.configurations.ConfigurationBuilder;
import com.bookbuf.api.responses.impl.user.CredentialResponse;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public abstract class AbstractCustomerTest {

    private static final String API_SEVER = "http://172.16.8.194/mapi";
    //    private static final String API_SEVER = "http://api.iputong.com/mapi";
    private static final String API_VERSION = "1.7.0";
    private static final int API_CLIENT = 7;

    // -------------------------------------------------
    // 接口的相关配置 请勿更改
    // -------------------------------------------------
    private static final String API_CLIENT_SECRET = "pudong*mp";
    private static final String API_CLIENT_SYSTEM_INFO = "mapi||1||2||3||4||api.iputong.com";
    // -------------------------------------------------
    // 以下内容请修改为你自己的测试帐号、密码
    // -------------------------------------------------
    protected static String username = "18668247777";
    protected static String password = "123456";
    protected static boolean isMock = false;
    private static CustomerApi resources;
    private static CustomerApi resourcesWithSession;
    private static CredentialResponse credentialResponse;

    @BeforeClass
    public static void setup() {
        resourcesWithSession = null;
        resources = null;
    }

    @AfterClass
    public static void clearDown() {
        resources = null;
        resourcesWithSession = null;
    }

    public CustomerApi withoutSession(boolean... bools) {
        if (bools != null && bools.length == 1) {
            isMock = bools[0];
        }
        if (resources == null) {
            resources = new CustomerFactoryImpl(
                    new ConfigurationBuilder()
                            .setApiServerURL(API_SEVER)
                            .setAppVersion(API_VERSION)
                            .setPlatForm(API_CLIENT)
                            .setPlatFormSecret(API_CLIENT_SECRET)
                            .setSessionId(null)
                            .setMockMode(isMock)
                            .setSystemInformation(API_CLIENT_SYSTEM_INFO)
                            .build()
            ).getInstance();
        }
        return resources;
    }

    public CustomerApi withoutSession(boolean isMock) {
        return withoutSession(new boolean[]{isMock});
    }

    public CustomerApi withSession(boolean isMock) {
        return withSession(new boolean[]{isMock});
    }

    public CustomerApi withSession(boolean... bools) {

        credentialResponse = withoutSession().loginTest(username, password, null).getModel();

        if (bools != null && bools.length == 1) {
            isMock = bools[0];
        }
        final String sessionId = credentialResponse != null ? credentialResponse.sessionId() : null;
        resourcesWithSession = new CustomerFactoryImpl(
                new ConfigurationBuilder()
                        .setApiServerURL(API_SEVER)
                        .setAppVersion(API_VERSION)
                        .setPlatForm(API_CLIENT)
                        .setPlatFormSecret(API_CLIENT_SECRET)
                        .setSessionId(sessionId)
                        .setSystemInformation(API_CLIENT_SYSTEM_INFO)
                        .setMockMode(isMock)
                        .build()
        ).getInstance();

        return resourcesWithSession;
    }

}
