package com.bookbuf.api;

import android.test.AndroidTestCase;

import com.bookbuf.api.apis.MayoAPI;
import com.bookbuf.api.apis.MonitorAPI;
import com.ipudong.core.network.RestServiceFactory;
import com.ipudong.core.network.response.impl.HealthbokResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: robert.
 * date :  16/11/21.
 */

public class MayoResourcesTest extends AndroidTestCase {

	public void test_diseaseDetail ()  {
		MayoAPI api = RestServiceFactory.getService ("", MayoAPI.class);
		HealthbokResponse response = api.diseaseDetail (1469707910298l, "UTF-8", 10008);
	}


	public void test_minitor(){
		MonitorAPI api = RestServiceFactory.getService ("http://www.uwonders-my.com:8988/pharmacy/alarm/oper/", MonitorAPI.class);
		String s = "{\"alarmDateEnd\":\"2017-12-24 06:00:00\",\"alarmDateStart\":\"2000-10-24 06:00:00\",\"chainPharmacyId\":4}";

		try {
			JSONObject result = new JSONObject(s);
			HealthbokResponse response = api.selectCount(result.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}
