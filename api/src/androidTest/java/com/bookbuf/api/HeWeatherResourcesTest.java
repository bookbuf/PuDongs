package com.bookbuf.api;

import android.test.AndroidTestCase;

import com.bookbuf.api.apis.HeWeatherAPI;
import com.bookbuf.api.clients.b.BusinessApi;
import com.bookbuf.api.clients.b.BusinessFactoryImpl;
import com.bookbuf.api.configurations.ConfigurationBuilder;
import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.ipudong.core.Result;
import com.ipudong.core.network.RestServiceFactory;

/**
 * author: robert.
 * date :  16/11/22.
 */

public class HeWeatherResourcesTest extends AndroidTestCase {

    private static final String HE_WEATHER_KEY = "8dc6b44697564543a932d8a06861bb60";

    private HeWeatherAPI heWeatherAPI() {
        return RestServiceFactory.getService("https://free-api.heweather.com/v5/", HeWeatherAPI.class);
    }

    public void test_weather_() {
        BusinessApi resources = new BusinessFactoryImpl(
                new ConfigurationBuilder()
                        .setApiServerURL("http://172.16.8.194/mapi")
                        .setAppVersion("3.2.2-api-module")
                        .setCachePath(null)/* now is featureless */
                        .setCacheSize(1024 * 10)/* now is featureless */
                        .setPlatForm(7)/* for bp*/
                        .setPlatFormSecret("pudong*mp")
                        .setSessionId(null)/* without logged in statement*/
                        .setSystemInformation("mapi||1||2||3||4||api.iputong.com")/* collect information for storage*/
                        .build()
        ).getInstance();
        Result<WeatherResponse> result = resources.fetchWeather("beijing");
    }

    public void test_alarm() {
        heWeatherAPI().alarm("beijing", HE_WEATHER_KEY);
    }

    public void test_forecast() {
        heWeatherAPI().forecast("beijing", HE_WEATHER_KEY);
    }

    public void test_historical() {
        heWeatherAPI().historical("beijing", "2013-12-30", HE_WEATHER_KEY);
    }

    public void test_hourly() {
        heWeatherAPI().hourly("beijing", HE_WEATHER_KEY);
    }

    public void test_now() {
        heWeatherAPI().now("beijing", HE_WEATHER_KEY);
    }

    public void test_scenic() {
        heWeatherAPI().scenic("beijing", HE_WEATHER_KEY);
    }

    public void test_search() {
        heWeatherAPI().search("beijing", HE_WEATHER_KEY);
    }

    public void test_suggestion() {
        heWeatherAPI().suggestion("beijing", HE_WEATHER_KEY);
    }

    public void test_weather() {
        heWeatherAPI().weather("beijing", HE_WEATHER_KEY);
    }
}
