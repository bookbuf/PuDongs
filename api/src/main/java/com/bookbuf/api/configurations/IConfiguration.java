package com.bookbuf.api.configurations;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface IConfiguration extends Serializable {

    /**
     * @return 应用平台版本号 用于区分不同的客户端
     */
    int getAppId();

    /**
     * @return 应用平台的 secret
     */
    String getAppSecret();

    /**
     * @return 获取Api 的服务器地址
     */
    String getApiServerURL();

    /**
     * @return 获取Api 缓存地址
     */
    String getCachePath();

    /**
     * @return 获取Api 缓存空间限制
     */
    int getCacheSize();

    /**
     * @return 获取App 的版本号
     */
    String getAppVersion();

    /**
     * @return 获取登陆鉴权之后的 sessionId
     */
    String getSessionId();

    /**
     * @return 获取系统版本信息
     */
    String getSystemInformation();

    /**
     * @return 模拟数据模式，将不请求API接口使用预设的数据返回。
     */
    boolean isMockMode();

}
