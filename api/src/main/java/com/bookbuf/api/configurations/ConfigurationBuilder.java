package com.bookbuf.api.configurations;

/**
 * author: robert.
 * date :  16/8/3.
 */
public final class ConfigurationBuilder {

    private ConfigurationImpl configuration = new ConfigurationImpl();

    private void checkNotBuilt() {
        if (configuration == null) {
            throw new IllegalStateException("Cannot use this builder any longer, build() has already been called");
        }
    }

    public ConfigurationBuilder setPlatForm(int platForm) {
        checkNotBuilt();
        configuration.setPlatForm(platForm);
        return this;
    }

    public ConfigurationBuilder setPlatFormSecret(String secret) {
        checkNotBuilt();
        configuration.setPlatFormSecret(secret);
        return this;
    }

    public ConfigurationBuilder setApiServerURL(String serverURL) {
        checkNotBuilt();
        configuration.setApiServerURL(serverURL);
        return this;
    }

    public ConfigurationBuilder setCachePath(String path) {
        checkNotBuilt();
        configuration.setCachePath(path);
        return this;
    }

    public ConfigurationBuilder setCacheSize(int size) {
        checkNotBuilt();
        configuration.setCacheSize(size);
        return this;
    }

    public ConfigurationBuilder setAppVersion(String version) {
        checkNotBuilt();
        configuration.setAppVersion(version);
        return this;
    }

    public ConfigurationBuilder setSessionId(String sessionId) {
        checkNotBuilt();
        configuration.setSessionId(sessionId);
        return this;
    }

    public ConfigurationBuilder setSystemInformation(String information) {
        checkNotBuilt();
        configuration.setSystemInformation(information);
        return this;
    }

    public ConfigurationBuilder setMockMode(boolean mockMode) {
        checkNotBuilt();
        configuration.setMockMode(mockMode);
        return this;
    }

    public IConfiguration build() {
        checkNotBuilt();
        return configuration;
    }

}
