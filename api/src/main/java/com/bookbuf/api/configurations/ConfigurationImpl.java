package com.bookbuf.api.configurations;

/**
 * author: robert.
 * date :  16/8/2.
 */
public class ConfigurationImpl implements IConfiguration {
    private static final String API_SERVER_URL = "http://api.iputong.com/mapi";
    private int platform = 7;
    private String platformSecret = "pudong*mp";
    private String cachePath = "";
    private int cacheSize = 1024 * 1024 * 10;
    private String appVersion = "";
    private String sessionId = "";
    private String systemInfo = "";
    private String apiServerURL = API_SERVER_URL;/*default*/
    private boolean mockMode = false;

    @Override
    public int getAppId() {
        return platform;
    }

    public void setPlatForm(int platForm) {
        this.platform = platForm;
    }

    @Override
    public String getAppSecret() {
        return platformSecret;
    }

    @Override
    public String getApiServerURL() {
        return apiServerURL;
    }

    public void setPlatFormSecret(String secret) {
        this.platformSecret = secret;
    }

    public void setApiServerURL(String serverURL) {
        this.apiServerURL = serverURL;
    }

    @Override
    public String getCachePath() {
        return cachePath;
    }

    public void setCachePath(String path) {
        this.cachePath = path;
    }

    @Override
    public int getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(int size) {
        this.cacheSize = size;
    }

    @Override
    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String version) {
        this.appVersion = version;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String getSystemInformation() {
        return systemInfo;
    }

    @Override
    public boolean isMockMode() {
        return mockMode;
    }

    public void setMockMode(boolean mockMode) {
        this.mockMode = mockMode;
    }

    public void setSystemInformation(String information) {
        this.systemInfo = information;
    }

}
