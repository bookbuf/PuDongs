package com.bookbuf.api.configurations;

/**
 * author: robert.
 * date :  16/8/3.
 */
class ConfigurationFactoryImpl implements IConfigurationFactory {

	private static final IConfiguration I_CONFIGURATION = new ConfigurationImpl ();

	@Override
	public IConfiguration getInstance () {
		return I_CONFIGURATION;
	}

	@Override
	public void dispose () {

	}
}
