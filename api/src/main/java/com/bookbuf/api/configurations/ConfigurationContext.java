package com.bookbuf.api.configurations;

/**
 * author: robert.
 * date :  16/8/3.
 */
public final class ConfigurationContext {

	private static final IConfigurationFactory factory;

	static {
		try {
			factory = (IConfigurationFactory) Class.forName ("com.bookbuf.api.configurations.ConfigurationFactoryImpl").newInstance ();
		} catch (InstantiationException e) {
			throw new AssertionError (e);
		} catch (IllegalAccessException e) {
			throw new AssertionError (e);
		} catch (ClassNotFoundException e) {
			throw new AssertionError (e);
		}
	}

	public static IConfiguration getConfiguration () {
		return factory.getInstance ();
	}
}
