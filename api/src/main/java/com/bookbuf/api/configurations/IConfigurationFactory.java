package com.bookbuf.api.configurations;

/**
 * author: robert.
 * date :  16/8/3.
 */
public interface IConfigurationFactory {
	/**
	 * returns the root configuration
	 *
	 * @return root configuration
	 */
	IConfiguration getInstance ();

	/**
	 * clean up resources acquired by this factory.
	 */
	void dispose ();
}
