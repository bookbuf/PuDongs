package com.bookbuf.api.clients.c;

import com.bookbuf.api.clients.resources.impl.CouponResources;
import com.bookbuf.api.clients.resources.impl.CustomerResources;
import com.bookbuf.api.clients.resources.impl.GlobalResources;
import com.bookbuf.api.clients.resources.impl.HealthLiveResources;
import com.bookbuf.api.clients.resources.impl.IntegralResources;
import com.bookbuf.api.clients.resources.impl.OrderResources;
import com.bookbuf.api.configurations.IConfiguration;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public interface CustomerApi extends Serializable,
        CustomerResources,
        CouponResources.Customer,
        GlobalResources.Customer,
        HealthLiveResources.Customer,
        OrderResources,
        IntegralResources{

    CustomerResources customerResources();

    OrderResources orderResources();

    CouponResources.Customer couponResources();

    GlobalResources.Customer globalResources();

    HealthLiveResources.Customer healthLiveResources();

    boolean checkSessionId(String outSideSessionId);

    void updateConfiguration(IConfiguration configuration);
}
