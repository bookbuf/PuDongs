package com.bookbuf.api.clients.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bookbuf.api.clients.ApiClubImpl;
import com.bookbuf.api.clients.resources.impl.CouponResources;
import com.bookbuf.api.clients.resources.impl.CustomerResources;
import com.bookbuf.api.clients.resources.impl.GlobalResources;
import com.bookbuf.api.clients.resources.impl.HealthLiveResources;
import com.bookbuf.api.clients.resources.impl.OrderResources;
import com.bookbuf.api.configurations.IConfiguration;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeListResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.bookbuf.api.responses.impl.customer.UserDocumentResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.bookbuf.api.responses.impl.integral.IntegralListResponse;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.bookbuf.api.responses.impl.order.OrderPayResponse;
import com.bookbuf.api.responses.impl.order.OrderResultResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.ipudong.core.Result;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public class CustomerApiMockImpl extends ApiClubImpl implements CustomerApi {

    CustomerApi resources;

    public CustomerApiMockImpl(IConfiguration configuration, CustomerApi resources) {
        super(configuration);
        this.resources = resources;
    }

    @Override
    public CustomerResources customerResources() {
        return this;
    }

    @Override
    public CouponResources.Customer couponResources() {
        return this;
    }

    @Override
    public GlobalResources.Customer globalResources() {
        return this;
    }

    @Override
    public HealthLiveResources.Customer healthLiveResources() {
        return this;
    }

    @Override
    public OrderResources orderResources() {
        return this;
    }

    @Override
    public boolean checkSessionId(String outSideSessionId) {
        return resources.checkSessionId(outSideSessionId);
    }

    @Override
    public void updateConfiguration(IConfiguration configuration) {
        resources.updateConfiguration(configuration);
    }


    @Override
    public Result<CredentialResponse> loginTest(String target, String password, String pushId) {
        return resources.loginTest(target, password, pushId);
    }

    @Override
    public Result<BooleanResponse> sendVerifyCode(String mobile, @SignDef String sign) {
        return null;
    }

    @Override
    public Result<BooleanResponse> updatePassword(String oldPwd, String newPwd) {
        return null;
    }

    @Override
    public Result<CustomerLoginResponse> login(@NonNull String mobile, @NonNull String code, @Nullable String pushId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "  \"success\": true,\n" +
                    "  \"msgCode\": null,\n" +
                    "  \"msgInfo\": null,\n" +
                    "  \"count\": 0,\n" +
                    "  \"model\": {\n" +
                    "    \"loginId\": 123,\n" +
                    "    \"sessionId\": \"abc\"\n" +
                    "  }\n" +
                    "}");
            return parser().createCustomerLoginResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.login(mobile, code, pushId);
    }

    @Override
    public Result<CustomerLoginResponse> wxBindLogin(@NonNull String mobile, @NonNull String code, @Nullable String openid, @Nullable String pushId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "  \"success\": true,\n" +
                    "  \"msgCode\": null,\n" +
                    "  \"msgInfo\": null,\n" +
                    "  \"count\": 0,\n" +
                    "  \"model\": {\n" +
                    "    \"loginId\": 123,\n" +
                    "    \"sessionId\": \"abc\"\n" +
                    "  }\n" +
                    "}");
            return parser().createCustomerLoginResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.wxBindLogin(mobile, code, openid, pushId);
    }

    @Override
    public Result<BooleanResponse> modifyProfile(String nickName, String gender, String idCard, String email, String province, String city, String distinct, String avatar) {
        return null;
    }

    @Override
    public Result<ProfileResponse> fetchProfile() {
        return null;
    }

    @Override
    public Result<DetectionListResponse> fetchDetectionList(@NonNull int vendorId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"name\": \"体质指数\",\n" +
                    "            \"value\": \"70/110\",\n" +
                    "            \"unit\": \"mmHg\",\n" +
                    "            \"status\": 0,\n" +
                    "            \"takeTime\": \"2016-8-22 18:02\"\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            return parser().createDetectionListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchDetectionList(vendorId);
    }

    @Override
    public Result<CouponResponse> gainCoupon(long configurationId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"status\": 1,\n" +
                    "        \"collectTime\": \"1\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 109,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"pic\": \"http://s1.healthbok.com/qrCode/2/126/a577a6b2457795ac.jpg\",\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "    }\n" +
                    "}");
            return parser().createCouponResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.gainCoupon(configurationId);
    }

    @Override
    public Result<CouponResponse> searhCoupon(long configurationId,Long userCouponId) {


        try {
            JSONObject mock = null;
            switch ((int) configurationId) {
                case 1:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"status\": 1,\n" +
                            "        \"limitCollectTime\": \"10\",\n" +
                            "        \"collectTime\": \"10\",\n" +
                            "        \"category\": \"1\",\n" +
                            "        \"isGained\": \"1\",\n" +
                            "        \"mode\": \"1\",\n" +
                            "        \"orderId\": \"012AD98G37348\",\n" +
                            "        \"userCouponId\": \"666\",\n" +
                            "        \"id\": 109,\n" +
                            "        \"title\": \"加¥20可换购\",\n" +
                            "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                            "        \"num\": \"加¥20可换购\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1.限好簿。\",\n" +
                            "            \"\\r\\n1.限好簿。\"\n" +
                            "        ],\n" +
                            "        \"startTime\": \"2017-04-19\",\n" +
                            "        \"endTime\": \"2017-04-23\",\n" +
                            "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                            "        \"isProduct\": 0,\n" +
                            "        \"className\": \"适用于生活类\",\n" +
                            "        \"productName\": \"生活用品\",\n" +
                            "        \"isChecked\": \"1\"\n" +
                            "    }\n" +
                            "}");
                    break;
                case 2:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"status\": 1,\n" +
                            "        \"limitCollectTime\": \"10\",\n" +
                            "        \"collectTime\": \"9\",\n" +
                            "        \"category\": \"1\",\n" +
                            "        \"isGained\": \"1\",\n" +
                            "        \"mode\": \"1\",\n" +
                            "        \"orderId\": \"012AD98G37348\",\n" +
                            "        \"userCouponId\": \"666\",\n" +
                            "        \"id\": 109,\n" +
                            "        \"title\": \"加¥40可换购\",\n" +
                            "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                            "        \"num\": \"加¥20可换购\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1.限好簿。\",\n" +
                            "            \"\\r\\n1.限好簿。\"\n" +
                            "        ],\n" +
                            "        \"startTime\": \"2017-04-19\",\n" +
                            "        \"endTime\": \"2017-04-23\",\n" +
                            "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                            "        \"isProduct\": 0,\n" +
                            "        \"className\": \"适用于生活类\",\n" +
                            "        \"productName\": \"生活用品\",\n" +
                            "        \"isChecked\": \"1\"\n" +
                            "    }\n" +
                            "}");
                    break;
                case 3:
                default:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"status\": 1,\n" +
                            "        \"limitCollectTime\": \"10\",\n" +
                            "        \"collectTime\": \"5\",\n" +
                            "        \"category\": \"1\",\n" +
                            "        \"isGained\": \"1\",\n" +
                            "        \"mode\": \"1\",\n" +
                            "        \"orderId\": \"012AD98G37348\",\n" +
                            "        \"userCouponId\": \"666\",\n" +
                            "        \"id\": 109,\n" +
                            "        \"title\": \"加¥20可换购\",\n" +
                            "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                            "        \"num\": \"加¥20可换购\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1.限好簿。\",\n" +
                            "            \"\\r\\n1.限好簿。\"\n" +
                            "        ],\n" +
                            "        \"startTime\": \"2017-04-19\",\n" +
                            "        \"endTime\": \"2017-04-23\",\n" +
                            "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                            "        \"isProduct\": 0,\n" +
                            "        \"className\": \"适用于生活类\",\n" +
                            "        \"productName\": \"生活用品\",\n" +
                            "        \"isChecked\": \"1\"\n" +
                            "    }\n" +
                            "}");
                    break;
            }
            return parser().createCouponResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.searhCoupon(configurationId,userCouponId);
    }


    @Override
    public Result<CouponResponse> searhCouponWithoutLogin(long configurationId,Long userCouponId) {


        try {
            JSONObject mock = null;
            switch ((int) configurationId) {
                case 1:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"status\": 1,\n" +
                            "        \"limitCollectTime\": \"10\",\n" +
                            "        \"collectTime\": \"10\",\n" +
                            "        \"category\": \"1\",\n" +
                            "        \"isGained\": \"1\",\n" +
                            "        \"mode\": \"1\",\n" +
                            "        \"orderId\": \"012AD98G37348\",\n" +
                            "        \"userCouponId\": \"666\",\n" +
                            "        \"id\": 109,\n" +
                            "        \"title\": \"加¥20可换购\",\n" +
                            "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                            "        \"num\": \"加¥20可换购\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1.限好簿。\",\n" +
                            "            \"\\r\\n1.限好簿。\"\n" +
                            "        ],\n" +
                            "        \"startTime\": \"2017-04-19\",\n" +
                            "        \"endTime\": \"2017-04-23\",\n" +
                            "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                            "        \"isProduct\": 0,\n" +
                            "        \"className\": \"适用于生活类\",\n" +
                            "        \"productName\": \"生活用品\",\n" +
                            "        \"isChecked\": \"1\"\n" +
                            "    }\n" +
                            "}");
                    break;
                case 2:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"status\": 1,\n" +
                            "        \"limitCollectTime\": \"10\",\n" +
                            "        \"collectTime\": \"9\",\n" +
                            "        \"category\": \"1\",\n" +
                            "        \"isGained\": \"1\",\n" +
                            "        \"mode\": \"1\",\n" +
                            "        \"orderId\": \"012AD98G37348\",\n" +
                            "        \"userCouponId\": \"666\",\n" +
                            "        \"id\": 109,\n" +
                            "        \"title\": \"加¥40可换购\",\n" +
                            "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                            "        \"num\": \"加¥20可换购\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1.限好簿。\",\n" +
                            "            \"\\r\\n1.限好簿。\"\n" +
                            "        ],\n" +
                            "        \"startTime\": \"2017-04-19\",\n" +
                            "        \"endTime\": \"2017-04-23\",\n" +
                            "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                            "        \"isProduct\": 0,\n" +
                            "        \"className\": \"适用于生活类\",\n" +
                            "        \"productName\": \"生活用品\",\n" +
                            "        \"isChecked\": \"1\"\n" +
                            "    }\n" +
                            "}");
                    break;
                case 3:
                default:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"status\": 1,\n" +
                            "        \"limitCollectTime\": \"10\",\n" +
                            "        \"collectTime\": \"5\",\n" +
                            "        \"category\": \"1\",\n" +
                            "        \"isGained\": \"1\",\n" +
                            "        \"mode\": \"1\",\n" +
                            "        \"orderId\": \"012AD98G37348\",\n" +
                            "        \"userCouponId\": \"666\",\n" +
                            "        \"id\": 109,\n" +
                            "        \"title\": \"加¥20可换购\",\n" +
                            "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                            "        \"num\": \"加¥20可换购\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1.限好簿。\",\n" +
                            "            \"\\r\\n1.限好簿。\"\n" +
                            "        ],\n" +
                            "        \"startTime\": \"2017-04-19\",\n" +
                            "        \"endTime\": \"2017-04-23\",\n" +
                            "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                            "        \"isProduct\": 0,\n" +
                            "        \"className\": \"适用于生活类\",\n" +
                            "        \"productName\": \"生活用品\",\n" +
                            "        \"isChecked\": \"1\"\n" +
                            "    }\n" +
                            "}");
                    break;
            }
            return parser().createCouponResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.searhCoupon(configurationId,userCouponId);
    }

    @Override
    public Result<CouponListResponse> fetchCouponList(@Nullable Long vendorId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "        \"status\": 1,\n" +
                    "        \"limitCollectTime\": \"10\",\n" +
                    "        \"collectTime\": \"10\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 1,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "        },{\n" +
                    "        \"status\": 1,\n" +
                    "        \"limitCollectTime\": \"10\",\n" +
                    "        \"collectTime\": \"5\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 2,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "        },{\n" +
                    "        \"status\": 1,\n" +
                    "        \"limitCollectTime\": \"10\",\n" +
                    "        \"collectTime\": \"5\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 3,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "        }" +
                    "}");
            return parser().createCouponListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchCouponList(vendorId);
    }

    @Override
    public Result<CouponListResponse> fetchCouponListWithoutLogin(@Nullable Long vendorId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "        \"status\": 1,\n" +
                    "        \"limitCollectTime\": \"10\",\n" +
                    "        \"collectTime\": \"10\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 1,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "        },{\n" +
                    "        \"status\": 1,\n" +
                    "        \"limitCollectTime\": \"10\",\n" +
                    "        \"collectTime\": \"5\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 2,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "        },{\n" +
                    "        \"status\": 1,\n" +
                    "        \"limitCollectTime\": \"10\",\n" +
                    "        \"collectTime\": \"5\",\n" +
                    "        \"category\": \"1\",\n" +
                    "        \"isGained\": \"1\",\n" +
                    "        \"mode\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"userCouponId\": \"666\",\n" +
                    "        \"id\": 3,\n" +
                    "        \"title\": \"加¥20可换购\",\n" +
                    "        \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "        \"num\": \"加¥20可换购\",\n" +
                    "        \"rule\": [\n" +
                    "            \"1.限好簿。\",\n" +
                    "            \"\\r\\n1.限好簿。\"\n" +
                    "        ],\n" +
                    "        \"startTime\": \"2017-04-19\",\n" +
                    "        \"endTime\": \"2017-04-23\",\n" +
                    "        \"couponIcon\": \"http: //s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "        \"isProduct\": 0,\n" +
                    "        \"className\": \"适用于生活类\",\n" +
                    "        \"productName\": \"生活用品\",\n" +
                    "        \"isChecked\": \"1\"\n" +
                    "        }" +
                    "}");
            return parser().createCouponListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchCouponList(vendorId);
    }

    @Override
    public Result<BooleanResponse> checkVerifyCode(String mobile, String sign, String code) {
        return resources.checkVerifyCode(mobile, sign, code);
    }

    @Override
    public Result<AppUpgradeResponse> fetchAppUpgrade(String appName) {
        return resources.fetchAppUpgrade(appName);
    }

    @Override
    public Result<BooleanResponse> sendNotice(String mobile, @SignDef String sign, String type) {
        return resources.sendNotice(mobile,sign,type);
    }

    @Override
    public Result<CustomerLoginResponse> globalLogin(String userName, String pushId, String password) {
        return resources.globalLogin(userName,pushId,password);
    }

    @Override
    public Result<CustomerLoginResponse> thirtyLogin(String uniqueId, String sign, String pushId) {
        return resources.thirtyLogin(uniqueId,sign,pushId);
    }

    @Override
    public Result<BooleanResponse> thirtyBind(String mobile, String uniqueId, String code, String sign) {
        return resources.thirtyBind(mobile,uniqueId,code,sign);
    }

    @Override
    public Result<BooleanResponse> resetPassword(String mobile, String verifyCode, String newPwd, String sign) {
        return resources.resetPassword(mobile,verifyCode,newPwd,sign);
    }

    @Override
    public Result<CategoryListResponse> fetchCategory() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"id\": 49,\n" +
                    "            \"title\": \"名医讲坛\",\n" +
                    "            \"background\": \"http://s1.healthbok.com/article/9/6/6b9ed1358003b32e.png\"\n" +
                    "        },\n" +
                    "        {\n" +
                    "            \"id\": 48,\n" +
                    "            \"title\": \"养生课堂\",\n" +
                    "            \"background\": \"http://s1.healthbok.com/article/9/6/6b9ed1358003b32e.png\"\n" +
                    "        },\n" +
                    "        {\n" +
                    "            \"id\": 47,\n" +
                    "            \"title\": \"精品优选\",\n" +
                    "            \"background\": \"http://s1.healthbok.com/article/9/6/6b9ed1358003b32e.png\"\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            return parser().createCategoryListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchCategory();
    }

    @Override
    public Result<FetchHealthLiveListResponse> fetchHealthLiveListByCategoryId() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"liveId\": 1,\n" +
                    "            \"rate\": 4.5,\n" +
                    "            \"visits\": 234,\n" +
                    "            \"background\": \"...\"\n" +
                    "        },\n" +
                    "        {\n" +
                    "            \"liveId\": 1,\n" +
                    "            \"rate\": 4.5,\n" +
                    "            \"visits\": 234,\n" +
                    "            \"background\": \"...\"\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            return parser().createFetchHealthLiveListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchHealthLiveListByCategoryId();
    }

    @Override
    public Result<FetchRelatedHealthLiveListResponse> fetchHealthLiveListByLiveId(long liveId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"album\": [\n" +
                    "            {\n" +
                    "                \"visits\": \"5000人看过\",\n" +
                    "                \"rate\": \"5.0\",\n" +
                    "                \"background\": \"http://s1.healthbok.com/img/img.png\",\n" +
                    "                \"description\": \"更新5期,每期¥9.9\",\n" +
                    "                \"originalCost\": \"原价¥99.9\",\n" +
                    "                \"liveId\": 5,\n" +
                    "                \"currentCoast\": \"¥68.8\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"visits\": \"5000人看过\",\n" +
                    "                \"rate\": \"5.0\",\n" +
                    "                \"background\": \"http://s1.healthbok.com/img/img.png\",\n" +
                    "                \"description\": \"更新5期,每期¥9.9\",\n" +
                    "                \"originalCost\": \"原价¥99.9\",\n" +
                    "                \"liveId\": 4,\n" +
                    "                \"currentCoast\": \"¥68.8\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"visits\": \"5000人看过\",\n" +
                    "                \"rate\": \"5.0\",\n" +
                    "                \"background\": \"http://s1.healthbok.com/img/img.png\",\n" +
                    "                \"description\": \"更新5期,每期¥9.9\",\n" +
                    "                \"originalCost\": \"原价¥99.9\",\n" +
                    "                \"liveId\": 3,\n" +
                    "                \"currentCoast\": \"¥68.8\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"visits\": \"5000人看过\",\n" +
                    "                \"rate\": \"5.0\",\n" +
                    "                \"background\": \"http://s1.healthbok.com/img/img.png\",\n" +
                    "                \"description\": \"更新5期,每期¥9.9\",\n" +
                    "                \"originalCost\": \"原价¥99.9\",\n" +
                    "                \"liveId\": 2,\n" +
                    "                \"currentCoast\": \"¥68.8\"\n" +
                    "            }\n" +
                    "        ],\n" +
                    "        \"main\": {\n" +
                    "            \"visits\": \"5000人看过\",\n" +
                    "            \"rate\": \"5.0\",\n" +
                    "            \"background\": \"http://s1.healthbok.com/img/img.png\",\n" +
                    "            \"description\": \"更新5期,每期¥9.9\",\n" +
                    "            \"originalCost\": \"原价¥99.9\",\n" +
                    "            \"liveId\": 6,\n" +
                    "            \"currentCoast\": \"¥68.8\",\n" +
                    "            \"sumary\": \"我是简介\",\n" +
                    "            \"url\": \"http://s1.healthbok.com/video/video.mp4\",\n" +
                    "            \"knowledge\": [\n" +
                    "                \"我是知识1\",\n" +
                    "                \"我是知识2\",\n" +
                    "                \"我是知识3\"\n" +
                    "            ]\n" +
                    "        }\n" +
                    "    },\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"msgCode\": null\n" +
                    "}");
            return parser().createFetchRelatedHealthLiveListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchHealthLiveListByLiveId(liveId);
    }

    @Override
    public Result<DetectionListResponse> fetchDetectionListByType(String type, int page, int pageSize) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"name\": \"体质指数\",\n" +
                    "            \"value\": \"70/110\",\n" +
                    "            \"unit\": \"mmHg\",\n" +
                    "            \"status\": 0,\n" +
                    "            \"takeTime\": \"2016-8-22 18:02\"\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            return parser().createDetectionListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchDetectionListByType(type, page, pageSize);
    }

    @Override
    public Result<UserDocumentResponse> fetchUserDocument() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"name\": \"陶青\",\n" +
                    "        \"gender\": 1,\n" +
                    "        \"birthday\": \"1987-10-25\",\n" +
                    "        \"mobile\": \"15757118194\",\n" +
                    "        \"height\": 0,\n" +
                    "        \"weight\": 0\n" +
                    "    }\n" +
                    "}");
            return parser().createUserDocumentResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchUserDocument();
    }

    @Override
    public Result<BooleanResponse> gainCouponByAlias(String couponAlias) {
        try {
            JSONObject mock = new JSONObject("{ \n" +
                    "\"success\": true, \n" +
                    "\"msgCode\": null, \n" +
                    "\"msgInfo\": null, \n" +
                    "\"count\": 0, \n" +
                    "\"model\": true \n" +
                    "}");
            return parser().createBooleanResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.gainCouponByAlias(couponAlias);
    }

    @Override
    public Result<CouponResponse> searchCouponByAlias(String couponAlias,Long couponId) {
        try {
            JSONObject mock = null;
            switch (1) {
                case 1:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"id\": 1,\n" +
                            "        \"title\": \"满￥999减￥200\",\n" +
                            "        \"detail\": \"满￥999可用\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                            "            \"2、适用品牌：谢琳燕窝。\",\n" +
                            "            \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                            "            \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                            "            \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                            "            \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                            "        ],\n" +
                            "            \"content\": {\n" +
                            "                \"full\": 999,\n" +
                            "                \"cut\": 200\n" +
                            "            },\n" +
                            "        \"startTime\": \"2017-02-21\",\n" +
                            "        \"endTime\": \"2017-02-24\",\n" +
                            "        \"status\": 1,\n" +
                            "        \"isChecked\": \"2\",\n" +
                            "        \"pic\": \"http://s1.healthbok.com/qrCode/5/15/f28188918b0fba9d.jpg\"\n" +
                            "    }\n" +
                            "}");
                    break;
                case 2:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"id\": 2,\n" +
                            "        \"title\": \"满￥999减￥200\",\n" +
                            "        \"detail\": \"满￥999可用\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                            "            \"2、适用品牌：谢琳燕窝。\",\n" +
                            "            \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                            "            \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                            "            \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                            "            \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                            "        ],\n" +
                            "            \"content\": {\n" +
                            "                \"full\": 999,\n" +
                            "                \"cut\": 200\n" +
                            "            },\n" +
                            "        \"startTime\": \"2017-02-21\",\n" +
                            "        \"endTime\": \"2017-02-24\",\n" +
                            "        \"status\": 2,\n" +
                            "        \"isChecked\": \"2\",\n" +
                            "        \"pic\": \"http://s1.healthbok.com/qrCode/5/15/f28188918b0fba9d.jpg\"\n" +
                            "    }\n" +
                            "}");
                    break;
                case 3:
                default:
                    mock = new JSONObject("{\n" +
                            "    \"success\": true,\n" +
                            "    \"msgCode\": null,\n" +
                            "    \"msgInfo\": null,\n" +
                            "    \"count\": 0,\n" +
                            "    \"model\": {\n" +
                            "        \"id\": 3,\n" +
                            "        \"title\": \"满￥999减￥200\",\n" +
                            "        \"detail\": \"满￥999可用\",\n" +
                            "        \"rule\": [\n" +
                            "            \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                            "            \"2、适用品牌：谢琳燕窝。\",\n" +
                            "            \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                            "            \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                            "            \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                            "            \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                            "        ],\n" +
                            "            \"content\": {\n" +
                            "                \"full\": 999,\n" +
                            "                \"cut\": 200\n" +
                            "            },\n" +
                            "        \"startTime\": \"2017-02-21\",\n" +
                            "        \"endTime\": \"2017-02-24\",\n" +
                            "        \"status\": 3,\n" +
                            "        \"isChecked\": \"2\",\n" +
                            "        \"pic\": \"http://s1.healthbok.com/qrCode/5/15/f28188918b0fba9d.jpg\"\n" +
                            "    }\n" +
                            "}");
                    break;
            }
            return parser().createCouponResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.searchCouponByAlias(couponAlias,couponId);
    }

    @Override
    public Result<CouponListResponse> searchMyCoupon() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"rule\": [\n" +
                    "                \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                    "                \"2、适用品牌：汤成倍健。\",\n" +
                    "                \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                    "                \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                    "                \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                    "                \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                    "            ],\n" +
                    "            \"className\": \"适用于汤成倍健\",\n" +
                    "            \"title\": \"满￥999减￥200\",\n" +
                    "            \"type\": 1,\n" +
                    "            \"content\": {\n" +
                    "                \"full\": 999,\n" +
                    "                \"cut\": 200\n" +
                    "            },\n" +
                    "            \"id\":1,\n" +
                    "            \"productName\": null,\n" +
                    "            \"startTime\": \"2017.03.21\",\n" +
                    "            \"detail\": \"满￥999可用\",\n" +
                    "            \"endTime\": \"2017.03.24\",\n" +
                    "            \"status\": 1\n" +
                    "        },{\n" +
                    "            \"rule\": [\n" +
                    "                \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                    "                \"2、适用品牌：汤成倍健。\",\n" +
                    "                \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                    "                \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                    "                \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                    "                \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                    "            ],\n" +
                    "            \"className\": \"适用于汤成倍健\",\n" +
                    "            \"title\": \"满￥299减￥50\",\n" +
                    "            \"type\": 1,\n" +
                    "            \"content\": {\n" +
                    "                \"full\": 299,\n" +
                    "                \"cut\": 50\n" +
                    "            },\n" +
                    "            \"id\":2,\n" +
                    "            \"productName\": null,\n" +
                    "            \"startTime\": \"2017.03.21\",\n" +
                    "            \"detail\": \"满￥299可用\",\n" +
                    "            \"endTime\": \"2017.03.24\",\n" +
                    "            \"status\": 1\n" +
                    "        },{\n" +
                    "            \"rule\": [\n" +
                    "                \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                    "                \"2、适用品牌：汤成倍健。\",\n" +
                    "                \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                    "                \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                    "                \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                    "                \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                    "            ],\n" +
                    "            \"className\": \"适用于汤成倍健\",\n" +
                    "            \"title\": \"满￥299减￥50\",\n" +
                    "            \"type\": 1,\n" +
                    "            \"content\": {\n" +
                    "                \"full\": 299,\n" +
                    "                \"cut\": 50\n" +
                    "            },\n" +
                    "            \"id\":3,\n" +
                    "            \"productName\": null,\n" +
                    "            \"startTime\": \"2017.03.21\",\n" +
                    "            \"detail\": \"满￥299可用\",\n" +
                    "            \"endTime\": \"2017.03.24\",\n" +
                    "            \"status\": 1\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            return parser().createCouponListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.searchMyCoupon();
    }

    @Override
    public Result<CouponAliasListResponse> searchMyCouponAlias() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"couponId\": \"36\",\n" +
                    "            \"title\": \"适用于汤成倍健\",\n" +
                    "            \"promoCode\": x240,\n" +
                    "            \"content\": {\n" +
                    "                \"full\": 999,\n" +
                    "                \"cut\": 200\n" +
                    "            },\n" +
                    "            \"status\": 1\n" +
                    "        },{\n" +
                    "\" +\n" +
                    "                    \"            \"couponId\": \"36\",\n" +
                    "                    \"            \"title\": \"适用于汤成倍健\",\\n\" +\n" +
                    "                    \"            \\\"promoCode\\\": x240,\\n\" +\n" +
                    "                    \"            \\\"content\\\": {\\n\" +\n" +
                    "                    \"                \\\"full\\\": 999,\\n\" +\n" +
                    "                    \"                \\\"cut\\\": 200\\n\" +\n" +
                    "                    \"            },\\n\" +\n" +
                    "                    \"            \\\"status\\\": 1\\n\" +\n" +
                    "                    \"        },{\n" +
                    "            \"rule\": [\n" +
                    "                \"1、一个微信号每种优惠券仅限领取一张；优惠券不可累加使用，单次消费仅限使用一张。\",\n" +
                    "                \"2、适用品牌：汤成倍健。\",\n" +
                    "                \"3、仅限在一树贵州省内门店使用，其他区域门店和线上商城不参加此次活动。\",\n" +
                    "                \"4、此优惠不与门店店内其他优惠活动同享。\",\n" +
                    "                \"5、可在“舒普玛”和“一树商城”微信公众号的“优惠券”菜单中查看已领取到的优惠券。\",\n" +
                    "                \"6、贵州一树药业有限公司保留对活动的最终解释权。\"\n" +
                    "            ],\n" +
                    "            \"className\": \"适用于汤成倍健\",\n" +
                    "            \"title\": \"满￥299减￥50\",\n" +
                    "            \"type\": 1,\n" +
                    "            \"content\": {\n" +
                    "                \"full\": 299,\n" +
                    "                \"cut\": 50\n" +
                    "            },\n" +
                    "            \"id\":3,\n" +
                    "            \"productName\": null,\n" +
                    "            \"startTime\": \"2017.03.21\",\n" +
                    "            \"detail\": \"满￥299可用\",\n" +
                    "            \"endTime\": \"2017.03.24\",\n" +
                    "            \"status\": 1\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
            return parser().createCouponAliasListResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.searchMyCouponAlias();
    }

    @Override
    public Result<CouponProductResponse> fetchProduct(long couponId,@Nullable Long userCouponId) {
        return null;
    }

    @Override
    public Result<CouponProductResponse> fetchProductByAlias(String couponAlias) {
        return null;
    }

    @Override
    public Result<BooleanResponse> logout() {
        return null;
    }

    @Override
    public Result<OrderPayResponse> orderPay(String couponId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"orderRequestId\": \"99988\"\n" +
                    "    }\n" +
                    "}");
            return parser().createOrderPayResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.orderPay(couponId);
    }

    @Override
    public Result<OrderPayResponse> orderPayByAlias(String promoCode) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"orderRequestId\": \"99988\"\n" +
                    "    }\n" +
                    "}");
            return parser().createOrderPayResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.orderPayByAlias(promoCode);
    }

    @Override
    public Result<OrderDetailResponse> orderDetail(String orderRequestId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"detail\":{  //商品信息,同优惠券详情\n" +
                    "            /*1.3新增数据*/\n" +
                    "            \"mode\":\"1\",//1.原优惠券  2.订单\n" +
                    "            \"orderId\":\"012AD98G37348\",//订单号\n" +
                    "            /***/\n" +
                    "            \"id\": 109,\n" +
                    "            \"title\": \"加¥20可换购\",\n" +
                    "            \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "            \"num\": \"加¥20可换购\",\n" +
                    "            \"rule\": [\n" +
                    "            \"1. 限好簿。\",\n" +
                    "            \"\\r\\n1. 限好簿。\"\n" +
                    "            ],\n" +
                    "            \"startTime\": \"2017-04-19\",\n" +
                    "            \"endTime\": \"2017-04-23\",\n" +
                    "            \"couponIcon\": \"http://s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\", //优惠劵icon\n" +
                    "            \"isProduct\": 0,  //是否包含商品图片\n" +
                    "            \"className\": \"适用于生活类\",   //分类\n" +
                    "            \"productName\": \"生活用品, \",   //商品\n" +
                    "            \"status\": 1,\n" +
                    "            \"isChecked\": \"1\"    \n" +
                    "            },\n" +
                    "        \"order\":{\n" +
                    "            \"currentPrice\":\"169\",//支付价格\n" +
                    "            \"totalPrice\":\"299\",//原价\n" +
                    "            \"couponCut\":\"100\",//优惠券优惠\n" +
                    "            \"promoCut\":\"30\",//优惠码优惠\n" +
                    "        },\n" +
                    "    }\n" +
                    "}");
            return parser().createOrderDetailResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.orderDetail(orderRequestId);
    }

    @Override
    public Result<OrderGenerateResponse> orderGenerateResponse(String name, String mobile, String payType, String orderRequestId,int way) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"partnerId\": \"998\",\n" +
                    "        \"prepayId\": \"998998\",\n" +
                    "        \"nonceStr\": \"iuyw990\",\n" +
                    "        \"timeStamp\": \"91230009\",\n" +
                    "        \"package\": \"AAA\",\n" +
                    "        \"sign\": \"BBB\"\n" +
                    "    }\n" +
                    "}");
            return parser().createOrderGenerateResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.orderGenerateResponse(name,mobile,payType,orderRequestId,way);
    }

    @Override
    public Result<OrderResultResponse> orderResult(String orderRequestId,String payType,String requestId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"isPaySuccess\": \"1\",\n" +
                    "        \"orderId\": \"012AD98G37348\",\n" +
                    "        \"error\": \"\",\n" +
                    "        \"detail\": {\n" +
                    "            \"mode\": \"1\",\n" +
                    "            \"orderId\": \"012AD98G37348\",\n" +
                    "            \"id\": 109,\n" +
                    "            \"title\": \"加¥20可换购\",\n" +
                    "            \"detail\": \"购5件加¥20换购价值¥50牛奶\",\n" +
                    "            \"num\": \"加¥20可换购\",\n" +
                    "            \"rule\": [\n" +
                    "                \"1. 限好簿。\",\n" +
                    "                \"\\r\\n1. 限好簿。\"\n" +
                    "            ],\n" +
                    "            \"startTime\": \"2017-04-19\",\n" +
                    "            \"endTime\": \"2017-04-23\",\n" +
                    "            \"couponIcon\": \"http://s1.healthbok.com/coupon/9/6/31d832fab9e32339.jpg\",\n" +
                    "            \"isProduct\": 0,\n" +
                    "            \"className\": \"适用于生活类\",\n" +
                    "            \"productName\": \"生活用品, \",\n" +
                    "            \"status\": 1,\n" +
                    "            \"isChecked\": \"1\"\n" +
                    "        }\n" +
                    "    }\n" +
                    "}");
            return parser().createOrderResultResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.orderResult(orderRequestId,payType,requestId);
    }

    @Override
    public Result<String> getVerifyCode(String mobile, @SignDef String sign) {
        return null;
    }

    @Override
    public Result<IntegralListResponse> fetchIntegral() {
        return null;
    }

    @Override
    public Result<BooleanResponse> cutIntegral(long userCouponId) {
        return null;
    }

    @Override
    public Result<CouponListResponse> fetchRecomCoupon() {
        return null;
    }

    @Override
    public Result<CouponTypeListResponse> getCouponType() {
        return null;
    }

    @Override
    public Result<BooleanResponse> getRecommendNotice() {
        return null;
    }

    @Override
    public Result<BooleanResponse> updateRecommend() {
        return null;
    }

    @Override
    public Result<BooleanResponse> postRealAvatar(Long uid, String url, Long operationUid, Long vendorId) {
        return null;
    }

    @Override
    public Result<BooleanResponse> createInvite(Long fromUid, int deviceType, String deviceUuid, Long receiveUid) {
        return null;
    }
}
