package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceContextResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceDayResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceStatusResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  16/12/05.
 */

public interface AttendanceResources {

    /**
     * 检测合作商是否开启考勤任务
     *
     * @return 考勤任务是否开启
     */
    Result<BooleanResponse> preference();

    /**
     * 查询某月考勤记录，此处返回的数据足够客户端生成日历。点击日历上的某一天的具体考勤记录，需再次请求“某日的考勤记录”接口。
     *
     * @param year  年
     * @param month 月
     * @return 查询指定yyyy-MM的考勤详情
     */
    Result<AttendanceMonthResponse> month(int year, int month);


    /**
     * 查询某天的考勤记录，返回该天的详细记录。若给定的【日期】是未来的时间，则给出相应的错误提示。
     *
     * @param year  年
     * @param month 月
     * @param day   日
     * @return 查询指定yyyy-MM-dd的考勤详情
     */
    Result<AttendanceDayResponse> day(int year, int month, int day);


    /**
     * 仅限于获取当日的考勤上下文，注意与接口`pd.attendance.day`加以区分。
     *
     * @return 查询今日的考勤任务
     */
    Result<AttendanceContextResponse> today();

    /**
     * 申请签卡
     *
     * @param type             签卡类型（签到、签退）
     * @param longitude        经度
     * @param latitude         纬度
     * @param deviceMacAddress 设备的mac地址
     * @param content          签卡补充说明
     * @return 签卡是否成功
     */
    Result<BooleanResponse> apply(int type, double longitude, double latitude, String deviceMacAddress, String content);

    /**
     * 使用当下的经纬度查询签卡状态（注意：只查询状态不执行签卡）
     *
     * @param type      签卡类型（签到、签退）
     * @param longitude 经度
     * @param latitude  纬度
     * @return 查询当前时间下，签卡状态查询
     */
    Result<AttendanceStatusResponse> status(int type, double longitude, double latitude);
}
