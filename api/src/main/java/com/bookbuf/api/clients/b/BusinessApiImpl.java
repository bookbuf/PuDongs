package com.bookbuf.api.clients.b;

import com.bookbuf.api.apis.AchievementAPI;
import com.bookbuf.api.apis.AttendanceAPI;
import com.bookbuf.api.apis.CallAPI;
import com.bookbuf.api.apis.ClerkAPI;
import com.bookbuf.api.apis.CouponAPI;
import com.bookbuf.api.apis.DiscoverAPI;
import com.bookbuf.api.apis.ExamAPI;
import com.bookbuf.api.apis.ExamPaperAPI;
import com.bookbuf.api.apis.GlobalAPI;
import com.bookbuf.api.apis.MeasureAPI;
import com.bookbuf.api.apis.MedicalRegisterAPI;
import com.bookbuf.api.apis.MessageAPI;
import com.bookbuf.api.apis.MonitorAPI;
import com.bookbuf.api.apis.QuestionAPI;
import com.bookbuf.api.apis.TaskAPI;
import com.bookbuf.api.apis.TrainAPI;
import com.bookbuf.api.clients.ApiClubImpl;
import com.bookbuf.api.clients.resources.APIResources;
import com.bookbuf.api.clients.resources.impl.AchievementResources;
import com.bookbuf.api.clients.resources.impl.AttendanceResources;
import com.bookbuf.api.clients.resources.impl.CallResources;
import com.bookbuf.api.clients.resources.impl.ClerkResources;
import com.bookbuf.api.clients.resources.impl.CouponResources;
import com.bookbuf.api.clients.resources.impl.DiscoverResources;
import com.bookbuf.api.clients.resources.impl.ExamPaperResources;
import com.bookbuf.api.clients.resources.impl.ExamResources;
import com.bookbuf.api.clients.resources.impl.GlobalResources;
import com.bookbuf.api.clients.resources.impl.HeWeatherResources;
import com.bookbuf.api.clients.resources.impl.MeasureResources;
import com.bookbuf.api.clients.resources.impl.MedicalRegisterResources;
import com.bookbuf.api.clients.resources.impl.MessageResources;
import com.bookbuf.api.clients.resources.impl.QuestionResources;
import com.bookbuf.api.clients.resources.impl.TaskResources;
import com.bookbuf.api.clients.resources.impl.TrainResources;
import com.bookbuf.api.configurations.IConfiguration;
import com.bookbuf.api.responses.impl.BannerResponse;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceContextResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceDayResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceStatusResponse;
import com.bookbuf.api.responses.impl.call.CallCancelResponse;
import com.bookbuf.api.responses.impl.call.CallResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponNewResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.bookbuf.api.responses.impl.customer.BindCardListResponse;
import com.bookbuf.api.responses.impl.customer.CreateUserResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperWithExamResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.bookbuf.api.responses.impl.examination.FetchDiscoverQuestionResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.bookbuf.api.responses.impl.global.SplashScreenResponse;
import com.bookbuf.api.responses.impl.measure.add.AddRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.LatestMeasureResponse;
import com.bookbuf.api.responses.impl.measure.list.ListRecordResponse;
import com.bookbuf.api.responses.impl.medical.DepartmentResponse;
import com.bookbuf.api.responses.impl.medical.DoctorResponse;
import com.bookbuf.api.responses.impl.medical.DoctorScheduleResponse;
import com.bookbuf.api.responses.impl.medical.HospitalResponse;
import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.bookbuf.api.responses.impl.message.MessageResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.bookbuf.api.responses.impl.user.ClerkResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.FetchClerkProfileResponse;
import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.ipudong.core.Result;
import com.ipudong.core.network.response.impl.HealthbokResponse;

import org.json.JSONObject;

import java.util.List;

/**
 * author: robert.
 * date :  16/8/2.
 */
public class BusinessApiImpl extends ApiClubImpl implements BusinessApi {

    public BusinessApiImpl(IConfiguration configuration) {
        super(configuration);
    }

    @Override
    public CouponResources.Business couponResources() {
        return this;
    }

    @Override
    public AchievementResources achievementResources() {
        return this;
    }

    @Override
    public TrainResources trainResources() {
        return this;
    }

    @Override
    public ClerkResources clerkResources() {
        return this;
    }

    @Override
    public MedicalRegisterResources medicalRegisterResources() {
        return this;
    }

    @Override
    public AttendanceResources attendanceResource() {
        return this;
    }

    @Override
    public TaskResources taskResources() {
        return this;
    }

    @Override
    public QuestionResources questionResources() {
        return this;
    }

    @Override
    public ExamPaperResources examPaperResources() {
        return this;
    }

    @Override
    public ExamResources examResources() {
        return this;
    }

    @Override
    public MessageResources messageResources() {
        return this;
    }

    @Override
    public GlobalResources.Business globalResources() {
        return this;
    }

    @Override
    public GlobalResources globalAllResources() {
        return this;
    }

    @Override
    public HeWeatherResources heWeatherResources() {
        return this;
    }

    @Override
    public MeasureResources measureResources() {
        return this;
    }

    @Override
    public CallResources callResources() {
        return this;
    }

    @Override
    public DiscoverResources discoverResources() {
        return this;
    }



    @Override
    public Result<AppUpgradeResponse> fetchAppUpgrade(String appName) {
        HealthbokResponse response = api(GlobalAPI.class).fetchAppUpgrade(
                getAppSecret(),
                APIResources.Global.CLERK_CHECK_UPGRADE.method(),
                APIResources.Global.CLERK_CHECK_UPGRADE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                appName);
        return parser().createAppUpgradeResponse(response.getResponse());
    }

    @Override
    public Result<LongResponse> checkUserByQuery(String mobile) {
        HealthbokResponse response = api(GlobalAPI.class).checkUserByQuery(
                getAppSecret(),
                APIResources.Global.CHECK_USER_BY_QUERY.method(),
                APIResources.Global.CHECK_USER_BY_QUERY.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile);
        return parser().createCheckUserByQueryResponse(response.getResponse());
    }

    public Result<CredentialResponse> login(String target, String password, String pushId) {
        HealthbokResponse response = api(ClerkAPI.class).loginTest(
                getAppSecret(),
                APIResources.Global.CLERK_LOGIN.method(),
                APIResources.Global.CLERK_LOGIN.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                target,
                password,
                pushId,
                "close");
        return parser().createCredentialResponse(response.getResponse());
    }

    @Override
    public Result<CustomerResponse> queryCustomer(String query, String type) {
        HealthbokResponse response = api(ClerkAPI.class).loginCustomer(
                getAppSecret(),
                APIResources.B.CLERK_QUERY_CUSTOMER.method(),
                APIResources.B.CLERK_QUERY_CUSTOMER.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                query,
                type);
        return parser().createCustomerResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> resetPassword(String mobile, String password, String verifyCode, String sign) {
        HealthbokResponse response = api(ClerkAPI.class).resetPassword(
                getAppSecret(),
                APIResources.Global.CLERK_RESET_PASSWORD.method(),
                APIResources.Global.CLERK_RESET_PASSWORD.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                password,
                verifyCode,
                sign);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> logout() {
        HealthbokResponse response = api(ClerkAPI.class).logout(
                getAppSecret(),
                APIResources.B.CLERK_LOGOUT.method(),
                APIResources.B.CLERK_LOGOUT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> register(String mobile, String idCard, String realname, String passwd) {
        HealthbokResponse response = api(ClerkAPI.class).register(
                getAppSecret(),
                APIResources.B.CLERK_REGISTER.method(),
                APIResources.B.CLERK_REGISTER.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                idCard,
                realname,
                passwd);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> updatePassword(String oldPwd, String newPwd) {
        HealthbokResponse response = api(ClerkAPI.class).updatePassword(
                getAppSecret(),
                APIResources.B.CLERK_UPDATE_PASSWORD.method(),
                APIResources.B.CLERK_UPDATE_PASSWORD.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                oldPwd,
                newPwd);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> updateCustomerProfileBasic(long userId, String realName, String mobile, String idCard, String birthday,String gender, String telephone, String address) {
        HealthbokResponse response = api(ClerkAPI.class).updateCustomerProfileBasic(
                getAppSecret(),
                APIResources.B.CLERK_UPDATE_PROFILE.method(),
                APIResources.B.CLERK_UPDATE_PROFILE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userId,
                realName,
                mobile,
                idCard,
                birthday,
                gender,
                telephone,
                address
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> updateProfileBasic(long userId, String realName, String mobile, String idCard, String birthday, String telephone, String address) {
        HealthbokResponse response = api(ClerkAPI.class).updateProfileBasic(
                getAppSecret(),
                APIResources.B.CLERK_UPDATE_PROFILE.method(),
                APIResources.B.CLERK_UPDATE_PROFILE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userId,
                realName,
                mobile,
                idCard,
                birthday,
                telephone,
                address
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<TaskContextResponse> fetchTaskList() {
        HealthbokResponse response = api(TaskAPI.class).fetchTaskList(
                getAppSecret(),
                APIResources.B.TASK_CONTEXT.method(),
                APIResources.B.TASK_CONTEXT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createTaskResponseList(response.getResponse());
    }

    @Override
    public Result<QuestionResponse> fetchQuestion(long articleId) {
        HealthbokResponse response = api(QuestionAPI.class).fetchQuestion(
                getAppSecret(),
                APIResources.B.QUESTION_FETCH.method(),
                APIResources.B.QUESTION_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                articleId);
        return parser().createQuestionResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> answerQuestion(long questionId, String answers) {
        HealthbokResponse response = api(QuestionAPI.class).answerQuestion(
                getAppSecret(),
                APIResources.B.QUESTION_ANSWER.method(),
                APIResources.B.QUESTION_ANSWER.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                questionId,
                answers);
        return parser().createQuestionAnswerBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> answerQuestionDiscovery(long questionId, String answers) {
        HealthbokResponse response = api(DiscoverAPI.class).answerQuestionDiscovery(
                getAppSecret(),
                APIResources.B.DISCOVERY_QUESTION_SUBMIT.method(),
                APIResources.B.DISCOVERY_QUESTION_SUBMIT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                questionId,
                answers);
        return parser().createCreateExaminationRecordResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> storeArticle(long id) {
        HealthbokResponse response = api(DiscoverAPI.class).storeArticle(
                getAppSecret(),
                APIResources.B.DISCOVERY_STORE_ARTICLE.method(),
                APIResources.B.DISCOVERY_STORE_ARTICLE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                id);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<ExamPaperResponse> fetchExamPaper(long paperId) {
        HealthbokResponse response = api(ExamPaperAPI.class).fetchExamPaper(
                getAppSecret(),
                APIResources.B.PAPER_FETCH.method(),
                APIResources.B.PAPER_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                paperId);
        return parser().createExamPaperResponse(response.getResponse());
    }

    @Override
    public Result<List<ExamPaperWithExamResponse>> fetchExamPaperWithExam() {
        HealthbokResponse response = api(ExamPaperAPI.class).fetchExamPaperWithExam(
                getAppSecret(),
                APIResources.B.PAPER_FETCH_WITH_EXAM.method(),
                APIResources.B.PAPER_FETCH_WITH_EXAM.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createExamPaperWithExamResponseList(response.getResponse());
    }

    @Override
    public Result<ExamResponse> beginExam(long paperId) {
        HealthbokResponse response = api(ExamAPI.class).beginExam(
                getAppSecret(),
                APIResources.B.EXAM_BEGIN.method(),
                APIResources.B.EXAM_BEGIN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                paperId);
        return parser().createExamResponse(response.getResponse());
    }

    @Override
    public Result<ExamResponse> getExamStatus(long examId) {
        HealthbokResponse response = api(ExamAPI.class).getExamStatus(
                getAppSecret(),
                APIResources.B.EXAM_STATUS.method(),
                APIResources.B.EXAM_STATUS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                examId);
        return parser().createExamResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> terminateExam(long examId) {
        HealthbokResponse response = api(ExamAPI.class).terminateExam(
                getAppSecret(),
                APIResources.B.EXAM_TERMINATE.method(),
                APIResources.B.EXAM_TERMINATE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                examId);
        return parser().createExamTerminateBooleanResponse(response.getResponse());
    }

    @Override
    public Result<ExamResponse> handleInExam(long examId, String answers) {
        HealthbokResponse response = api(ExamAPI.class).handleInExam(
                getAppSecret(),
                APIResources.B.EXAM_HANDLE_IN.method(),
                APIResources.B.EXAM_HANDLE_IN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                examId,
                answers,
                null,
                null);
        return parser().createExamResponse(response.getResponse());
    }

    @Override
    public Result<ExamResponse> handleInExam(long examId, String answers, long courseId) {
        HealthbokResponse response = api(ExamAPI.class).handleInExam(
                getAppSecret(),
                APIResources.B.EXAM_HANDLE_IN.method(),
                APIResources.B.EXAM_HANDLE_IN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                examId,
                answers,
                courseId,
                null);
        return parser().createExamResponse(response.getResponse());
    }

    @Override
    public Result<ExamResponse> handleInExam(long examId, String answers, long courseId, long levelId) {
        HealthbokResponse response = api(ExamAPI.class).handleInExam(
                getAppSecret(),
                APIResources.B.EXAM_HANDLE_IN.method(),
                APIResources.B.EXAM_HANDLE_IN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                examId,
                answers,
                courseId,
                levelId);
        return parser().createExamResponse(response.getResponse());
    }


    @Override
    public Result<ExamResponse> fetchExamHistory(long examId) {
        HealthbokResponse response = api(ExamAPI.class).fetchExamHistory(
                getAppSecret(),
                APIResources.B.EXAM_HISTORY.method(),
                APIResources.B.EXAM_HISTORY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                examId);
        return parser().createExamResponse(response.getResponse());
    }

    @Override
    public Result<List<MessageResponse>> fetchSystemMessages() {
        HealthbokResponse response = api(MessageAPI.class).fetchSystemMessages(
                getAppSecret(),
                APIResources.B.MESSAGE_SYSTEM_LIST.method(),
                APIResources.B.MESSAGE_SYSTEM_LIST.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createMessageListResponse(response.getResponse());
    }

    @Override
    public Result<MessageResponse> fetchDetailMessage(long messageId) {
        HealthbokResponse response = api(MessageAPI.class).fetchDetailMessage(
                getAppSecret(),
                APIResources.B.MESSAGE_DETAIL.method(),
                APIResources.B.MESSAGE_DETAIL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                messageId);
        return parser().createMessageResponse(response.getResponse());
    }

    @Override
    public Result<LongResponse> fetchMessageCenter() {
        HealthbokResponse response = api(MessageAPI.class).fetchMessageCenter(
                getAppSecret(),
                APIResources.B.MESSAGE_DISTRIBUTION.method(),
                APIResources.B.MESSAGE_DISTRIBUTION.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createFetchNewsMessageResponse(response.getResponse());
    }

    @Override
    public Result<WeatherResponse> fetchWeather(String city) {
        HealthbokResponse response = heWeatherAPI().weather(city, "8dc6b44697564543a932d8a06861bb60");
        return parser().createWeatherResponse(response.getResponse());
    }

    @Override
    public Result<MedicalRegisterResponse> sendSMS(long id) {
        HealthbokResponse response = api(MedicalRegisterAPI.class).sendSMS(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_MESSAGE_SEND.method(),
                APIResources.B.MEDICAL_REGISTER_MESSAGE_SEND.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                id);
        return parser().createMedicalRegisterResponse(response.getResponse());
    }

    @Override
    public Result<MedicalRegisterResponse> bookService(long hospitalId, int lockQueueNo, String scheduleItemCode, long userId) {
        HealthbokResponse response = api(MedicalRegisterAPI.class).bookService(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_BOOK_SERVICE.method(),
                APIResources.B.MEDICAL_REGISTER_BOOK_SERVICE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                hospitalId,
                lockQueueNo,
                scheduleItemCode,
                userId
        );
        return parser().createMedicalRegisterResponse(response.getResponse());
    }

    @Override
    public Result<List<DoctorScheduleResponse>> listDoctorSchedules(long hospitalId, int departmentCode, int doctorCode) {
        HealthbokResponse response = api(MedicalRegisterAPI.class).queryAdmSchedule(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_QUERY_SCHEDULE.method(),
                APIResources.B.MEDICAL_REGISTER_QUERY_SCHEDULE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                hospitalId,
                departmentCode,
                doctorCode
        );
        return parser().createDoctorScheduleResponseList(response.getResponse());
    }

    @Override
    public Result<List<MedicalRegisterResponse>> listMedicalRegisters(String mobile) {
        HealthbokResponse response = api(MedicalRegisterAPI.class).search(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_SEARCH.method(),
                APIResources.B.MEDICAL_REGISTER_SEARCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile
        );
        return parser().createMedicalRegisterResponseList(response.getResponse());
    }

    @Override
    public Result<List<HospitalResponse>> listHospitals() {
        HealthbokResponse response = api(MedicalRegisterAPI.class).listHospital(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_LIST_HOSPITAL.method(),
                APIResources.B.MEDICAL_REGISTER_LIST_HOSPITAL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createHospitalResponseList(response.getResponse());
    }

    @Override
    public Result<List<DoctorResponse>> listDoctors(long hospitalId, long departmentCode) {
        HealthbokResponse response = api(MedicalRegisterAPI.class).queryDoctorGroup(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_QUERY_DOCTOR_GROUP.method(),
                APIResources.B.MEDICAL_REGISTER_QUERY_DOCTOR_GROUP.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                hospitalId,
                departmentCode
        );
        return parser().createDoctorResponseList(response.getResponse());
    }

    @Override
    public Result<List<DepartmentResponse>> listDepartments(long hospitalId) {
        HealthbokResponse response = api(MedicalRegisterAPI.class).queryDepartmentGroup(
                getAppSecret(),
                APIResources.B.MEDICAL_REGISTER_QUERY_DEPARTMENT_GROUP.method(),
                APIResources.B.MEDICAL_REGISTER_QUERY_DEPARTMENT_GROUP.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                hospitalId
        );
        return parser().createDepartmentResponseList(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> sendNotice(String mobile, String sign ,String type) {
        HealthbokResponse response = api(GlobalAPI.class).sendNotice(
                getAppSecret(),
                APIResources.Global.SEND_NOTICE.method(),
                APIResources.Global.SEND_NOTICE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign,
                type);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> sendVerifyCode(String mobile, @SignDef String sign) {
        HealthbokResponse response = api(GlobalAPI.class).sendVerifyCode(
                getAppSecret(),
                APIResources.Global.SEND_VERIFY_CODE.method(),
                APIResources.Global.SEND_VERIFY_CODE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> sendClerkVerifyCode(String mobile, String sign) {
        HealthbokResponse response = api(GlobalAPI.class).sendClerkVerifyCode(
                getAppSecret(),
                APIResources.B.SEND_CLERK_VERIFY_CODE.method(),
                APIResources.B.SEND_CLERK_VERIFY_CODE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign);
        return parser().createBooleanResponse(response.getResponse());
    }


    /**
     * 绑定店长
     *
     * @param mobile
     * @param clerk_num
     * @param code
     * @return
     */
    @Override
    public Result<BooleanResponse> bindVendor(String mobile, String clerk_num, String code) {
        HealthbokResponse response = api(ClerkAPI.class).bindVendor(
                getAppSecret(),
                APIResources.B.CLERK_BIND_VENDOR.method(),
                APIResources.B.CLERK_BIND_VENDOR.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                clerk_num,
                code
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<RoleResponse> queryRole(String mobile) {
        HealthbokResponse response = api(GlobalAPI.class).queryRole(
                getAppSecret(),
                APIResources.B.CHECK_QUERY_ROLE.method(),
                APIResources.B.CHECK_QUERY_ROLE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile);
        return parser().createRoleResponse(response.getResponse());
    }

    @Override
    public Result<SplashScreenResponse> querySplashScreen() {
        HealthbokResponse response = api(GlobalAPI.class).querySplashScreen(
                getAppSecret(),
                APIResources.Global.GLOBAL_SPLASH_SCREEN.method(),
                APIResources.Global.GLOBAL_SPLASH_SCREEN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createSplashScreenResponse(response.getResponse());
    }

    @Override
    public Result<CallCancelResponse> callCancel(String sid, int type) {
        HealthbokResponse response = api(CallAPI.class).callCancel(
                getAppSecret(),
                APIResources.B.CALL_CALL_CANCEL.method(),
                APIResources.B.CALL_CALL_CANCEL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                sid,
                type
        );
        return parser().createCallCancelResponse(response.getResponse());
    }

    @Override
    public Result<CallResponse> call(String mobile) {
        HealthbokResponse response = api(CallAPI.class).call(
                getAppSecret(),
                APIResources.B.CALL_CALL.method(),
                APIResources.B.CALL_CALL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile
        );
        return parser().createCallResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> cancelStoreArticle(long id) {
        HealthbokResponse response = api(DiscoverAPI.class).cancel(
                getAppSecret(),
                APIResources.B.DISCOVERY_CANCEL_FAVORITE.method(),
                APIResources.B.DISCOVERY_CANCEL_FAVORITE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                id
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> checkVerifyCode(String mobile, String sign, String code) {
        HealthbokResponse response = api(GlobalAPI.class).checkVerifyCode(
                getAppSecret(),
                APIResources.Global.CHECK_VERIFY_CODE.method(),
                APIResources.Global.CHECK_VERIFY_CODE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign,
                code
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<ClerkResponse> queryClerkProfile() {
        HealthbokResponse response = api(ClerkAPI.class).queryProfile(
                getAppSecret(),
                APIResources.B.CLERK_QUERY_PROFILE.method(),
                APIResources.B.CLERK_QUERY_PROFILE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                "close"
        );
        return parser().createClerkResponse(response.getResponse());
    }

    @Override
    public Result<FetchClerkProfileResponse> queryClerkProfile_2017_01_22() {
        HealthbokResponse response = api(ClerkAPI.class).queryProfile(
                getAppSecret(),
                APIResources.B.CLERK_QUERY_PROFILE.method(),
                APIResources.B.CLERK_QUERY_PROFILE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                "close"
        );
        return parser().createFetchClerkProfileResponse(response.getResponse());
    }

    @Override
    public Result<CustomerResponse> queryCustomerProfile(long userId) {
        HealthbokResponse response = api(ClerkAPI.class).queryCustomerProfile(
                getAppSecret(),
                APIResources.B.CLERK_QUERY_CUSTOMER_PROFILE.method(),
                APIResources.B.CLERK_QUERY_CUSTOMER_PROFILE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userId
        );
        return parser().createCustomerResponse(response.getResponse());
    }

    @Override
    public Result<BannerResponse> fetchBannerConfiguration() {
        HealthbokResponse response = api(ClerkAPI.class).fetchBannerConfiguration(
                getAppSecret(),
                APIResources.B.CLERK_FETCH_BANNER.method(),
                APIResources.B.CLERK_FETCH_BANNER.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createBannerResponse(response.getResponse());
    }

    @Override
    public Result<LongResponse> createCustomer(String vendorMemberId, String mobile, String phone, String realname, String idCard, String detailAddress) {
        HealthbokResponse response = api(ClerkAPI.class).createCustomer(
                getAppSecret(),
                APIResources.B.CREATE_CUSTOMER.method(),
                APIResources.B.CREATE_CUSTOMER.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                vendorMemberId,
                mobile,
                phone,
                realname,
                idCard,
                detailAddress
        );
        return parser().createLongResponse(response.getResponse());
    }

    @Override
    public Result<FetchDiscoverQuestionResponse> fetchDiscoverQuestion(long id) {
        HealthbokResponse response = api(DiscoverAPI.class).fetchDiscoverQuestion(
                getAppSecret(),
                APIResources.B.DISCOVERY_FETCH_QUESTION.method(),
                APIResources.B.DISCOVERY_FETCH_QUESTION.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                id
        );
        return parser().createFetchDiscoverResponse(response.getResponse());
    }

    @Override
    public Result<AddRecordResponse> addMeasureRecord(long userId, String detectionCode, String values, double longitude, double latitude, double elevation) {
        HealthbokResponse response = api(MeasureAPI.class).addIndicator(
                getAppSecret(),
                APIResources.B.MEASURE_ADD_RECORD.method(),
                APIResources.B.MEASURE_ADD_RECORD.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userId,
                detectionCode,
                values,
                longitude,
                latitude,
                elevation
        );
        return parser().createAddRecordResponse(response.getResponse());
    }

    @Override
    public Result<LatestMeasureResponse> listLatestMeasureRecord(long userId) {
        HealthbokResponse response = api(MeasureAPI.class).listLatestRecord(
                getAppSecret(),
                APIResources.B.MEASURE_LATEST_FOR_B.method(),
                APIResources.B.MEASURE_LATEST_FOR_B.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userId
        );
        return parser().createLatestMeasureResponse(response.getResponse());
    }

    @Override
    public Result<ListRecordResponse> listMeasureRecord(long userId, String typeCode, int page, int pageSize) {
        HealthbokResponse response = api(MeasureAPI.class).listRecord(
                getAppSecret(),
                APIResources.B.MEASURE_LIST_RECORD.method(),
                APIResources.B.MEASURE_LIST_RECORD.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userId,
                typeCode,
                page,
                pageSize
        );
        return parser().createListRecordResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> preference() {
        HealthbokResponse response = api(AttendanceAPI.class).preference(
                getAppSecret(),
                APIResources.B.ATTENDANCE_PREFERENCE.method(),
                APIResources.B.ATTENDANCE_PREFERENCE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<AttendanceMonthResponse> month(int year, int month) {
        HealthbokResponse response = api(AttendanceAPI.class).month(
                getAppSecret(),
                APIResources.B.ATTENDANCE_MONTH.method(),
                APIResources.B.ATTENDANCE_MONTH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                yyyyMM(year, month)
        );
        return parser().createAttendanceMonthResponse(response.getResponse());
    }

    private String yyyyMM(int year, int month) {
        StringBuilder builder = new StringBuilder();
        builder.append(year);
        builder.append("-");
        if (month < 10) {
            builder.append("0");
            builder.append(month);
        } else {
            builder.append(month);
        }
        return builder.toString();
    }

    private String yyyyMMdd(int year, int month, int day) {
        StringBuilder builder = new StringBuilder();
        builder.append(year);
        builder.append("-");
        if (month < 10) {
            builder.append("0");
            builder.append(month);
        } else {
            builder.append(month);
        }
        builder.append("-");
        if (day < 10) {
            builder.append("0");
            builder.append(day);
        } else {
            builder.append(day);
        }
        return builder.toString();
    }

    @Override
    public Result<AttendanceDayResponse> day(int year, int month, int day) {
        HealthbokResponse response = api(AttendanceAPI.class).day(
                getAppSecret(),
                APIResources.B.ATTENDANCE_DAY.method(),
                APIResources.B.ATTENDANCE_DAY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                yyyyMMdd(year, month, day)
        );
        return parser().createAttendanceDayResponse(response.getResponse());
    }

    @Override
    public Result<AttendanceContextResponse> today() {
        HealthbokResponse response = api(AttendanceAPI.class).context(
                getAppSecret(),
                APIResources.B.ATTENDANCE_CONTEXT.method(),
                APIResources.B.ATTENDANCE_CONTEXT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createAttendanceContextResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> apply(int type, double longitude, double latitude, String deviceMacAddress, String content) {
        HealthbokResponse response = api(AttendanceAPI.class).apply(
                getAppSecret(),
                APIResources.B.ATTENDANCE_APPLY.method(),
                APIResources.B.ATTENDANCE_APPLY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                type,
                longitude,
                latitude,
                deviceMacAddress,
                content
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<AttendanceStatusResponse> status(int type, double longitude, double latitude) {
        HealthbokResponse response = api(AttendanceAPI.class).status(
                getAppSecret(),
                APIResources.B.ATTENDANCE_STATUS.method(),
                APIResources.B.ATTENDANCE_STATUS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                type,
                longitude,
                latitude
        );
        return parser().createAttendanceStatusResponse(response.getResponse());
    }

    @Override
    public Result<FetchCareerMapResponse> fetchCareerMap() {

        HealthbokResponse response = api(AchievementAPI.class).fetch(
                getAppSecret(),
                APIResources.B.ACHIEVEMENT_FETCH.method(),
                APIResources.B.ACHIEVEMENT_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                1
        );
        return parser().createCareerMapResponse(response.getResponse());
    }

    @Override
    public Result<FetchAchievementDetailResponse> fetchAchievement(long achievementId) {

        HealthbokResponse response = api(AchievementAPI.class).detail(
                getAppSecret(),
                APIResources.B.ACHIEVEMENT_DETAIL.method(),
                APIResources.B.ACHIEVEMENT_DETAIL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                achievementId
        );
        return parser().createAchievementResponse(response.getResponse());
    }

    @Override
    public Result<FetchTrainResponse> fetchTrain(long courseId, long levelId) {
        HealthbokResponse response = api(TrainAPI.class).fetchTrain(
                getAppSecret(),
                APIResources.B.TRAIN_DETAIL.method(),
                APIResources.B.TRAIN_DETAIL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                courseId,
                levelId
        );
        return parser().createFetchTrainResponse(response.getResponse());
    }

    @Override
    public Result<FetchTrainResponse> fetchTrain(long courseId) {
        HealthbokResponse response = api(TrainAPI.class).fetchTrain(
                getAppSecret(),
                APIResources.B.TRAIN_DETAIL.method(),
                APIResources.B.TRAIN_DETAIL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                courseId,
                null
        );
        return parser().createFetchTrainResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> signUpTrain(long courseId) {
        HealthbokResponse response = api(TrainAPI.class).signUpTrain(
                getAppSecret(),
                APIResources.B.TRAIN_SIGN_UP.method(),
                APIResources.B.TRAIN_SIGN_UP.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                courseId
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<FetchSignUpCourseResponse> fetchSignUpCourse() {

        HealthbokResponse response = api(TrainAPI.class).fetchCourse(
                getAppSecret(),
                APIResources.B.COURSE_FETCH.method(),
                APIResources.B.COURSE_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                2 /*type = 2 表示已报名的课程*/
        );
        return parser().createApplyCourseResponse(response.getResponse());
    }

    @Override
    public Result<FetchLessonResponse> fetchLesson(long courseId, long levelId) {

        HealthbokResponse response = api(TrainAPI.class).fetchLesson(
                getAppSecret(),
                APIResources.B.LESSON_FETCH.method(),
                APIResources.B.LESSON_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                courseId,
                levelId
        );
        return parser().createLessonResponse(response.getResponse());
    }

    @Override
    public Result<FetchLessonResponse> fetchLesson(long courseId) {

        HealthbokResponse response = api(TrainAPI.class).fetchLesson(
                getAppSecret(),
                APIResources.B.LESSON_FETCH.method(),
                APIResources.B.LESSON_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                courseId,
                null
        );
        return parser().createLessonResponse(response.getResponse());
    }

    @Override
    public Result<VerifyCouponResponse> verifyCoupon(String coupon) {
        HealthbokResponse response = api(CouponAPI.class).verify(
                getAppSecret(),
                APIResources.B.COUPON_VERIFY.method(),
                APIResources.B.COUPON_VERIFY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                coupon
        );
        return parser().createVerifyCouponResponse(response.getResponse());
    }


    @Override
    public Result<FetchCouponStatusResponse> fetchCoupon(String couponNum) {
        HealthbokResponse response = api(CouponAPI.class).fetchCouponForB(
                getAppSecret(),
                APIResources.B.COUPON_STATUS.method(),
                APIResources.B.COUPON_STATUS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponNum
        );
        return parser().createFetchCouponStatusResponse(response.getResponse());
    }

    @Override
    public Result<CouponAliasResponse> fetchCouponAlias(long couponId) {
        HealthbokResponse response = api(CouponAPI.class).fetchCouponAlias(
                getAppSecret(),
                APIResources.B.COUPON_FETCH_COUPON_ALIAS.method(),
                APIResources.B.COUPON_FETCH_COUPON_ALIAS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponId
        );
        return parser().createCouponAliasResponse(response.getResponse());
    }

    @Override
    public Result<CouponAliasListResponse> searchCouponCodeList() {
        HealthbokResponse response = api(CouponAPI.class).searchCouponCodeList(
                getAppSecret(),
                APIResources.B.COUPON_CODE_LIST.method(),
                APIResources.B.COUPON_CODE_LIST.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createCouponAliasListResponse(response.getResponse());
    }

    /**
     * debug 模式下获取验证码
     * @param mobile
     * @param sign
     * @return
     */
    @Override
    public Result<String> getVerifyCode(String mobile, String sign) {
        HealthbokResponse response = api(GlobalAPI.class).getVerifyCode(
                getAppSecret(),
                APIResources.Global.GET_VERIFY_CODE.method(),
                APIResources.Global.GET_VERIFY_CODE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign
        );
        return parser().createStringResponse(response.getResponse());
    }

    @Override
    public Result<PermissionResponse> getPermission() {
        HealthbokResponse response = api(ClerkAPI.class).clerkPermission(
                getAppSecret(),
                APIResources.B.CLERK_GET_PERMISSION.method(),
                APIResources.B.CLERK_GET_PERMISSION.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createPermissionResponse(response.getResponse());
    }

    @Override
    public Result<CustomerLoginResponse> userLogin(String mobile,String code,String pushId) {
        HealthbokResponse response = api(com.bookbuf.api.apis.ClerkAPI.class).userLogin(
                getAppSecret(),
                APIResources.B.USER_LOGIN.method(),
                APIResources.B.USER_LOGIN.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                code,
                pushId
        );
        return parser().createCustomerLoginResponse(response.getResponse());
    }

    @Override
    public Result<BindCardListResponse> bindCard(String uid, String vendorId, String vipcard) {
        HealthbokResponse response = api(com.bookbuf.api.apis.ClerkAPI.class).bindCard(
                getAppSecret(),
                APIResources.B.CARD_BIND.method(),
                APIResources.B.CARD_BIND.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                uid,
                vendorId,
                vipcard
        );
        return parser().createBindCardListResponse(response.getResponse());
    }

    @Override
    public Result<CreateUserResponse> createUser(String name, String mobile, String telephone, String idCard, String address, String birthday, String gender, String verifiedUrl) {
        HealthbokResponse response = api(com.bookbuf.api.apis.GlobalAPI.class).createUser(
                getAppSecret(),
                APIResources.Global.CREATE_USER.method(),
                APIResources.Global.CREATE_USER.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                name,
                mobile,
                telephone,
                idCard,
                address,
                birthday,
                gender,
                verifiedUrl
        );
        return parser().createUserResponse(response.getResponse());
    }

    @Override
    public Result<MonitorCountResponse> monitorSelectCount(String jsonObject) {
        HealthbokResponse response = api(MonitorAPI.class).selectCount(jsonObject);
        return parser().createMonitorResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> postRealAvatar(Long uid,String url,Long operationUid,Long vendorId) {
        HealthbokResponse response = api(GlobalAPI.class).postRealAvatar(
                getAppSecret(),
                APIResources.Global.GLOBAL_POST_REAL_AVATAR.method(),
                APIResources.Global.GLOBAL_POST_REAL_AVATAR.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                url,
                uid,
                operationUid,
                vendorId);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<CouponNewResponse> getVerfiedCoupon(Long vendorId,Long uid) {
        HealthbokResponse response = api(CouponAPI.class).getVerifiedCoupon(
                getAppSecret(),
                APIResources.B.GET_VERIFIED_COUPON.method(),
                APIResources.B.GET_VERIFIED_COUPON.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                vendorId,
                uid);
        return parser().createCouponNewResponse(response.getResponse());
    }
}
