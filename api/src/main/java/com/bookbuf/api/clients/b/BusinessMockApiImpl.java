package com.bookbuf.api.clients.b;

import com.bookbuf.api.clients.ApiClubImpl;
import com.bookbuf.api.clients.resources.impl.AchievementResources;
import com.bookbuf.api.clients.resources.impl.AttendanceResources;
import com.bookbuf.api.clients.resources.impl.CallResources;
import com.bookbuf.api.clients.resources.impl.ClerkResources;
import com.bookbuf.api.clients.resources.impl.CouponResources;
import com.bookbuf.api.clients.resources.impl.DiscoverResources;
import com.bookbuf.api.clients.resources.impl.ExamPaperResources;
import com.bookbuf.api.clients.resources.impl.ExamResources;
import com.bookbuf.api.clients.resources.impl.GlobalResources;
import com.bookbuf.api.clients.resources.impl.HeWeatherResources;
import com.bookbuf.api.clients.resources.impl.MeasureResources;
import com.bookbuf.api.clients.resources.impl.MedicalRegisterResources;
import com.bookbuf.api.clients.resources.impl.MessageResources;
import com.bookbuf.api.clients.resources.impl.QuestionResources;
import com.bookbuf.api.clients.resources.impl.TaskResources;
import com.bookbuf.api.clients.resources.impl.TrainResources;
import com.bookbuf.api.configurations.IConfiguration;
import com.bookbuf.api.responses.impl.BannerResponse;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceContextResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceDayResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceStatusResponse;
import com.bookbuf.api.responses.impl.call.CallCancelResponse;
import com.bookbuf.api.responses.impl.call.CallResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponNewResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.bookbuf.api.responses.impl.customer.BindCardListResponse;
import com.bookbuf.api.responses.impl.customer.CreateUserResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperWithExamResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.bookbuf.api.responses.impl.examination.FetchDiscoverQuestionResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.bookbuf.api.responses.impl.global.SplashScreenResponse;
import com.bookbuf.api.responses.impl.measure.add.AddRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.LatestMeasureResponse;
import com.bookbuf.api.responses.impl.measure.list.ListRecordResponse;
import com.bookbuf.api.responses.impl.medical.DepartmentResponse;
import com.bookbuf.api.responses.impl.medical.DoctorResponse;
import com.bookbuf.api.responses.impl.medical.DoctorScheduleResponse;
import com.bookbuf.api.responses.impl.medical.HospitalResponse;
import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.bookbuf.api.responses.impl.message.MessageResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.bookbuf.api.responses.impl.user.ClerkResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.FetchClerkProfileResponse;
import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.ipudong.core.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public class BusinessMockApiImpl extends ApiClubImpl implements BusinessApi {

    BusinessApi resources;

    public BusinessMockApiImpl(IConfiguration configuration, BusinessApi resources) {
        super(configuration);
        this.resources = resources;
    }

    @Override
    public CouponResources.Business couponResources() {
        return this;
    }

    @Override
    public AchievementResources achievementResources() {
        return this;
    }

    @Override
    public TrainResources trainResources() {
        return this;
    }

    @Override
    public ClerkResources clerkResources() {
        return this;
    }

    @Override
    public MedicalRegisterResources medicalRegisterResources() {
        return this;
    }

    @Override
    public AttendanceResources attendanceResource() {
        return this;
    }

    @Override
    public TaskResources taskResources() {
        return this;
    }

    @Override
    public QuestionResources questionResources() {
        return this;
    }

    @Override
    public ExamPaperResources examPaperResources() {
        return this;
    }

    @Override
    public ExamResources examResources() {
        return this;
    }

    @Override
    public MessageResources messageResources() {
        return this;
    }

    @Override
    public GlobalResources.Business globalResources() {
        return this;
    }

    @Override
    public GlobalResources globalAllResources() {
        return this;
    }

    @Override
    public HeWeatherResources heWeatherResources() {
        return this;
    }

    @Override
    public MeasureResources measureResources() {
        return this;
    }

    @Override
    public CallResources callResources() {
        return this;
    }

    @Override
    public DiscoverResources discoverResources() {
        return this;
    }

    @Override
    public Result<FetchCareerMapResponse> fetchCareerMap() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"type\": 100,\n" +
                    "        \"isHot\": false,\n" +
                    "        \"achievements\": [\n" +
                    "            {\n" +
                    "                \"id\": 101,\n" +
                    "                \"title\": \"实习店员\",\n" +
                    "                \"description\": \"实习店员是迈向人生巅峰的第一步啊\",\n" +
                    "                \"status\": 2,\n" +
                    "                \"passedCount\": 10,\n" +
                    "                \"totalCount\": 10\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"id\": 102,\n" +
                    "                \"title\": \"正式店员\",\n" +
                    "                \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                    "                \"status\": 1,\n" +
                    "                \"passedCount\": 5,\n" +
                    "                \"totalCount\": 6\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"id\": 103,\n" +
                    "                \"title\": \"储备组长\",\n" +
                    "                \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                    "                \"status\": 0,\n" +
                    "                \"passedCount\": 0,\n" +
                    "                \"totalCount\": 5\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}");
            return parser().createCareerMapResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchCareerMap();
    }

    @Override
    public Result<BooleanResponse> postRealAvatar(Long uid, String url,Long operationUid,Long vendorId) {
        return null;
    }

    @Override
    public Result<FetchAchievementDetailResponse> fetchAchievement(long achievementId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"id\": 1001,\n" +
                    "        \"title\": \"储备组长\",\n" +
                    "        \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                    "        \"status\": 1,\n" +
                    "        \"passedCount\": 5,\n" +
                    "        \"totalCount\": 6,\n" +
                    "        \"events\": [\n" +
                    "            {\n" +
                    "                \"id\": 10011,\n" +
                    "                \"title\": \"思维与心态\",\n" +
                    "                \"online\": false,\n" +
                    "                \"status\": 0\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"id\": 10031,\n" +
                    "                \"title\": \"销售话术与技巧\",\n" +
                    "                \"status\": 2,\n" +
                    "                \"online\": true\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}");
            return parser().createAchievementResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchAchievement(achievementId);
    }

    @Override
    public Result<BooleanResponse> preference() {
        return resources.preference();
    }

    @Override
    public Result<AttendanceMonthResponse> month(int year, int month) {
        return resources.month(year, month);
    }

    @Override
    public Result<AttendanceDayResponse> day(int year, int month, int day) {
        return resources.day(year, month, day);
    }

    @Override
    public Result<AttendanceContextResponse> today() {
        return resources.today();
    }

    @Override
    public Result<BooleanResponse> apply(int type, double longitude, double latitude, String deviceMacAddress, String content) {
        return resources.apply(type, longitude, latitude, deviceMacAddress, content);
    }

    @Override
    public Result<AttendanceStatusResponse> status(int type, double longitude, double latitude) {
        return resources.status(type, longitude, latitude);
    }

    @Override
    public Result<CallCancelResponse> callCancel(String sid, int type) {
        return resources.callCancel(sid, type);
    }

    @Override
    public Result<CallResponse> call(String mobile) {
        return resources.call(mobile);
    }

    @Override
    public Result<CredentialResponse> login(String target, String password, String pushId) {
        return resources.login(target, password, pushId);
    }

    @Override
    public Result<CustomerResponse> queryCustomer(String query, String type) {
        return resources.queryCustomer(query, type);
    }

    @Override
    public Result<BooleanResponse> resetPassword(String mobile, String password, String verifyCode, String sign) {
        return resources.resetPassword(mobile, password, verifyCode, sign);
    }

    @Override
    public Result<BooleanResponse> logout() {
        return resources.logout();
    }

    @Override
    public Result<BooleanResponse> register(String mobile, String idCard, String realName, String password) {
        try {
            String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":1832409}";
            JSONObject mock = new JSONObject(json);
            return parser().createBooleanResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.register(mobile, idCard, realName, password);
    }

    @Override
    public Result<BooleanResponse> updatePassword(String oldPwd, String newPwd) {
        return resources.updatePassword(oldPwd, newPwd);
    }

    @Override
    public Result<BooleanResponse> updateCustomerProfileBasic(long userId, String realName, String mobile, String idCard, String birthday,String gender, String telephone, String address) {
        return resources.updateCustomerProfileBasic(userId, realName, mobile, idCard, birthday,gender, telephone, address);
    }

    @Override
    public Result<BooleanResponse> updateProfileBasic(long userId, String realName, String mobile, String idCard, String birthday, String telephone, String address) {
        return resources.updateProfileBasic(userId, realName, mobile, idCard, birthday, telephone, address);
    }

    @Override
    public Result<BooleanResponse> bindVendor(String mobile, String clerkNum, String code) {
        return resources.bindVendor(mobile, clerkNum, code);
    }

    @Override
    public Result<LongResponse> createCustomer(String vendorMemberId, String mobile, String phone, String realName, String idCard, String detailAddress) {
        try {
            String json = "{\"success\":true,\"count\":0,\"model\":1832309,\"msgInfo\":null,\"msgCode\":null}";
            JSONObject mock = new JSONObject(json);
            return parser().createLongResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.createCustomer(vendorMemberId, mobile, phone, realName, idCard, detailAddress);
    }

    @Override
    public Result<ClerkResponse> queryClerkProfile() {

        String json = "{\n" +
                "    \"success\": true,\n" +
                "    \"count\": 0,\n" +
                "    \"model\": {\n" +
                "        \"vendor_id\": \"2\",\n" +
                "        \"vendor_shop_id\": \"28\",\n" +
                "        \"birthday\": \"1974-04-14\",\n" +
                "        \"clerk_status\": \"0\",\n" +
                "        \"note\": \"\",\n" +
                "        \"vendor_code\": \"dsl\",\n" +
                "        \"address\": {\n" +
                "            \"detail\": \"55555555\"\n" +
                "        },\n" +
                "        \"vendor_logo_url\": \"http://s1.healthbok.com/logo/6/36775/145wrhe97b.jpg\",\n" +
                "        \"gender\": \"女\",\n" +
                "        \"mobile\": \"13555555555\",\n" +
                "        \"id_card\": \"532628197404143101\",\n" +
                "        \"vendor_name\": \"大参林医药\",\n" +
                "        \"vendor_position\": \"1\",\n" +
                "        \"full_address\": \"55555555\",\n" +
                "        \"realname\": \"小白龙\",\n" +
                "        \"role_name\": \"clerk\",\n" +
                "        \"user_id\": \"38254\",\n" +
                "        \"clerk_num\": \"003\",\n" +
                "        \"vendor_shop_name\": \"总经办\",\n" +
                "        \"email\": \"\",\n" +
                "        \"status\": \"1\"\n" +
                "    },\n" +
                "    \"msgInfo\": null,\n" +
                "    \"msgCode\": null\n" +
                "}";

        JSONObject mock = null;
        try {
            mock = new JSONObject(json);
            return parser().createClerkResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.queryClerkProfile();
    }

    @Override
    public Result<FetchClerkProfileResponse> queryClerkProfile_2017_01_22() {
        String json = "{\n" +
                "    \"success\": true,\n" +
                "    \"count\": 0,\n" +
                "    \"model\": {\n" +

                "        \"vendor_shop_id\": \"28\",\n" +
                "        \"vendor_id\": \"2\",\n" +
                "        \"birthday\": \"1974-04-14\",\n" +
                "        \"clerk_status\": \"0\",\n" +
                "        \"note\": \"\",\n" +
                "        \"vendor_code\": \"dsl\",\n" +
                "        \"address\": {\n" +
                "            \"detail\": \"55555555\"\n" +
                "        },\n" +
                "        \"vendor_logo_url\": \"http://s1.healthbok.com/logo/6/36775/145wrhe97b.jpg\",\n" +
                "        \"gender\": \"女\",\n" +
                "        \"mobile\": \"13555555555\",\n" +
                "        \"id_card\": \"532628197404143101\",\n" +
                "        \"vendor_name\": \"大参林医药\",\n" +
                "        \"vendor_position\": \"1\",\n" +
                "        \"full_address\": \"55555555\",\n" +
                "        \"realname\": \"小白龙\",\n" +
                "        \"role_name\": \"clerk\",\n" +
                "        \"user_id\": \"38254\",\n" +
                "        \"clerk_num\": \"003\",\n" +
                "        \"vendor_shop_name\": \"总经办\",\n" +
                "        \"email\": \"\",\n" +
                "        \"status\": \"1\"\n" +
                "    },\n" +
                "    \"msgInfo\": null,\n" +
                "    \"msgCode\": null\n" +
                "}";

        JSONObject mock = null;
        try {
            mock = new JSONObject(json);
            return parser().createFetchClerkProfileResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.queryClerkProfile_2017_01_22();
    }

    @Override
    public Result<CustomerResponse> queryCustomerProfile(long userId) {
        return resources.queryCustomerProfile(userId);
    }

    @Override
    public Result<BannerResponse> fetchBannerConfiguration() {
        String json = "{\n" +
                "    \"success\": true,\n" +
                "    \"msgCode\": null,\n" +
                "    \"msgInfo\": null,\n" +
                "    \"count\": 0,\n" +
                "    \"model\": [\n" +
                "            {\n" +
                "                \"intent\": \"http://s1.healthbok.com/article/9/6/783ce083bf111b07.jpg\",\n" +
                "                \"url\": \"www.healthbok.com\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"intent\": \"http://s1.healthbok.com/article/9/6/ac50a806c4bbffe5.jpg\",\n" +
                "                \"url\": \"www.healthbok.com\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"intent\": \"http://s1.healthbok.com/article/9/6/9a2f62a3ae1e8b22.jpeg\",\n" +
                "                \"url\": \"www.healthbok.com\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"intent\": \"http://s1.healthbok.com/article/9/6/b7851fdfa945bc6b.jpg\",\n" +
                "                \"url\": \"www.healthbok.com\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"intent\": \"http://s1.healthbok.com/article/9/6/171b97df33a343ce.jpg\",\n" +
                "                \"url\": \"www.healthbok.com\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"intent\": \"http://s1.healthbok.com/article/9/6/4e3a14ef5ac438ea.jpg\",\n" +
                "                \"url\": \"http://www.healthbok.com\"\n" +
                "            }\n" +
                "        ]\n" +
                "}";

        JSONObject mock = null;
        try {
            mock = new JSONObject(json);
            return parser().createBannerResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchBannerConfiguration();
    }

    @Override
    public Result<BooleanResponse> cancelStoreArticle(long id) {
        return resources.cancelStoreArticle(id);
    }

    @Override
    public Result<FetchDiscoverQuestionResponse> fetchDiscoverQuestion(long id) {
        return resources.fetchDiscoverQuestion(id);
    }

    @Override
    public Result<BooleanResponse> answerQuestionDiscovery(long questionId, String answers) {
        return resources.answerQuestionDiscovery(questionId, answers);
    }

    @Override
    public Result<BooleanResponse> storeArticle(long id) {
        return resources.storeArticle(id);
    }

    @Override
    public Result<ExamPaperResponse> fetchExamPaper(long paperId) {
        return resources.fetchExamPaper(paperId);
    }

    @Override
    public Result<List<ExamPaperWithExamResponse>> fetchExamPaperWithExam() {
        return resources.fetchExamPaperWithExam();
    }

    @Override
    public Result<ExamResponse> beginExam(long paperId) {
        return resources.beginExam(paperId);
    }

    @Override
    public Result<ExamResponse> getExamStatus(long examId) {
        return resources.getExamStatus(examId);
    }

    @Override
    public Result<BooleanResponse> terminateExam(long examId) {
        return resources.terminateExam(examId);
    }

    @Override
    public Result<ExamResponse> handleInExam(long examId, String answers) {
        return resources.handleInExam(examId, answers);
    }

    @Override
    public Result<ExamResponse> handleInExam(long examId, String answers, long courseId) {
        return resources.handleInExam(examId, answers, courseId);
    }

    @Override
    public Result<ExamResponse> handleInExam(long examId, String answers, long courseId, long levelId) {
        return resources.handleInExam(examId, answers, courseId, levelId);
    }

    @Override
    public Result<ExamResponse> fetchExamHistory(long examId) {
        return resources.fetchExamHistory(examId);
    }

    @Override
    public Result<BooleanResponse> checkVerifyCode(String mobile, String sign, String code) {
        return resources.checkVerifyCode(mobile, sign, code);
    }

    @Override
    public Result<AppUpgradeResponse> fetchAppUpgrade(String appName) {
        return resources.fetchAppUpgrade(appName);
    }

    @Override
    public Result<LongResponse> checkUserByQuery(String mobile) {
        return resources.checkUserByQuery(mobile);
    }

    @Override
    public Result<BooleanResponse> sendClerkVerifyCode(String mobile, @SignDef String sign) {
        return resources.sendClerkVerifyCode(mobile, sign);
    }

    @Override
    public Result<RoleResponse> queryRole(String mobile) {
        String json = "{\"success\":true,\"count\":0,\"model\":{\"role\":1},\"msgInfo\":null,\"msgCode\":null}";
        try {
            JSONObject mock = new JSONObject(json);
            return parser().createRoleResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.queryRole(mobile);
    }

    @Override
    public Result<BooleanResponse> sendNotice(String mobile, @SignDef String sign, String type) {
        return resources.sendNotice(mobile, sign,type);
    }

    @Override
    public Result<SplashScreenResponse> querySplashScreen() {

        String json = "{\n" +
                "    \"success\": true,\n" +
                "    \"msgCode\": null,\n" +
                "    \"msgInfo\": null,\n" +
                "    \"count\": 0,\n" +
                "    \"model\": {\n" +
                "        \"ui\": {\n" +
                "            \"background\": \"http://uploads.yjbys.com/allimg/201701/46-1F114102Z3-50.jpg\",\n" +
                "            \"start\": 1484720921,\n" +
                "            \"end\": 1485720921\n" +
                "        },\n" +
                "        \"target\": {\n" +
                "            \"type\": \"open_discover\"\n" +
                "        },\n" +
                "        \"desc\": {},\n" +
                "        \"extras\": {\n" +
                "            \"discover\": {\n" +
                "                \"url\": \"http://www.163.com/\"\n" +
                "            }\n" +
                "        },\n" +
                "        \"type\": \"splashscreen\",\n" +
                "        \"id\": 2,\n" +
                "        \"vendor\": -1\n" +
                "    }\n" +
                "}";
        try {
            JSONObject mock = new JSONObject(json);
            return parser().createSplashScreenResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.querySplashScreen();
    }

    @Override
    public Result<WeatherResponse> fetchWeather(String city) {
        return resources.fetchWeather(city);
    }

    @Override
    public Result<BooleanResponse> sendVerifyCode(String mobile, @SignDef String sign) {
        return null;
    }

    @Override
    public Result<AddRecordResponse> addMeasureRecord(long userId, String detectionCode, String values, double longitude, double latitude, double elevation) {
        return resources.addMeasureRecord(userId, detectionCode, values, longitude, latitude, elevation);
    }

    @Override
    public Result<LatestMeasureResponse> listLatestMeasureRecord(long userId) {
        return resources.listLatestMeasureRecord(userId);
    }

    @Override
    public Result<ListRecordResponse> listMeasureRecord(long userId, String typeCode, int page, int pageSize) {
        return resources.listMeasureRecord(userId, typeCode, page, pageSize);
    }

    @Override
    public Result<MedicalRegisterResponse> sendSMS(long id) {
        return resources.sendSMS(id);
    }

    @Override
    public Result<MedicalRegisterResponse> bookService(long hospitalId, int lockQueueNo, String scheduleItemCode, long userId) {
        return resources.bookService(hospitalId, lockQueueNo, scheduleItemCode, userId);
    }

    @Override
    public Result<List<DoctorScheduleResponse>> listDoctorSchedules(long hospitalId, int departmentCode, int doctorCode) {
        return resources.listDoctorSchedules(hospitalId, departmentCode, doctorCode);
    }

    @Override
    public Result<List<MedicalRegisterResponse>> listMedicalRegisters(String mobile) {
        return resources.listMedicalRegisters(mobile);
    }

    @Override
    public Result<List<HospitalResponse>> listHospitals() {
        return resources.listHospitals();
    }

    @Override
    public Result<List<DoctorResponse>> listDoctors(long hospitalId, long departmentCode) {
        return resources.listDoctors(hospitalId, departmentCode);
    }

    @Override
    public Result<List<DepartmentResponse>> listDepartments(long hospitalId) {
        String json = "{\"success\":true,\"msgCode\":null,\"msgInfo\":null,\"count\":0,\"model\":{\"ResultCode\":0,\"RecordCount\":7,\"Departments\":{\"Department\":[{\"DepartmentCode\":\"0\",\"DepartmentName\":\"内科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"198\",\"DepartmentName\":\"血液专科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"206\",\"DepartmentName\":\"血液专科门诊（2）\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"199\",\"DepartmentName\":\"血液移植门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"128\",\"DepartmentName\":\"血液透析中心\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"119\",\"DepartmentName\":\"心血管内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"193\",\"DepartmentName\":\"消化内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"189\",\"DepartmentName\":\"糖尿病咨询门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"95\",\"DepartmentName\":\"肾脏内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"481\",\"DepartmentName\":\"神经内科帕金森病专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"499\",\"DepartmentName\":\"神经内科脑卒中筛查门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"92\",\"DepartmentName\":\"神经内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"498\",\"DepartmentName\":\"神经内科肌病专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"190\",\"DepartmentName\":\"神经内科癫痫专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"79\",\"DepartmentName\":\"内分泌科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"32\",\"DepartmentName\":\"呼吸内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"184\",\"DepartmentName\":\"骨质疏松门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"170\",\"DepartmentName\":\"肝病专科门诊\",\"ResRowIdFlag\":\"Y\"}]},{\"DepartmentCode\":\"1\",\"DepartmentName\":\"外科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"393\",\"DepartmentName\":\"冠心病介入门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"196\",\"DepartmentName\":\"心脏外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"124\",\"DepartmentName\":\"胸外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"197\",\"DepartmentName\":\"血管外科、疝及腹壁外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"192\",\"DepartmentName\":\"胃肠外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"191\",\"DepartmentName\":\"疼痛科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"88\",\"DepartmentName\":\"烧伤整形科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"188\",\"DepartmentName\":\"伤口门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"185\",\"DepartmentName\":\"神经外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"76\",\"DepartmentName\":\"泌尿外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"50\",\"DepartmentName\":\"甲状腺外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"174\",\"DepartmentName\":\"骨一(脊柱脊髓病、颈肩腰腿痛)门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"172\",\"DepartmentName\":\"骨科肿瘤(骨二)门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"173\",\"DepartmentName\":\"骨科关节(骨二)门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"20\",\"DepartmentName\":\"肝胆外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"171\",\"DepartmentName\":\"肛肠外科门诊\",\"ResRowIdFlag\":\"Y\"}]},{\"DepartmentCode\":\"2\",\"DepartmentName\":\"妇产科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"277\",\"DepartmentName\":\"产前检查超声科\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"490\",\"DepartmentName\":\"产科营养门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"168\",\"DepartmentName\":\"习惯性流产门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"398\",\"DepartmentName\":\"生殖中心\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"84\",\"DepartmentName\":\"乳腺外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"166\",\"DepartmentName\":\"人流术后关爱门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"183\",\"DepartmentName\":\"男科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"48\",\"DepartmentName\":\"计划生育科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"504\",\"DepartmentName\":\"妇科阴道镜门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"17\",\"DepartmentName\":\"妇科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"487\",\"DepartmentName\":\"妇科宫腔镜室\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"458\",\"DepartmentName\":\"妇科更年期保健门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"497\",\"DepartmentName\":\"妇科妇女保健门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"281\",\"DepartmentName\":\"妇科不孕症门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"488\",\"DepartmentName\":\"产科遗传咨询门诊（产前诊断门诊）\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"489\",\"DepartmentName\":\"产科门诊(母乳喂养咨询)\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"157\",\"DepartmentName\":\"产科门诊\",\"ResRowIdFlag\":\"Y\"}]},{\"DepartmentCode\":\"3\",\"DepartmentName\":\"儿科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"501\",\"DepartmentName\":\"新生儿随访门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"112\",\"DepartmentName\":\"小儿外科门诊(综合)\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"164\",\"DepartmentName\":\"儿血液专科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"163\",\"DepartmentName\":\"儿哮喘专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"461\",\"DepartmentName\":\"儿童生长发育门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"162\",\"DepartmentName\":\"儿童矮小科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"160\",\"DepartmentName\":\"儿神经专科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"161\",\"DepartmentName\":\"儿肾病专科门诊\",\"ResRowIdFlag\":\"N\"}]},{\"DepartmentCode\":\"4\",\"DepartmentName\":\"五官科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"394\",\"DepartmentName\":\"造口门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"130\",\"DepartmentName\":\"眼科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"65\",\"DepartmentName\":\"口腔修复正畸门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"179\",\"DepartmentName\":\"口腔外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"63\",\"DepartmentName\":\"口腔内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"165\",\"DepartmentName\":\"耳鼻喉科门诊\",\"ResRowIdFlag\":\"Y\"}]},{\"DepartmentCode\":\"5\",\"DepartmentName\":\"中医科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"204\",\"DepartmentName\":\"中医肿瘤专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"203\",\"DepartmentName\":\"中医心脑血管病专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"146\",\"DepartmentName\":\"中医科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"202\",\"DepartmentName\":\"针炙损美性疾病专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"140\",\"DepartmentName\":\"针灸科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"201\",\"DepartmentName\":\"穴位注射鼻炎专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"200\",\"DepartmentName\":\"推拿按摩乳腺增生专科门诊\",\"ResRowIdFlag\":\"N\"}]},{\"DepartmentCode\":\"6\",\"DepartmentName\":\"肿瘤科\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"149\",\"DepartmentName\":\"肿瘤生物治疗中心\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"205\",\"DepartmentName\":\"肿瘤科门诊\",\"ResRowIdFlag\":\"Y\"}]},{\"DepartmentCode\":\"7\",\"DepartmentName\":\"其它\",\"DepartmentGroup\":\"Y\",\"cDepartment\":[{\"DepartmentCode\":\"349\",\"DepartmentName\":\"疑难病会诊中心\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"496\",\"DepartmentName\":\"综合ICU门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"585\",\"DepartmentName\":\"营养咨询门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"589\",\"DepartmentName\":\"婴幼儿过敏干预门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"137\",\"DepartmentName\":\"影像科\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"138\",\"DepartmentName\":\"预防保健科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"573\",\"DepartmentName\":\"运动及骨关节疾病康复专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"507\",\"DepartmentName\":\"遗传检测中心门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"194\",\"DepartmentName\":\"心理咨询门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"116\",\"DepartmentName\":\"心理科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"534\",\"DepartmentName\":\"消化生殖骨软组织肿瘤特需门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"576\",\"DepartmentName\":\"心肺康复专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"577\",\"DepartmentName\":\"小儿心血管专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"521\",\"DepartmentName\":\"小儿普通外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"523\",\"DepartmentName\":\"小儿泌尿外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"522\",\"DepartmentName\":\"小儿骨科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"331\",\"DepartmentName\":\"特中医科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"314\",\"DepartmentName\":\"特肿瘤科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"444\",\"DepartmentName\":\"特针灸科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"545\",\"DepartmentName\":\"特椎管内疾病门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"584\",\"DepartmentName\":\"体重管理门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"442\",\"DepartmentName\":\"特影像科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"313\",\"DepartmentName\":\"特眼科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"473\",\"DepartmentName\":\"特心脏外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"298\",\"DepartmentName\":\"特血液内科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"296\",\"DepartmentName\":\"特心血管科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"400\",\"DepartmentName\":\"特胸外科门诊2\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"301\",\"DepartmentName\":\"特胸外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"317\",\"DepartmentName\":\"特心理科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"295\",\"DepartmentName\":\"特消化内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"328\",\"DepartmentName\":\"特血管外科、疝及腹壁外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"325\",\"DepartmentName\":\"特小儿外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"303\",\"DepartmentName\":\"特胃肠外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"302\",\"DepartmentName\":\"特疼痛科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"529\",\"DepartmentName\":\"疼痛科头痛门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"294\",\"DepartmentName\":\"特肾脏内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"306\",\"DepartmentName\":\"特生殖男科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"293\",\"DepartmentName\":\"特烧伤整形外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"311\",\"DepartmentName\":\"特神经科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"305\",\"DepartmentName\":\"特乳腺外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"310\",\"DepartmentName\":\"特皮肤科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"544\",\"DepartmentName\":\"特脑先天性疾病脑积水\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"546\",\"DepartmentName\":\"特脑血管疾病门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"250\",\"DepartmentName\":\"特脑外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"440\",\"DepartmentName\":\"特内镜中心门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"181\",\"DepartmentName\":\"特内分泌科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"249\",\"DepartmentName\":\"特泌尿外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"327\",\"DepartmentName\":\"特口腔修复正畸科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"326\",\"DepartmentName\":\"特口腔外科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"315\",\"DepartmentName\":\"特口腔内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"324\",\"DepartmentName\":\"特康复理疗科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"322\",\"DepartmentName\":\"特甲状腺外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"100\",\"DepartmentName\":\"体检中心\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"323\",\"DepartmentName\":\"特介入科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"443\",\"DepartmentName\":\"特核医学甲状腺门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"335\",\"DepartmentName\":\"特呼吸内科门诊(2)\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"253\",\"DepartmentName\":\"特呼吸内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"251\",\"DepartmentName\":\"特骨科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"304\",\"DepartmentName\":\"特肝胆外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"247\",\"DepartmentName\":\"特肛肠外科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"482\",\"DepartmentName\":\"特肝病及门脉高压性胃病门诊(2)\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"255\",\"DepartmentName\":\"特肝病及门脉高压性胃病门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"321\",\"DepartmentName\":\"特妇科门诊(2)\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"307\",\"DepartmentName\":\"特妇科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"309\",\"DepartmentName\":\"特儿科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"312\",\"DepartmentName\":\"特耳鼻喉科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"548\",\"DepartmentName\":\"特垂体联合门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"320\",\"DepartmentName\":\"特产科门诊（胎儿医学门诊）\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"308\",\"DepartmentName\":\"特不孕不育症专科\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"332\",\"DepartmentName\":\"碎石中心\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"580\",\"DepartmentName\":\"神经外科三叉神经痛专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"540\",\"DepartmentName\":\"神经外科脑肿瘤脑积水门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"539\",\"DepartmentName\":\"神经外科脑血管疾病门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"579\",\"DepartmentName\":\"神经外科面肌痉挛及三叉神经痛专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"541\",\"DepartmentName\":\"神经外科脊柱脊髓病门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"543\",\"DepartmentName\":\"神经外科垂体联合门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"509\",\"DepartmentName\":\"神经内科眩晕专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"531\",\"DepartmentName\":\"神经内科头痛亚专科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"530\",\"DepartmentName\":\"神经内科神经免疫亚专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"520\",\"DepartmentName\":\"神经内科肌张力障碍专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"480\",\"DepartmentName\":\"神经内科记忆障碍专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"574\",\"DepartmentName\":\"神经康复专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"536\",\"DepartmentName\":\"乳腺妇科肿瘤特需门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"586\",\"DepartmentName\":\"全科医学科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"503\",\"DepartmentName\":\"全科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"186\",\"DepartmentName\":\"皮肤科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"519\",\"DepartmentName\":\"皮肤科美容亚专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"441\",\"DepartmentName\":\"内镜中心门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"391\",\"DepartmentName\":\"麻醉科(门诊楼)\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"559\",\"DepartmentName\":\"老年心血管内科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"561\",\"DepartmentName\":\"老年神经内科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"558\",\"DepartmentName\":\"老年内科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"562\",\"DepartmentName\":\"老年内分泌科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"560\",\"DepartmentName\":\"老年呼吸内科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"182\",\"DepartmentName\":\"临床营养科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"538\",\"DepartmentName\":\"淋巴肿瘤特需门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"178\",\"DepartmentName\":\"康复理疗科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"575\",\"DepartmentName\":\"脊柱脊髓康复专科门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"177\",\"DepartmentName\":\"介入门诊(2)\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"176\",\"DepartmentName\":\"介入门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"570\",\"DepartmentName\":\"静配中心\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"395\",\"DepartmentName\":\"静疗门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"175\",\"DepartmentName\":\"核医学甲状腺科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"276\",\"DepartmentName\":\"高压氧仓\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"590\",\"DepartmentName\":\"骨科肿瘤(骨一)门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"246\",\"DepartmentName\":\"GCP科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"169\",\"DepartmentName\":\"干部门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"495\",\"DepartmentName\":\"肥胖门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"537\",\"DepartmentName\":\"肺癌食管癌特需门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"159\",\"DepartmentName\":\"儿科门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"563\",\"DepartmentName\":\"产科二胎门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"158\",\"DepartmentName\":\"肠道发热门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"535\",\"DepartmentName\":\"鼻咽头颈肿瘤特需门诊\",\"ResRowIdFlag\":\"N\"},{\"DepartmentCode\":\"156\",\"DepartmentName\":\"便民门诊\",\"ResRowIdFlag\":\"Y\"},{\"DepartmentCode\":\"525\",\"DepartmentName\":\"病理科门诊\",\"ResRowIdFlag\":\"Y\"}]}]}}}";
        try {
            JSONObject mock = new JSONObject(json);
            return parser().createDepartmentResponseList(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.listDepartments(hospitalId);
    }

    @Override
    public Result<List<MessageResponse>> fetchSystemMessages() {
        return resources.fetchSystemMessages();
    }

    @Override
    public Result<MessageResponse> fetchDetailMessage(long messageId) {
        return resources.fetchDetailMessage(messageId);
    }

    @Override
    public Result<LongResponse> fetchMessageCenter() {
        return resources.fetchMessageCenter();
    }

    @Override
    public Result<QuestionResponse> fetchQuestion(long articleId) {
        return resources.fetchQuestion(articleId);
    }

    @Override
    public Result<BooleanResponse> answerQuestion(long questionId, String answers) {
        return resources.answerQuestion(questionId, answers);
    }

    @Override
    public Result<TaskContextResponse> fetchTaskList() {

        try {
            JSONObject mock = new JSONObject("{ \"success\":true, \"msgCode\":null, \"msgInfo\":null, \"count\":0, \"model\":{ \"progress\":6, \"tasks\":[ { \"ui\":{ \"background\":\"http://s1.healthbok.com/task/9/4761099221213/e0ff43006891e154.png\" }, \"target\":{ \"type\":\"card\" }, \"stat\":{ \"totalNum\":5, \"performNum\":0 }, \"desc\":{ \"title\":\"会员开卡\", \"summary\":\"会员开卡任务完成0%\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"limb\", \"id\":1472450731004, \"tasks\":[ { \"ui\":{ }, \"target\":{ \"type\":\"card\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"今日第一卡\", \"summary\":\"未完成\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450749560 }, { \"ui\":{ }, \"target\":{ \"type\":\"card\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"今日第二卡\", \"summary\":\"未完成\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450749560 }, { \"ui\":{ }, \"target\":{ \"type\":\"card\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"今日第三卡\", \"summary\":\"未完成\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450749560 }, { \"ui\":{ }, \"target\":{ \"type\":\"card\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"今日第四卡\", \"summary\":\"未完成\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450749560 }, { \"ui\":{ }, \"target\":{ \"type\":\"card\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"今日第五卡\", \"summary\":\"未完成\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450749560 } ] }, { \"ui\":{ \"background\":\"http://s1.healthbok.com/task/9/4761099221213/7f65764c8aeca91d.png\" }, \"target\":{ \"type\":\"detect\" }, \"stat\":{ \"totalNum\":10, \"performNum\":0 }, \"desc\":{ \"title\":\"日常检测\", \"summary\":\"日常检测任务完成0%\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"limb\", \"id\":1472450774061, \"tasks\":[ { \"ui\":{ }, \"target\":{ \"type\":\"bp\" }, \"stat\":{ \"totalNum\":5, \"performNum\":0 }, \"desc\":{ \"title\":\"今日需检测血压5次\", \"summary\":\"\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450804625 }, { \"ui\":{ }, \"target\":{ \"type\":\"tc\" }, \"stat\":{ \"totalNum\":5, \"performNum\":0 }, \"desc\":{ \"title\":\"今日需检测总胆固醇5次\", \"summary\":\"\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"leaf\", \"id\":1472450877744 } ] }, { \"ui\":{ \"background\":\"http://s1.healthbok.com/task/9/4761099221213/1e7ce778c7ff9a5e.png\" }, \"target\":{ \"type\":\"know\" }, \"stat\":{ \"totalNum\":10, \"performNum\":0 }, \"desc\":{ \"title\":\"勤学知识\", \"summary\":\"勤学知识任务完成0%\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"limb\", \"id\":1472450848145, \"tasks\":[ ] }, { \"ui\":{ }, \"target\":{ \"type\":\"exam\" }, \"stat\":{ \"totalNum\":4, \"performNum\":2 }, \"desc\":{ \"title\":\"自学考试\", \"summary\":\"自学考试任务完成50%\" }, \"intent\":{ }, \"extras\":{ }, \"type\":\"limb\", \"id\":1478509985874, \"tasks\":[ { \"ui\":{ }, \"target\":{ \"type\":\"exam\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"1\", \"summary\":\"去考试\" }, \"intent\":{ }, \"extras\":{ \"examPaper\":{ \"id\":30, \"title\":\"1\", \"duration\":60, \"summary\":\"1\" } }, \"type\":\"leaf\", \"id\":\"99\" }, { \"ui\":{ }, \"target\":{ \"type\":\"exam\" }, \"stat\":{ \"totalNum\":1, \"performNum\":1 }, \"desc\":{ \"title\":\"2\", \"summary\":\"去考试\" }, \"intent\":{ }, \"extras\":{ \"examPaper\":{ \"id\":7, \"title\":\"2\", \"duration\":120, \"summary\":\"2\" } }, \"type\":\"leaf\", \"id\":\"99\" }, { \"ui\":{ }, \"target\":{ \"type\":\"exam\" }, \"stat\":{ \"totalNum\":1, \"performNum\":1 }, \"desc\":{ \"title\":\"100-你的名字\", \"summary\":\"去考试\" }, \"intent\":{ }, \"extras\":{ \"examPaper\":{ \"id\":24, \"title\":\"100-你的名字\", \"duration\":60, \"summary\":\"100-你的名字100-你的名字100-你的名字100-你的名字100-你的名字\" } }, \"type\":\"leaf\", \"id\":\"99\" }, { \"ui\":{ }, \"target\":{ \"type\":\"exam\" }, \"stat\":{ \"totalNum\":1, \"performNum\":0 }, \"desc\":{ \"title\":\"101-你的朋友\", \"summary\":\"去考试\" }, \"intent\":{ }, \"extras\":{ \"examPaper\":{ \"id\":25, \"title\":\"101-你的朋友\", \"duration\":60, \"summary\":\"101-你的朋友101-你的朋友101-你的朋友101-你的朋友101-你的朋友\" } }, \"type\":\"leaf\", \"id\":\"99\" } ] } ] } }\n");
            return parser().createTaskResponseList(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchTaskList();
    }

    @Override
    public Result<FetchTrainResponse> fetchTrain(long courseId, long levelId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"model\": {\n" +
                    "        \"isSignUp\": false,\n" +
                    "        \"course\": {\n" +
                    "            \"id\": 10012,\n" +
                    "            \"title\": \"北京暂不启动公交地铁票价调整\",\n" +
                    "            \"description\": \"市发改委昨天发布消息，按照相关办法，本市轨道交通价格已达到动态调整启动条件，但市政府在多方听取意见后决定，今年暂缓启动轨道交通票价动态调整，地面公交和轨道交通的调整，整体纳入下一调价周期累计。\",\n" +
                    "            \"online\": false,\n" +
                    "            \"status\": 0,\n" +
                    "            \"passedCount\": 3,\n" +
                    "            \"totalCount\": 6\n" +
                    "        },\n" +
                    "        \"offLine\": {\n" +
                    "            \"teacher\": \"王老师\",\n" +
                    "            \"period\": \"60分钟\",\n" +
                    "            \"time\": \"2016年12月12日 14:00\",\n" +
                    "            \"address\": \"文一西路998号海创园18幢701\"\n" +
                    "        }\n" +
                    "    }\n" +
                    "}");
            return parser().createFetchTrainResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchTrain(courseId, levelId);
    }

    @Override
    public Result<FetchTrainResponse> fetchTrain(long courseId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"model\": {\n" +
                    "        \"isSignUp\": false,\n" +
                    "        \"course\": {\n" +
                    "            \"id\": 10012,\n" +
                    "            \"title\": \"北京暂不启动公交地铁票价调整\",\n" +
                    "            \"description\": \"市发改委昨天发布消息，按照相关办法，本市轨道交通价格已达到动态调整启动条件，但市政府在多方听取意见后决定，今年暂缓启动轨道交通票价动态调整，地面公交和轨道交通的调整，整体纳入下一调价周期累计。\",\n" +
                    "            \"online\": false,\n" +
                    "            \"status\": 0,\n" +
                    "            \"passedCount\": 3,\n" +
                    "            \"totalCount\": 6\n" +
                    "        },\n" +
                    "        \"offLine\": {\n" +
                    "            \"teacher\": \"王老师\",\n" +
                    "            \"period\": \"60分钟\",\n" +
                    "            \"time\": \"2016年12月12日 14:00\",\n" +
                    "            \"address\": \"文一西路998号海创园18幢701\"\n" +
                    "        }\n" +
                    "    }\n" +
                    "}");
            return parser().createFetchTrainResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchTrain(courseId);
    }

    @Override
    public Result<BooleanResponse> signUpTrain(long courseId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"model\": true\n" +
                    "}");
            return parser().createBooleanResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.signUpTrain(courseId);
    }

    @Override
    public Result<FetchSignUpCourseResponse> fetchSignUpCourse() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"title\": \"热门培训\",\n" +
                    "        \"description\": \"完成了这些培训，你往高富帅又踏进了一步！\",\n" +
                    "        \"passedCount\": 5,\n" +
                    "        \"totalCount\": 6,\n" +
                    "        \"courses\": [\n" +
                    "            {\n" +
                    "                \"id\": 10011,\n" +
                    "                \"title\": \"思维与心态\",\n" +
                    "                \"status\": 0\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"id\": 10031,\n" +
                    "                \"title\": \"销售话术与技巧\",\n" +
                    "                \"status\": 2\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}");
            return parser().createApplyCourseResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchSignUpCourse();
    }

    @Override
    public Result<FetchLessonResponse> fetchLesson(long courseId, long levelId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"id\": 10011,\n" +
                    "        \"title\": \"储备组长：销售话术和技巧\",\n" +
                    "        \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                    "        \"status\": 1,\n" +
                    "        \"passedCount\": 2,\n" +
                    "        \"totalCount\": 3,\n" +
                    "        \"lessons\": [\n" +
                    "            {\n" +
                    "                \"title\": \"课时1：如何判断顾客类型\",\n" +
                    "                \"status\": 1,\n" +
                    "                \"online\": true,\n" +
                    "                \"redirect\": \"http://www.healthbok-inc.com/course/100111/\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"title\": \"课时2：如何描述产品卖点\",\n" +
                    "                \"status\": 0,\n" +
                    "                \"online\": true,\n" +
                    "                \"redirect\": \"http://www.healthbok-inc.com/course/100211/\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"title\": \"课时3：如何正确的给顾客推荐药品\",\n" +
                    "                \"status\": 2,\n" +
                    "                \"online\": true,\n" +
                    "                \"redirect\": \"http://www.healthbok-inc.com/course/100311/\"\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}");
            return parser().createLessonResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchLesson(courseId, levelId);
    }

    @Override
    public Result<FetchLessonResponse> fetchLesson(long courseId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"id\": 10011,\n" +
                    "        \"title\": \"储备组长：销售话术和技巧\",\n" +
                    "        \"description\": \"恭喜你正式迈向人生巅峰\",\n" +
                    "        \"status\": 1,\n" +
                    "        \"passedCount\": 2,\n" +
                    "        \"totalCount\": 3,\n" +
                    "        \"lessons\": [\n" +
                    "            {\n" +
                    "                \"title\": \"课时1：如何判断顾客类型\",\n" +
                    "                \"status\": 1,\n" +
                    "                \"online\": true,\n" +
                    "                \"redirect\": \"http://www.healthbok-inc.com/course/100111/\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"title\": \"课时2：如何描述产品卖点\",\n" +
                    "                \"status\": 0,\n" +
                    "                \"online\": true,\n" +
                    "                \"redirect\": \"http://www.healthbok-inc.com/course/100211/\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"title\": \"课时3：如何正确的给顾客推荐药品\",\n" +
                    "                \"status\": 2,\n" +
                    "                \"online\": true,\n" +
                    "                \"redirect\": \"http://www.healthbok-inc.com/course/100311/\"\n" +
                    "            }\n" +
                    "        ]\n" +
                    "    }\n" +
                    "}");
            return parser().createLessonResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchLesson(courseId);
    }

    @Override
    public Result<VerifyCouponResponse> verifyCoupon(String coupon) {
        try {
            JSONObject mock = new JSONObject("{ \n" +
                    "\"success\": true, \n" +
                    "\"msgCode\": null, \n" +
                    "\"msgInfo\": null, \n" +
                    "\"count\": 0, \n" +
                    "\"model\": true \n" +
                    "}");
            return parser().createVerifyCouponResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.verifyCoupon(coupon);
    }

    @Override
    public Result<FetchCouponStatusResponse> fetchCoupon(String coupon) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "  \"success\": true,\n" +
                    "  \"msgCode\": null,\n" +
                    "  \"msgInfo\": null,\n" +
                    "  \"count\": 0,\n" +
                    "  \"model\": {\n" +
                    "    \"off_conditions_price\": 699,\n" +
                    "    \"off_price\": 100,\n" +
                    "    \"type\": 1,\n" +
                    "    \"description\": \"奶粉类商品\",\n" +
                    "    \"status\": 1,\n" +
                    "    \"overdue\": 2,\n" +
                    "    \"outService\": 1,\n" +
                    "    \"activity_begin\": \"2017.01.18\",\n" +
                    "    \"activity_end\": \"2017.01.22\",\n" +
                    "    \"activity_goods\": [\n" +
                    "      \"贝因美\",\n" +
                    "      \"雅士利\",\n" +
                    "      \"川普贝尔\"\n" +
                    "    ]\n" +
                    "  }\n" +
                    "}");
            return parser().createFetchCouponStatusResponse(mock);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchCoupon(coupon);
    }

    @Override
    public Result<CouponAliasResponse> fetchCouponAlias(long couponId) {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": {\n" +
                    "        \"alias\": \"1234\",\n" +
                    "        \"isEnd\": 0\n" +
                    "    }\n" +
                    "}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.fetchCouponAlias(couponId);
    }

    @Override
    public Result<CouponAliasListResponse> searchCouponCodeList() {
        try {
            JSONObject mock = new JSONObject("{\n" +
                    "    \"success\": true,\n" +
                    "    \"msgCode\": null,\n" +
                    "    \"msgInfo\": null,\n" +
                    "    \"count\": 0,\n" +
                    "    \"model\": [\n" +
                    "        {\n" +
                    "            \"couponId\": 36,\n" +
                    "            \"title\": \"领取优惠码了\",\n" +
                    "            \"activityTime\": \"活动时间 2017.04.04-2017.10.10\",\n" +
                    "            \"promoCode\": \"2s5ah\",\n" +
                    "            \"isEnd\": 0\n" +
                    "        }\n" +
                    "    ]\n" +
                    "}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resources.searchCouponCodeList();
    }

    @Override
    public Result<String> getVerifyCode(String mobile, String sign) {
        return null;
    }

    @Override
    public Result<PermissionResponse> getPermission() {
        return null;
    }

    @Override
    public Result<CustomerLoginResponse> userLogin(String mobile, String code, String pushId) {
        return null;
    }

    @Override
    public Result<BindCardListResponse> bindCard(String uid, String vendorId, String vipcard) {
        return null;
    }

    @Override
    public Result<CreateUserResponse> createUser(String name, String mobile, String telephone, String idCard, String address, String birthday, String gender,String verifiedUrl) {
        return null;
    }

    @Override
    public Result<MonitorCountResponse> monitorSelectCount(String jsonObject) {
        return null;
    }

    @Override
    public Result<CouponNewResponse> getVerfiedCoupon(Long vendorId,Long uid) {
        return null;
    }
}
