package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface HeWeatherResources {

    /**
     * 给定城市查询天气情况
     *
     * @param city 城市名称 city可通过城市中英文名称、ID和IP地址进行，例如city=北京，city=beijing，city=CN101010100，city= 60.194.130.1
     * @return 天气情况
     */
    Result<WeatherResponse> fetchWeather(String city);
}
