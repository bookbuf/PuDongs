package com.bookbuf.api.clients.resources.impl;

import android.support.annotation.Nullable;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponNewResponse;
import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeResponse;
import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.ipudong.core.Result;


/**
 * Created by bo.wei on 2016/11/23.
 */

public interface CouponResources {


    interface Business extends CouponResources {
        /**
         * 核销优惠券
         *
         * @param couponNum 优惠券编号
         * @return 优惠券是否核销成功
         */
        Result<VerifyCouponResponse> verifyCoupon(String couponNum);

        Result<FetchCouponStatusResponse> fetchCoupon(String couponNum);

        /**
         * 获取优惠码（B端）
         * @return
         */
        Result<CouponAliasResponse> fetchCouponAlias(long couponId);

        /**
         * 获取优惠码列表（B端）
         * @return
         */
        Result<CouponAliasListResponse> searchCouponCodeList();

        Result<CouponNewResponse> getVerfiedCoupon(Long vendorId,Long uid);
    }

    interface Customer extends CouponResources {
        /**
         * 获取优惠券的列表(需要登录态)，
         *
         * @return 获取优惠券的列表
         */
        Result<CouponListResponse> fetchCouponList(@Nullable Long categoryVendorId);

        /**
         * 获取优惠券的列表(无需登录态)
         *
         * @return 获取优惠券的列表
         */
        Result<CouponListResponse> fetchCouponListWithoutLogin(@Nullable Long categoryVendorId);

        /**
         * 领用优惠券
         *
         * @param configurationId 优惠券编号
         * @return true/false
         */
        Result<CouponResponse> gainCoupon(long configurationId);

        /**
         * 通过优惠码领取优惠券
         * @param couponAlias   优惠码
         * @return
         */
        Result<BooleanResponse> gainCouponByAlias(String couponAlias);

        /**
         * 获取优惠码优惠券详情
         * @param couponAlias   优惠码
         * @return
         */
        Result<CouponResponse> searchCouponByAlias(String couponAlias,@Nullable Long couponId);

        /**
         * 查询优惠券详情
         *
         * @param configurationId 优惠券编号
         * @return 优惠券详情
         */
        Result<CouponResponse> searhCoupon(long configurationId,@Nullable Long userCouponId);

        /**
         * 查询优惠券详情
         *
         * @param configurationId 优惠券编号
         * @return 优惠券详情
         */
        Result<CouponResponse> searhCouponWithoutLogin(long configurationId,Long userCouponId);

        /**
         * 查询我的优惠券
         * @return
         */
        Result<CouponListResponse> searchMyCoupon();

        /**
         * 查询我的优惠码
         * @return
         */
        Result<CouponAliasListResponse> searchMyCouponAlias();

        /**
         * 查询优惠券商品
         */
        Result<CouponProductResponse> fetchProduct(long couponId,@Nullable Long userCouponId);

        /**
         * 查询优惠券商品
         */
        Result<CouponProductResponse> fetchProductByAlias(String couponAlias);

        /**
         * 获取推荐券列表
         * @return
         */
        Result<CouponListResponse> fetchRecomCoupon();

        /**
         * 获取优惠券类型
         * @return
         */
        Result<CouponTypeListResponse> getCouponType();
    }

}
