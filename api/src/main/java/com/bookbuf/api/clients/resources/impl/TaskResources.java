package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface TaskResources {

    /**
     * @return 获取任务列表
     */
    Result<TaskContextResponse> fetchTaskList();

}
