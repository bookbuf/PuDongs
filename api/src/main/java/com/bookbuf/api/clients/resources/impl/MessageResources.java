package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.message.MessageResponse;
import com.ipudong.core.Result;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/27.
 */
public interface MessageResources {

    /**
     * 获取系统消息列表
     *
     * @return 消息列表
     */
    Result<List<MessageResponse>> fetchSystemMessages();

    /**
     * 获取消息详情
     *
     * @param messageId 消息编号
     * @return 消息详情
     */
    Result<MessageResponse> fetchDetailMessage(long messageId);

    /**
     * @return 获取消息中心
     */
    Result<LongResponse> fetchMessageCenter();
}
