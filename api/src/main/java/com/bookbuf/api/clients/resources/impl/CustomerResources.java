package com.bookbuf.api.clients.resources.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.bookbuf.api.responses.impl.customer.UserDocumentResponse;
import com.ipudong.core.Result;

public interface CustomerResources {

    /**
     * @param mobile 手机号码
     * @param code   验证码
     * @param pushId 推送 id
     * @return 登录结果
     */
    Result<CustomerLoginResponse> login(@NonNull String mobile, @NonNull String code, @Nullable String pushId);

    Result<CustomerLoginResponse> wxBindLogin(@NonNull String mobile, @NonNull String code, @Nullable String openid, @Nullable String pushId);

    /**
     * @param vendorId 合作商 id
     * @return 检测记录
     */
    Result<DetectionListResponse> fetchDetectionList(@NonNull int vendorId);

    Result<DetectionListResponse> fetchDetectionListByType(String type, int page, int pageSize);

    Result<UserDocumentResponse> fetchUserDocument();

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 不限制调用者身份。
     *
     * @return 是否注销成功
     */
    Result<BooleanResponse> logout();

    Result<BooleanResponse> modifyProfile(String nickName,String gender,String idCard,String email,String province,String city,String distinct,String avatar);

    Result<ProfileResponse> fetchProfile();

    Result<BooleanResponse> updatePassword(String oldPwd, String newPwd);

    Result<BooleanResponse> getRecommendNotice();

    Result<BooleanResponse> updateRecommend();

    Result<BooleanResponse> createInvite(Long fromUid,int deviceType,String deviceUuid,Long receiveUid);
}
