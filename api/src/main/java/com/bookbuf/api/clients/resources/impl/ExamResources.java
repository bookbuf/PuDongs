package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface ExamResources {

    /**
     * 开始指定试卷编号的考试
     *
     * @param paperId 试卷编号
     * @return 考试详情
     */
    Result<ExamResponse> beginExam(long paperId);

    /**
     * 获取考试状态
     *
     * @param examId 考试编号
     * @return 考试详情
     */
    Result<ExamResponse> getExamStatus(long examId);

    /**
     * 终止指定试卷编号的考试
     *
     * @param examId 考试编号
     * @return 终止操作是否成功
     */
    Result<BooleanResponse> terminateExam(long examId);

    /**
     * 考试结果，不计入成就
     *
     * @param examId  考试编号
     * @param answers 考试答案
     * @return 考试详情
     */
    Result<ExamResponse> handleInExam(long examId, String answers);

    /**
     * XXX: 该接口的出现是因为服务端的表结构设计存在缺陷
     * 考试结果，关联课程但是不计入成就（非培训成就的课程）
     *
     * @param examId   考试编号
     * @param answers  考试答案
     * @param courseId 课程编号
     * @return 考试详情
     */
    Result<ExamResponse> handleInExam(long examId, String answers, long courseId);

    /**
     * XXX: 该接口的出现是因为服务端的表结构设计存在缺陷
     * 考试结果，关联课程并且计入成就（培训成就的课程）
     *
     * @param examId   考试编号
     * @param answers  考试答案
     * @param courseId 课程编号
     * @param levelId  培训成就编号
     * @return 考试详情
     */
    Result<ExamResponse> handleInExam(long examId, String answers, long courseId, long levelId);

    /**
     * 获取考试详情，包含作答答案。
     *
     * @param examId 考试编号
     * @return 考试详情
     */
    Result<ExamResponse> fetchExamHistory(long examId);
}
