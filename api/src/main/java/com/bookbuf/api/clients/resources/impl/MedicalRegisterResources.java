package com.bookbuf.api.clients.resources.impl;


import com.bookbuf.api.responses.impl.medical.DepartmentResponse;
import com.bookbuf.api.responses.impl.medical.DoctorResponse;
import com.bookbuf.api.responses.impl.medical.DoctorScheduleResponse;
import com.bookbuf.api.responses.impl.medical.HospitalResponse;
import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.ipudong.core.Result;

import java.util.List;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface MedicalRegisterResources {

    /**
     * 发送挂号成功的短信
     *
     * @param id 挂号编号
     * @return 挂号信息
     */
    Result<MedicalRegisterResponse> sendSMS(long id);

    /**
     * 执行挂号
     *
     * @param hospitalId       医院编号
     * @param lockQueueNo      锁定的队列号
     * @param scheduleItemCode 门诊排班编号
     * @param userId           用户编号
     * @return 挂号信息
     */
    Result<MedicalRegisterResponse> bookService(long hospitalId, int lockQueueNo, String scheduleItemCode, long userId);

    /**
     * 列出医生的排班计划
     *
     * @param hospitalId     医院编号
     * @param departmentCode 部门编号
     * @param doctorCode     医生编号
     * @return 医生的排班计划列表
     */
    Result<List<DoctorScheduleResponse>> listDoctorSchedules(long hospitalId, int departmentCode, int doctorCode);

    /**
     * 列出已挂号成功的列表
     *
     * @param mobile 手机号码
     * @return 挂号信息列表
     */
    Result<List<MedicalRegisterResponse>> listMedicalRegisters(String mobile);

    /**
     * @return 医院信息列表
     */
    Result<List<HospitalResponse>> listHospitals();

    /**
     * 列出医生列表
     *
     * @param hospitalId     医院编号
     * @param departmentCode 部门编号
     * @return 医生列表
     */
    Result<List<DoctorResponse>> listDoctors(long hospitalId, long departmentCode);

    /**
     * 列出部门列表
     *
     * @param hospitalId 医院编号
     * @return 部门列表
     */
    Result<List<DepartmentResponse>> listDepartments(long hospitalId);
}
