package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.examination.FetchDiscoverQuestionResponse;
import com.ipudong.core.Result;

/**
 * Created by bo.wei on 2016/11/23.
 */

public interface DiscoverResources {

    /**
     * 取消收藏
     *
     * @param id 文章编号
     * @return 取消操作是否成功
     */
    Result<BooleanResponse> cancelStoreArticle(long id);

    /**
     * 获取文章关联的问题
     *
     * @param id 文章编号
     * @return 文章关联的问题
     */
    Result<FetchDiscoverQuestionResponse> fetchDiscoverQuestion(long id);

    /**
     * 回答文章关联的问题
     *
     * @param questionId 问题编号
     * @param answers    问题答案
     * @return 问题是否回答正确
     */
    Result<BooleanResponse> answerQuestionDiscovery(long questionId, String answers);

    /**
     * 收藏文章
     *
     * @param id 文章编号
     * @return 收藏操作是否成功
     */
    Result<BooleanResponse> storeArticle(long id);
}
