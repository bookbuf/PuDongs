package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperWithExamResponse;
import com.ipudong.core.Result;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface ExamPaperResources {

    /**
     * 获取试卷
     *
     * @param paperId 试卷编号
     * @return 试卷详情
     */
    Result<ExamPaperResponse> fetchExamPaper(long paperId);

    /**
     * 查询试卷列表
     *
     * @return 试卷列表
     */
    Result<List<ExamPaperWithExamResponse>> fetchExamPaperWithExam();
}
