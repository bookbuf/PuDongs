package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface TrainResources {

    /**
     * 获取课程报名的培训详情
     *
     * @param courseId 课程编号
     * @param levelId  成就编号
     * @return 培训详情
     */
    Result<FetchTrainResponse> fetchTrain(long courseId, long levelId);

    /**
     * 获取课程报名的培训详情
     *
     * @param courseId 课程编号
     * @return 培训详情
     */
    Result<FetchTrainResponse> fetchTrain(long courseId);

    /**
     * 报名培训
     *
     * @param courseId 课程编号
     * @return 培训详情
     */
    Result<BooleanResponse> signUpTrain(long courseId);

    /**
     * 获取已报名的培训内含有的所有课程列表
     *
     * @return 课程详情
     */
    Result<FetchSignUpCourseResponse> fetchSignUpCourse();

    /**
     * 获取课件详情
     *
     * @param courseId 课程编号
     * @param levelId  培训编号
     * @return 课件详情
     */
    Result<FetchLessonResponse> fetchLesson(long courseId, long levelId);

    /**
     * 获取课件详情
     *
     * @param courseId 课程编号
     * @return 课件详情
     */
    Result<FetchLessonResponse> fetchLesson(long courseId);


}
