package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.measure.add.AddRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.LatestMeasureResponse;
import com.bookbuf.api.responses.impl.measure.list.ListRecordResponse;
import com.ipudong.core.Result;


/**
 * Created by bo.wei on 2016/11/23.
 */

public interface MeasureResources {

    /**
     * 添加检测记录
     *
     * @param userId        用户编号
     * @param detectionCode 检测项目编号
     * @param values        检测结果使用JSON包装
     * @param longitude     经度
     * @param latitude      纬度
     * @param elevation     高度
     * @return 检测记录添加状态，包含结果判定与建议
     */
    Result<AddRecordResponse> addMeasureRecord(long userId, String detectionCode, String values, double longitude, double latitude, double elevation);

    /**
     * 列出最新一次的检测指标
     *
     * @param userId 用户编号
     * @return 每个检测项的最新一次的检测指标
     */
    Result<LatestMeasureResponse> listLatestMeasureRecord(long userId);

    /**
     * 列出某检测项的检测指标列表
     *
     * @param userId   用户编号
     * @param typeCode 指标类型
     * @param page     指定页码
     * @param pageSize 页大小
     * @return 检测指标列表
     */
    Result<ListRecordResponse> listMeasureRecord(long userId, String typeCode, int page, int pageSize);
}
