package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.ipudong.core.Result;

public interface HealthLiveResources {

    interface Customer extends HealthLiveResources {

        // 获取分类列表
        Result<CategoryListResponse> fetchCategory();

        // 获取分类下的专辑列表
        Result<FetchHealthLiveListResponse> fetchHealthLiveListByCategoryId(/*long categoryId*/);

        // 获取某篇文章及相关联的文章
        Result<FetchRelatedHealthLiveListResponse> fetchHealthLiveListByLiveId(long liveId);

    }


}
