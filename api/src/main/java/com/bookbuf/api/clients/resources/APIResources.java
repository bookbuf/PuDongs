package com.bookbuf.api.clients.resources;

/**
 * author: robert.
 * date :  16/8/2.
 */
public class APIResources {


    public enum Global {

        /*全局*/
        CLERK_LOGIN("pd.general.user.login", "1.0"),
        GLOBAL_SPLASH_SCREEN("pd.global.splashscreen", "1.0"),
        CLERK_CHECK_UPGRADE("pd.update.getinfo", "3.0"),
        CHECK_USER_BY_QUERY("pd.user.checkUserByQuery", "1.0"),
        SEND_NOTICE("pd.generic.notice", "1.0"),
        SEND_VERIFY_CODE("pd.general.verify.sendVerifyCode", "1.0"),
        CHECK_VERIFY_CODE("pd.general.verify.checkVerifyCode", "1.0"),
        GET_VERIFY_CODE("pd.verifycode.get","1.0"),
        GLOBAL_LOGIN("pd.general.user.login","1.0"),

        CLERK_RESET_PASSWORD("pd.user.resetPassword", "1.0"),

        //三方登录绑定
        GLOBAL_THIRTY_LOGIN("pd.generic.thirty.login","1.0"),
        GLOBAL_THIRTY_BIND("pd.generic.thirty.bind","1.0"),

        //上传真实头像
        GLOBAL_POST_REAL_AVATAR("pd.user.verified.photo","1.0"),

        //注册用户
        CREATE_USER("pd.user.createUser","4.0");



        String method = null;
        String version = null;

        Global(String method, String version) {
            this.method = method;
            this.version = version;
        }

        public String method() {
            return method;
        }

        public String version() {
            return version;
        }
    }

    public enum C {

        // 优惠券
        COUPON_SHARE("pd.coupon.share", "1.0"),
        COUPON_GAIN("pd.c.coupon.gain", "1.0"),
        COUPON_SEARCH("pd.c.coupon.search", "3.0"),
        COUPON_SEARH_COUPON("pd.c.coupon.dtl", "1.0"),
        COUPON_GAIN_BY_ALIAS("pd.coupon.gainByAlias", "4.0"),
        COUPON_SEARH_COUPON_BY_ALIAS("pd.coupon.searchCouponByAlias", "5.0"),
        COUPON_SEARCH_MY_COUPON("pd.coupon.searchMyCoupon","2.0"),
        COUPON_SEARCH_MY_COUPON_ALIAS("pd.coupon.searchMyCouponAlias","4.0"),
        COUPON_PRODUCT("pd.coupon.product","5.0"),
        COUPON_PRODUCTBYALIAS("pd.coupon.product","4.0"),
        COUPON_CATEGORY("pd.c.coupon.category","2.0"),
        COUPON_RECOM("pd.c.coupon.recom","1.0"),

        //积分
        MY_INTEGRAL("pd.user.integral","1.0"),
        CUT_INTEGRAL("pd.coupon.award","1.0"),

        //邀请码
        CREATE_INVITE("pd.inviteRecord.creat","1.0"),

        // 支付
        ORDER_PAY("pd.c.order.gainPayOrder","2.0"),
        ORDER_DETAIL("pd.c.order.requestOrderDetail","1.0"),
        ORDER_GENERATE("pd.c.thirdpay.generateOrder","1.0"),
        ORDER_RESULT("pd.c.thirdpay.payResult","1.0"),

        // 健康检测首页
        C_DETECTION_INDEX("pd.c.index", "2.0"),
        C_DETECTION_FIND_LAST("pd.c.findLast", "2.0"),
        C_DETECTION_FIND_LIST("pd.c.findList", "3.0"),
        C_DETECTION_USER_RECORD("pd.c.userRecord", "2.0"),

        //  账户
        C_PROFILE_FETCH("pd.c.profile.personalInfo","2.0"),
        C_PROFILE_MODIFY("pd.c.profile.setting","1.0"),

        // 登录
        C_USER_LOGIN("pd.user.login", "1.0"),
        C_WX_BIND("pd.consumer.bind", "1.0"),

        // 推荐券推送消息
        C_GET_RECOMMEND_NOTICE("pd.msgCount.findByUserId","1.0"),
        C_UPDATE_RECOMMEND("pd.msgCount.updateMsgCountByUserId","1.0"),
        C_VERIFIED_FETCHCOUPON("pd.coupon.verified.fetchCoupon","1.0"),

        // 健康生活
        C_LIVE_FETCH("pd.live.fetch", "1.0"),
        C_LIVE_CATEGORY("pd.live.category", "1.0"),
        C_UPDATE_PASSWORD("pd.user.updatePassword", "1.0"),
        C_LIVE_DETAIL("pd.live.detail", "2.0");
//        C_LIVE_ALBUM("pd.live.album", "1.0");



        String method = null;
        String version = null;

        C(String method, String version) {
            this.method = method;
            this.version = version;
        }

        public String method() {
            return method;
        }

        public String version() {
            return version;
        }
    }

    public enum B {

        /*优惠券*/
        // 通过优惠券的优惠码核查优惠券的状态
        COUPON_STATUS("pd.coupon.fetch", "2.0"),
        // 核销优惠券
        COUPON_VERIFY("pd.coupon.verify", "1.0"),
        COUPON_CODE_LIST("pd.coupon.codeList","1.0"),
        COUPON_FETCH_COUPON_ALIAS("pd.coupon.fetchCouponAlias","1.0"),

        /*店员*/
        CHECK_QUERY_ROLE("pd.getClerkRole", "4.0"),
        SEND_CLERK_VERIFY_CODE("pd.verify.sendClerkCode", "1.0"),
        CLERK_FETCH_BANNER("pd.bp.fetchbanners", "1.0"),
        CLERK_QUERY_CUSTOMER("pd.b.user.loginCustomer", "4.0"),     //新增资料完整性查询
        CLERK_UPDATE_PASSWORD("pd.user.updatePassword", "1.0"),
        CLERK_LOGOUT("pd.user.logout", "1.0"),
        CLERK_REGISTER("pd.user.createUser", "3.0"),
        CLERK_BIND_VENDOR("pd.bindClerkManager", "2.0"),
        CLERK_QUERY_PROFILE("pd.b.clerk.profile", "3.0"),
        CLERK_QUERY_CUSTOMER_PROFILE("pd.b.user.getProfile", "3.0"),
        CLERK_UPDATE_PROFILE("pd.user.updateUserProfile", "3.0"),
        CLERK_GET_PERMISSION("pd.clerk.permission","1.0"),
        CREATE_CUSTOMER("pd.b.user.createCustomer", "3.0"),
        /*任务*/
        TASK_CONTEXT("pd.task.context", "1.0"),
        /*问题*/
        QUESTION_ANSWER("pd.question.answer", "1.0"),
        QUESTION_FETCH("pd.question.fetch", "1.0"),
        /*考卷*/
        PAPER_FETCH("pd.paper.fetch", "1.0"),
        PAPER_FETCH_WITH_EXAM("pd.paper.fetchList", "1.0"),

        /*考试*/
        EXAM_BEGIN("pd.exam.begin", "1.0"),
        EXAM_STATUS("pd.exam.status", "1.0"),
        EXAM_TERMINATE("pd.exam.terminate", "1.0"),
        EXAM_HANDLE_IN("pd.exam.handleIn", "2.0"),
        EXAM_HISTORY("pd.exam.history", "1.0"),

        /*我的消息*/
        MESSAGE_SYSTEM_LIST("pd.msg.fetchSystemMessages", "1.0"),
        MESSAGE_DETAIL("pd.msg.fetchDetailMessage", "1.0"),
        MESSAGE_DISTRIBUTION("pd.msg.fetchMessageCenter", "1.0"),
        /*挂号*/
        MEDICAL_REGISTER_MESSAGE_SEND("pd.bp.guahao.sms", "1.0"),
        MEDICAL_REGISTER_BOOK_SERVICE("pd.b.guahao.bookService", "2.0"),
        MEDICAL_REGISTER_QUERY_SCHEDULE("pd.b.guahao.queryAdmSchedule", "2.0"),
        MEDICAL_REGISTER_SEARCH("pd.bp.guahao.search", "1.0"),
        MEDICAL_REGISTER_LIST_HOSPITAL("pd.b.guahao.getHospitalList", "2.0"),
        MEDICAL_REGISTER_QUERY_DOCTOR_GROUP("pd.b.guahao.queryDoctorGroup", "2.0"),
        MEDICAL_REGISTER_QUERY_DEPARTMENT_GROUP("pd.b.guahao.queryDepartmentGroup", "2.0"),

        /*回访*/
        CALL_CALL_CANCEL("pd.p.callCancel", "2.0"),
        CALL_CALL("pd.p.callback", "3.0"),

        //视频监控
        VIDEO_MONITOR_COUNT("",""),

        /*发现*/
        DISCOVERY_QUESTION_SUBMIT("pd.discovery.createExaminationRecord", "1.0"),
        DISCOVERY_STORE_ARTICLE("pd.bp.discovery.storeArticle", "1.0"),
        DISCOVERY_CANCEL_FAVORITE("pd.bp.discovery.cancelArticle", "1.0"),
        DISCOVERY_FETCH_QUESTION("pd.discovery.fetchExaminationDObyArticleId", "1.0"),

        /*我的消息*/
        // 检测合作商是否开启考勤任务
        ATTENDANCE_PREFERENCE("pd.attendance.preference", "1.0"),
        // 查询某月考勤记录
        ATTENDANCE_MONTH("pd.attendance.month", "1.0"),
        // 查询某天的考勤记录
        ATTENDANCE_DAY("pd.attendance.day", "1.0"),
        // 获取当日的考勤上下文，与接口pd.attendance.day部分相似
        ATTENDANCE_CONTEXT("pd.attendance.context", "1.0"),
        // 申请签卡（包括签到、签退）
        ATTENDANCE_APPLY("pd.attendance.apply", "1.0"),
        // 使用当下的经纬度查询签卡状态（注意：只查询状态不执行签卡）
        ATTENDANCE_STATUS("pd.attendance.status", "1.0"),

		/*检测*/

        MEASURE_ADD_RECORD("pd.indicator.addByClerk", "2.0"),
        MEASURE_LIST_RECORD("pd.b.valuesMapi.valueList", "1.0"),
        MEASURE_LATEST_FOR_B("pd.indicator.lastForB", "3.0"),

        /*成就*/
        ACHIEVEMENT_FETCH("pd.achievement.fetch", "1.0"),
        ACHIEVEMENT_DETAIL("pd.achievement.detail", "1.0"),
        /*课程*/
        COURSE_FETCH("pd.course.fetch", "1.0"),
        /*课件*/
        LESSON_FETCH("pd.lesson.fetch", "1.0"),
        ARTICLE_FETCH("pd.article.getArticle", "1.0"),
        /*培训*/
        TRAIN_DETAIL("pd.train.detail", "1.0"),
        /*培训报名*/
        TRAIN_SIGN_UP("pd.train.signup", "1.0"),
        GET_VERIFIED_COUPON("pd.coupon.verified.fetchCoupon","1.0"),
        /*新建会员&&完善信息*/
        USER_LOGIN("pd.user.login", "1.0"),
        CARD_BIND("pd.member.card.bind","1.0");     //绑卡


        String method = null;
        String version = null;

        B(String method, String version) {
            this.method = method;
            this.version = version;
        }

        public String method() {
            return method;
        }

        public String version() {
            return version;
        }
    }
}
