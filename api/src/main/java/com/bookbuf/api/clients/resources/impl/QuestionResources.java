package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface QuestionResources {

    /**
     * 获取问题详情，与{@link DiscoverResources#fetchDiscoverQuestion(long)}功能上重复。
     *
     * @param articleId 文章编号
     * @return
     */
    Result<QuestionResponse> fetchQuestion(long articleId);

    /**
     * @param questionId 题目编号
     * @param answers    作答答案，采用字符串形式"ABC"表示选中了"ABC"
     * @return 作答结果判定：正确或错误
     */
    Result<BooleanResponse> answerQuestion(long questionId, String answers);

}
