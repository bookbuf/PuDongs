package com.bookbuf.api.clients.resources.impl;


import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.integral.IntegralListResponse;
import com.ipudong.core.Result;


/**
 * Created by bo.wei on 2016/11/23.
 * 积分
 */

public interface IntegralResources {

    /**
     * 查询积分List
     * @return
     */
    Result<IntegralListResponse> fetchIntegral();

    /**
     * 查询积分是否扣除
     * @return
     */
    Result<BooleanResponse> cutIntegral(long userCouponId);
}
