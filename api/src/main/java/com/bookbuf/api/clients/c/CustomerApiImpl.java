package com.bookbuf.api.clients.c;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bookbuf.api.apis.ClerkAPI;
import com.bookbuf.api.apis.CouponAPI;
import com.bookbuf.api.apis.CustomerAPI;
import com.bookbuf.api.apis.GlobalAPI;
import com.bookbuf.api.apis.HealthLiveAPI;
import com.bookbuf.api.apis.IntegralAPI;
import com.bookbuf.api.apis.OrderAPI;
import com.bookbuf.api.clients.ApiClubImpl;
import com.bookbuf.api.clients.resources.APIResources;
import com.bookbuf.api.clients.resources.impl.CouponResources;
import com.bookbuf.api.clients.resources.impl.CustomerResources;
import com.bookbuf.api.clients.resources.impl.GlobalResources;
import com.bookbuf.api.clients.resources.impl.HealthLiveResources;
import com.bookbuf.api.clients.resources.impl.OrderResources;
import com.bookbuf.api.configurations.IConfiguration;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeListResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.bookbuf.api.responses.impl.customer.UserDocumentResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.bookbuf.api.responses.impl.integral.IntegralListResponse;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.bookbuf.api.responses.impl.order.OrderPayResponse;
import com.bookbuf.api.responses.impl.order.OrderResultResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.ipudong.core.Result;
import com.ipudong.core.network.response.impl.HealthbokResponse;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public class CustomerApiImpl extends ApiClubImpl implements CustomerApi {
    public CustomerApiImpl(IConfiguration configuration) {
        super(configuration);
    }

    @Override
    public CustomerResources customerResources() {
        return this;
    }

    @Override
    public CouponResources.Customer couponResources() {
        return this;
    }

    @Override
    public GlobalResources.Customer globalResources() {
        return this;
    }

    @Override
    public HealthLiveResources.Customer healthLiveResources() {
        return this;
    }

    @Override
    public OrderResources orderResources() {
        return this;
    }

    @Override
    public boolean checkSessionId(String outSideSessionId) {
        return outSideSessionId != null && outSideSessionId.equals(getSessionId());
    }

    @Override
    public void updateConfiguration(IConfiguration configuration) {
        super.updateIConfiguration(configuration);
    }

    @Override
    public Result<CredentialResponse> loginTest(String target, String password, String pushId) {
        HealthbokResponse response = api(ClerkAPI.class).loginTest(
                getAppSecret(),
                APIResources.Global.CLERK_LOGIN.method(),
                APIResources.Global.CLERK_LOGIN.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                target,
                password,
                pushId,
                "close");
        return parser().createCredentialResponse(response.getResponse());
    }

    @Override
    public Result<CustomerLoginResponse> login(@NonNull String mobile, @NonNull String code, @Nullable String pushId) {
        HealthbokResponse response = api(com.bookbuf.api.apis.CustomerAPI.class).login(
                getAppSecret(),
                APIResources.C.C_USER_LOGIN.method(),
                APIResources.C.C_USER_LOGIN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                code,
                pushId
        );
        return parser().createCustomerLoginResponse(response.getResponse());
    }

    @Override
    public Result<CustomerLoginResponse> globalLogin(String userName, String pushId, String password) {
        HealthbokResponse response = api(GlobalAPI.class).globalLogin(
                getAppSecret(),
                APIResources.Global.GLOBAL_LOGIN.method(),
                APIResources.Global.GLOBAL_LOGIN.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userName,
                pushId,
                password
        );
        return parser().createCustomerLoginResponse(response.getResponse());
    }

    @Override
    public Result<CustomerLoginResponse> thirtyLogin(String uniqueId, String sign, String pushId) {
        HealthbokResponse response = api(GlobalAPI.class).thirtyLogin(
                getAppSecret(),
                APIResources.Global.GLOBAL_THIRTY_LOGIN.method(),
                APIResources.Global.GLOBAL_THIRTY_LOGIN.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                uniqueId,
                sign,
                pushId
        );
        return parser().createCustomerLoginResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> sendVerifyCode(String mobile, @SignDef String sign) {
        HealthbokResponse response = api(GlobalAPI.class).sendVerifyCode(
                getAppSecret(),
                APIResources.Global.SEND_VERIFY_CODE.method(),
                APIResources.Global.SEND_VERIFY_CODE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> thirtyBind(String mobile, String uniqueId, String code, String sign) {
        HealthbokResponse response = api(GlobalAPI.class).thirtyBind(
                getAppSecret(),
                APIResources.Global.GLOBAL_THIRTY_BIND.method(),
                APIResources.Global.GLOBAL_THIRTY_BIND.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                uniqueId,
                sign,
                code
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> resetPassword(String mobile, String verifyCode, String newPwd, String sign) {
        HealthbokResponse response = api(GlobalAPI.class).resetPassword(
                getAppSecret(),
                APIResources.Global.CLERK_RESET_PASSWORD.method(),
                APIResources.Global.CLERK_RESET_PASSWORD.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                verifyCode,
                newPwd,
                sign
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<CustomerLoginResponse> wxBindLogin(@NonNull String mobile, @NonNull String code, @Nullable String openid, @Nullable String pushId) {
        HealthbokResponse response = api(com.bookbuf.api.apis.CustomerAPI.class).wxLogin(
                getAppSecret(),
                APIResources.C.C_WX_BIND.method(),
                APIResources.C.C_WX_BIND.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                code,
                openid,
                pushId
        );
        return parser().createCustomerLoginResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> modifyProfile(String nickName, String gender, String idCard, String email, String province, String city, String distinct, String avatar) {
        HealthbokResponse response = api(com.bookbuf.api.apis.CustomerAPI.class).modifyProfile(
                getAppSecret(),
                APIResources.C.C_PROFILE_MODIFY.method(),
                APIResources.C.C_PROFILE_MODIFY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                nickName,
                gender,
                idCard,
                email,
                province,
                city,
                distinct,
                avatar
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<ProfileResponse> fetchProfile() {
        HealthbokResponse response = api(com.bookbuf.api.apis.CustomerAPI.class).fetchProfile(
                getAppSecret(),
                APIResources.C.C_PROFILE_FETCH.method(),
                APIResources.C.C_PROFILE_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createProfileResponse(response.getResponse());
    }

    @Override
    public Result<DetectionListResponse> fetchDetectionList(@NonNull int vendorId) {
        HealthbokResponse response = api(com.bookbuf.api.apis.CustomerAPI.class).fetchDetectionIndex(
                getAppSecret(),
                APIResources.C.C_DETECTION_INDEX.method(),
                APIResources.C.C_DETECTION_INDEX.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                vendorId
        );
        return parser().createDetectionListResponse(response.getResponse());
    }

    @Override
    public Result<CouponResponse> gainCoupon(long configurationId) {
        HealthbokResponse response = api(CouponAPI.class).gain(
                getAppSecret(),
                APIResources.C.COUPON_GAIN.method(),
                APIResources.C.COUPON_GAIN.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                configurationId
        );
        return parser().createCouponResponse(response.getResponse());
    }

    @Override
    public Result<CouponListResponse> fetchCouponList(@Nullable Long categoryVendorId) {
        HealthbokResponse response = api(CouponAPI.class).fetchList(
                getAppSecret(),
                APIResources.C.COUPON_SEARCH.method(),
                APIResources.C.COUPON_SEARCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                categoryVendorId

        );
        return parser().createCouponListResponse(response.getResponse());
    }

    @Override
    public Result<CouponListResponse> fetchCouponListWithoutLogin(@Nullable Long categoryVendorId) {
        HealthbokResponse response = api(CouponAPI.class).fetchList(
                getAppSecret(),
                APIResources.C.COUPON_SEARCH.method(),
                APIResources.C.COUPON_SEARCH.version(),
                null,
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                categoryVendorId
        );
        return parser().createCouponListResponse(response.getResponse());
    }

    @Override
    public Result<CouponResponse> searhCoupon(long configurationId,@Nullable Long userCouponId) {
        HealthbokResponse response = api(CouponAPI.class).searhCoupon(
                getAppSecret(),
                APIResources.C.COUPON_SEARH_COUPON.method(),
                APIResources.C.COUPON_SEARH_COUPON.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                configurationId,
                userCouponId
        );
        return parser().createCouponResponse(response.getResponse());
    }

    @Override
    public Result<CouponResponse> searhCouponWithoutLogin(long configurationId,@Nullable Long userCouponId) {
        HealthbokResponse response = api(CouponAPI.class).searhCoupon(
                getAppSecret(),
                APIResources.C.COUPON_SEARH_COUPON.method(),
                APIResources.C.COUPON_SEARH_COUPON.version(),
                null,
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                configurationId,
                userCouponId
        );
        return parser().createCouponResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> checkVerifyCode(String mobile, String sign, String code) {
        HealthbokResponse response = api(GlobalAPI.class).checkVerifyCode(
                getAppSecret(),
                APIResources.Global.CHECK_VERIFY_CODE.method(),
                APIResources.Global.CHECK_VERIFY_CODE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign,
                code
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<AppUpgradeResponse> fetchAppUpgrade(String appName) {
        HealthbokResponse response = api(GlobalAPI.class).fetchAppUpgrade(
                getAppSecret(),
                APIResources.Global.CLERK_CHECK_UPGRADE.method(),
                APIResources.Global.CLERK_CHECK_UPGRADE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                appName);
        return parser().createAppUpgradeResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> sendNotice(String mobile, String sign, String type) {
        HealthbokResponse response = api(GlobalAPI.class).sendNotice(
                getAppSecret(),
                APIResources.Global.SEND_NOTICE.method(),
                APIResources.Global.SEND_NOTICE.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign,
                type);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> updatePassword(String oldPwd, String newPwd) {
        HealthbokResponse response = api(ClerkAPI.class).updatePassword(
                getAppSecret(),
                APIResources.C.C_UPDATE_PASSWORD.method(),
                APIResources.C.C_UPDATE_PASSWORD.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                oldPwd,
                newPwd);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<CategoryListResponse> fetchCategory() {
        HealthbokResponse response = api(HealthLiveAPI.class).fetchCategory(
                getAppSecret(),
                APIResources.C.C_LIVE_CATEGORY.method(),
                APIResources.C.C_LIVE_CATEGORY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createCategoryListResponse(response.getResponse());
    }

    @Override
    public Result<FetchHealthLiveListResponse> fetchHealthLiveListByCategoryId() {
        HealthbokResponse response = api(HealthLiveAPI.class).fetchHealthLiveList(
                getAppSecret(),
                APIResources.C.C_LIVE_FETCH.method(),
                APIResources.C.C_LIVE_FETCH.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createFetchHealthLiveListResponse(response.getResponse());
    }

    @Override
    public Result<FetchRelatedHealthLiveListResponse> fetchHealthLiveListByLiveId(long liveId) {
        HealthbokResponse response = api(HealthLiveAPI.class).fetchAlbum(
                getAppSecret(),
                APIResources.C.C_LIVE_DETAIL.method(),
                APIResources.C.C_LIVE_DETAIL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                liveId);
        return parser().createFetchRelatedHealthLiveListResponse(response.getResponse());
    }

    @Override
    public Result<DetectionListResponse> fetchDetectionListByType(String type, int page, int pageSize) {
        HealthbokResponse response = api(CustomerAPI.class).fetchDetectionList(
                getAppSecret(),
                APIResources.C.C_DETECTION_FIND_LIST.method(),
                APIResources.C.C_DETECTION_FIND_LIST.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                null,
                type,
                page,
                pageSize);
        return parser().createDetectionListResponse(response.getResponse());
    }

    @Override
    public Result<UserDocumentResponse> fetchUserDocument() {
        HealthbokResponse response = api(CustomerAPI.class).fetchUserDocument(
                getAppSecret(),
                APIResources.C.C_DETECTION_USER_RECORD.method(),
                APIResources.C.C_DETECTION_USER_RECORD.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                null
        );
        return parser().createUserDocumentResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> gainCouponByAlias(String couponAlias) {
        HealthbokResponse response = api(CouponAPI.class).gainByAlias(
                getAppSecret(),
                APIResources.C.COUPON_GAIN_BY_ALIAS.method(),
                APIResources.C.COUPON_GAIN_BY_ALIAS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponAlias
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<CouponResponse> searchCouponByAlias(String couponAlias,@Nullable Long couponId) {
        HealthbokResponse response = api(CouponAPI.class).searhCouponByAlias(
                getAppSecret(),
                APIResources.C.COUPON_SEARH_COUPON_BY_ALIAS.method(),
                APIResources.C.COUPON_SEARH_COUPON_BY_ALIAS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponAlias,
                couponId
        );
        return parser().createCouponResponse(response.getResponse());
    }

    @Override
    public Result<CouponListResponse> searchMyCoupon() {
        HealthbokResponse response = api(CouponAPI.class).searchMyCoupon(
                getAppSecret(),
                APIResources.C.COUPON_SEARCH_MY_COUPON.method(),
                APIResources.C.COUPON_SEARCH_MY_COUPON.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createCouponListResponse(response.getResponse());
    }


    @Override
    public Result<CouponAliasListResponse> searchMyCouponAlias() {
        HealthbokResponse response = api(CouponAPI.class).searchMyCouponAlias(
                getAppSecret(),
                APIResources.C.COUPON_SEARCH_MY_COUPON_ALIAS.method(),
                APIResources.C.COUPON_SEARCH_MY_COUPON_ALIAS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createCouponAliasListResponse(response.getResponse());
    }

    @Override
    public Result<CouponProductResponse> fetchProduct(long couponId,@Nullable Long userCouponId) {
        HealthbokResponse response = api(CouponAPI.class).fetchProduct(
                getAppSecret(),
                APIResources.C.COUPON_PRODUCT.method(),
                APIResources.C.COUPON_PRODUCT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponId,
                userCouponId
        );
        return parser().createFetchProductResponse(response.getResponse());
    }

    @Override
    public Result<CouponProductResponse> fetchProductByAlias(String couponAlias) {
        HealthbokResponse response = api(CouponAPI.class).fetchProductByAlias(
                getAppSecret(),
                APIResources.C.COUPON_PRODUCTBYALIAS.method(),
                APIResources.C.COUPON_PRODUCTBYALIAS.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponAlias
        );
        return parser().createFetchProductResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> logout() {
        HealthbokResponse response = api(ClerkAPI.class).logout(
                getAppSecret(),
                APIResources.B.CLERK_LOGOUT.method(),
                APIResources.B.CLERK_LOGOUT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation());
        return parser().createBooleanResponse(response.getResponse());
    }

    //    订单
    @Override
    public Result<OrderPayResponse> orderPay(String couponId) {
        HealthbokResponse response = api(OrderAPI.class).orderPay(
                getAppSecret(),
                APIResources.C.ORDER_PAY.method(),
                APIResources.C.ORDER_PAY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                couponId
        );
        return parser().createOrderPayResponse(response.getResponse());
    }

    @Override
    public Result<OrderPayResponse> orderPayByAlias(String promoCode) {
        HealthbokResponse response = api(OrderAPI.class).orderPayByAlias(
                getAppSecret(),
                APIResources.C.ORDER_PAY.method(),
                APIResources.C.ORDER_PAY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                promoCode
        );
        return parser().createOrderPayResponse(response.getResponse());
    }

    @Override
    public Result<OrderDetailResponse> orderDetail(String orderRequestId) {
        HealthbokResponse response = api(OrderAPI.class).orderDetail(
                getAppSecret(),
                APIResources.C.ORDER_DETAIL.method(),
                APIResources.C.ORDER_DETAIL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                orderRequestId
        );
        return parser().createOrderDetailResponse(response.getResponse());
    }

    @Override
    public Result<OrderGenerateResponse> orderGenerateResponse(String name, String mobile, String payType, String orderRequestId,int way) {
        HealthbokResponse response = api(OrderAPI.class).orderGenerate(
                getAppSecret(),
                APIResources.C.ORDER_GENERATE.method(),
                APIResources.C.ORDER_GENERATE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                name,
                mobile,
                payType,
                orderRequestId,
                way
        );
        return parser().createOrderGenerateResponse(response.getResponse());
    }

    /**
     *
     * @param orderRequestId        //fanhuicanshu
     * @param payType       //1WX,2ALI
     * @param requestId     //dingdan id
     * @return
     */
    @Override
    public Result<OrderResultResponse> orderResult(String orderRequestId,String payType,String requestId) {
        HealthbokResponse response = api(OrderAPI.class).orderResult(
                getAppSecret(),
                APIResources.C.ORDER_RESULT.method(),
                APIResources.C.ORDER_RESULT.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                orderRequestId,
                payType,
                requestId
        );
        return parser().createOrderResultResponse(response.getResponse());
    }

    @Override
    public Result<IntegralListResponse> fetchIntegral() {
        HealthbokResponse response = api(IntegralAPI.class).fetchIntegralList(
                getAppSecret(),
                APIResources.C.MY_INTEGRAL.method(),
                APIResources.C.MY_INTEGRAL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createIntergralListResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> cutIntegral(long userCouponId) {
        HealthbokResponse response = api(IntegralAPI.class).cutIntegral(
                getAppSecret(),
                APIResources.C.CUT_INTEGRAL.method(),
                APIResources.C.CUT_INTEGRAL.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                userCouponId
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<String> getVerifyCode(String mobile,String sign) {
        HealthbokResponse response = api(GlobalAPI.class).getVerifyCode(
                getAppSecret(),
                APIResources.Global.GET_VERIFY_CODE.method(),
                APIResources.Global.GET_VERIFY_CODE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                mobile,
                sign
        );
        return parser().createStringResponse(response.getResponse());
    }

    @Override
    public Result<CouponListResponse> fetchRecomCoupon() {
        HealthbokResponse response = api(CouponAPI.class).fetchRecomCouponList(
                getAppSecret(),
                APIResources.C.COUPON_RECOM.method(),
                APIResources.C.COUPON_RECOM.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createRecomCouponResponse(response.getResponse());
    }

    @Override
    public Result<CouponTypeListResponse> getCouponType() {
        HealthbokResponse response = api(CouponAPI.class).fetchCouponType(
                getAppSecret(),
                APIResources.C.COUPON_CATEGORY.method(),
                APIResources.C.COUPON_CATEGORY.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createCouponTypeResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> getRecommendNotice() {
        HealthbokResponse response = api(CustomerAPI.class).getRecommendNotice(
                getAppSecret(),
                APIResources.C.C_GET_RECOMMEND_NOTICE.method(),
                APIResources.C.C_GET_RECOMMEND_NOTICE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> updateRecommend() {
        HealthbokResponse response = api(CustomerAPI.class).getRecommendNotice(
                getAppSecret(),
                APIResources.C.C_UPDATE_RECOMMEND.method(),
                APIResources.C.C_UPDATE_RECOMMEND.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation()
        );
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> postRealAvatar(Long uid,String url,Long operationUid,Long vendorId) {
        HealthbokResponse response = api(GlobalAPI.class).postRealAvatar(
                getAppSecret(),
                APIResources.Global.GLOBAL_POST_REAL_AVATAR.method(),
                APIResources.Global.GLOBAL_POST_REAL_AVATAR.version(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                url,
                uid,
                operationUid,
                vendorId);
        return parser().createBooleanResponse(response.getResponse());
    }

    @Override
    public Result<BooleanResponse> createInvite(Long fromUid, int deviceType, String deviceUuid, Long receiveUid) {
        HealthbokResponse response = api(CustomerAPI.class).createInvite(
                getAppSecret(),
                APIResources.C.CREATE_INVITE.method(),
                APIResources.C.CREATE_INVITE.version(),
                getSessionId(),
                getAppId(),
                getAppVersion(),
                System.currentTimeMillis(),
                getSystemInformation(),
                fromUid,
                deviceType,
                deviceUuid,
                receiveUid);
        return parser().createBooleanResponse(response.getResponse());
    }
}
