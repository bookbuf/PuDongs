package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.clients.resources.APIResources;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.bookbuf.api.responses.impl.order.OrderPayResponse;
import com.bookbuf.api.responses.impl.order.OrderResultResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;

/**
 * Created by bo.wei on 2017/5/3.
 */

public interface OrderResources {
    Result<OrderPayResponse> orderPay(String couponId);

    Result<OrderPayResponse> orderPayByAlias(String promoCode);

    Result<OrderDetailResponse> orderDetail(String orderRequestId);

    Result<OrderGenerateResponse> orderGenerateResponse(String name,String mobile,String payType,String orderRequestId,int way);

    Result<OrderResultResponse> orderResult(String orderRequestId,String payType,String requestId);
}
