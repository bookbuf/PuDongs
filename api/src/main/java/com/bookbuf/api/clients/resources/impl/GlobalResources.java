package com.bookbuf.api.clients.resources.impl;

import android.support.annotation.StringDef;

import com.bookbuf.api.apis.GlobalAPI;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.customer.CreateUserResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.bookbuf.api.responses.impl.global.SplashScreenResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.ipudong.core.Result;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface GlobalResources {

    /**
     * 校对验证码是否正确
     *
     * @param mobile 手机号码
     * @param sign   验证码类型
     * @param code   验证码
     * @return 验证码校对是否通过
     */
    Result<BooleanResponse> checkVerifyCode(String mobile, String sign, String code);

    /**
     * 检查是否需要更新
     *
     * @param appName 应用程序在服务端定义的别名
     * @return 更新信息
     */
    Result<AppUpgradeResponse> fetchAppUpgrade(String appName);

    /**
     * 发送验证码
     *
     * @param mobile 手机号码
     * @param sign   验证码类型
     * @param type   发送方式，sms表示短信，voice表示语音
     * @return 验证码是否发送成功
     */
    Result<BooleanResponse> sendNotice(String mobile, @SignDef String sign,String type);

    /**
     * 发送验证码
     *
     * @param mobile 手机号码
     * @param sign   验证码类型
     * @return 验证码是否发送成功
     */
    Result<BooleanResponse> sendVerifyCode(String mobile, @SignDef String sign);

    /**
     * 获取验证码
     * @param mobile
     * @param sign
     * @return
     */
    Result<String> getVerifyCode(String mobile,String sign);

    Result<BooleanResponse> postRealAvatar(Long uid,String url,Long operationUid,Long vendorId);


    interface Business extends GlobalResources {
        /**
         * XXX:这个接口与上个接口重复了，需要服务端合并接口
         * 发送店员验证码
         *
         * @param mobile 手机号码
         * @param sign   验证码类型
         * @return 验证码是否发送成功
         */
        @Deprecated
        Result<BooleanResponse> sendClerkVerifyCode(String mobile, @SignDef String sign);

        /**
         * 检查用户是否存在。
         *
         * @param mobile 手机号码
         * @return 返回用户编号，若为-1则表明用户不存在。
         */
        Result<LongResponse> checkUserByQuery(String mobile);

        /**
         * 查询用户角色。
         *
         * @param mobile 手机号码
         * @return 返回用户角色，若为-1则表明该用户不存在。
         */
        Result<RoleResponse> queryRole(String mobile);

        /**
         * 用于描绘客户端响应服务端的某些特定行为的UI呈现及响应，该API返回结构与推送结构设计兼容。
         *
         * @return 闪页的结构
         */
        Result<SplashScreenResponse> querySplashScreen();

        Result<CreateUserResponse> createUser(String name, String mobile, String telephone, String idCard, String address, String birthday, String gender, String verifiedUrl);
    }

    interface Customer extends GlobalResources {

        Result<CredentialResponse> loginTest(String target, String password, String pushId);

        /**
         * 密码登录
         * @param userName      用户名，手机号
         * @param pushId        推送Id
         * @param password      密码
         * @return
         */
        Result<CustomerLoginResponse> globalLogin(String userName,String pushId,String password);

        /**
         * 三方登录
         * @param uniqueId      第三方唯一标识
         * @param sign      标识第三方来源，如wechat
         * @param pushId        推送id
         * @return
         */
        Result<CustomerLoginResponse> thirtyLogin(String uniqueId,String sign,String pushId);

        /**
         * 第三方绑定
         * @param mobile        手机
         * @param uniqueId      id
         * @param code          验证码
         * @param sign          标识第三方来源，如wechat
         * @return
         */
        Result<BooleanResponse> thirtyBind(String mobile,String uniqueId,String code,String sign);

        Result<BooleanResponse> resetPassword(String mobile,String verifyCode,String newPwd,String sign);



    }

    @StringDef({
            GlobalAPI.Sign.login,
            GlobalAPI.Sign.follow,
            GlobalAPI.Sign.register,
            GlobalAPI.Sign.forgot,
            GlobalAPI.Sign.clerk
    })
    @Retention(RetentionPolicy.SOURCE)
    @interface SignDef {
    }
}
