package com.bookbuf.api.clients.b;

import com.bookbuf.api.clients.resources.impl.AchievementResources;
import com.bookbuf.api.clients.resources.impl.AttendanceResources;
import com.bookbuf.api.clients.resources.impl.CallResources;
import com.bookbuf.api.clients.resources.impl.ClerkResources;
import com.bookbuf.api.clients.resources.impl.CouponResources;
import com.bookbuf.api.clients.resources.impl.CustomerResources;
import com.bookbuf.api.clients.resources.impl.DiscoverResources;
import com.bookbuf.api.clients.resources.impl.ExamPaperResources;
import com.bookbuf.api.clients.resources.impl.ExamResources;
import com.bookbuf.api.clients.resources.impl.GlobalResources;
import com.bookbuf.api.clients.resources.impl.HeWeatherResources;
import com.bookbuf.api.clients.resources.impl.MeasureResources;
import com.bookbuf.api.clients.resources.impl.MedicalRegisterResources;
import com.bookbuf.api.clients.resources.impl.MessageResources;
import com.bookbuf.api.clients.resources.impl.MonitorResources;
import com.bookbuf.api.clients.resources.impl.QuestionResources;
import com.bookbuf.api.clients.resources.impl.TaskResources;
import com.bookbuf.api.clients.resources.impl.TrainResources;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface BusinessApi extends Serializable,
        ClerkResources,
        AttendanceResources,
        CallResources,
        DiscoverResources,
        TaskResources,
        QuestionResources,
        ExamPaperResources,
        ExamResources,
        MessageResources,
        GlobalResources,
        GlobalResources.Business,
        MonitorResources,
        HeWeatherResources,
        MedicalRegisterResources,
        MeasureResources,
        AchievementResources,
        TrainResources,
        CouponResources.Business {

    CouponResources.Business couponResources();

    AchievementResources achievementResources();

    TrainResources trainResources();

    ClerkResources clerkResources();

    MedicalRegisterResources medicalRegisterResources();

    AttendanceResources attendanceResource();

    CallResources callResources();

    DiscoverResources discoverResources();

    TaskResources taskResources();

    QuestionResources questionResources();

    ExamPaperResources examPaperResources();

    ExamResources examResources();

    MessageResources messageResources();

    GlobalResources globalAllResources();

    GlobalResources.Business globalResources();

    HeWeatherResources heWeatherResources();

    MeasureResources measureResources();
}
