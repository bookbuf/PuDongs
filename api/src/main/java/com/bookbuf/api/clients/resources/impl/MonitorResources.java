package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;

/**
 * Created by bo.wei on 2017/11/28.
 */

public interface MonitorResources {

    /**
     * 获取药店告警总数
     * @return
     */
    Result<MonitorCountResponse> monitorSelectCount(String jsonObject);
}
