package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.call.CallCancelResponse;
import com.bookbuf.api.responses.impl.call.CallResponse;
import com.ipudong.core.Result;

/**
 * 网络电话功能已经停止服务。
 */
@Deprecated
public interface CallResources {

    /**
     * 取消会员
     *
     * @param sid  会话唯一编号
     * @param type 会话类型
     * @return 取消会话
     */
    Result<CallCancelResponse> callCancel(String sid, int type);

    /**
     * 拨打网络电话
     *
     * @param mobile 电话号码
     * @return 拨打网络电话
     */
    Result<CallResponse> call(String mobile);
}
