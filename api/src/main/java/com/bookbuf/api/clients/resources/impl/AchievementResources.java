package com.bookbuf.api.clients.resources.impl;

import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.ipudong.core.Result;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface AchievementResources {

    /**
     * 获取职业体系下的成就地图（即：储备店员、店员、储备班长、班长、储备店长、店长）。
     *
     * @return 职业体系下的成就地图
     */
    Result<FetchCareerMapResponse> fetchCareerMap();

    /**
     * 依据 成就ID 查询要做的事项（即：传入 “实习店员”成就ID，查询“实习店员”要做的相关事宜）。
     *
     * @param achievementId 成就编号
     * @return 成就详情
     */
    Result<FetchAchievementDetailResponse> fetchAchievement(long achievementId);


}
