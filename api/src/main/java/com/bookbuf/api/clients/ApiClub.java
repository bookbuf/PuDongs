package com.bookbuf.api.clients;

import com.bookbuf.api.configurations.IConfiguration;
import com.bookbuf.api.responses.parsers.InternalFactory;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface ApiClub extends IConfiguration {

    <T> T api(String baseURL, Class<T> clazz);

    <T> T api(Class<T> clazz);


    InternalFactory parser();

    /**
     * 关闭该实例分配和释放资源。
     */
    void shutdown();

}
