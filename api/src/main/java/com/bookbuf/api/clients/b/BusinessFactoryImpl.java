package com.bookbuf.api.clients.b;

import com.bookbuf.api.configurations.ConfigurationContext;
import com.bookbuf.api.configurations.IConfiguration;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * author: robert.
 * date :  16/8/3.
 */
public final class BusinessFactoryImpl {

    private static final Constructor<BusinessApi> _CONSTRUCTOR;
    private static final Constructor<BusinessApi> _CONSTRUCTOR_MOCK;
    private static IConfiguration conf;

    static {

        Constructor constructor;
        Class clazz;
        try {
            clazz = Class.forName("com.bookbuf.api.clients.b.BusinessApiImpl");
            constructor = clazz.getDeclaredConstructor(IConfiguration.class);
        } catch (NoSuchMethodException e) {
            throw new AssertionError(e);
        } catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
        _CONSTRUCTOR = constructor;

    }

    static {

        Constructor constructor;
        Class clazz;
        try {
            clazz = Class.forName("com.bookbuf.api.clients.b.BusinessMockApiImpl");
            constructor = clazz.getDeclaredConstructor(IConfiguration.class, BusinessApi.class);
        } catch (NoSuchMethodException e) {
            throw new AssertionError(e);
        } catch (ClassNotFoundException e) {
            throw new AssertionError(e);
        }
        _CONSTRUCTOR_MOCK = constructor;

    }

    public BusinessFactoryImpl() {
        this(ConfigurationContext.getConfiguration());
    }

    public BusinessFactoryImpl(IConfiguration IConfiguration) {
        if (IConfiguration == null) {
            throw new NullPointerException("configuration cannot be null");
        }
        conf = IConfiguration;
    }

    public BusinessApi getInstance() {
        try {
            final BusinessApi resources = _CONSTRUCTOR.newInstance(conf);
            if (conf.isMockMode())
                return _CONSTRUCTOR_MOCK.newInstance(conf, resources);
            else
                return resources;
        } catch (InstantiationException e) {
            throw new AssertionError(e);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        } catch (InvocationTargetException e) {
            throw new AssertionError(e);
        }
    }
}
