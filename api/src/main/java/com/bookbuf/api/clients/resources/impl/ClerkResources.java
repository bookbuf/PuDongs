package com.bookbuf.api.clients.resources.impl;


import android.support.annotation.Nullable;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.BannerResponse;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.customer.BindCardListResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.impl.user.ClerkResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.FetchClerkProfileResponse;
import com.ipudong.core.Result;


/**
 * author: robert.
 * date :  16/8/1.
 */
public interface ClerkResources {

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 不限制调用者身份。
     *
     * @param target   登录帐号
     * @param password 登录密码
     * @param pushId   推送编号
     * @return 登录成功后的凭据
     */
    Result<CredentialResponse> login(String target, String password, String pushId);

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 调用者身份必须是店员。
     *
     * @param query 查询的编号
     * @param type  查询的类别，表明是身份证号、手机号码、固话号码、会员卡号中的一种
     * @return 顾客的资料
     */
    Result<CustomerResponse> queryCustomer(String query, String type);

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 不限制调用者身份。
     *
     * @param mobile     手机号码
     * @param password   密码
     * @param verifyCode 验证码
     * @param sign       用于标识验证码的使用类型，被服务端所需要
     * @return 密码是否修改成功
     */
    Result<BooleanResponse> resetPassword(String mobile, String password, String verifyCode, String sign);

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 不限制调用者身份。
     *
     * @return 是否注销成功
     */
    Result<BooleanResponse> logout();

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 不限制调用者身份。
     *
     * @param mobile   手机号码
     * @param idCard   身份证号码
     * @param realName 真实姓名
     * @param password 密码
     * @return 注册是否成功
     */
    Result<BooleanResponse> register(String mobile, String idCard, String realName, String password);

    /**
     * 不支持多线程请求，同一时间内的多次请求应当被忽略。
     * 不限制调用者身份。
     *
     * @param oldPwd 旧密码
     * @param newPwd 新密码
     * @return 密码是否修改成功
     */
    Result<BooleanResponse> updatePassword(String oldPwd, String newPwd);

    /**
     * 店员替顾客编辑基本资料，调用者身份必须是店员。
     *
     * @param userId    顾客的唯一编号
     * @param realName  真实姓名
     * @param mobile    手机号码
     * @param idCard    身份证号码
     * @param birthday  生日
     * @param telephone 固定电话
     * @param address   长居地址
     * @return 资料更新是否成功
     */
    Result<BooleanResponse> updateCustomerProfileBasic(long userId, String realName, String mobile, String idCard, String birthday,String gender, String telephone, String address);

    /**
     * 更新用户的基本资料，调用者身份必须是店员。
     *
     * @param userId    待更新的用户唯一编号
     * @param realName  真实姓名
     * @param mobile    手机号码
     * @param idCard    身份证号码
     * @param birthday  生日
     * @param telephone 固定电话
     * @param address   长居地址
     * @return 资料更新是否成功
     */
    Result<BooleanResponse> updateProfileBasic(long userId, String realName, String mobile, String idCard, String birthday, String telephone, String address);

    /**
     * 用户申请绑定到合作商，调用者身份只能是用户。
     *
     * @param mobile   店长手机号码
     * @param clerkNum 店员编号（可为空）
     * @param code     验证码
     * @return 用户申请绑定到合作商是否成功
     */
    Result<BooleanResponse> bindVendor(String mobile, @Nullable String clerkNum, String code);

    /**
     * 店员替顾客创建用户，调用者身份必须是店员。
     * @param vendorMemberId 会员卡号，大参林专享
     * @param mobile        手机号码
     * @param phone         号码
     * @param realName      真实姓名
     * @param idCard        身份证号码
     * @param detailAddress 详细地址
     * @return 创建用户是否成功
     */
    Result<LongResponse> createCustomer(String vendorMemberId, String mobile, String phone, String realName, String idCard, String detailAddress);

    /**
     * 查询店员自己的资料，调用者身份必须是店员。
     *
     * @return 用户资料
     */
    Result<ClerkResponse> queryClerkProfile();

    /**
     * 查询店员自己的资料，调用者身份必须是店员。
     *
     * @return 用户资料
     */
    Result<FetchClerkProfileResponse> queryClerkProfile_2017_01_22();

    /**
     * 查询顾客资料，调用者身份必须是店员。
     *
     * @param userId 用户唯一编号
     * @return 用户资料
     */
    Result<CustomerResponse> queryCustomerProfile(long userId);

    /**
     * 查询 Banner 的配置
     *
     * @return 配置
     */
    Result<BannerResponse> fetchBannerConfiguration();

    Result<PermissionResponse> getPermission();

    Result<CustomerLoginResponse> userLogin(String mobile,String code,String pushId);

    Result<BindCardListResponse> bindCard(String uid, String vendorId, String vipcard);
}
