package com.bookbuf.api.apis;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.HEAD;
import com.ipudong.core.network.annotation.method.HEADER;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.response.impl.HealthbokResponse;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by bo.wei on 2017/11/28.
 * 一树监控接口
 */

@RestService
public interface MonitorAPI {

    @POST("http://www.uwonders-my.com:8988/pharmacy/alarm/oper/selectCount")
    @HEADER(contentType = "application/json")
    HealthbokResponse selectCount(
            //统计药店告总数接口
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );

    @POST("http://www.uwonders-my.com:8988/pharmacy/alarm/oper/selectCountType")
    @HEADER(contentType = "application/json")
    HealthbokResponse selectCountType(
            //统计药店告总数接口
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );

    @POST("http://www.uwonders-my.com:8988/pharmacy/alarm/oper/select")
    @HEADER(contentType = "application/json")
    HealthbokResponse select(
            //根据条件分页查询告警记录
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );

    @POST("http://www.uwonders-my.com:8988/pharmacy/back/passengerAction/selectByDateTime")
    @HEADER(contentType = "application/json")
    HealthbokResponse passengerActionSelectByDateTime(
            //统计某个药店某段时间的人数总数接口
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );

    @POST("http://www.uwonders-my.com:8988/pharmacy/back/passengerAction/selectCountBychainPharmacy")
    @HEADER(contentType = "application/json")
    HealthbokResponse selectCountBychainPharmacy(
            //统计药店人数总数
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );

    @POST("http://www.uwonders-my.com:8988/pharmacy/api/terminalUser/selecatAllSite")
    @HEADER(contentType = "application/json")
    HealthbokResponse selectAllSite(
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );

    @POST("http://www.uwonders-my.com:8988/pharmacy/back/AlarmType/selectByRetrieval.page")
    @HEADER(contentType = "application/json")
    HealthbokResponse getType(
            @Param (RestClient.Context.KEY_PARAM_JSON) String json
    );



}
