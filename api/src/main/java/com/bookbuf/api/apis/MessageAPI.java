package com.bookbuf.api.apis;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

/**
 * author: robert.
 * date :  16/9/27.
 */
@RestService
public interface MessageAPI {

	@POST
	HealthbokResponse fetchSystemMessages (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation
	) ;

	@POST
	HealthbokResponse fetchDetailMessage (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("messageId") long messageId) ;

	@POST
	HealthbokResponse fetchMessageCenter (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation
	) ;
}
