package com.bookbuf.api.apis;

import android.support.annotation.Nullable;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.method.TIMEOUT;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

@RestService
public interface CouponAPI {

    // 获取优惠券的列表
    @POST
    HealthbokResponse fetchList(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("categoryVendorId") @Nullable Long categoryVendorId
    );

    // 核销验证优惠券
    @POST
    HealthbokResponse verify(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponNum") String couponNum
    );

    // 领取优惠券
    @POST
    @TIMEOUT(connectTimeout = 30000L,writeTimeout = 30000L,readTimeout = 30000L)
    HealthbokResponse gain(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponId") long couponId
    );

    // 查询优惠券状态
    @POST
    HealthbokResponse fetchStatus(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponId") long couponId
    );

    // 获取优惠券的分享内容，可供第三方分享
    @POST
    HealthbokResponse fetchShareContent(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponId") long couponId,
            @Param("type") String type

    );

    // 查询优惠券详情
    @POST
    HealthbokResponse searhCoupon(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponId") long couponId,
            @Param("userCouponId") Long userCouponId
    );

    // 查询优惠券详情
    @POST
    HealthbokResponse fetchCouponForB(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponNum") String couponNum
    );

    // 领取优惠码
    @POST
    HealthbokResponse gainByAlias(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("promoCode") String alias
    );

    //使用优惠码查询优惠券的详情（C端）
    @POST
    HealthbokResponse searhCouponByAlias(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("promoCode") String alias,
            @Param("couponId") @Nullable Long couponId
    );


    //我的优惠券（C端）
    @POST
    HealthbokResponse searchMyCoupon(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );

    //我的优惠码（C端）
    @POST
    HealthbokResponse searchMyCouponAlias(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );

    //获取产品信息（c端）
    @POST
    HealthbokResponse fetchProduct(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponId") long couponId,
            @Param("userCouponId") @Nullable Long userCouponId
    );

    //根据优惠码获取产品信息（c端）
    @POST
    HealthbokResponse fetchProductByAlias(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("promoCode") String couponAlias
    );

    //获取优惠码列表（B端）
    @POST
    HealthbokResponse searchCouponCodeList(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );

    //获取优惠码（B端）
    @POST
    HealthbokResponse fetchCouponAlias(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("couponId") long couponId
    );

    //获取优惠券分类
    @POST
    HealthbokResponse fetchCouponType(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );

    //获取推荐券列表
    @POST
    HealthbokResponse fetchRecomCouponList(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );

    //获取认证优惠券
    @POST
    HealthbokResponse getVerifiedCoupon(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("vendorId") Long vendorId,
            @Param("uid") Long uid
    );
}
