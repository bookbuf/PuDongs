package com.bookbuf.api.apis;


import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

@RestService
public interface MedicalRegisterAPI {

	@POST
	HealthbokResponse sendSMS (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("id") long id
	) ;

	@POST
	HealthbokResponse bookService (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("hospitalId") long hospitalId,
			@Param ("lockQueueNo") int lockQueueNo,
			@Param ("scheduleItemCode") String scheduleItemCode,
			@Param ("userId") long userId
	) ;

	@POST
	HealthbokResponse queryAdmSchedule (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("hospitalId") long hospitalId,
			@Param ("departmentCode") int departmentCode,
			@Param ("doctorCode") int doctorCode
	) ;

	@POST
	HealthbokResponse search (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("mobile") String mobile
	) ;

	@POST
	HealthbokResponse listHospital (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation
	) ;


	@POST
	HealthbokResponse queryDoctorGroup (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("hospitalId") long hospitalId,
			@Param ("departmentCode") long departmentCode
	) ;

	@POST
	HealthbokResponse queryDepartmentGroup (
			@SignParam (RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
			@Param (RestClient.RestEntity.KEY_API) String api,
			@Param (RestClient.RestEntity.KEY_API_VERSION) String version,
			@Param (RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
			@Param (RestClient.RestEntity.KEY_APP_ID) int appId,
			@Param (RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
			@Param (RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
			@Param (RestClient.RestEntity.KEY_SY) String systemInformation,
			@Param ("hospitalId") long hospitalId
	) ;

}
