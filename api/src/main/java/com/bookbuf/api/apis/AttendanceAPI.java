package com.bookbuf.api.apis;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

/**
 * author: robert.
 * date :  16/11/21.
 */
@RestService
public interface AttendanceAPI {

    /**
     * 检测合作商是否开启考勤任务，本期默认所有合作商都是开启考勤任务
     *
     * @param appSecret         appSecret
     * @param api               api
     * @param version           version
     * @param sessionId         sessionId
     * @param appId             appId
     * @param appVersion        appVersion
     * @param timeMillis        timeMillis
     * @param systemInformation systemInformation
     * @return 检测合作商是否开启考勤任务
     */
    @POST
    HealthbokResponse preference(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation);

    /**
     * 查询某月考勤记录，此处返回的数据足够客户端生成日历。点击日历上的某一天的具体考勤记录，需再次请求“某日的考勤记录”接口。
     *
     * @param appSecret         appSecret
     * @param api               api
     * @param version           version
     * @param sessionId         sessionId
     * @param appId             appId
     * @param appVersion        appVersion
     * @param timeMillis        timeMillis
     * @param systemInformation systemInformation
     * @param date              date(yyyy-MM)
     * @return 查询某月考勤记录
     */
    @POST
    HealthbokResponse month(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("date") String date
    );

    /**
     * 查询某天的考勤记录，返回该天的详细记录。若给定的【日期】是未来的时间，则给出相应的错误提示。
     *
     * @param appSecret         appSecret
     * @param api               api
     * @param version           version
     * @param sessionId         sessionId
     * @param appId             appId
     * @param appVersion        appVersion
     * @param timeMillis        timeMillis
     * @param systemInformation systemInformation
     * @param date              date(yyyy-MM-dd)
     * @return 查询某月考勤记录
     */
    @POST
    HealthbokResponse day(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("date") String date
    );


    /**
     * 仅限于获取当日的考勤上下文，注意与接口`pd.attendance.day`加以区分。
     *
     * @param appSecret         appSecret
     * @param api               api
     * @param version           version
     * @param sessionId         sessionId
     * @param appId             appId
     * @param appVersion        appVersion
     * @param timeMillis        timeMillis
     * @param systemInformation systemInformation
     * @return 仅限于获取当日的考勤上下文
     */
    @POST
    HealthbokResponse context(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation);

    /**
     * 申请签卡（包括签到、签退）
     *
     * @param appSecret         appSecret
     * @param api               api
     * @param version           version
     * @param sessionId         sessionId
     * @param appId             appId
     * @param appVersion        appVersion
     * @param timeMillis        timeMillis
     * @param systemInformation systemInformation
     * @param type              签卡 还是 签退【必填】
     * @param longitude         经度，若APP实在无法定位则会传-1 兼容无法定位的情况【必填】
     * @param latitude          纬度，若APP实在无法定位则会传-1 兼容无法定位的情况【必填】
     * @param deviceMacAddress  打卡设备mac地址【选填】
     * @param content           补充说明，当出现异常打卡（经纬度未获得，距离范围之外，早退、迟到、旷工）时，【必填】。
     * @return 申请签卡（包括签到、签退）
     */
    @POST
    HealthbokResponse apply(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("type") int type,
            @Param("longitude") double longitude,
            @Param("latitude") double latitude,
            @Param("deviceMacAddress") String deviceMacAddress,
            @Param("content") String content);


    /**
     * 申请签卡（包括签到、签退）
     *
     * @param appSecret         appSecret
     * @param api               api
     * @param version           version
     * @param sessionId         sessionId
     * @param appId             appId
     * @param appVersion        appVersion
     * @param timeMillis        timeMillis
     * @param systemInformation systemInformation
     * @param type              签卡 还是 签退【必填】
     * @param longitude         经度，若APP实在无法定位则会传-1 兼容无法定位的情况【必填】
     * @param latitude          纬度，若APP实在无法定位则会传-1 兼容无法定位的情况【必填】
     * @return 申请签卡（包括签到、签退）
     */
    @POST
    HealthbokResponse status(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("type") int type,
            @Param("longitude") double longitude,
            @Param("latitude") double latitude
    );
}
