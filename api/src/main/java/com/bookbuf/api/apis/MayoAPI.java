package com.bookbuf.api.apis;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.GET;
import com.ipudong.core.network.annotation.method.HEADER;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.HeaderAdditional;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.URI;
import com.ipudong.core.network.response.impl.HealthbokResponse;

import org.json.JSONArray;

/**
 * author: robert.
 * date :  16/11/21.
 */
@RestService
public interface MayoAPI {
	@GET ("http://api.huimei.com/v_1_0/condition/{docId}")
	@HEADER (authorization = "HM XxSawiGLH13v3lTo:mfo+3gJcI39RzfW7kgFguYK6qdw=")
	HealthbokResponse diseaseDetail (
			@HeaderAdditional ("X-HM-Timestamp") long timeStamp,
			@HeaderAdditional ("charset") String charset,
			@URI ("docId") long docId
	) ;


	@POST ("http://api.huimei.com/v_1_0/drug/interaction")
	@HEADER (authorization = "HM XxSawiGLH13v3lTo:LpfnawIFJqOdQRbMQnLrb665mv8=", contentType = "application/json")
	HealthbokResponse interaction (
			@HeaderAdditional ("X-HM-Timestamp") long timeStamp,
			@HeaderAdditional ("charset") String charset,
			@Param (RestClient.Context.KEY_PARAM_JSON) JSONArray array
	) ;
}
