package com.bookbuf.api.apis;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

/**
 * author: robert.
 * date :  2017/3/20.
 */
@RestService
public interface CustomerAPI {
    @POST
    HealthbokResponse login(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("mobile") String mobile,
            @Param("code") String code,
            @Param("pushId") String pushId
    );

    @POST
    HealthbokResponse wxLogin(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("mobile") String mobile,
            @Param("code") String code,
            @Param("openid") String openid,
            @Param("pushId") String pushId
    );

    @POST
    HealthbokResponse Logout(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );


    @POST
    HealthbokResponse fetchDetectionIndex(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("vendorId") int vendorId
    );

    @POST
    HealthbokResponse fetchDetectionLastOnce(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("userId") Long userId/* if null,means query self*/
    );

    @POST
    HealthbokResponse fetchDetectionList(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("userId") Long userId,/* if null,means query self*/
            @Param("typeCode") String typeCode,
            @Param("page") int page,
            @Param("pageSize") int pageSize
    );

    @POST
    HealthbokResponse fetchUserDocument(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("userId") Long userId/* if null,means query self*/
    );

    @POST
    HealthbokResponse fetchProfile(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation);

    @POST
    HealthbokResponse modifyProfile(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("nickName") String nickName,
            @Param("gender") String gender,
            @Param("IDCard") String idCard,
            @Param("email") String email,
            @Param("province") String province,
            @Param("city") String city,
            @Param("distinct") String distinct,
            @Param("avatar") String avatar
    );

    @POST
    HealthbokResponse getRecommendNotice(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation);

    @POST
    HealthbokResponse updateRecommend(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation);

    @POST
    HealthbokResponse createInvite(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("fromUid") Long fromUid,
            @Param("deviceType") int deviceType,
            @Param("deviceUuid") String deviceUuid,
            @Param("receiveUid") Long receiveUid);
}
