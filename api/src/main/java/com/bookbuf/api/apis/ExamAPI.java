package com.bookbuf.api.apis;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

/**
 * author: robert.
 * date :  16/9/14.
 */
@RestService
public interface ExamAPI {
    @POST
    HealthbokResponse beginExam(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("exampaperId") long paperId);

    @POST
    HealthbokResponse getExamStatus(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("exampaperlogId") long examId);

    @POST
    HealthbokResponse terminateExam(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("exampaperLogId") long examId);

    /**
     * @param examId  考试编号
     * @param answers 作答答案答案字符串提交，服务端原样保存即可
     *                [{"questionId":101,"answers":"ABC"},{"questionId":100,"answers":""}]
     * @return ExamResultResponse
     */
    @POST
    HealthbokResponse handleInExam(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("exampaperLogId") @NonNull long examId,
            @Param("answers") @NonNull String answers,
            @Param("courseId") @Nullable Long courseId,
            @Param("levelId") @Nullable Long levelId
    );

    @POST
    HealthbokResponse fetchExamHistory(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("exampaperLogId") long examId);
}
