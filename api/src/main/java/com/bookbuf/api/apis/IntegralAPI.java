package com.bookbuf.api.apis;

import com.ipudong.core.network.RestClient;
import com.ipudong.core.network.annotation.RestService;
import com.ipudong.core.network.annotation.method.POST;
import com.ipudong.core.network.annotation.method.TIMEOUT;
import com.ipudong.core.network.annotation.param.Param;
import com.ipudong.core.network.annotation.param.SignParam;
import com.ipudong.core.network.response.impl.HealthbokResponse;

/**
 * Created by bo.wei on 2017/6/13.
 */

@RestService
public interface IntegralAPI {
    // 获取我的积分的列表
    @POST
    @TIMEOUT(connectTimeout = 30000L,writeTimeout = 30000L,readTimeout = 30000L)
    HealthbokResponse fetchIntegralList(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation
    );

    @POST
    @TIMEOUT(connectTimeout = 30000L,writeTimeout = 30000L,readTimeout = 30000L)
    HealthbokResponse cutIntegral(
            @SignParam(RestClient.RestEntity.KEY_APP_SECRET) String appSecret,
            @Param(RestClient.RestEntity.KEY_API) String api,
            @Param(RestClient.RestEntity.KEY_API_VERSION) String version,
            @Param(RestClient.RestEntity.KEY_SESSION_ID) String sessionId,
            @Param(RestClient.RestEntity.KEY_APP_ID) int appId,
            @Param(RestClient.RestEntity.KEY_APP_VERSION) String appVersion,
            @Param(RestClient.RestEntity.KEY_TIME_MILLIS) long timeMillis,
            @Param(RestClient.RestEntity.KEY_SY) String systemInformation,
            @Param("userCouponId") long userCouponId
    );
}
