package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskIntentResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskIntentResponseJSONImpl extends PuDongParserImpl implements TaskIntentResponse, Serializable {

    @Key("type")
    private String type;
    @Key("url")
    private String url;

    public TaskIntentResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String type() {
        return this.type;
    }

    @Override
    public String url() {
        return this.url;
    }

}
