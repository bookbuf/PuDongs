package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.bookbuf.api.responses.impl.user.PhotoVerifiedResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.user.PhotoVerifiedJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by new on 2017/5/19.
 */

public class ProfileResponseJSONImpl extends PuDongParserImpl implements ProfileResponse, Serializable {

    @Key("nickName")
    String nickName;
    @Key("avatar")
    String avatar;
    @Key("gender")
    String gender;
    @Key("region")
    String region;
    @Key("IDCard")
    String idCard;
    @Key("email")
    String email;
    @Key("province")
    String province;
    @Key("city")
    String city;
    @Key("distinct")
    String distinct;
    @Key("mobile")
    String mobile;
    @Key("coupon")
    String coupon;
    @Key("integral")
    String integral;
    @Key("alias")
    String alias;
    @Key("age")
    String age;
    @Key("photo_verified")
    @Implementation(PhotoVerifiedJSONImpl.class)
    PhotoVerifiedResponse photoVerifiedResponse;

    public ProfileResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String nickName() {
        return nickName;
    }

    @Override
    public String avatar() {
        return avatar;
    }

    @Override
    public String gender() {
        return gender;
    }

    @Override
    public String address() {
        return region;
    }

    @Override
    public String idCard() {
        return idCard;
    }

    @Override
    public String email() {
        return email;
    }

    @Override
    public String province() {
        return province;
    }

    @Override
    public String city() {
        return city;
    }

    @Override
    public String distinct() {
        return distinct;
    }

    @Override
    public String mobile() {
        return mobile;
    }

    @Override
    public String integral() {
        return integral;
    }

    @Override
    public String coupon() {
        return coupon;
    }

    @Override
    public String alias() {
        return alias;
    }

    @Override
    public String age() {
        return age;
    }

    @Override
    public PhotoVerifiedResponse response() {
        return photoVerifiedResponse;
    }
}
