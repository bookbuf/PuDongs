package com.bookbuf.api.responses.impl.call;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2016/11/23.
 */

public interface CallCancelResponse extends Response {
//    int type();
//
//    String sid();

	String originCode ();
}
