package com.bookbuf.api.responses.impl;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface BooleanResponse extends Response {

	boolean get ();
}
