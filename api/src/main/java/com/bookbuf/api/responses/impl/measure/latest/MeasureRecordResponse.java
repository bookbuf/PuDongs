package com.bookbuf.api.responses.impl.measure.latest;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/28.
 */

public interface MeasureRecordResponse extends Response {

	String id ();

	String groupName ();

	String indicatorName ();

	String unit ();

	long takeTime ();

	String timeFormat ();

	int status ();

	List<MeasureFieldResponse> fields ();
}
