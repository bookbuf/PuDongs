package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.FetchClerkProfileResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class FetchClerkProfileResponseJSONImpl extends PuDongParserImpl implements FetchClerkProfileResponse, Serializable {

    @Key("vendor_shop_id")
    private long vendorShopId;
    @Key("vendor_id")
    private long vendorId;
    @Key("vendor_name")
    private String vendorName;
    @Key("vendor_position")
    private long vendorPosition;
    @Key("vendor_logo_url")
    private String vendorLogoUrl;
    @Key("vendor_code")
    private String vendorCode;
    @Key("vendor_shop_name")
    private String vendorShopName;
    @Key("birthday")
    private String birthday;
    @Key("gender")
    private String gender;
    @Key("mobile")
    private String mobile;
    @Key("id_card")
    private String idCard;
    @Key("realname")
    private String realname;
    @Key("user_id")
    private long userId;
    @Key("email")
    private String email;
    @Key("status")
    private int status;
    @Key("clerk_status")
    private int clerkStatus;
    @Key("note")
    private String note;
    @Key("clerk_num")
    private String clerkNum;
    @Key("role_name")
    private String roleName;

    public FetchClerkProfileResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long vendorShopId() {
        return vendorShopId;
    }

    @Override
    public long vendorId() {
        return vendorId;
    }

    @Override
    public String vendorName() {
        return vendorName;
    }

    @Override
    public long vendorPosition() {
        return vendorPosition;
    }

    @Override
    public String vendorLogoUrl() {
        return vendorLogoUrl;
    }

    @Override
    public String vendorCode() {
        return vendorCode;
    }

    @Override
    public String vendorShopName() {
        return vendorShopName;
    }

    @Override
    public String birthday() {
        return birthday;
    }

    @Override
    public String gender() {
        return gender;
    }

    @Override
    public String mobile() {
        return mobile;
    }

    @Override
    public String idCard() {
        return idCard;
    }

    @Override
    public String realname() {
        return realname;
    }

    @Override
    public long userId() {
        return userId;
    }

    @Override
    public String email() {
        return email;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public int clerkStatus() {
        return clerkStatus;
    }

    @Override
    public String note() {
        return note;
    }

    @Override
    public String clerkNum() {
        return clerkNum;
    }

    @Override
    public String roleName() {
        return roleName;
    }
}
