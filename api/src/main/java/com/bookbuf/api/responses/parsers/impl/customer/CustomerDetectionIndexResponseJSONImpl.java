//package com.bookbuf.api.responses.parsers.impl.customer;
//
//import com.bookbuf.api.responses.impl.customer.CustomerDetectionIndexResponse;
//import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
//import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
//import com.bookbuf.api.responses.parsers.impl.customer.detections.HasBeenDetectedResponseJSONImpl;
//import com.bookbuf.api.responses.parsers.impl.customer.detections.NotDetectedResponseJSONImpl;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.Serializable;
//
///**
// * author: robert.
// * date :  2017/3/20.
// */
//
//public class CustomerDetectionIndexResponseJSONImpl extends PuDongParserImpl implements CustomerDetectionIndexResponse, Serializable {
//    @IgnoreKey
//    private HasBeenDetected hasBeenDetected;
//    @IgnoreKey
//    private NotDetected notDetected;
//
//    public CustomerDetectionIndexResponseJSONImpl(JSONObject jsonObject) {
//        super(null);// 忽略智能模板解析
//
//        JSONObject reality = (JSONObject) getRealityJSONObject(jsonObject);
//
//        try {
//            JSONObject key2 = reality.getJSONObject("key2");
//            JSONArray hasBeenDetected = key2.getJSONArray("hasBeenDetected");
//            JSONArray notDetected = key2.getJSONArray("notDetected");
//
//            this.hasBeenDetected = new HasBeenDetectedResponseJSONImpl(hasBeenDetected);
//            this.notDetected = new NotDetectedResponseJSONImpl(notDetected);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public HasBeenDetected hasBeenDetected() {
//        return hasBeenDetected;
//    }
//
//    @Override
//    public NotDetected notDetected() {
//        return notDetected;
//    }
//}
