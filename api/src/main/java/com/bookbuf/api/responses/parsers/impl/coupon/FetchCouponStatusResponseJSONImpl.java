package com.bookbuf.api.responses.parsers.impl.coupon;


import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class FetchCouponStatusResponseJSONImpl extends PuDongParserImpl implements FetchCouponStatusResponse, Serializable {

    @Key("title")
    private String title;
    @Key("type")
    private int type;
    @Key("description")
    private String description;

    @Key("status")
    private boolean isUsed;
    @Key("overdue")
    private int timeLimit;
    @Key("outService")
    private boolean isOutService;

    @Key("activity_begin")
    private String beginDate;
    @Key("activity_end")
    private String endDate;
    @Key("activity_goods")
    private List<String> goods;

    public FetchCouponStatusResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public int type() {
        return type;
    }

    @Override
    public boolean isUsed() {
        return isUsed;
    }

    @Override
    public int timeLimit() {
        return timeLimit;
    }

    @Override
    public boolean isOutService() {
        return isOutService;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String beginDate() {
        return beginDate;
    }

    @Override
    public String endDate() {
        return endDate;
    }

    @Override
    public List<String> goods() {
        return goods;
    }
}