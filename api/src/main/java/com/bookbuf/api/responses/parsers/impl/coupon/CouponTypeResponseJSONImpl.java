package com.bookbuf.api.responses.parsers.impl.coupon;

import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/8/16.
 */

public class CouponTypeResponseJSONImpl extends PuDongParserImpl implements CouponTypeResponse, Serializable {

    @Key("id")
    private long id;
    @Key("categoryName")
    private String categoryName;
    @Key("categoryIcon")
    private String categoryIcon;

    public CouponTypeResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String categoryName() {
        return categoryName;
    }

    @Override
    public String categoryIcon() {
        return categoryIcon;
    }
}
