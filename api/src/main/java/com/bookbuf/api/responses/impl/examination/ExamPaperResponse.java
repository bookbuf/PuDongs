package com.bookbuf.api.responses.impl.examination;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.question.QuestionResponse;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface ExamPaperResponse extends Response {

	/**
	 * 试卷编号
	 *
	 * @return id
	 */
	long id ();

	/**
	 * 试卷标题
	 *
	 * @return title
	 */
	String title ();

	/**
	 * 试卷作答时间
	 *
	 * @return duration
	 */
	int duration ();

	/**
	 * 试卷大纲
	 *
	 * @return summary
	 */
	String summary ();

	/**
	 * 试卷关联的复习资料
	 *
	 * @return advice
	 */
	String advice ();

	/**
	 * 试卷内包含的题目集合
	 *
	 * @return questions
	 */
	List<QuestionResponse> questions ();
}
