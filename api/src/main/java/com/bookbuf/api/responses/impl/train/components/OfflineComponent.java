package com.bookbuf.api.responses.impl.train.components;

/**
 * author: robert.
 * date :  2016/12/22.
 */

public interface OfflineComponent {
    String teacher();

    String time();

    String address();

    String period();

    String content();
}
