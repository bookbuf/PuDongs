package com.bookbuf.api.responses.parsers.impl.question;

import android.text.TextUtils;

import com.bookbuf.api.responses.impl.question.AnswerResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Arrays;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class AnswerResponseJSONImpl extends PuDongParserImpl implements AnswerResponse, Serializable {

    @Key("questionId")
    private long questionId;
    @Key("answers")
    private String answer;
    @Key("correctAnswers")
    private String correctAnswer;

    public AnswerResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public long questionId() {
        return this.questionId;
    }

    @Override
    public String answer() {
        return this.answer;
    }

    @Override
    public String correctAnswer() {
        return this.correctAnswer;
    }

    /**
     * 将在1.4.0版本中被废弃
     */
    @Deprecated
    @Override
    public String sort(String value) {
        if (TextUtils.isEmpty(value)) {
            return "";/*若未作答，空字符串*/
        } else {
            char[] chars = value.toCharArray();
            Arrays.sort(chars);
            return new String(chars);
        }
    }

    /**
     * 将在1.4.0版本中被废弃
     */
    @Deprecated
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    /**
     * 将在1.4.0版本中被废弃
     */
    @Deprecated
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     * 将在1.4.0版本中被废弃
     */
    @Deprecated
    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

}
