package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSiteListResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSiteResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorTypeDataResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorTypeListResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/12/1.
 */

public class MonitorTypeDataResponseJSONImpl extends PuDongParserImpl implements MonitorTypeDataResponse,Serializable {

    @Key("code")
    int code;
    @Key("data")
    @Implementation(MonitorTypeListResponseJSONImpl.class)
    MonitorTypeListResponse data;
    @Key("message")
    String message;

    public MonitorTypeDataResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public MonitorTypeListResponse data() {
        return data;
    }
}
