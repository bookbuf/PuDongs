package com.bookbuf.api.responses.impl.medical;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface SectionResponse extends Response {

    /**
     * 科室编号
     *
     * @return id
     */
    int code();

    /**
     * 科室名称
     *
     * @return name
     */
    String name();

    boolean isResRowIdFlag();

    String hospitalCode();
}
