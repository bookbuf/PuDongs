package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorPassengerByDateResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorPassengerByDateResponseJSONImpl extends PuDongParserImpl implements MonitorPassengerByDateResponse,Serializable {

    @Key("passengerOff")
    int passengerOff;
    @Key("storeNumber")
    String storeNumber;
    @Key("terminalUserId")
    int terminalUserId;
    @Key("id")
    long id;
    @Key("passengerGet")
    int passengerGet;
    @Key("passengerPass")
    int passengerPass;
    @Key("generateDateTime")
    String generateDateTime;

    public MonitorPassengerByDateResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int passergerOff() {
        return passengerOff;
    }

    @Override
    public String storeNumber() {
        return null;
    }

    @Override
    public long terminalUserId() {
        return terminalUserId;
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public int passengerGet() {
        return passengerGet;
    }

    @Override
    public int passengerPass() {
        return passengerPass;
    }

    @Override
    public String generateDateTime() {
        return generateDateTime;
    }
}
