package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSelectListResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSelectResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorSelectListResponseJSONImpl extends PuDongParserImpl implements MonitorSelectListResponse,Serializable {

    @Key("totalPages")
    int totalPages;
    @Key("pageSize")
    int pageSize;
    @Key("totalRows")
    int totalRows;
    @Key("pageNum")
    int pageNum;
    @Key("list")
    @Implementation(MonitorSelectResponseJSONImpl.class)
    List<MonitorSelectResponse> list;

    public MonitorSelectListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int totalPages() {
        return totalPages;
    }

    @Override
    public int pageSize() {
        return pageSize;
    }

    @Override
    public int totalRows() {
        return totalRows;
    }

    @Override
    public int pageNum() {
        return pageNum;
    }

    @Override
    public List<MonitorSelectResponse> list() {
        return list;
    }
}
