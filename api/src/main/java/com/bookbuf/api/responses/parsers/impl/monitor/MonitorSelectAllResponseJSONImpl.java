package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSelectAllResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSelectDataResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorSelectAllResponseJSONImpl extends PuDongParserImpl implements MonitorSelectAllResponse,Serializable {

    @Key("code")
    int code;
    @Key("data")
    @Implementation(MonitorSelectDataResponseJSONImpl.class)
    MonitorSelectDataResponse dataResponse;
    @Key("message")
    String message;

    public MonitorSelectAllResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public MonitorSelectDataResponse data() {
        return dataResponse;
    }

    @Override
    public String message() {
        return message;
    }
}
