package com.bookbuf.api.responses.impl.measure.list;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/28.
 */

public interface ListRecordResponse extends Response {

	String name ();

	List<RecordResponse> records ();
}
