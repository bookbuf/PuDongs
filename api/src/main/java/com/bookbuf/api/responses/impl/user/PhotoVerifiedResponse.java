package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2018/1/18.
 */

public interface PhotoVerifiedResponse extends Response {

    boolean verified();

    String verifiedUrl();
}
