package com.bookbuf.api.responses.impl.measure.latest;

import com.bookbuf.api.responses.Response;

/**
 * {
 * "text": "您的血压偏高",
 * "color": "#ffa22a",
 * "jump_id": 1,
 * "jump_action_desc": "再次检测",
 * "id": "bp",
 * "unit_name": "mmHg",
 * "value_list": {
 * "value1": {
 * "name": "收缩压",
 * "value": "134",
 * "status": 2
 * },
 * "value2": {
 * "name": "舒张压",
 * "value": "78",
 * "status": 1
 * }
 * },
 * "last_time": "152天前"
 * }
 * author: robert.
 * date :  16/11/28.
 */

public interface MeasureTaskResponse extends Response {


	String title ();

	String action ();

	String color ();

	int jumpId ();

	MeasureRecordResponse latestRecord ();

	String lastDateFormat ();
}
