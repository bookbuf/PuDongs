package com.bookbuf.api.responses.parsers.impl.global;

import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * 通用的布尔类型
 * {
 * "success": true,
 * "msgCode": null,
 * "msgInfo": null,
 * "count": null,
 * "model": true // 格式恒定
 * }
 * author: robert.
 * date :  16/10/20.
 */

public final class RoleResponseJSONImpl extends PuDongParserImpl implements RoleResponse, Serializable {


    @Key("role")
    private int id;
    @Key("isVirtual")
    private boolean isVirtual;

    public RoleResponseJSONImpl(JSONObject wrapperJSONObject) {
        super(wrapperJSONObject);
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public String description() {
        switch (id()) {
            case ROLE_USER:
                return "会员";
            case ROLE_CLERK:
                return "店员";
            case ROLE_VENDOR:
                return "店长";
            case ROLE_AREA_MANAGER:
                return "区域管理员";
            case ROLE_VENDOR_OWNER:
                return "品牌商负责人";
            case ROLE_CHANNEL_MANAGER:
                return "管理员";
            case ROLE_ROOT_MANAGER:
                return "超级管理员";
            case ROLE_HOSPITAL:
                return "医院";
            default:
                return "未知身份";
        }
    }

    @Override
    public boolean isVirtual() {
        return isVirtual;
    }
}
