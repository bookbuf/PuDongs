package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.bookbuf.api.responses.impl.task.TaskResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskContextResponseJSONImpl extends PuDongParserImpl implements TaskContextResponse, Serializable {

    @Key("progress")
    private int progress;
    @Key("tasks")
    @Implementation(TaskResponseJSONImpl.class)
    private List<TaskResponse> tasks;

    public TaskContextResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public List<TaskResponse> tasks() {
        return this.tasks;
    }

    @Override
    public int progress() {
        return this.progress;
    }

}
