package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherAQIResponse extends Response {

	String aqi ();

	String co ();

	String no2 ();

	String o3 ();

	String pm10 ();

	String pm25 ();

	String qlty ();

	String so2 ();
}
