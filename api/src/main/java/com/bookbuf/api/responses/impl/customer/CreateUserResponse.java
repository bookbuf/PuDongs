package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/17.
 */

public interface CreateUserResponse extends Response{
    long uid();
}
