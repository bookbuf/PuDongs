package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorCountByIdResponse extends Response {

    int code();

    MonitorCountByIdDataResponse data();

    String message();
}
