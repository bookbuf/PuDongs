package com.bookbuf.api.responses.parsers.impl.examination;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2016/12/20.
 */

public final class ExamTerminateBooleanResponseJSONImpl extends PuDongParserImpl implements BooleanResponse, Serializable {

    @Key("isTerminated")
    private boolean bool;

    public ExamTerminateBooleanResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public boolean get() {
        return bool;
    }
}
