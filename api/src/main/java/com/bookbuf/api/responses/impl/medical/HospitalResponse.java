package com.bookbuf.api.responses.impl.medical;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface HospitalResponse extends Response {

	long id ();

	String name ();

	String fullName ();

	String description ();

	String address ();

	String tel ();

	String logo ();

}
