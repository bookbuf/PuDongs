package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.BindCardResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/15.
 */

public class BindCardResponseJSONImpl extends PuDongParserImpl implements BindCardResponse, Serializable {
    @Key("card")
    String card;

    public BindCardResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String card() {
        return card;
    }
}
