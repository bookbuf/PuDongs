package com.bookbuf.api.responses.impl.healthlive;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public interface CategoryResponse extends Response {

    long id();

    String title();

    String background();
}
