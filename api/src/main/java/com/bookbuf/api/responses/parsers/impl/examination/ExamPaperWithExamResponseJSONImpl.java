package com.bookbuf.api.responses.parsers.impl.examination;

import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperWithExamResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/23.
 */
public final class ExamPaperWithExamResponseJSONImpl extends PuDongParserImpl implements ExamPaperWithExamResponse, Serializable {
    @Key("examHighest")
    @Implementation(ExamResponseJSONImpl.class)
    private ExamResponse examHighest;
    @Key("examPaper")
    @Implementation(ExamPaperResponseJSONImpl.class)
    private ExamPaperResponse paper;

    public ExamPaperWithExamResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public ExamResponse latestExamRecord() {
        return this.examHighest;
    }

    @Override
    public ExamPaperResponse paper() {
        return this.paper;
    }

}
