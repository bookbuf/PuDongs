package com.bookbuf.api.responses.impl.achievement;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;

import java.util.List;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface FetchAchievementDetailResponse extends Response {

    AchievementComponent achievement();

    /**
     * @return 达成成就需要完成的事件
     */
    List<CourseComponent> events();
}


