package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/12/5.
 */

public interface MonitorCountTypeDataResponse extends Response {

    List<MonitorCountTypeResponse> alarmData();

    String storeNumber();
}
