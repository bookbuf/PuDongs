package com.bookbuf.api.responses.parsers;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: robert.
 * date :  16/8/2.
 */
// FIXME: 2016/12/20 将会在1.4.0中移除该类
@Deprecated
public final class InternalParseUtil {

    public static Object getObject(String name, JSONObject json) {
        try {
            if (json.isNull(name)) {
                return null;
            } else {
                return json.get(name);
            }
        } catch (JSONException e) {
            return null;
        }
    }

    public static String getRawString(String name, JSONObject json) {
        try {
            return (String) getObject(name, json);
        } catch (Exception e) {
            return null;
        }
    }

    public static int getInt(String name, JSONObject json) {
        try {
            return Integer.parseInt(String.valueOf(getObject(name, json)));
        } catch (Exception e) {
            return -1;
        }
    }

    public static long getLong(String name, JSONObject json) {
        try {
            return Long.parseLong(String.valueOf(getObject(name, json)));
        } catch (Exception e) {
            return -1;
        }
    }

    public static double getDouble(String name, JSONObject json) {
        try {
            return Double.parseDouble(String.valueOf(getObject(name, json)));
        } catch (Exception e) {
            return -1;
        }
    }

    public static boolean getBoolean(String name, JSONObject json) {
        try {
            return (boolean) getObject(name, json);
        } catch (Exception e) {
            try {
                String bool = (String) getObject(name, json);
                if (bool.equalsIgnoreCase("true") || bool.equalsIgnoreCase("1"))
                    return true;
                return false;
            } catch (Exception ee) {
                return false;
            }
        }
    }

}
