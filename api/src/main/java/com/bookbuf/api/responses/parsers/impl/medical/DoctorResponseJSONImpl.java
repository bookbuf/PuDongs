package com.bookbuf.api.responses.parsers.impl.medical;

import com.bookbuf.api.responses.impl.medical.DoctorResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class DoctorResponseJSONImpl extends PuDongParserImpl implements DoctorResponse, Serializable {


    @Key("DepartmentCode")
    private String departmentCode;
    @Key("DepartmentName")
    private String departmentName;
    @Key("DoctorCode")
    private int code;
    @Key("DoctorName")
    private String name;
    @Key("DoctorTitleCode")
    private String titleCode;
    @Key("DoctorTitle")
    private String title;
    @Key("DoctorSessTypeCode")
    private String typeCode;
    @Key("DoctorSessType")
    private String type;
    @Key("Fee")
    private String fee;
    @Key("AvailableNum")
    private int availableNum;
    @Key("DoctorPic")
    private String avatar;
    @Key("DoctorSpec")
    private String summary;

    public DoctorResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String departmentCode() {
        return departmentCode;
    }

    @Override
    public String departmentName() {
        return departmentName;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String titleCode() {
        return titleCode;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String typeCode() {
        return typeCode;
    }

    @Override
    public String type() {
        return type;
    }

    @Override
    public String fee() {
        return fee;
    }

    @Override
    public int availableNum() {
        return availableNum;
    }

    @Override
    public String avatar() {
        return avatar;
    }

    @Override
    public String summary() {
        return summary;
    }
}
