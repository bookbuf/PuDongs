package com.bookbuf.api.responses.parsers.impl.healthlive;

import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.HealthLiveResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public class FetchRelatedHealthLiveListResponseJSONImpl extends PuDongParserImpl implements FetchRelatedHealthLiveListResponse, Serializable {

    @Key("main")
    @Implementation(HealthLiveResponseJSONImpl.class)
    private HealthLiveResponse main;
    @Key("album")
    @Implementation(HealthLiveResponseJSONImpl.class)
    private List<HealthLiveResponse> lists;

    public FetchRelatedHealthLiveListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<HealthLiveResponse> lists() {
        return lists;
    }

    @Override
    public HealthLiveResponse main() {
        return main;
    }

}
