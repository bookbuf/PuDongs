package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public interface DetectionItemResponse extends Response {


    String name();

    String value();

    String typeCode();

    String description();

    String unit();

    int status();

    String takeTime();
}


