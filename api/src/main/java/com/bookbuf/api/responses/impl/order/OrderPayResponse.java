package com.bookbuf.api.responses.impl.order;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/5/2.
 */

public interface OrderPayResponse extends Response {
    String orderRequestId();
}
