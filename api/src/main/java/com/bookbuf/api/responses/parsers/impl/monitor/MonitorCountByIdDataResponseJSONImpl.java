package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountByIdDataResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/29.
 */

public class MonitorCountByIdDataResponseJSONImpl extends PuDongParserImpl implements MonitorCountByIdDataResponse,Serializable {

    @Key("generateDateTime")
    String generateDateTime;
    @Key("id")
    int id;
    @Key("passengerGet")
    int passengerGet;
    @Key("passengerOff")
    int passengerOff;
    @Key("passengerPass")
    int passengerPass;
    @Key("terminalUserId")
    String terminalUserId;

    public MonitorCountByIdDataResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String generateDateTime() {
        return generateDateTime;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public int passengerGet() {
        return passengerGet;
    }

    @Override
    public int passengerOff() {
        return passengerOff;
    }

    @Override
    public int passengerPass() {
        return passengerPass;
    }

    @Override
    public String terminalUserId() {
        return terminalUserId;
    }
}
