package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSiteResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/28.
 */

public class MonitorSiteResponseJSONImpl extends PuDongParserImpl implements MonitorSiteResponse,Serializable{

    @Key("city")
    String city;
    @Key("id")
    int id;
    @Key("storeNumber")
    String storeNumber;

    public MonitorSiteResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String city() {
        return city;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public String storeNumber() {
        return storeNumber;
    }
}
