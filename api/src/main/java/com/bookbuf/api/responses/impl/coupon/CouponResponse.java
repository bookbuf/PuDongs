package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

import java.util.List;

public interface CouponResponse extends Response {

    String startTime();

    long id();

    long userCouponId();

    String category();

    int isGained();

    String mode();

    String orderId();

    String detail();

    String title();

    String num();

    int status();

    List<String> rule();

    String className();

    String endTime();

    int type();

    String couponIcon();

    int isProduct();

    @Deprecated
    String productName();

    @Deprecated
    String isChecked();

    @Deprecated
    String pic();

    int limitCollectTime();

    int collectTime();

    int verifyForm();

    String special();       //"专享推荐", 如果没有传null

    String cutPoint();         // 所需积分

    String integralTitle();

    int isIntegral();

    String isAward();      //isAward  为空时, 表示没有扣过积分   isAward:true   表示扣积分成功  isAward：false  表示还在扣积分

    int codeNum();      //抢券总数量

    long codeStartTime();       //抢劵开始时间   （时间戳）

    long codeEndTime();         //抢劵结束时间   （时间戳）

    int remainNum();        //抢劵剩余数量

    int isRush();       //是否抢劵： 0-非抢劵  1-抢劵

    long systemTime();       //系统时间       （时间戳）

    long totalSec();        //抢劵总时间     （单位：秒）

    int couponNumber();     //优惠券序号

    String categoryName();      //分类名称

    String couponType();        //优惠券类型

    String channel();       //适用渠道

    String couponNum();
}
