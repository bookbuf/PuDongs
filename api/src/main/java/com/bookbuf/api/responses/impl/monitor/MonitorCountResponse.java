package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/28.
 */

public interface MonitorCountResponse extends Response {

    int code();

    String data();

    String message();
}
