package com.bookbuf.api.responses.parsers.impl.attendance;

import com.bookbuf.api.responses.impl.attendance.AttendanceStatusResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/27.
 */
public final class AttendanceStatusResponseJSONImpl extends PuDongParserImpl implements AttendanceStatusResponse, Serializable {
    @Key("type")
    private int type;
    @Key("distance")
    private double distance;
    @Key("status")
    private int status;
    @Key("date")
    private String date;

    public AttendanceStatusResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int type() {
        return type;
    }

    @Override
    public double distance() {
        return distance;
    }

    @Override
    public String date() {
        return date;
    }

    @Override
    public int status() {
        return status;
    }
}
