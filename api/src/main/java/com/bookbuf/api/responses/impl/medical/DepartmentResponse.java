package com.bookbuf.api.responses.impl.medical;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface DepartmentResponse extends Response {

	/**
	 * 部门编号
	 *
	 * @return code
	 */
	int code ();

	/**
	 * 部门名称
	 *
	 * @return name
	 */
	String name ();

	/**
	 * 部门下是否划有科室
	 *
	 * @return isGroup
	 */
	boolean isGroup ();


	List<SectionResponse> sections ();
}
