package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherHourlyResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherHourlyResponseJSONImpl extends PuDongParserImpl implements WeatherHourlyResponse, Serializable {


    @Key("date")
    private String date;
    @Key("hum")
    private String hum;
    @Key("pop")
    private String pop;
    @Key("pres")
    private String pres;
    @Key("tmp")
    private String tmp;
    @Key("wind")
    @Implementation(WindResponseJSONImpl.class)
    private WindResponse wind;
    @Key("cond")
    @Implementation(CondResponseJSONImpl.class)
    private CondResponse cond;

    public WeatherHourlyResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String date() {
        return date;
    }

    @Override
    public String hum() {
        return hum;
    }

    @Override
    public String pop() {
        return pop;
    }

    @Override
    public String pres() {
        return pres;
    }

    @Override
    public String tmp() {
        return tmp;
    }

    @Override
    public CondResponse cond() {
        return cond;
    }

    @Override
    public WindResponse wind() {
        return wind;
    }

    public static final class CondResponseJSONImpl extends PuDongParserImpl implements WeatherHourlyResponse.CondResponse, Serializable {

        @Key("code")
        private String code;
        @Key("txt")
        private String txt;

        public CondResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String code() {
            return code;
        }

        @Override
        public String txt() {
            return txt;
        }
    }

    public static final class WindResponseJSONImpl extends PuDongParserImpl implements WeatherHourlyResponse.WindResponse, Serializable {

        @Key("deg")
        private String deg;
        @Key("dir")
        private String dir;
        @Key("sc")
        private String sc;
        @Key("spd")
        private String spd;

        public WindResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String deg() {
            return deg;
        }

        @Override
        public String dir() {
            return dir;
        }

        @Override
        public String sc() {
            return sc;
        }

        @Override
        public String spd() {
            return spd;
        }
    }

}
