package com.bookbuf.api.responses.parsers.impl.train;

import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.impl.train.components.LessonComponent;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.train.components.CourseComponentJSONImpl;
import com.bookbuf.api.responses.parsers.impl.train.components.LessonComponentJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public class FetchLessonResponseJSONImpl extends PuDongParserImpl implements FetchLessonResponse, Serializable {


    @IgnoreKey
    CourseComponent course;

    @Key("lessons")
    @Implementation(LessonComponentJSONImpl.class)
    private List<LessonComponent> lessons;

    public FetchLessonResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.course = new CourseComponentJSONImpl(jsonObject);
    }


    @Override
    public CourseComponent course() {
        return course;
    }

    @Override
    public List<LessonComponent> lessons() {
        return lessons;
    }
}
