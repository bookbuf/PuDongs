package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.ClerkResponse;
import com.bookbuf.api.responses.impl.user.ProfileResponse;
import com.bookbuf.api.responses.impl.user.VendorResponse;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class ClerkResponseJSONImpl extends PuDongParserImpl implements ClerkResponse, Serializable {

    @IgnoreKey
    private ProfileResponse profileResponse;
    @IgnoreKey
    private VendorResponse vendor;

    public ClerkResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.profileResponse = new ProfileResponseJSONImpl(jsonObject);
        this.vendor = new VendorResponseJSONImpl(jsonObject);
    }

    @Override
    public ProfileResponse profile() {
        return this.profileResponse;
    }

    @Override
    public VendorResponse vendor() {
        return vendor;
    }
}
