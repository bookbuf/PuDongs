package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskTargetResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskTargetResponseJSONImpl extends PuDongParserImpl implements TaskTargetResponse, Serializable {

    @Key("type")
    private String type;

    public TaskTargetResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String type() {
        return this.type;
    }

}
