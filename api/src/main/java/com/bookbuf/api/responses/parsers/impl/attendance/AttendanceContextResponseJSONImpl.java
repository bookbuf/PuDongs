package com.bookbuf.api.responses.parsers.impl.attendance;

import com.bookbuf.api.responses.impl.attendance.AttendanceContextResponse;
import com.bookbuf.api.responses.impl.attendance.components.AttendanceRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.attendance.components.AttendanceRecordResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/27.
 */
public final class AttendanceContextResponseJSONImpl extends PuDongParserImpl implements AttendanceContextResponse, Serializable {
    @Key("date")
    private String date;
    @Key("description")
    private String description;
    @Key("records")
    @Implementation(AttendanceRecordResponseJSONImpl.class)
    private List<AttendanceRecordResponse> records;

    public AttendanceContextResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String date() {
        return date;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public List<AttendanceRecordResponse> records() {
        return records;
    }
}
