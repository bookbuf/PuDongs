package com.bookbuf.api.responses.parsers.impl.attendance.components;

import com.bookbuf.api.responses.impl.attendance.components.AttendanceRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/27.
 */
public final class AttendanceRecordResponseJSONImpl extends PuDongParserImpl implements AttendanceRecordResponse, Serializable {
    @Key("type")
    private int type;
    @Key("title")
    private String title;
    @Key("status")
    private int status;
    @Key("tag")
    private String tag;
    @Key("time")
    private String time;
    @Key("address")
    private String address;
    @Key("remark")
    private String remark;

    public AttendanceRecordResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int type() {
        return type;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public String tag() {
        return tag;
    }

    @Override
    public String time() {
        return time;
    }

    @Override
    public String address() {
        return address;
    }

    @Override
    public String remark() {
        return remark;
    }
}
