package com.bookbuf.api.responses.parsers.impl.order;

import com.bookbuf.api.responses.impl.order.OrderResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/5/3.
 */

public class OrderResponseJSONImpl extends PuDongParserImpl implements OrderResponse, Serializable {

    @Key("currentPrice")
    private String currentPrice;
    @Key("totalPrice")
    private String totalPrice;
    @Key("couponCut")
    private String couponCut;
    @Key("promoCut")
    private String promoCut;
    @Key("cutPoint")
    private String cutPoint;

    public OrderResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String currentPrice() {
        return currentPrice;
    }

    @Override
    public String totalPrice() {
        return totalPrice;
    }

    @Override
    public String couponCut() {
        return couponCut;
    }

    @Override
    public String promoCut() {
        return promoCut;
    }

    @Override
    public String cutPoint() {
        return cutPoint;
    }
}
