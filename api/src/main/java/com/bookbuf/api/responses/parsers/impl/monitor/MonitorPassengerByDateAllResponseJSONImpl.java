package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorPassengerByDateAllResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorPassengerByDateResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSelectAllResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorPassengerByDateAllResponseJSONImpl extends PuDongParserImpl implements MonitorPassengerByDateAllResponse,Serializable {

    @Key("code")
    int code;
    @Key("data")
    @Implementation(MonitorPassengerByDateResponseJSONImpl.class)
    MonitorPassengerByDateResponse dateResponse;
    @Key("message")
    String message;

    public MonitorPassengerByDateAllResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public MonitorPassengerByDateResponse data() {
        return dateResponse;
    }

    @Override
    public String message() {
        return message;
    }
}
