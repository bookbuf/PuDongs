package com.bookbuf.api.responses.impl.examination;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.question.AnswerResponse;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface ExamResponse extends Response {


	long id ();

	/**
	 * 考试状态（进行中、已结束等）
	 *
	 * @return status
	 */
	int status ();

	/**
	 * 考试剩余时间
	 *
	 * @return remainTime
	 */
	int remainTime ();

	/**
	 * 考试成绩所属的奖牌分类：金、银、铜、铁；
	 *
	 * @return medal
	 */
	//@Deprecated 可以合并到ExamRankResponse中
	int medal ();

	/**
	 * 考试评价
	 *
	 * @return evaluate
	 */
	// @Deprecated 可以合并到ExamRankResponse中
	String evaluate ();

	/**
	 * 我的考试概述
	 *
	 * @return mineRank
	 */
	ExamRankResponse mineRank ();

	/**
	 * 该场考试所关联的试卷的排行榜
	 *
	 * @return rankList
	 */
	List<ExamRankResponse> ranks ();

	/**
	 * 考试关联的考卷
	 *
	 * @return paper
	 */
	ExamPaperResponse paper ();

	/**
	 * 考试题号、作答答案、正确答案
	 *
	 * @return answers
	 */
	List<AnswerResponse> answers ();
}
