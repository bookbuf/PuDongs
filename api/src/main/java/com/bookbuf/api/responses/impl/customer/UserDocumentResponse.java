package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public interface UserDocumentResponse extends Response {


    String name();

    int gender();

    String birthday();

    String mobile();

    String height();

    String weight();
}


