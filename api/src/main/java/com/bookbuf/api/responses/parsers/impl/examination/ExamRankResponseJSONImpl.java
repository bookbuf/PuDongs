package com.bookbuf.api.responses.parsers.impl.examination;

import com.bookbuf.api.responses.impl.examination.ExamRankResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class ExamRankResponseJSONImpl extends PuDongParserImpl implements ExamRankResponse, Serializable {

    @Key("name")
    private String name;
    @Key("rank")
    private int rank;
    @Key("avatar")
    private String avatar;
    @Key("score")
    private int score;

    public ExamRankResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int rank() {
        return this.rank;
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public int score() {
        return this.score;
    }

    @Override
    public String avatar() {
        return this.avatar;
    }

}
