package com.bookbuf.api.responses.impl.task;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface TaskResponse extends Response {

    String TYPE_LIMB = "limb";
    String TYPE_LEAF = "leaf";

    /**
     * 取值为 limb | leaf，表明是枝干 或 叶子
     *
     * @return type
     */
    String type();

    boolean isLeaf();

    /**
     * 任务编号
     *
     * @return id
     */
    long id();

    /**
     * 若 type = limb，则表明含有tasks节点。
     * 若 type = leaf，则表明该tasks节点为空。
     *
     * @return tasks
     */
    List<TaskResponse> tasks();

    TaskDescResponse desc();

    String extras();

    TaskIntentResponse intent();

    TaskStatResponse stat();

    TaskUIResponse ui();

    TaskTargetResponse target();

    interface TaskDesc {
        String title();

        String summary();
    }

    interface TaskExtras {
        /**
         * 扩展字段
         *
         * @return extra
         */
        String extra();
    }

    interface TaskIntent {
        String type();

        String url();// FIXME: 16/9/14 目前只有打开H5类型，后面url字段扩展成JSON。
    }

    interface TaskStat {
        int totalNum();

        int performNum();
    }

    interface TaskUI {
        String background();
    }

    interface TaskTarget {
        String type();
    }

}
