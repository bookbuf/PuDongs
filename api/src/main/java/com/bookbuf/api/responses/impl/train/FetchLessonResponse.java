package com.bookbuf.api.responses.impl.train;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.impl.train.components.LessonComponent;

import java.util.List;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface FetchLessonResponse extends Response {

    CourseComponent course();

    List<LessonComponent> lessons();

}
