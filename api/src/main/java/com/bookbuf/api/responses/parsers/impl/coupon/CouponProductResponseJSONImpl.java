package com.bookbuf.api.responses.parsers.impl.coupon;

import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class CouponProductResponseJSONImpl extends PuDongParserImpl implements CouponProductResponse, Serializable {


    @Key("id")
    private long couponId;
    @Key("status")
    private int status;
    @Key("productImgs")
    private List<String> productImgs;
    @Key("title")
    private String title;
    @Key("detail")
    private String detail;
    @Key("className")
    private String className;
    @Key("productName")
    private String productName;
    @Key("startTime")
    private String startTime;
    @Key("endTime")
    private String endTime;
    @Key("promoCode")
    private String promoCode;
    @Key("userCouponId")
    private long userCouponId;
    @Key("mode")
    private String mode;

    public CouponProductResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }


    @Override
    public long couponId() {
        return couponId;
    }

    @Override
    public List<String> productImgs() {
        return productImgs;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String detail() {
        return detail;
    }

    @Override
    public String className() {
        return className;
    }

    @Override
    public String productName() {
        return productName;
    }

    @Override
    public String startTime() {
        return startTime;
    }

    @Override
    public String endTime() {
        return endTime;
    }

    @Override
    public String promoCode() {
        return promoCode;
    }

    @Override
    public long userCouponId() {
        return userCouponId;
    }

    @Override
    public String mode() {
        return mode;
    }
}
