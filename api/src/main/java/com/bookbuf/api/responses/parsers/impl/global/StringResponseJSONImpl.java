package com.bookbuf.api.responses.parsers.impl.global;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.bookbuf.api.responses.impl.global.StringResponse;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/6/30.
 */

public class StringResponseJSONImpl extends PuDongParserImpl implements StringResponse , Serializable {
    public StringResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }
}
