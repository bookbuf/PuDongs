package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskDescResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskDescResponseJSONImpl extends PuDongParserImpl implements TaskDescResponse, Serializable {
    @Key("summary")
    private String summary;
    @Key("title")
    private String title;

    public TaskDescResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String title() {
        return this.title;
    }

    @Override
    public String summary() {
        return this.summary;
    }

}
