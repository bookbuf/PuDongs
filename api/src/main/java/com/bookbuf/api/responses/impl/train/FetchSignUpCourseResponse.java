package com.bookbuf.api.responses.impl.train;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;

import java.util.List;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface FetchSignUpCourseResponse extends Response {

    AchievementComponent achievement();

    List<CourseComponent> courses();
}
