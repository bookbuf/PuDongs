package com.bookbuf.api.responses.parsers.impl.coupon;

import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/8/16.
 */

public class CouponTypeListResponseJSONImpl extends PuDongParserImpl implements CouponTypeListResponse, Serializable {

    @Implementation(CouponTypeResponseJSONImpl.class)
    private List<CouponTypeResponse> couponTypeList;

    public CouponTypeListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<CouponTypeResponse> couponTypeList() {
        return couponTypeList;
    }
}
