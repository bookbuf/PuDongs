package com.bookbuf.api.responses.parsers.impl.examination;

import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.question.QuestionResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class ExamPaperResponseJSONImpl extends PuDongParserImpl implements ExamPaperResponse, Serializable {

    @Key("id")
    private long id;
    @Key("title")
    private String title;
    @Key("summary")
    private String summary;
    @Key("goToReview")
    private String goToReview;
    @Key("duration")
    private int duration;
    @Key("questions")
    @Implementation(QuestionResponseJSONImpl.class)
    private List<QuestionResponse> questionResponseList;

    public ExamPaperResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public long id() {
        return this.id;
    }

    @Override
    public String title() {
        return this.title;
    }

    @Override
    public int duration() {
        return this.duration;
    }

    @Override
    public String summary() {
        return this.summary;
    }

    @Override
    public String advice() {
        return this.goToReview;
    }

    @Override
    public List<QuestionResponse> questions() {
        return this.questionResponseList;
    }


}
