package com.bookbuf.api.responses.parsers.impl.origin;

import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

public final class LongResponseJSONImpl extends PuDongParserImpl implements LongResponse, Serializable {

    private long value;

    public LongResponseJSONImpl(JSONObject object) {
        super(object);
    }


    @Override
    public long get() {
        return value;
    }
}
