package com.bookbuf.api.responses.parsers.impl.attendance.components;

import com.bookbuf.api.responses.impl.attendance.components.AttendanceMonthRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/27.
 */
public final class AttendanceMonthRecordResponseJSONImpl extends PuDongParserImpl implements AttendanceMonthRecordResponse, Serializable {
    @Key("day")
    private int day;
    @Key("isExceptionStatus")
    private int status;
    @Key("signInCount")
    private int signInCount;

    public AttendanceMonthRecordResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int day() {
        return day;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public int signInCount() {
        return signInCount;
    }
}
