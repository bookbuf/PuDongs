package com.bookbuf.api.responses.parsers.impl.integral;

import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.integral.IntegralResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class IntegralResponseJSONImpl extends PuDongParserImpl implements IntegralResponse, Serializable {

    @Key("vendorName")
    private String vendorName;
    @Key("logo")
    private String logo;
    @Key("cardNumber")
    private String cardNumber;
    @Key("integral")
    private String integral;
    @Key("integral_unit")
    private String integralUnit;

    public IntegralResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String vendorName() {
        return vendorName;
    }

    @Override
    public String logo() {
        return logo;
    }

    @Override
    public String cardNumber() {
        return cardNumber;
    }

    @Override
    public String integral() {
        return integral;
    }

    @Override
    public String integral_unit() {
        return integralUnit;
    }
}
