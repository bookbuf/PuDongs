package com.bookbuf.api.responses.impl.train.components;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface CourseComponent {

    long id();

    String title();

    String description();

    /**
     * 0-未学习
     * 1-学习中
     * 2-已通过
     *
     * @return 课程的学习状态
     */
    int status();

    boolean online();

    int passedCount();

    int totalCount();

}
