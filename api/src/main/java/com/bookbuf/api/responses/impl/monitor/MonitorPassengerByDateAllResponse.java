package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/12/6.
 */

public interface MonitorPassengerByDateAllResponse extends Response {
    int code();

    MonitorPassengerByDateResponse data();

    String message();
}
