package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeDataResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorCountTypeDataResponseJSONImpl extends PuDongParserImpl implements MonitorCountTypeDataResponse,Serializable {

    @Key("storeNumber")
    String storeNumber;
    @Key("alarmData")
    @Implementation(MonitorCountTypeResponseJSONImpl.class)
    List<MonitorCountTypeResponse> list;

    public MonitorCountTypeDataResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<MonitorCountTypeResponse> alarmData() {
        return list;
    }

    @Override
    public String storeNumber() {
        return storeNumber;
    }
}
