package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.ProfileResponse;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class CustomerResponseJSONImpl extends PuDongParserImpl implements CustomerResponse, Serializable {

    @IgnoreKey
    private ProfileResponse profileResponse;

    public CustomerResponseJSONImpl(JSONObject object) {
        super(object);
        this.profileResponse = new ProfileResponseJSONImpl(object);
    }

    @Override
    public ProfileResponse profile() {
        return this.profileResponse;
    }
}
