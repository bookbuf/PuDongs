package com.bookbuf.api.responses.impl;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface BannerResponse extends Response {

    List<BannerDetailResponse> banners();

    interface BannerDetailResponse extends Response {
        String intent();

        String url();
    }
}
