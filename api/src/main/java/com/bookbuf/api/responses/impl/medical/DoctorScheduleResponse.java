package com.bookbuf.api.responses.impl.medical;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface DoctorScheduleResponse extends Response {

	String scheduleItemCode ();

	String serviceDate ();

	Integer weekDay ();

	String sessionName ();

	String startTime ();

	String endTime ();

	int departmentCode ();

	String departmentName ();

	Integer doctorCode ();

	String doctorName ();

	Integer doctorTitleCode ();

	String doctorTitle ();

	Integer doctorSessTypeCode ();

	String doctorSessType ();

	String fee ();

	Integer availableNum ();

	String admitTimeRange ();

	Integer lockQueueNo ();

	String doctorSpec ();

	String admitAddress ();
}
