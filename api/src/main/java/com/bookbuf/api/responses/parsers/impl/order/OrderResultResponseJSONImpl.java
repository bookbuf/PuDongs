package com.bookbuf.api.responses.parsers.impl.order;

import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderResultResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.coupon.CouponResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/5/3.
 */

public class OrderResultResponseJSONImpl extends PuDongParserImpl implements OrderResultResponse, Serializable {
    @Key("isPaySuccess")
    private String isPaySuccess;
    @Key("orderId")
    private String orderId;
    @Key("error")
    private String error;
    @Key("productDetail")
    @Implementation(CouponResponseJSONImpl.class)
    private CouponResponse detail;

    public OrderResultResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String isPaySuccess() {
        return isPaySuccess;
    }

    @Override
    public String orderId() {
        return orderId;
    }

    @Override
    public String error() {
        return error;
    }

    @Override
    public CouponResponse detail() {
        return detail;
    }
}
