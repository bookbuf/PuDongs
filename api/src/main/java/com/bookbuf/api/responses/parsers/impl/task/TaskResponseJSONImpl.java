package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskDescResponse;
import com.bookbuf.api.responses.impl.task.TaskIntentResponse;
import com.bookbuf.api.responses.impl.task.TaskResponse;
import com.bookbuf.api.responses.impl.task.TaskStatResponse;
import com.bookbuf.api.responses.impl.task.TaskTargetResponse;
import com.bookbuf.api.responses.impl.task.TaskUIResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskResponseJSONImpl extends PuDongParserImpl implements TaskResponse, Serializable {

    @Key("type")
    private String type;
    @Key("id")
    private long id;
    @Key("tasks")
    @Implementation(TaskResponseJSONImpl.class)
    private List<TaskResponse> taskResponsesList;
    @Key("desc")
    @Implementation(TaskDescResponseJSONImpl.class)
    private TaskDescResponse descResponse;
    @Key("extras")
    private String extrasResponse;
    @Key("intent")
    @Implementation(TaskIntentResponseJSONImpl.class)
    private TaskIntentResponse intentResponse;
    @Key("stat")
    @Implementation(TaskStatResponseJSONImpl.class)
    private TaskStatResponse statResponse;
    @Key("ui")
    @Implementation(TaskUIResponseJSONImpl.class)
    private TaskUIResponse uiResponse;
    @Key("target")
    @Implementation(TaskTargetResponseJSONImpl.class)
    private TaskTargetResponse targetResponse;

    public TaskResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String type() {
        return this.type;
    }

    @Override
    public boolean isLeaf() {
        return TYPE_LEAF.equalsIgnoreCase(this.type);
    }

    @Override
    public long id() {
        return this.id;
    }

    @Override
    public List<TaskResponse> tasks() {
        return this.taskResponsesList;
    }

    @Override
    public TaskDescResponse desc() {
        return this.descResponse;
    }

    @Override
    public String extras() {
        return this.extrasResponse;
    }

    @Override
    public TaskIntentResponse intent() {
        return this.intentResponse;
    }

    @Override
    public TaskStatResponse stat() {
        return this.statResponse;
    }

    @Override
    public TaskUIResponse ui() {
        return this.uiResponse;
    }

    @Override
    public TaskTargetResponse target() {
        return this.targetResponse;
    }

}
