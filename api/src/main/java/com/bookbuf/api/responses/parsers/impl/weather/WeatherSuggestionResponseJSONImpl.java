package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherMapResponse;
import com.bookbuf.api.responses.impl.weather.WeatherSuggestionResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherSuggestionResponseJSONImpl extends PuDongParserImpl implements WeatherSuggestionResponse, Serializable {

    @Key("comf")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse comf;
    @Key("cw")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse cw;
    @Key("drsg")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse drsg;
    @Key("flu")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse flu;
    @Key("sport")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse sport;
    @Key("trav")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse trav;
    @Key("uv")
    @Implementation(WeatherMapResponseJSONImpl.class)
    private WeatherMapResponse uv;

    public WeatherSuggestionResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public WeatherMapResponse comf() {
        return comf;
    }

    @Override
    public WeatherMapResponse cw() {
        return cw;
    }

    @Override
    public WeatherMapResponse drsg() {
        return drsg;
    }

    @Override
    public WeatherMapResponse flu() {
        return flu;
    }

    @Override
    public WeatherMapResponse sport() {
        return sport;
    }

    @Override
    public WeatherMapResponse trav() {
        return trav;
    }

    @Override
    public WeatherMapResponse uv() {
        return uv;
    }
}
