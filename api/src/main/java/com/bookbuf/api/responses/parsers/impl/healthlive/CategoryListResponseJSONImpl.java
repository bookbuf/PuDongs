package com.bookbuf.api.responses.parsers.impl.healthlive;

import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.CategoryResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class CategoryListResponseJSONImpl extends PuDongParserImpl implements CategoryListResponse, Serializable {

    @Implementation(CategoryResponseJSONImpl.class)
    private List<CategoryResponse> categorys;

    public CategoryListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<CategoryResponse> categorys() {
        return categorys;
    }
}
