package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bo.wei on 2017/4/20.
 */

public interface CouponProductResponse extends Response{

    long couponId();

    String title();

    String detail();

    String className();

    String productName();

    String startTime();

    String endTime();

    String promoCode();

    String mode();

    long userCouponId();

    int status();

    List<String> productImgs();


}
