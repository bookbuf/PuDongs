package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/15.
 */

public interface IntegrityResponse extends Response{

    String contactFlag();

    String identificationFlag();

    String membershipCardFlag();
}
