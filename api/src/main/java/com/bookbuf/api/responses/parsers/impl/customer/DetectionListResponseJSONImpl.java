package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.DetectionItemResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public class DetectionListResponseJSONImpl extends PuDongParserImpl implements DetectionListResponse, Serializable {


    @Implementation(DetectionItemResponseJSONImpl.class)
    @Key("indicator_values")
    private List<DetectionItemResponse> list;
    @Key("prompts")
    private String prompts;

    public DetectionListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<DetectionItemResponse> list() {
        return list;
    }

    @Override
    public String prompts() {
        return prompts;
    }
}
