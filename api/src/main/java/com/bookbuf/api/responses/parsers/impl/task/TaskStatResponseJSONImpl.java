package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskStatResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskStatResponseJSONImpl extends PuDongParserImpl implements TaskStatResponse, Serializable {
    @Key("performNum")
    private int performNum;
    @Key("totalNum")
    private int totalNum;

    public TaskStatResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public int totalNum() {
        return this.totalNum;
    }

    @Override
    public int performNum() {
        return this.performNum;
    }

}
