package com.bookbuf.api.responses.parsers.impl.origin;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

public final class BooleanResponseJSONImpl extends PuDongParserImpl implements BooleanResponse, VerifyCouponResponse, Serializable {

    private boolean bool;

    public BooleanResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public boolean get() {
        return bool;
    }

}
