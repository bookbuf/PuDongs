package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherAQIResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherAQIResponseJSONImpl extends PuDongParserImpl implements WeatherAQIResponse, Serializable {

    @Key("aqi")
    private String aqi;
    @Key("co")
    private String co;
    @Key("no2")
    private String no2;
    @Key("o3")
    private String o3;
    @Key("pm10")
    private String pm10;
    @Key("pm25")
    private String pm25;
    @Key("qlty")
    private String qlty;
    @Key("so2")
    private String so2;

    public WeatherAQIResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String aqi() {
        return aqi;
    }

    @Override
    public String co() {
        return co;
    }

    @Override
    public String no2() {
        return no2;
    }

    @Override
    public String o3() {
        return o3;
    }

    @Override
    public String pm10() {
        return pm10;
    }

    @Override
    public String pm25() {
        return pm25;
    }

    @Override
    public String qlty() {
        return qlty;
    }

    @Override
    public String so2() {
        return so2;
    }
}
