package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public interface CustomerLoginResponse extends Response {

    // 用户登录的 userId
    long loginId();

    // 用户登录的 sessionId
    String sessionId();

    // 登录状态，0为登录，1为注册
    int status();
}
