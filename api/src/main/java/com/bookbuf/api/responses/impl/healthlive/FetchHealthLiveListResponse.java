package com.bookbuf.api.responses.impl.healthlive;

import java.util.List;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public interface FetchHealthLiveListResponse {

    List<HealthLiveResponse> lists();

}
