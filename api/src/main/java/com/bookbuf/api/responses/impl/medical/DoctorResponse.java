package com.bookbuf.api.responses.impl.medical;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface DoctorResponse extends Response {

	String departmentCode ();

	String departmentName ();

	int code ();

	String name ();

	String titleCode ();

	String title ();

	String typeCode ();

	String type ();

	String fee ();

	int availableNum ();

	String avatar ();

	String summary ();

}
