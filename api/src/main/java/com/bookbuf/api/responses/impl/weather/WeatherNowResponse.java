package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherNowResponse extends Response {

    String fl();

    String hum();

    String pcpn();

    String pres();

    String tmp();

    String vis();

    WindResponse wind();

    CondResponse cond();

    interface CondResponse extends Response {
        String code();

        String txt();
    }

    interface WindResponse extends Response {
        String deg();

        String dir();

        String sc();

        String spd();
    }
}
