package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherAlarmResponse extends Response {

	String level ();

	String stat ();

	String title ();

	String txt ();

	String type ();
}
