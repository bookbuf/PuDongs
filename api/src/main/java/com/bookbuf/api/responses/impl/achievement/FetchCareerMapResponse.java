package com.bookbuf.api.responses.impl.achievement;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;

import java.util.List;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface FetchCareerMapResponse extends Response {

    /**
     * type = 1 表示职业规划
     *
     * @return type
     */
    int type();

    boolean isHot();

    /**
     * 要求按等级从低到高排序
     *
     * @return 成就路径
     */
    List<AchievementComponent> achievements();
}
