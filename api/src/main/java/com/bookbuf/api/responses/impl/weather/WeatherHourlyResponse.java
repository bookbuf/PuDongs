package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherHourlyResponse extends Response {

    String date();

    String hum();

    String pop();

    String pres();

    String tmp();

    CondResponse cond();

    WindResponse wind();

    interface CondResponse extends Response {
        String code();

        String txt();
    }

    interface WindResponse extends Response {
        String deg();

        String dir();

        String sc();

        String spd();
    }
}
