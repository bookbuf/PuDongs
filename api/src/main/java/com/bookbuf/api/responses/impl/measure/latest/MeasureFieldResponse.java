package com.bookbuf.api.responses.impl.measure.latest;

import com.bookbuf.api.responses.Response;

/**
 * "value1": {
 * "name": "收缩压",
 * "value": "111",
 * "status": 1
 * },
 * "value2": {
 * "name": "舒张压",
 * "value": "69",
 * "status": 1
 * }
 * author: robert.
 * date :  16/11/28.
 */

public interface MeasureFieldResponse extends Response {

	String id ();

	String name ();

	String value ();

	int status ();
}
