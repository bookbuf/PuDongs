package com.bookbuf.api.responses.impl.coupon;

import java.util.List;

/**
 * Created by bo.wei on 2017/8/16.
 */

public interface CouponTypeListResponse {
    List<CouponTypeResponse> couponTypeList();
}
