package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/12/1.
 */

public interface MonitorSiteListResponse extends Response {

    int code();

    List<MonitorSiteResponse> list();

    String message();
}
