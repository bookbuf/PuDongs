package com.bookbuf.api.responses.parsers.impl.question;

import com.bookbuf.api.responses.impl.examination.FetchDiscoverQuestionResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2016/11/24.
 */

public class FetchDiscoverQuestionResponseJSONImpl extends PuDongParserImpl implements FetchDiscoverQuestionResponse, Serializable {

    @Key("examId")
    private int id;
    @Key("question")
    private String title;
    @Key("options")
    private List<String> list;

    public FetchDiscoverQuestionResponseJSONImpl(JSONObject object) {
        super(object);

    }

    @Override
    public int id() {
        return this.id;
    }

    @Override
    public String title() {
        return this.title;
    }

    @Override
    public List<String> choiceList() {
        return this.list;
    }
}
