package com.bookbuf.api.responses.parsers.impl.medical;

import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

import static com.bookbuf.api.responses.parsers.InternalParseUtil.getInt;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class MedicalRegisterResponseJSONImpl extends PuDongParserImpl implements MedicalRegisterResponse, Serializable {

    private long id;
    @Key("hospital_name")
    private String hospitalName;
    @Key("hospital_department")
    private String hospitalDepartment;
    @Key("hospital_doctor")
    private String hospitalDoctor;
    @Key("hospital_tel")
    private String hospitalTel;
    @Key("clinical_time")
    private String clinicalTime;
    @Key("clinical_cost")
    private String clinicalCost;
    @Key("clinical_person")
    private String clinicalPerson;
    @Key("clinical_person_tel")
    private String clinicalPersonTel;
    @Key("remainNum")
    private int remainNum;
    @Key("totalNum")
    private int remainMaxNum;
    @Key("hospital_address")
    private String hospitalAddress;

    public MedicalRegisterResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.id = getInt("guahaoId", jsonObject);
        if (this.id == -1) {
            this.id = getInt("id", jsonObject);
        }
    }

    @Override
    public long id() {
        return id;
    }


    @Override
    public String hospitalName() {
        return hospitalName;
    }


    @Override
    public String hospitalDepartment() {
        return hospitalDepartment;
    }


    @Override
    public String hospitalDoctor() {
        return hospitalDoctor;
    }


    @Override
    public String hospitalAddress() {
        return hospitalAddress;
    }

    @Override
    public String hospitalTel() {
        return hospitalTel;
    }

    @Override
    public String clinicalTime() {
        return clinicalTime;
    }

    @Override
    public String clinicalCost() {
        return clinicalCost;
    }

    @Override
    public String clinicalPerson() {
        return clinicalPerson;
    }


    @Override
    public String clinicalPersonTel() {
        return clinicalPersonTel;
    }


    @Override
    public int remainNum() {
        return remainNum;
    }


    @Override
    public int remainMaxNum() {
        return remainMaxNum;
    }
}
