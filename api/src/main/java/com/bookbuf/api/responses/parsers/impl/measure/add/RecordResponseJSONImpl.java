package com.bookbuf.api.responses.parsers.impl.measure.add;

import com.bookbuf.api.responses.impl.measure.list.RecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

public final class RecordResponseJSONImpl extends PuDongParserImpl implements RecordResponse, Serializable {


    @Key("indicator_id")
    private String indicatorId;
    @Key("status")
    private int status;
    @Key("date")
    private String dateFormat;
    @Key("value")
    private String valueFormat;

    public RecordResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String indicatorId() {
        return indicatorId;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public String dateFormat() {
        return dateFormat;
    }

    @Override
    public String valueFormat() {
        return valueFormat;
    }

}
