package com.bookbuf.api.responses.parsers.impl.achievement;

import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.achievement.components.AchievementComponentJSONImpl;
import com.bookbuf.api.responses.parsers.impl.train.components.CourseComponentJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public final class FetchAchievementDetailResponseJSONImpl extends PuDongParserImpl implements FetchAchievementDetailResponse, Serializable {

    @IgnoreKey
    private AchievementComponent component;

    @Key("events")
    @Implementation(CourseComponentJSONImpl.class)
    private List<CourseComponent> events;

    public FetchAchievementDetailResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.component = new AchievementComponentJSONImpl(jsonObject);
    }

    @Override
    public AchievementComponent achievement() {
        return component;
    }

    @Override
    public List<CourseComponent> events() {
        return events;
    }
}
