package com.bookbuf.api.responses.parsers.impl.measure.add;

import com.bookbuf.api.responses.impl.measure.add.AddRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.MeasureRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.measure.latest.MeasureRecordResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;

public final class AddRecordResponseJSONImpl extends PuDongParserImpl implements AddRecordResponse, Serializable {


    @Key("suggestion")
    private String suggestion;
    @IgnoreKey
    private MeasureRecordResponse record;


    public AddRecordResponseJSONImpl(JSONObject object) {
        super(object);
        // 因为record在返回数据层级上平级，但是实际上他们是有层级关系的。
        // 所以此处视作兼容服务端的不正确的数据返回。
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.record = new MeasureRecordResponseJSONImpl(jsonObject);
    }

    @Override
    public String suggestion() {
        return suggestion;
    }

    @Override
    public MeasureRecordResponse record() {
        return record;
    }

}
