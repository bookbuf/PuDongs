package com.bookbuf.api.responses.impl.coupon;

/**
 * Created by bo.wei on 2017/8/16.
 * 优惠券分类
 */

public interface CouponTypeResponse {
    long id();

    String categoryName();

    String categoryIcon();
}
