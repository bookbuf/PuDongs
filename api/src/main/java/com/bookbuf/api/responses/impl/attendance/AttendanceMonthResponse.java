package com.bookbuf.api.responses.impl.attendance;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.attendance.components.AttendanceMonthRecordResponse;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface AttendanceMonthResponse extends Response {

    int year();

    int month();

    List<AttendanceMonthRecordResponse> records();
}
