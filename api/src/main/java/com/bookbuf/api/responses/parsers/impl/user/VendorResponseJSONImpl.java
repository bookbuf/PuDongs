package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.VendorResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class VendorResponseJSONImpl extends PuDongParserImpl implements VendorResponse, Serializable {

    @Key("vendor_id")
    private long id;
    @Key("vendor_shop_id")
    private long shopId;
    @Key("vendor_code")
    private String code;
    @Key("clerk_num")
    private String num;
    @Key("vendor_logo_url")
    private String logo;
    @Key("vendor_shop_name")
    private String name;
    @Key("vendor_name")
    private String department;
    @Key("role_name")
    private String role;
    @Key("vendor_position")
    private int position;
    @Key("vendor_shop_number")
    private String storeNumber;

    public VendorResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public long shopId() {
        return shopId;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String num() {
        return num;
    }

    @Override
    public String logo() {
        return logo;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String department() {
        return department;
    }

    @Override
    public String role() {
        return role;
    }

    @Override
    public int position() {
        return position;
    }

    @Override
    public String storeNumber() {
        return storeNumber;
    }
}
