package com.bookbuf.api.responses.parsers.impl.healthlive;

import com.bookbuf.api.responses.impl.healthlive.CategoryResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class CategoryResponseJSONImpl extends PuDongParserImpl implements CategoryResponse, Serializable {

    @Key("id")
    private long id;
    @Key("title")
    private String title;
    @Key("background")
    private String background;

    public CategoryResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String background() {
        return background;
    }
}
