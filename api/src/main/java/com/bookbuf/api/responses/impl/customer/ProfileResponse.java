package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.user.PhotoVerifiedResponse;

/**
 * Created by new on 2017/5/19.
 */

public interface ProfileResponse extends Response{

    String nickName();

    String avatar();

    String gender();

    String address();

    String idCard();

    String email();

    String province();

    String city();

    String distinct();

    String mobile();

    //1.6新增
    String integral();

    String coupon();

    String alias();

    String age();

    PhotoVerifiedResponse response();
}
