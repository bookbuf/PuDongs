package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.impl.BooleanResponse;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public interface VerifyCouponResponse extends BooleanResponse {
}
