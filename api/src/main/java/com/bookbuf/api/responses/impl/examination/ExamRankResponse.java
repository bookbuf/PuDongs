package com.bookbuf.api.responses.impl.examination;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface ExamRankResponse extends Response {

	/**
	 * 考试排名
	 *
	 * @return rank
	 */
	int rank ();

	/**
	 * 姓名
	 *
	 * @return name
	 */
	String name ();

	/**
	 * 得分
	 *
	 * @return score
	 */
	int score ();

	/**
	 * 头像
	 *
	 * @return avatar
	 */
	String avatar ();
}
