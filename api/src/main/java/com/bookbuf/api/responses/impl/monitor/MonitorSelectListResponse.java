package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/12/6.
 */

public interface MonitorSelectListResponse extends Response {
    int totalPages();

    int pageSize();

    int totalRows();

    int pageNum();

    List<MonitorSelectResponse> list();
}
