//package com.bookbuf.api.responses.parsers.impl.customer.detections;
//
//import com.bookbuf.api.responses.impl.customer.CustomerDetectionIndexResponse;
//import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
//import com.bookbuf.api.responses.parsers.annotations.Implementation;
//import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * author: robert.
// * date :  2017/3/20.
// */
//
//public class FieldWrapperResponseJSONImpl extends PuDongParserImpl implements CustomerDetectionIndexResponse.FieldWrapper, Serializable {
//
//    @IgnoreKey
//    @Implementation(FieldResponseJSONImpl.class)
//    private List<CustomerDetectionIndexResponse.Field> fields;
//
//    public FieldWrapperResponseJSONImpl(JSONObject jsonObject) {
//        super(null);// ignore
//        JSONObject reality = (JSONObject) getRealityJSONObject(jsonObject);
//        String[] keys = {"value1", "value2", "value3"};
//        fields = new ArrayList<>();
//        try {
//            for (String key : keys) {
//                if (reality.has(key)) {
//                    fields.add(new FieldResponseJSONImpl(reality.getJSONObject(key)));
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public List<CustomerDetectionIndexResponse.Field> fields() {
//        return fields;
//    }
//}