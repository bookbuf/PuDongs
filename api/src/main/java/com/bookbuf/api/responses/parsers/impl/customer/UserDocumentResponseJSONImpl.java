package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.UserDocumentResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public class UserDocumentResponseJSONImpl extends PuDongParserImpl implements UserDocumentResponse, Serializable {

    @Key("name")
    private String name;
    @Key("birthday")
    private String birthday;
    @Key("mobile")
    private String mobile;
    @Key("gender")
    private int gender;
    @Key("weight")
    private String weight;
    @Key("height")
    private String height;

    public UserDocumentResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int gender() {
        return gender;
    }

    @Override
    public String birthday() {
        return birthday;
    }

    @Override
    public String mobile() {
        return mobile;
    }

    @Override
    public String height() {
        return height;
    }

    @Override
    public String weight() {
        return weight;
    }
}
