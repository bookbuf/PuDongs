package com.bookbuf.api.responses.impl.task;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface TaskStatResponse extends Response, TaskResponse.TaskStat {
}
