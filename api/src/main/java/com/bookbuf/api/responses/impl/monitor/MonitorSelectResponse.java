package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/12/6.
 */

public interface MonitorSelectResponse extends Response {
    long alarmRecordid();

    String alarmSite();

    String alarmType();

    String imagePath();

    String alarmdescription();

    long alarmDate();
}
