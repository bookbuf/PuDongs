package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.customer.IntegrityResponse;
import com.bookbuf.api.responses.impl.user.AddressResponse;
import com.bookbuf.api.responses.impl.user.PhotoVerifiedResponse;
import com.bookbuf.api.responses.impl.user.ProfileResponse;
import com.bookbuf.api.responses.parsers.InternalParseUtil;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.customer.IntegrityResponseJSONImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class ProfileResponseJSONImpl extends PuDongParserImpl implements ProfileResponse, Serializable {

    @IgnoreKey
    private long userId;
    @IgnoreKey
    private int weight;
    @IgnoreKey
    private int height;
    @IgnoreKey
    private String idcard;

    @Key("createdUserId")
    private long createdUserId;
    @Key("vendorMemberId")
    private String vendorMemberId;
    @Key("realname")
    private String realname;
    @Key("mobile")
    private String mobile;
    @Key("email")
    private String email;
    @Key("phone")
    private String phone;
    @Key("avatar")
    private String avatar;
    @Key("birthday")
    private String birthday;
    @Key("gender")
    private String gender;
    @Key("age")
    private int age;
    @Key("status")
    private int status;
    @IgnoreKey
    private AddressResponse addressResponse;
    @IgnoreKey
    IntegrityResponse integrityResponse;
    @Key("photo_verified")
    @Implementation(PhotoVerifiedJSONImpl.class)
    PhotoVerifiedResponse photoVerifiedResponse;

    public ProfileResponseJSONImpl(JSONObject json) {
        super(json);

        JSONObject jsonObject = (JSONObject) getRealityJSONObject(json);
        this.userId = InternalParseUtil.getLong("user_id", jsonObject);
        if (this.userId == -1) {
            this.userId = InternalParseUtil.getLong("userId", jsonObject);
        }
        this.idcard = InternalParseUtil.getRawString("id_card", jsonObject);
        if (this.idcard == null) {
            this.idcard = InternalParseUtil.getRawString("idCard", jsonObject);
        }
        this.weight = InternalParseUtil.getInt("weight", jsonObject);
        if (this.weight != -1) {
            this.weight = InternalParseUtil.getInt("body_weight", jsonObject);
        }
        this.height = InternalParseUtil.getInt("height", jsonObject);
        if (this.height != -1) {
            this.height = InternalParseUtil.getInt("body_height", jsonObject);
        }
        Object address = InternalParseUtil.getObject("address", jsonObject);
        if (address instanceof JSONObject) {
            JSONObject addressJSON = (JSONObject) address;
            this.addressResponse = new AddressResponseJSONImpl(addressJSON);
        } else if (address instanceof String) {
            try {
                JSONObject addressJSON = new JSONObject((String) address);
                this.addressResponse = new AddressResponseJSONImpl(addressJSON);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Object integrity = InternalParseUtil.getObject("integrity", jsonObject);
        if (integrity instanceof JSONObject) {
            JSONObject addressJSON = (JSONObject) integrity;
            this.integrityResponse = new IntegrityResponseJSONImpl(addressJSON);
        } else if (integrity instanceof String) {
            try {
                JSONObject addressJSON = new JSONObject((String) integrity);
                this.integrityResponse = new IntegrityResponseJSONImpl(addressJSON);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public long userId() {
        return this.userId;
    }

    @Override
    public int status() {
        return this.status;
    }

    @Override
    public String realname() {
        return this.realname;
    }

    @Override
    public String mobile() {
        return this.mobile;
    }

    @Override
    public String email() {
        return this.email;
    }

    @Override
    public String idcard() {
        return this.idcard;
    }

    @Override
    public String vendorMemberId() {
        return this.vendorMemberId;
    }

    @Override
    public String avatar() {
        return this.avatar;
    }

    @Override
    public int weight() {
        return this.weight;
    }

    @Override
    public int height() {
        return this.height;
    }

    @Override
    public String birthday() {
        return this.birthday;
    }

    @Override
    public int age() {
        return this.age;
    }

    @Override
    public String gender() {
        return this.gender;
    }

    @Override
    public long createdUserId() {
        return this.createdUserId;
    }

    @Override
    public AddressResponse address() {
        return this.addressResponse;
    }

    @Override
    public String phone() {
        return this.phone;
    }

    @Override
    public IntegrityResponse response() {
        return integrityResponse;
    }

    @Override
    public PhotoVerifiedResponse photoResponse() {
        return photoVerifiedResponse;
    }
}
