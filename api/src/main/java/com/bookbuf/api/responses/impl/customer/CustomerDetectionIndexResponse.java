//package com.bookbuf.api.responses.impl.customer;
//
//import com.bookbuf.api.responses.Response;
//
//import java.util.List;
//
///**
// * author: robert.
// * date :  2017/3/20.
// */
//
//public interface CustomerDetectionIndexResponse extends Response {
//
//
//    HasBeenDetected hasBeenDetected();
//
//    NotDetected notDetected();
//
//    interface HasBeenDetected extends Response {
//
//        List<Detection> detections();
//    }
//
//    interface NotDetected extends Response {
//
//        List<Detection> detections();
//    }
//
//
//    interface Detection extends Response {
//
//        // "bmi"
//        String typeCode();
//
//        // "体质指数"
//        String name();
//
//        String unitName();
//
//        // 01-12 11:02
//        String takeTime();
//
//        // 2016-01-12 11:02:43
//        String time();
//
//        // 1
//        int status();
//
//        FieldWrapper fieldWrapper();
//
//    }
//
//    interface FieldWrapper extends Response {
//
//        List<Field> fields();
//    }
//
//    interface Field extends Response {
//
//        // 18.5,
//        double normalMin();
//
//        //  24,
//        double normalMax();
//
//        // 1,
//        int hasNormalMin();
//
//        //  1,
//        int hasNormalMax();
//
//        // "体质指数",
//        String name();
//
//        // "w_bmi",
//        String indicatorCode();
//
//        // "",
//        String unitName();
//
//        //  "23.0",
//        String value();
//
//        //  1
//        int status();
//
//    }
//}
