package com.bookbuf.api.responses.parsers.impl.measure.latest;

import com.bookbuf.api.responses.impl.measure.latest.MeasureRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.MeasureTaskResponse;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * {
 * "typeCode": "bp",
 * "value1": 2,
 * "value2": 3,
 * "status": 3
 * }
 */
public final class MeasureTaskResponseJSONImpl extends PuDongParserImpl implements MeasureTaskResponse, Serializable {

    @Key("text")
    private String title;
    @Key("jump_action_desc")
    private String action;
    @Key("color")
    private String color;
    @Key("jump_id")
    private int jumpId;
    @Key("last_time")
    private String lastDateFormat;
    @IgnoreKey
    private MeasureRecordResponse latestRecord;

    public MeasureTaskResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.latestRecord = new MeasureRecordResponseJSONImpl(jsonObject);
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String action() {
        return action;
    }

    @Override
    public String color() {
        return color;
    }

    @Override
    public int jumpId() {
        return jumpId;
    }

    @Override
    public MeasureRecordResponse latestRecord() {
        return latestRecord;
    }

    @Override
    public String lastDateFormat() {
        return lastDateFormat;
    }
}
