package com.bookbuf.api.responses.parsers.impl.question;

import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class QuestionResponseJSONImpl extends PuDongParserImpl implements QuestionResponse, Serializable {

    @Key("id")
    private long id;
    @Key("title")
    private String title;
    @Key("choicesNum")
    private int choicesNum;
    @Key("choices")
    @Implementation(ChoiceResponseJSONImpl.class)
    private List<ChoiceResponse> choiceResponses;

    public QuestionResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public long id() {
        return this.id;
    }

    @Override
    public String title() {
        return this.title;
    }

    @Override
    public int choicesNum() {
        return this.choicesNum;
    }

    @Override
    public List<ChoiceResponse> choices() {
        return this.choiceResponses;
    }
}
