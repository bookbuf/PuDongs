package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorCountByIdDataResponse extends Response{

    String generateDateTime();

    int id();

    int passengerGet();

    int passengerOff();

    int passengerPass();

    String terminalUserId();
}
