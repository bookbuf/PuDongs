package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface AddressResponse extends Response {

	/**
	 * @return 省份
	 */
	String province ();

	/**
	 * @return 地级市
	 */
	String city ();

	/**
	 * @return 行政区
	 */
	String district ();

	/**
	 * @return 街道地址详细到门牌号
	 */
	String detail ();

	/**
	 * @return 完整的地址
	 */
	String fullAddress ();
}
