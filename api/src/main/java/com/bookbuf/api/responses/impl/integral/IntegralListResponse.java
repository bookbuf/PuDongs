package com.bookbuf.api.responses.impl.integral;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bo.wei on 2017/6/12.
 */

public interface IntegralListResponse {
    List<IntegralResponse> list();
}
