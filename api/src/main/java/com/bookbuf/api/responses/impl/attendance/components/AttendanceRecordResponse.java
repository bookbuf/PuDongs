package com.bookbuf.api.responses.impl.attendance.components;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/12/5.
 */

public interface AttendanceRecordResponse extends Response {

    /**
     * 0签到 1签退
     *
     * @return int
     */
    int type();

    String title();

    int status();

    String tag();

    String time();

    String address();

    String remark();
}
