package com.bookbuf.api.responses.parsers.impl.healthlive;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bookbuf.api.responses.impl.healthlive.HealthLiveResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class HealthLiveResponseJSONImpl extends PuDongParserImpl implements HealthLiveResponse, Serializable {

    @Key("liveId")
    private long liveId;
    @Key("rate")
    private String rate;
    @Key("visits")
    private String visits;
    @Key("background")
    private String background;
    @Key("sumary")
    private String sumary;
    @Key("url")
    private String url;
    @Key("knowledge")
    private List<String> knowledge;
    @Key("originalCost")
    private String originalCost;
    @Key("currentCoast")
    private String currentCoast;
    @Key("description")
    private String description;
    @Key("title")
    private String title;
    @Key("avatar")
    private String avatar;
    @Key("detailDescription")
    private String detailDescription;

    public HealthLiveResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long liveId() {
        return liveId;
    }

    @Override
    public String rate() {
        return rate;
    }

    @Override
    public String visits() {
        return visits;
    }

    @Override
    public String background() {
        return background;
    }

    @NonNull
    @Override
    public String description() {
        return description;
    }

    @Override
    public String summary() {
        return sumary;
    }

    @Override
    public String url() {
        return url;
    }

    @Override
    public List<String> knowledge() {
        return knowledge;
    }

    @Nullable
    @Override
    public String originalCost() {
        return originalCost;
    }

    @Nullable
    @Override
    public String currentCoast() {
        return currentCoast;
    }

    @Nullable
    @Override
    public String title() {
        return title;
    }

    @Nullable
    @Override
    public String avatar() {
        return avatar;
    }

    @Override
    public String detailDescription() {
        return detailDescription;
    }
}
