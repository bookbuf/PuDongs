package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherAQIResponse;
import com.bookbuf.api.responses.impl.weather.WeatherAlarmResponse;
import com.bookbuf.api.responses.impl.weather.WeatherBasicResponse;
import com.bookbuf.api.responses.impl.weather.WeatherDailyResponse;
import com.bookbuf.api.responses.impl.weather.WeatherHourlyResponse;
import com.bookbuf.api.responses.impl.weather.WeatherNowResponse;
import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.bookbuf.api.responses.impl.weather.WeatherSuggestionResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import static com.bookbuf.api.responses.parsers.InternalParseUtil.getObject;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherResponseJSONImpl extends PuDongParserImpl implements WeatherResponse, Serializable {
    @Key("status")
    private String status;
    @Key("alarms")
    @Implementation(WeatherAlarmResponseJSONImpl.class)
    private List<WeatherAlarmResponse> alarms;
    @Key("basic")
    @Implementation(WeatherBasicResponseJSONImpl.class)
    private WeatherBasicResponse basic;
    @Key("daily_forecast")
    @Implementation(WeatherDailyResponseJSONImpl.class)
    private List<WeatherDailyResponse> daily;
    @Key("hourly_forecast")
    @Implementation(WeatherHourlyResponseJSONImpl.class)
    private List<WeatherHourlyResponse> hourly;
    @Key("now")
    @Implementation(WeatherNowResponseJSONImpl.class)
    private WeatherNowResponse now;
    @Key("suggestion")
    @Implementation(WeatherSuggestionResponseJSONImpl.class)
    private WeatherSuggestionResponse suggestion;
    private WeatherAQIResponse cityAQI;

    public WeatherResponseJSONImpl(JSONObject o) {
        super(o);
        JSONObject aqiJSON = (JSONObject) getObject("aqi", o);
        if (aqiJSON != null) {
            JSONObject cityJSON = (JSONObject) getObject("city", aqiJSON);
            if (cityJSON != null) {
                this.cityAQI = new WeatherAQIResponseJSONImpl(cityJSON);
            }
        }
    }

    @Override
    public List<WeatherAlarmResponse> alarms() {
        return alarms;
    }

    @Override
    public WeatherAQIResponse cityAQI() {
        return cityAQI;
    }

    @Override
    public WeatherBasicResponse basic() {
        return basic;
    }

    @Override
    public List<WeatherDailyResponse> daily() {
        return daily;
    }

    @Override
    public List<WeatherHourlyResponse> hourly() {
        return hourly;
    }

    @Override
    public WeatherNowResponse now() {
        return now;
    }

    @Override
    public WeatherSuggestionResponse suggestion() {
        return suggestion;
    }

    @Override
    public String status() {
        return status;
    }
}
