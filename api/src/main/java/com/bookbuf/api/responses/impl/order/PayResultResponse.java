package com.bookbuf.api.responses.impl.order;

/**
 * Created by bo.wei on 2017/5/10.
 */

public interface PayResultResponse {
    String alipayRequest();

    String partnerId();

    String prepayId();

    String nonceStr();

    String timeStamp();

    String packageName();

    String sign();
}
