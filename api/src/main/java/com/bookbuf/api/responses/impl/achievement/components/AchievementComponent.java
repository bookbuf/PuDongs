package com.bookbuf.api.responses.impl.achievement.components;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface AchievementComponent extends Response {

    /**
     * @return 成就编号
     */
    long id();

    /**
     * @return 成就标题
     */
    String title();

    /**
     * @return 成就描述
     */
    String description();

    /**
     * 0 表示未解锁
     * 1 表示进行中
     * 2 表示已通过
     *
     * @return 成就状态
     */
    int status();

    /**
     * @return 已通过数量
     */
    int passedCount();

    /**
     * @return 总数量
     */
    int totalCount();
}


