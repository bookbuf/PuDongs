package com.bookbuf.api.responses.parsers.impl.order;


import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.bookbuf.api.responses.impl.order.PayResultResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/5/3.
 */

public class PayResultResponseJSONImpl extends PuDongParserImpl implements PayResultResponse, Serializable {
    @Key("alipayRequest")
    private String alipayRequest;
    @Key("partnerId")
    private String partnerId;
    @Key("prepayId")
    private String prepayId;
    @Key("nonceStr")
    private String nonceStr;
    @Key("timeStamp")
    private String timeStamp;
    @Key("package")
    private String packageName;
    @Key("sign")
    private String sign;

    public PayResultResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String alipayRequest() {
        return alipayRequest;
    }

    @Override
    public String partnerId() {
        return partnerId;
    }

    @Override
    public String prepayId() {
        return prepayId;
    }

    @Override
    public String nonceStr() {
        return nonceStr;
    }

    @Override
    public String timeStamp() {
        return timeStamp;
    }

    @Override
    public String packageName() {
        return packageName;
    }

    @Override
    public String sign() {
        return sign;
    }
}
