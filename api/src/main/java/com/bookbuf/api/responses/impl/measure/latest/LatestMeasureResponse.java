package com.bookbuf.api.responses.impl.measure.latest;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/11/28.
 */

public interface LatestMeasureResponse extends Response {

	List<MeasureTaskResponse> tasks ();

	List<MeasureRecordResponse> records ();
}
