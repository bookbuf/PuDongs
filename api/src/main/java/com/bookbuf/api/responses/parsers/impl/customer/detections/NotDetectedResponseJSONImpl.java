//package com.bookbuf.api.responses.parsers.impl.customer.detections;
//
//import com.bookbuf.api.responses.impl.customer.CustomerDetectionIndexResponse;
//import com.bookbuf.api.responses.parsers.annotations.Implementation;
//import com.bookbuf.api.responses.parsers.annotations.Key;
//import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
//
//import org.json.JSONArray;
//
//import java.io.Serializable;
//import java.util.List;
//
///**
// * author: robert.
// * date :  2017/3/20.
// */
//
//public class NotDetectedResponseJSONImpl extends PuDongParserImpl implements CustomerDetectionIndexResponse.NotDetected, Serializable {
//
//    @Key("notDetected")
//    @Implementation(DetectionResponseJSONImpl.class)
//    private List<CustomerDetectionIndexResponse.Detection> detections;
//
//    public NotDetectedResponseJSONImpl(JSONArray array) {
//        super(array, null);
//    }
//
//    @Override
//    public List<CustomerDetectionIndexResponse.Detection> detections() {
//        return detections;
//    }
//}
