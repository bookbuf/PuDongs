package com.bookbuf.api.responses.parsers.impl.global;

import com.bookbuf.api.responses.impl.global.SplashScreenResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public final class SplashScreenResponseJSONImpl extends PuDongParserImpl implements SplashScreenResponse, Serializable {

    @Key("vendor")
    private long vendor;
    @Key("type")
    private String type;
    @Key("id")
    private long id;
    @Key("extras")
    private String extras;
    @Key("ui")
    @Implementation(UIJSONImpl.class)
    private UI ui;
    @Key("target")
    @Implementation(TargetJSONImpl.class)
    private Target target;

    public SplashScreenResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long vendor() {
        return vendor;
    }

    @Override
    public String type() {
        return type;
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public UI ui() {
        return ui;
    }

    @Override
    public Target target() {
        return target;
    }

    @Override
    public JSONObject extras() {
        try {
            return new JSONObject(extras);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    public final static class TargetJSONImpl extends PuDongParserImpl implements Target, Serializable {

        @Key("type")
        private String type;

        public TargetJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String type() {
            return type;
        }
    }

    public final static class UIJSONImpl extends PuDongParserImpl implements UI, Serializable {

        @Key("background")
        private String background;
        @Key("start")
        private long start;
        @Key("end")
        private long end;

        public UIJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String background() {
            return background;
        }

        @Override
        public long start() {
            return start;
        }

        @Override
        public long end() {
            return end;
        }
    }
}
