package com.bookbuf.api.responses.parsers.impl.order;

import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.coupon.CouponResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/5/2.
 */

public class OrderDetailResponseJSONImpl extends PuDongParserImpl implements OrderDetailResponse, Serializable {

    @Key("orderRequestId")
    private String orderRequestId;

    @Key("productDetail")
    @Implementation(CouponResponseJSONImpl.class)
    private CouponResponse detail;

    @Key("orderDetail")
    @Implementation(OrderResponseJSONImpl.class)
    private OrderResponse order;

    public OrderDetailResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String orderRequestId() {
        return orderRequestId;
    }

    @Override
    public CouponResponse detail() {
        return detail;
    }

    @Override
    public OrderResponse order() {
        return order;
    }
}
