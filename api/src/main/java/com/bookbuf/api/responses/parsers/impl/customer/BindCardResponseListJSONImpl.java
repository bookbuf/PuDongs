package com.bookbuf.api.responses.parsers.impl.customer;

import android.databinding.Bindable;

import com.bookbuf.api.responses.impl.customer.BindCardListResponse;
import com.bookbuf.api.responses.impl.customer.BindCardResponse;
import com.bookbuf.api.responses.impl.customer.IntegrityResponse;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/11/15.
 */

public class BindCardResponseListJSONImpl extends PuDongParserImpl implements BindCardListResponse, Serializable {

    @Implementation(BindCardResponseJSONImpl.class)
    @Key("array")
    List<BindCardResponse> list;

    public BindCardResponseListJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<BindCardResponse> list() {
        return list;
    }
}
