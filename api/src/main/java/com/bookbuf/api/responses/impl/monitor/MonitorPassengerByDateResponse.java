package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/12/6.
 */

public interface MonitorPassengerByDateResponse extends Response {
    int passergerOff();

    String storeNumber();

    long terminalUserId();

    long id();

    int passengerGet();

    int passengerPass();

    String generateDateTime();

}
