package com.bookbuf.api.responses.parsers.impl.healthlive;

import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.HealthLiveResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public class FetchHealthLiveListResponseJSONImpl extends PuDongParserImpl implements FetchHealthLiveListResponse, Serializable {

    @Implementation(HealthLiveResponseJSONImpl.class)
    private List<HealthLiveResponse> lists;

    public FetchHealthLiveListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<HealthLiveResponse> lists() {
        return lists;
    }

}
