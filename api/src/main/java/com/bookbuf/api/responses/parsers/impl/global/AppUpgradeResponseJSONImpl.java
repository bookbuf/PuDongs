package com.bookbuf.api.responses.parsers.impl.global;

import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class AppUpgradeResponseJSONImpl extends PuDongParserImpl implements AppUpgradeResponse, Serializable {

    @Key("appName")
    private String platform;
    @Key("content")
    private String content;
    @Key("channel")
    private String apkChannel;
    @Key("url")
    private String apkUrl;
    @Key("serialId")
    private String serialId;
    @Key("version")
    private int apkVersion;
    @Key("isUpdate")
    private int updateStrategy;
    @Key("vendorId")
    private int vendorId;

    public AppUpgradeResponseJSONImpl(JSONObject wrapperJSONObject) {
        super(wrapperJSONObject);
    }

    @Override
    public String platform() {
        return platform;
    }

    @Override
    public String content() {
        return content;
    }

    @Override
    public String apkChannel() {
        return apkChannel;
    }

    @Override
    public String apkUrl() {
        return apkUrl;
    }

    @Override
    public int apkVersion() {
        return apkVersion;
    }

    @Override
    public int updateStrategy() {
        return updateStrategy;
    }

    @Override
    public int vendorId() {
        return vendorId;
    }

    @Override
    public String serialId() {
        return serialId;
    }
}
