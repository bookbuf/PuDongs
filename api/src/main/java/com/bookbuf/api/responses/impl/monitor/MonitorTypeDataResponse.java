package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorTypeDataResponse extends Response {

    int code();

    MonitorTypeListResponse data();

    String message();
}
