package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorTypeResponse extends Response {

    long alarmRecordDateTime();

    int id();

    String status();

    String typeDesc();

    String typeId();

    String typeName();
}
