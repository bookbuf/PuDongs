package com.bookbuf.api.responses.parsers.impl.examination;

import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamRankResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.bookbuf.api.responses.impl.question.AnswerResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.question.AnswerResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class ExamResponseJSONImpl extends PuDongParserImpl implements ExamResponse, Serializable {

    @Key("id")
    private long id;
    @Key("state")
    private int state;
    @Key("remainTime")
    private int remainTime;
    @Key("examPaper")
    @Implementation(ExamPaperResponseJSONImpl.class)
    private ExamPaperResponse examPaperResponse;
    @Key("answers")
    @Implementation(AnswerResponseJSONImpl.class)
    private List<AnswerResponse> answerResponseList;
    @Key("level")
    private int medal;
    @Key("summary")
    private String evaluate;
    @Key("rankList")
    @Implementation(ExamRankResponseJSONImpl.class)
    private List<ExamRankResponse> rankResponseList;
    @Key("myRank")
    @Implementation(ExamRankResponseJSONImpl.class)
    private ExamRankResponse mineRank;

    public ExamResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public long id() {
        return this.id;
    }

    @Override
    public int status() {
        return this.state;
    }

    @Override
    public int remainTime() {
        return this.remainTime;
    }

    @Override
    public int medal() {
        return this.medal;
    }

    @Override
    public String evaluate() {
        return this.evaluate;
    }

    @Override
    public List<ExamRankResponse> ranks() {
        return this.rankResponseList;
    }

    @Override
    public ExamPaperResponse paper() {
        return this.examPaperResponse;
    }

    @Override
    public List<AnswerResponse> answers() {
        return this.answerResponseList;
    }

    @Override
    public ExamRankResponse mineRank() {
        return this.mineRank;
    }


}
