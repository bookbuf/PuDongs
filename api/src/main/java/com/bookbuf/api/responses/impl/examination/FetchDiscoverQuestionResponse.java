package com.bookbuf.api.responses.impl.examination;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2016/11/24.
 */

public interface FetchDiscoverQuestionResponse extends Response {
    int id();
    String title();
    List<String> choiceList();
}
