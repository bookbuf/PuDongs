package com.bookbuf.api.responses.parsers.impl.integral;

import com.bookbuf.api.responses.impl.integral.IntegralResponse;
import com.bookbuf.api.responses.impl.integral.IntegralListResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class IntegralListJSONImplResponse extends PuDongParserImpl implements IntegralListResponse, Serializable {

    @Implementation(IntegralResponseJSONImpl.class)
    private List<IntegralResponse> integrals;

    public IntegralListJSONImplResponse(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<IntegralResponse> list() {
        return integrals;
    }
}
