package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSiteResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/28.
 */

public class MonitorTypeResponseJSONImpl extends PuDongParserImpl implements MonitorTypeResponse,Serializable{

    @Key("alarmRecordDateTime")
    long alarmRecordDateTime;
    @Key("id")
    int id;
    @Key("status")
    String status;
    @Key("typeDesc")
    String typeDesc;
    @Key("typeId")
    String typeId;
    @Key("typeName")
    String typeName;

    public MonitorTypeResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long alarmRecordDateTime() {
        return alarmRecordDateTime;
    }

    @Override
    public String status() {
        return status;
    }

    @Override
    public String typeDesc() {
        return typeDesc;
    }

    @Override
    public String typeId() {
        return typeId;
    }

    @Override
    public String typeName() {
        return typeName;
    }

    @Override
    public int id() {
        return id;
    }
}
