package com.bookbuf.api.responses.parsers.impl.attendance;

import com.bookbuf.api.responses.impl.attendance.AttendanceDayResponse;
import com.bookbuf.api.responses.impl.attendance.components.AttendanceRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.attendance.components.AttendanceRecordResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/27.
 */
public final class AttendanceDayResponseJSONImpl extends PuDongParserImpl implements AttendanceDayResponse, Serializable {
    @Key("year")
    private int year;
    @Key("month")
    private int month;
    @Key("date")
    private String date;
    @Key("description")
    private String description;
    @Key("records")
    @Implementation(AttendanceRecordResponseJSONImpl.class)
    private List<AttendanceRecordResponse> records;

    public AttendanceDayResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int year() {
        return year;
    }

    @Override
    public int month() {
        return month;
    }

    @Override
    public String date() {
        return date;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public List<AttendanceRecordResponse> records() {
        return records;
    }
}
