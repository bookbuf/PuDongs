package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/28.
 */

public class MonitorCountResponseJSONImpl extends PuDongParserImpl implements MonitorCountResponse,Serializable{

    @Key("data")
    String data;
    @Key("code")
    int code;
    @Key("message")
    String message;

    public MonitorCountResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String data() {
        return data;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
