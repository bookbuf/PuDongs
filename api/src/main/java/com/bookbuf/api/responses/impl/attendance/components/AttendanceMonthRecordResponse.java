package com.bookbuf.api.responses.impl.attendance.components;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/12/5.
 */

public interface AttendanceMonthRecordResponse extends Response {

    int day();

    int status();

    int signInCount();
}
