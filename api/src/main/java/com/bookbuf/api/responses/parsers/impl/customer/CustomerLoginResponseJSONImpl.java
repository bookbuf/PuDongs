package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public class CustomerLoginResponseJSONImpl extends PuDongParserImpl implements CustomerLoginResponse, Serializable {

    @Key("loginId")
    private long loginId;

    @Key("sessionId")
    private String sessionId;

    @Key("status")
    private int status;

    public CustomerLoginResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long loginId() {
        return loginId;
    }

    @Override
    public String sessionId() {
        return sessionId;
    }

    @Override
    public int status() {
        return status;
    }
}
