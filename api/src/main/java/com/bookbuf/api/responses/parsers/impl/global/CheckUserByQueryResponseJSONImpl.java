package com.bookbuf.api.responses.parsers.impl.global;

import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2016/12/19.
 */

public class CheckUserByQueryResponseJSONImpl extends PuDongParserImpl implements LongResponse, Serializable {

    @Key("uid")
    private long uid;

    public CheckUserByQueryResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long get() {
        return uid;
    }
}
