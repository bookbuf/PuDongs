package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  2017/3/21.
 */

public interface DetectionListResponse extends Response {


    List<DetectionItemResponse> list();

    String prompts();
}


