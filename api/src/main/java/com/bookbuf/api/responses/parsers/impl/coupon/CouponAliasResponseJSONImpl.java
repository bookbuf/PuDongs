package com.bookbuf.api.responses.parsers.impl.coupon;

import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class CouponAliasResponseJSONImpl extends PuDongParserImpl implements CouponAliasResponse, Serializable {

    @Key("couponId")
    private long bCouponId;
    @Key("id")
    private long couponId;
    @Key("title")
    private String title;
    @Key("promoCode")
    private String promoCode;
    @Key("status")
    private int status;
    @Key("startTime")
    private String startTime;
    @Key("endTime")
    private String endTime;
    @Key("alias")
    private String alias;
    @Key("activityTime")
    private String activityTime;
    @Key("openTime")
    private String openTime;
    @Key("isEnd")
    private int isEnd;
    @Key("couponIcon")
    private String couponIcon;
    @Key("isProduct")
    private int isProduct;
    @Key("mode")
    private String mode;
    @Key("orderId")
    private String orderId;
    @Key("category")
    private String category;

    //1.5.0 add
    @Key("special")
    private String special;
    @Key("receiveRight")
    private int receiveRight;
    @Key("cutPoint")
    private int cutPoint;

    @Key("couponNum")
    private String couponNum;

    public CouponAliasResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }


    @Override
    public String title() {
        return title;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public long couponId() {
        return couponId;
    }

    @Override
    public long bCouponId() {
        return bCouponId;
    }

    @Override
    public String promoCode() {
        return promoCode;
    }

    @Override
    public String startTime() {
        return startTime;
    }

    @Override
    public String endTime() {
        return endTime;
    }

    @Override
    public String alias() {
        return alias;
    }

    @Override
    public int isEnd() {
        return isEnd;
    }

    @Override
    public String activityTime() {
        return activityTime;
    }

    /**
     * b端发码时间
     * @return
     */
    @Override
    public String openTime() {
        return openTime;
    }

    @Override
    public String couponIcon() {
        return couponIcon;
    }

    @Override
    public int isProduct() {
        return isProduct;
    }

    @Override
    public String mode() {
        return mode;
    }

    @Override
    public String orderId() {
        return orderId;
    }

    @Override
    public String category() {
        return category;
    }

    @Override
    public String special() {
        return special;
    }

    @Override
    public int receiveRight() {
        return receiveRight;
    }

    @Override
    public int cutPoint() {
        return cutPoint;
    }

    @Override
    public String couponNum() {
        return couponNum;
    }
}
