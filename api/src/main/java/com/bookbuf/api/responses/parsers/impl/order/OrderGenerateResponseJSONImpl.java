package com.bookbuf.api.responses.parsers.impl.order;


import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.bookbuf.api.responses.impl.order.PayResultResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/5/3.
 */

public class OrderGenerateResponseJSONImpl extends PuDongParserImpl implements OrderGenerateResponse, Serializable {

    @Key("orderRequestId")
    private String orderRequestId;

    @Key("payType")
    private String payType;

    @Key("payRequest")
    @Implementation(PayResultResponseJSONImpl.class)
    private PayResultResponse payResultResponse;


    public OrderGenerateResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public PayResultResponse payResult() {
        return payResultResponse;
    }

    @Override
    public String orderRequestId() {
        return orderRequestId;
    }

    @Override
    public String payType() {
        return payType;
    }
}
