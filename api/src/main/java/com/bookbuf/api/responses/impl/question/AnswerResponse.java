package com.bookbuf.api.responses.impl.question;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface AnswerResponse extends Response {

	/**
	 * 作答的题目编号
	 *
	 * @return questionId
	 */
	long questionId ();

	/**
	 * 作答答案，字符串形式
	 * 如："ABC"表示多选；
	 * 如："A"表示单选；
	 * 如：""表示未选择；
	 *
	 * @return answerQuestion
	 */
	String answer ();


	/**
	 * 将答案按照字母序排序
	 *
	 * @param value 原始字符串
	 * @return sortValue
	 */
	String sort (String value);

	/**
	 * 作答答案，字符串形式
	 * 如："ABC"表示多选；
	 * 如："A"表示单选；
	 *
	 * @return correctAnswer
	 */
	String correctAnswer ();
}
