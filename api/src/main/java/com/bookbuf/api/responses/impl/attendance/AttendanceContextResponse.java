package com.bookbuf.api.responses.impl.attendance;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.attendance.components.AttendanceRecordResponse;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface AttendanceContextResponse extends Response {

    String date();

    String description();

    List<AttendanceRecordResponse> records();
}
