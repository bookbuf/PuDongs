package com.bookbuf.api.responses.parsers.impl.call;

import com.bookbuf.api.responses.impl.call.CallCancelResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class CallCancelResponseJSONImpl extends PuDongParserImpl implements CallCancelResponse, Serializable {

    @Key("model")
    private String originCode;

    public CallCancelResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String originCode() {
        return originCode;
    }
}
