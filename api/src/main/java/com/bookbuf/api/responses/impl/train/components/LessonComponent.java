package com.bookbuf.api.responses.impl.train.components;

/**
 * author: robert.
 * date :  2016/12/22.
 */

public interface LessonComponent {

    /**
     * @return 课件标题
     */
    String title();

    /**
     * 0 未考试 1-金  2-银 3-铜 4-铁
     *
     * @return status
     */
    int status();

    /**
     * @return 上下架状态
     */
    boolean online();

    /**
     * @return 课件详情
     */
    String redirect();

    /**
     * @return 关联的试卷编号，若未关联试卷则为-1。
     */
    long paperId();
}
