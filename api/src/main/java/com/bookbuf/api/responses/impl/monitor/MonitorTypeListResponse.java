package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorTypeListResponse extends Response {

    List<MonitorTypeResponse> list();

    int pageNum();

    int pageSize();

    int totalPages();

    int totalRows();
}
