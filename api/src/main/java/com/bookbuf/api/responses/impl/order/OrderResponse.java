package com.bookbuf.api.responses.impl.order;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/5/2.
 */

public interface OrderResponse extends Response {

    String cutPoint();

    String currentPrice();

    String totalPrice();

    String couponCut();

    String promoCut();
}
