package com.bookbuf.api.responses.parsers.impl.attendance;

import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.components.AttendanceMonthRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.attendance.components.AttendanceMonthRecordResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/9/27.
 */
public final class AttendanceMonthResponseJSONImpl extends PuDongParserImpl implements AttendanceMonthResponse, Serializable {
    @Key("year")
    private int year;
    @Key("month")
    private int month;
    @Key("records")
    @Implementation(AttendanceMonthRecordResponseJSONImpl.class)
    private List<AttendanceMonthRecordResponse> records;

    public AttendanceMonthResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int year() {
        return year;
    }

    @Override
    public int month() {
        return month;
    }

    @Override
    public List<AttendanceMonthRecordResponse> records() {
        return records;
    }
}
