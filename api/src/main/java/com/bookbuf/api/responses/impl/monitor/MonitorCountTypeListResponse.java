package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorCountTypeListResponse extends Response {

    int code();

    MonitorCountTypeDataResponse data();

    String message();
}
