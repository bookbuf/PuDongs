package com.bookbuf.api.responses.impl.customer;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/7/19.
 */

public interface PermissionResponse extends Response {

    boolean geneEnable();

    boolean withdrawEnable();

    boolean hospitalEnable();
}
