package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface ClerkResponse extends Response {

	ProfileResponse profile ();

	VendorResponse vendor ();
}
