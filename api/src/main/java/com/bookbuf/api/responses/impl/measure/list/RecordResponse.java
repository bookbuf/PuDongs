package com.bookbuf.api.responses.impl.measure.list;

import com.bookbuf.api.responses.Response;

/**
 * 与AddRecordResponse关联
 * author: robert.
 * date :  16/11/28.
 */

public interface RecordResponse extends Response {

	String indicatorId ();

	int status ();

	String dateFormat ();

	String valueFormat ();
}
