package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/4/12.
 */

public interface CouponAliasResponse extends Response{

    long bCouponId();   //区分c端的couponId

    long couponId();

    String title();

    String mode();      //1.原优惠券  2.订单

    String category();

    String orderId();

    String promoCode();

    int status();

    String startTime();

    String endTime();

    String couponIcon();

    int isProduct();

    //一個接口用到
    String alias();

    int isEnd();

    String activityTime();

    String openTime();

    String special();       //"专享推荐", 如果没有传null

    int receiveRight();     //1.达到领取（购买）条件 0.未满足领取条件（如积分不足）

    int cutPoint();         // 所需积分

    String couponNum();
}
