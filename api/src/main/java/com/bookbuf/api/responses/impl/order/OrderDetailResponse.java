package com.bookbuf.api.responses.impl.order;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;

/**
 * Created by bo.wei on 2017/5/2.
 */

public interface OrderDetailResponse extends Response {

    String orderRequestId();

    CouponResponse detail();

    OrderResponse order();
}
