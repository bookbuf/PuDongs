package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class CredentialResponseJSONImpl extends PuDongParserImpl implements CredentialResponse, Serializable {

    @Key("loginId")
    private long loginId;
    @Key("sessionId")
    private String sessionId;
    @Key("status")
    private int status;
    @Key("username")
    private String username;

    public CredentialResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public long loginId() {
        return this.loginId;
    }

    @Override
    public String sessionId() {
        return this.sessionId;
    }

    @Override
    public int status() {
        return this.status;
    }

    @Override
    public String username() {
        return this.username;
    }

}
