package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.DetectionItemResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/3/20.
 */

public class DetectionItemResponseJSONImpl extends PuDongParserImpl implements DetectionItemResponse, Serializable {


    @Key("takeTime")
    private String takeTime;
    @Key("name")
    private String name;
    @Key("value")
    private String value;
    @Key("unit")
    private String unit;
    @Key("status")
    private int status;
    @Key("typeCode")
    private String typeCode;
    @Key("description")
    private String description;


    public DetectionItemResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public String typeCode() {
        return typeCode;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String unit() {
        return unit;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public String takeTime() {
        return takeTime;
    }


}
