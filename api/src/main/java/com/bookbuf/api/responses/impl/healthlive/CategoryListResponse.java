package com.bookbuf.api.responses.impl.healthlive;


import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  2017/3/23.
 */

public interface CategoryListResponse extends Response {

    List<CategoryResponse> categorys();
}
