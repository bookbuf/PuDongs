package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherDailyResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherDailyResponseJSONImpl extends PuDongParserImpl implements WeatherDailyResponse, Serializable {

    @Key("date")
    private String date;
    @Key("hum")
    private String hum;
    @Key("pcpn")
    private String pcpn;
    @Key("pop")
    private String pop;
    @Key("pres")
    private String pres;
    @Key("vis")
    private String vis;
    @Key("tmp")
    @Implementation(TmpResponseJSONImpl.class)
    private TmpResponse tmp;
    @Key("wind")
    @Implementation(WindResponseJSONImpl.class)
    private WindResponse wind;
    @Key("astro")
    @Implementation(AstroResponseJSONImpl.class)
    private AstroResponse astro;
    @Key("cond")
    @Implementation(CondResponseJSONImpl.class)
    private CondResponse cond;

    public WeatherDailyResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String date() {
        return date;
    }

    @Override
    public String hum() {
        return hum;
    }

    @Override
    public String pcpn() {
        return pcpn;
    }

    @Override
    public String pop() {
        return pop;
    }

    @Override
    public String pres() {
        return pres;
    }

    @Override
    public String vis() {
        return vis;
    }

    @Override
    public TmpResponse tmp() {
        return tmp;
    }

    @Override
    public WindResponse wind() {
        return wind;
    }

    @Override
    public AstroResponse astro() {
        return astro;
    }

    @Override
    public CondResponse cond() {
        return cond;
    }

    public static final class CondResponseJSONImpl extends PuDongParserImpl implements CondResponse, Serializable {

        @Key("code_d")
        private String codeDaily;
        @Key("code_n")
        private String codeNight;
        @Key("txt_d")
        private String txtDaily;
        @Key("txt_n")
        private String txtNight;

        public CondResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String codeDaily() {
            return codeDaily;
        }

        @Override
        public String codeNight() {
            return codeNight;
        }

        @Override
        public String txtDaily() {
            return txtDaily;
        }

        @Override
        public String txtNight() {
            return txtNight;
        }
    }

    public static final class WindResponseJSONImpl extends PuDongParserImpl implements WindResponse, Serializable {

        @Key("deg")
        private String deg;
        @Key("dir")
        private String dir;
        @Key("sc")
        private String sc;
        @Key("spd")
        private String spd;

        public WindResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String deg() {
            return deg;
        }

        @Override
        public String dir() {
            return dir;
        }

        @Override
        public String sc() {
            return sc;
        }

        @Override
        public String spd() {
            return spd;
        }
    }

    public static final class AstroResponseJSONImpl extends PuDongParserImpl implements AstroResponse, Serializable {

        @Key("mr")
        private String mr;
        @Key("ms")
        private String ms;
        @Key("sr")
        private String sr;
        @Key("ss")
        private String ss;

        public AstroResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String mr() {
            return mr;
        }

        @Override
        public String ms() {
            return ms;
        }

        @Override
        public String sr() {
            return sr;
        }

        @Override
        public String ss() {
            return ss;
        }
    }

    public static final class TmpResponseJSONImpl extends PuDongParserImpl implements TmpResponse, Serializable {

        @Key("max")
        private String max;
        @Key("min")
        private String min;

        public TmpResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String max() {
            return max;
        }

        @Override
        public String min() {
            return min;
        }
    }

}
