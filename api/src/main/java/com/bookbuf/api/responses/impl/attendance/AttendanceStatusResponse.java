package com.bookbuf.api.responses.impl.attendance;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface AttendanceStatusResponse extends Response {

    int type();

    /**
     * 实际距离
     *
     * @return distance
     */
    double distance();

    /**
     * 实际打卡时间（服务器上的北京时间）
     *
     * @return date
     */
    String date();

    /**
     * 0 正常状态
     * 1 异常状态，距离超出（当经纬度都为-1时，也返回该状态）
     * 2 异常状态，迟到
     * 3 异常状态，旷工
     * 4 异常状态，早退
     *
     * @return status
     */
    int status();
}
