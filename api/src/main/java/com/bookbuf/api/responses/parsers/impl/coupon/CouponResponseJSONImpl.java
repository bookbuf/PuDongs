package com.bookbuf.api.responses.parsers.impl.coupon;

import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class CouponResponseJSONImpl extends PuDongParserImpl implements CouponResponse, Serializable {

    @Key("startTime")
    private String startTime;
    @Key("id")
    private long id;
    @Key("detail")
    private String detail;
    @Key("title")
    private String title;
    @Key("num")
    private String num;
    @Key("status")
    private int status;
    @Key("rule")
    @Implementation(String.class)
    private List<String> rule;
    @Key("className")
    private String className;
    @Key("endTime")
    private String endTime;
    @Key("type")
    private int type;
    @Key("couponIcon")
    private String couponIcon;
    @Key("isProduct")
    private int isProduct;
    @Key("productName")
    private String productName;
    @Key("mode")
    private String mode;
    @Key("orderId")
    private String orderId;
    @Key("userCouponId")
    private long userCouponId;
    @Key("category")
    private String category;
    @Key("isGained")
    private int isGained;

    @Key("isChecked")
    private String isChecked;
    @Key("pic")
    private String pic;
    @Key("limitCollectTime")
    private int limitCollectTime;
    @Key("collectTime")
    private int collectTime;

    //1.5.0 add
    @Key("verifyForm")
    private int verifyForm;
    @Key("special")
    private String special;
    @Key("cutPoint")
    private String cutPoint;
    @Key("integralTitle")
    private String integralTitle;
    @Key("isIntegral")
    private int isIntegral;
    @Key("isAward")
    private String isAward;

    //1.6.0 add
    @Key("codeNum")
    private int codeNum;
    @Key("codeStartTime")
    private long codeStartTime;
    @Key("codeEndTime")
    private long codeEndTime;
    @Key("remainNum")
    private int remainNum;
    @Key("isRush")
    private int isRush;
    @Key("systemTime")
    private long systemTime;
    @Key("totalSec")
    private long totalSec;

    //1.7.0 add
    @Key("couponNumber")
    private int couponNumber;
    @Key("couponType")
    private String couponType;
    @Key("categoryName")
    private String categoryName;
    @Key("channelName")
    private String channel;

    @Key("couponNum")
    private String couponNum;



    public CouponResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String startTime() {
        return startTime;
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String detail() {
        return detail;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String num() {
        return num;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public List<String> rule() {
        return rule;
    }

    @Override
    public String className() {
        return className;
    }

    @Override
    public String endTime() {
        return endTime;
    }

    @Override
    public int type() {
        return type;
    }

    @Override
    public String productName() {
        return productName;
    }

    @Override
    public String isChecked() {
        return isChecked;
    }

    @Override
    public String pic() {
        return pic;
    }

    @Override
    public String couponIcon() {
        return couponIcon;
    }

    @Override
    public String mode() {
        return mode;
    }

    @Override
    public int isProduct() {
        return isProduct;
    }

    @Override
    public String orderId() {
        return orderId;
    }

    @Override
    public long userCouponId() {
        return userCouponId;
    }

    @Override
    public int isGained() {
        return isGained;
    }

    @Override
    public String category() {
        return category;
    }

    @Override
    public int collectTime() {
        return collectTime;
    }

    @Override
    public int limitCollectTime() {
        return limitCollectTime;
    }

    @Override
    public int verifyForm() {
        return verifyForm;
    }

    @Override
    public String special() {
        return special;
    }

    @Override
    public String cutPoint() {
        return cutPoint;
    }

    @Override
    public int isIntegral() {
        return isIntegral;
    }

    @Override
    public String integralTitle() {
        return integralTitle;
    }

    @Override
    public String isAward() {
        return isAward;
    }

    @Override
    public int codeNum() {
        return codeNum;
    }

    @Override
    public long codeStartTime() {
        return codeStartTime;
    }

    @Override
    public long codeEndTime() {
        return codeEndTime;
    }

    @Override
    public int remainNum() {
        return remainNum;
    }

    @Override
    public int isRush() {
        return isRush;
    }

    @Override
    public long systemTime() {
        return systemTime;
    }

    @Override
    public long totalSec() {
        return totalSec;
    }

    @Override
    public int couponNumber() {
        return couponNumber;
    }

    @Override
    public String categoryName() {
        return categoryName;
    }

    @Override
    public String couponType() {
        return couponType;
    }

    @Override
    public String channel() {
        return channel;
    }

    @Override
    public String couponNum() {
        return couponNum;
    }
}
