package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

import java.util.List;

public interface CouponNewResponse extends Response {

    String startTime();

    long id();

    long userCouponId();

    String category();

    String mode();

    String orderId();

    String detail();

    String title();

    String num();

    int status();

    List<String> rule();

    String className();

    String endTime();

    int type();

    String couponIcon();


    @Deprecated
    String productName();

    @Deprecated
    String isChecked();

    @Deprecated
    String pic();

    int limitCollectTime();

    int collectTime();

    int verifyForm();

    String special();       //"专享推荐", 如果没有传null

    String cutPoint();         // 所需积分

    String integralTitle();

    int codeNum();      //抢券总数量

    long codeStartTime();       //抢劵开始时间   （时间戳）

    long codeEndTime();         //抢劵结束时间   （时间戳）

    int remainNum();        //抢劵剩余数量

    long systemTime();       //系统时间       （时间戳）

    long totalSec();        //抢劵总时间     （单位：秒）

    int couponNumber();     //优惠券序号

    String categoryName();      //分类名称

    String couponType();        //优惠券类型

    String channel();       //适用渠道

    String couponNum();
}
