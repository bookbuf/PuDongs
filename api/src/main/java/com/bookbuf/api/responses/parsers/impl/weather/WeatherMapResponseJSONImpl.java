package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherMapResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherMapResponseJSONImpl extends PuDongParserImpl implements WeatherMapResponse, Serializable {

    @Key("brf")
    private String brf;
    @Key("txt")
    private String txt;

    public WeatherMapResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String brf() {
        return brf;
    }

    @Override
    public String txt() {
        return txt;
    }
}
