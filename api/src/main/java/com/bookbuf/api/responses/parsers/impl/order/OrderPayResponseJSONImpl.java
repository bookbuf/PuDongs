package com.bookbuf.api.responses.parsers.impl.order;

import com.bookbuf.api.responses.impl.order.OrderPayResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/5/2.
 */

public class OrderPayResponseJSONImpl extends PuDongParserImpl implements OrderPayResponse, Serializable {
    @Key("orderRequestId")
    private String orderRequestId;

    public OrderPayResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String orderRequestId() {
        return orderRequestId;
    }
}
