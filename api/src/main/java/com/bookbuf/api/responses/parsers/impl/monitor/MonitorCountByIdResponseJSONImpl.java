package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountByIdDataResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountByIdResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/29.
 */

public class MonitorCountByIdResponseJSONImpl extends PuDongParserImpl implements MonitorCountByIdResponse,Serializable {

    @Key("code")
    int code;
    @Key("data")
    @Implementation(MonitorCountByIdDataResponseJSONImpl.class)
    MonitorCountByIdDataResponse data;
    @Key("message")
    String message;

    public MonitorCountByIdResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public MonitorCountByIdDataResponse data() {
        return data;
    }

    @Override
    public String message() {
        return message;
    }
}
