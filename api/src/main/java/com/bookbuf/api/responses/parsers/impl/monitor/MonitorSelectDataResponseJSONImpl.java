package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSelectDataResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSelectListResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorSelectDataResponseJSONImpl extends PuDongParserImpl implements MonitorSelectDataResponse,Serializable {

    @Key("storeNumber")
    String storeNumber;
    @Key("alarmData")
    @Implementation(MonitorSelectListResponseJSONImpl.class)
    MonitorSelectListResponse alarmData;

    public MonitorSelectDataResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String storeNumber() {
        return storeNumber;
    }

    @Override
    public MonitorSelectListResponse alarmData() {
        return alarmData;
    }
}
