package com.bookbuf.api.responses.parsers.impl.train.components;

import com.bookbuf.api.responses.impl.train.components.LessonComponent;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public class LessonComponentJSONImpl extends PuDongParserImpl implements LessonComponent, Serializable {

    @Key("online")
    private boolean online;
    @Key("title")
    private String title;
    @Key("redirect")
    private String redirect;
    @Key("status")
    private int status;
    @Key("paperId")
    private long paperId;

    public LessonComponentJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public boolean online() {
        return online;
    }

    @Override
    public String redirect() {
        return redirect;
    }

    @Override
    public long paperId() {
        return paperId;
    }
}
