package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.customer.IntegrityResponse;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface ProfileResponse extends Response {

	long userId ();

	/**
	 * @return 用户的状态
	 */
	int status ();

	String realname ();

	String mobile ();

	String email ();

	String idcard ();

	/**
	 * @return 药店会员卡号
	 */
	String vendorMemberId ();

	/**
	 * @return 用户头像
	 */
	String avatar ();

	int weight ();

	int height ();

	String birthday ();

	int age ();

	String gender ();

	long createdUserId ();

	String phone();

	AddressResponse address ();

	IntegrityResponse response();

	PhotoVerifiedResponse photoResponse();
}
