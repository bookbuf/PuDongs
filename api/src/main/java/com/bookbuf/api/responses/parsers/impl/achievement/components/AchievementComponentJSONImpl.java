package com.bookbuf.api.responses.parsers.impl.achievement.components;

import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

public final class AchievementComponentJSONImpl extends PuDongParserImpl implements AchievementComponent, Serializable {

    @Key("id")
    private long id;
    @Key("title")
    private String title;
    @Key("description")
    private String description;
    @Key("status")
    private int status;
    @Key("totalCount")
    private int totalCount;
    @Key("passedCount")
    private int passedCount;

    public AchievementComponentJSONImpl(JSONObject object) {
        super(object);

    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public int passedCount() {
        return passedCount;
    }

    @Override
    public int totalCount() {
        return totalCount;
    }
}
