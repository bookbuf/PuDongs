//package com.bookbuf.api.responses.parsers.impl.customer.detections;
//
//import com.bookbuf.api.responses.impl.customer.CustomerDetectionIndexResponse;
//import com.bookbuf.api.responses.parsers.annotations.Implementation;
//import com.bookbuf.api.responses.parsers.annotations.Key;
//import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
//
//import org.json.JSONObject;
//
//import java.io.Serializable;
//
///**
// * author: robert.
// * date :  2017/3/20.
// */
//
//public class DetectionResponseJSONImpl extends PuDongParserImpl implements CustomerDetectionIndexResponse.Detection, Serializable {
//
//    @Key("typeCode")
//    private String typeCode;
//    @Key("name")
//    private String name;
//    @Key("unitName")
//    private String unitName;
//    @Key("takeTime")
//    private String takeTime;
//    @Key("time")
//    private String time;
//    @Key("status")
//    private int status;
//    @Key("fields")
//    @Implementation(FieldWrapperResponseJSONImpl.class)
//    private CustomerDetectionIndexResponse.FieldWrapper fieldWrapper;
//
//    public DetectionResponseJSONImpl(JSONObject jsonObject) {
//        super(jsonObject);
//    }
//
//    @Override
//    public String typeCode() {
//        return typeCode;
//    }
//
//    @Override
//    public String name() {
//        return name;
//    }
//
//    @Override
//    public String unitName() {
//        return unitName;
//    }
//
//    @Override
//    public String takeTime() {
//        return takeTime;
//    }
//
//    @Override
//    public String time() {
//        return time;
//    }
//
//    @Override
//    public int status() {
//        return status;
//    }
//
//    @Override
//    public CustomerDetectionIndexResponse.FieldWrapper fieldWrapper() {
//        return fieldWrapper;
//    }
//
//}
