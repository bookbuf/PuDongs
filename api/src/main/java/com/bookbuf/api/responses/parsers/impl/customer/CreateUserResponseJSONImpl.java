package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.CreateUserResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.ipudong.core.network.annotation.param.Param;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/17.
 */

public class CreateUserResponseJSONImpl extends PuDongParserImpl implements CreateUserResponse, Serializable {
    @Key("uid")
    long uid;

    public CreateUserResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long uid() {
        return uid;
    }
}
