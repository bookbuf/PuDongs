package com.bookbuf.api.responses.parsers.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * author: robert.
 * date :  2016/12/19.
 * <p>
 * 标记为该注解的属性，将不会被解析器解析。
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface IgnoreKey {

}