package com.bookbuf.api.responses.impl.task;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface TaskContextResponse extends Response {

	/**
	 * 店员任务集合
	 *
	 * @return tasks
	 */
	List<TaskResponse> tasks ();

	/**
	 * 任务完成进度
	 *
	 * @return progress
	 */
	int progress ();
}
