package com.bookbuf.api.responses.parsers.impl.question;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2016/12/20.
 */

public class CreateExaminationRecordResponseJSONImpl extends PuDongParserImpl implements BooleanResponse, Serializable {

    @Key("is_answer_right")
    private boolean bool;

    public CreateExaminationRecordResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public boolean get() {
        return bool;
    }
}
