package com.bookbuf.api.responses.impl.global;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface AppUpgradeResponse extends Response {

    /**
     * app 平台版本名称
     *
     * @return platform
     */
    String platform();

    /**
     * 更新描述
     *
     * @return content
     */
    String content();

    /**
     * 软件安装包的渠道，举例如：豌豆荚、应用宝等
     *
     * @return apkChannel
     */
    String apkChannel();

    /**
     * 软件安装包的下载地址
     *
     * @return apkUrl
     */
    String apkUrl();

    /**
     * 安装包的编译版本号，是一个整形。如，73.
     *
     * @return apkVersion
     */
    int apkVersion();

    /**
     * 更新策略，{@link UpdateStrategy}
     *
     * @return updateStrategy
     */
    int updateStrategy();

    /**
     * 合作供应商的id。
     *
     * @return vendorId
     */
    int vendorId();

    /**
     * 版本号
     *
     * @return serialId
     */
    String serialId();

    interface UpdateStrategy {
        /**
         * 不更新
         */
        int STRATEGY_NONE = 0;
        /**
         * 提示更新
         */
        int STRATEGY_PROMPT = 1;
        /**
         * 强制更新
         */
        int STRATEGY_FORCE = 2;
    }

}
