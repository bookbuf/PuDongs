package com.bookbuf.api.responses.parsers.impl.call;

import com.bookbuf.api.responses.impl.call.CallResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2016/11/23.
 */

public class CallResponseJSONImpl extends PuDongParserImpl implements CallResponse, Serializable {
    @Key("customerSerNum")
    private String customerSerNum;
    @Key("appId")
    private String appId;
    @Key("fromSerNum")
    private String fromSerNum;
    @Key("dateCreated")
    private String dateCreated;
    @Key("callSid")
    private String callSid;
    @Key("orderId")
    private String orderId;

    public CallResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String appId() {
        return appId;
    }

    @Override
    public String customerSerNum() {
        return customerSerNum;
    }

    @Override
    public String fromSerNum() {
        return fromSerNum;
    }

    @Override
    public String dateCreated() {
        return dateCreated;
    }

    @Override
    public String callSid() {
        return callSid;
    }

    @Override
    public String orderId() {
        return orderId;
    }
}
