package com.bookbuf.api.responses.parsers.impl.measure.latest;

import com.bookbuf.api.responses.impl.measure.latest.MeasureFieldResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

//{
//		"id": "sbp",
//		"name": "体质指数",
//		"value": "16.6",
//		"status": 4
//		}
public final class MeasureFieldResponseJSONImpl extends PuDongParserImpl implements MeasureFieldResponse, Serializable {

    @Key("id")
    private String id;
    @Key("name")
    private String name;
    @Key("value")
    private String value;
    @Key("status")
    private int status;

    public MeasureFieldResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public int status() {
        return status;
    }
}
