package com.bookbuf.api.responses.impl.message;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/27.
 */
public interface MessageResponse extends Response {

	long id ();

	int status ();

	String title ();

	String content ();

	String time ();

	String imgUrl();

	String integral();

	String userName();

	String posTime();
}
