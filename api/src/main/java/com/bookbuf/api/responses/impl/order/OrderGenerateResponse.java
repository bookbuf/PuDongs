package com.bookbuf.api.responses.impl.order;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/5/3.
 */

public interface OrderGenerateResponse extends Response{

    String orderRequestId();

    String payType();

    PayResultResponse payResult();
}
