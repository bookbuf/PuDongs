package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherAlarmResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherAlarmResponseJSONImpl extends PuDongParserImpl implements WeatherAlarmResponse, Serializable {
    @Key("level")
    private String level;
    @Key("stat")
    private String stat;
    @Key("title")
    private String title;
    @Key("txt")
    private String txt;
    @Key("type")
    private String type;

    public WeatherAlarmResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String level() {
        return level;
    }

    @Override
    public String stat() {
        return stat;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String txt() {
        return txt;
    }

    @Override
    public String type() {
        return type;
    }
}
