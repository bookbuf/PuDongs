package com.bookbuf.api.responses.impl.healthlive;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bookbuf.api.responses.Response;

import java.util.List;

public interface HealthLiveResponse extends Response {

    @NonNull
    long liveId();

    @NonNull
    String rate();

    @NonNull
    String visits();

    @NonNull
    String background();

    @NonNull
    String description();

    @Nullable
    String summary();

    @Nullable
    String url();

    @Nullable
    List<String> knowledge();

    @Nullable
    String originalCost();

    @Nullable
    String currentCoast();

    @Nullable
    String title();

    @Nullable
    String avatar();

    String detailDescription();
}
