package com.bookbuf.api.responses.impl.train;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.impl.train.components.OfflineComponent;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public interface FetchTrainResponse extends Response {


    boolean isSignUp();

    CourseComponent course();

    OfflineComponent offline();

}
