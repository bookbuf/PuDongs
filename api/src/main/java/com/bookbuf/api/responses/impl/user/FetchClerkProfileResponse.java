package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  2017/1/22.
 */

public interface FetchClerkProfileResponse extends Response {

    long vendorShopId();

    long vendorId();

    String vendorName();

    long vendorPosition();

    String vendorLogoUrl();

    String vendorCode();

    String vendorShopName();

    String birthday();

    String gender();

    String mobile();

    String idCard();

    String realname();

    long userId();

    String email();

    int status();

    int clerkStatus();

    String note();

    String clerkNum();

    String roleName();
}
