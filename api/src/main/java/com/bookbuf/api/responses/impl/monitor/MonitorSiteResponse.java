package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/12/1.
 */

public interface MonitorSiteResponse extends Response {
    String city();

    int id();

    String storeNumber();
}
