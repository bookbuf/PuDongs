package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.IntegrityResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/15.
 */

public class IntegrityResponseJSONImpl extends PuDongParserImpl implements IntegrityResponse, Serializable {

    @Key("contact_flag")
    String contactFlag;
    @Key("identification_flag")
    String identificationFlag;
    @Key("membership_card_flag")
    String membershipCardFlag;

    public IntegrityResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String contactFlag() {
        return contactFlag;
    }

    @Override
    public String identificationFlag() {
        return identificationFlag;
    }

    @Override
    public String membershipCardFlag() {
        return membershipCardFlag;
    }
}
