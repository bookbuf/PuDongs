package com.bookbuf.api.responses.impl.global;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface RoleResponse extends Response {

    int ROLE_NOT_REGISTER = -1;
    int ROLE_USER = 0;
    int ROLE_CLERK = 1;
    int ROLE_VENDOR = 2;
    int ROLE_AREA_MANAGER = 3;
    int ROLE_VENDOR_OWNER = 4;
    int ROLE_CHANNEL_MANAGER = 5;
    int ROLE_ROOT_MANAGER = 6;
    int ROLE_HOSPITAL = 7;


    /**
     * 身份id
     *
     * @return id
     */
    int id();

    /**
     * 身份对应的描述
     *
     * @return description
     */
    String description();

    /**
     * @return 是否是虚拟店铺
     */
    boolean isVirtual();
}
