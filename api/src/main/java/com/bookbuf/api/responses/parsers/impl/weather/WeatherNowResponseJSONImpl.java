package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherNowResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherNowResponseJSONImpl extends PuDongParserImpl implements WeatherNowResponse, Serializable {

    @Key("vis")
    private String vis;
    @Key("fl")
    private String fl;
    @Key("hum")
    private String hum;
    @Key("tmp")
    private String tmp;
    @Key("pcpn")
    private String pcpn;
    @Key("pres")
    private String pres;
    @Key("wind")
    @Implementation(WindResponseJSONImpl.class)
    private WindResponse wind;
    @Key("cond")
    @Implementation(CondResponseJSONImpl.class)
    private CondResponse cond;

    public WeatherNowResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String fl() {
        return fl;
    }

    @Override
    public String hum() {
        return hum;
    }

    @Override
    public String pcpn() {
        return pcpn;
    }

    @Override
    public String pres() {
        return pres;
    }

    @Override
    public String tmp() {
        return tmp;
    }

    @Override
    public String vis() {
        return vis;
    }

    @Override
    public WindResponse wind() {
        return wind;
    }

    @Override
    public CondResponse cond() {
        return cond;
    }

    public static final class CondResponseJSONImpl extends PuDongParserImpl implements CondResponse, Serializable {

        @Key("code")
        private String code;
        @Key("txt")
        private String txt;

        public CondResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String code() {
            return code;
        }

        @Override
        public String txt() {
            return txt;
        }
    }

    public static final class WindResponseJSONImpl extends PuDongParserImpl implements WindResponse, Serializable {

        @Key("deg")
        private String deg;
        @Key("dir")
        private String dir;
        @Key("sc")
        private String sc;
        @Key("spd")
        private String spd;

        public WindResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String deg() {
            return deg;
        }

        @Override
        public String dir() {
            return dir;
        }

        @Override
        public String sc() {
            return sc;
        }

        @Override
        public String spd() {
            return spd;
        }
    }

}
