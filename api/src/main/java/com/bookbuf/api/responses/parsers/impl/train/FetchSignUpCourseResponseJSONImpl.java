package com.bookbuf.api.responses.parsers.impl.train;

import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.achievement.components.AchievementComponentJSONImpl;
import com.bookbuf.api.responses.parsers.impl.train.components.CourseComponentJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public final class FetchSignUpCourseResponseJSONImpl extends PuDongParserImpl implements FetchSignUpCourseResponse, Serializable {
    @IgnoreKey
    private AchievementComponent achievement;
    @Key("courses")
    @Implementation(CourseComponentJSONImpl.class)
    private List<CourseComponent> courses;

    public FetchSignUpCourseResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.achievement = new AchievementComponentJSONImpl(jsonObject);
    }


    @Override
    public AchievementComponent achievement() {
        return achievement;
    }

    @Override
    public List<CourseComponent> courses() {
        return courses;
    }
}
