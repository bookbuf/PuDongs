package com.bookbuf.api.responses.parsers.impl.medical;

import com.bookbuf.api.responses.impl.medical.DoctorScheduleResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class DoctorScheduleResponseJSONImpl extends PuDongParserImpl implements DoctorScheduleResponse, Serializable {

    @Key("ScheduleItemCode")
    private String scheduleItemCode;
    @Key("ServiceDate")
    private String serviceDate;
    @Key("WeekDay")
    private Integer weekDay;
    @Key("SessionName")
    private String sessionName;
    @Key("StartTime")
    private String startTime;
    @Key("EndTime")
    private String endTime;
    @Key("DepartmentCode")
    private int departmentCode;
    @Key("DepartmentName")
    private String departmentName;
    @Key("DoctorCode")
    private Integer doctorCode;
    @Key("DoctorName")
    private String doctorName;
    @Key("DoctorTitleCode")
    private Integer doctorTitleCode;
    @Key("DoctorTitle")
    private String doctorTitle;
    @Key("DoctorSessTypeCode")
    private Integer doctorSessTypeCode;
    @Key("DoctorSessType")
    private String doctorSessType;
    @Key("Fee")
    private String fee;
    @Key("AvailableNum")
    private Integer availableNum;
    @Key("AdmitTimeRange")
    private String admitTimeRange;
    @Key("LockQueueNo")
    private Integer lockQueueNo;
    @Key("DoctorSpec")
    private String doctorSpec;
    @Key("AdmitAddress")
    private String admitAddress;

    public DoctorScheduleResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String scheduleItemCode() {
        return scheduleItemCode;
    }

    @Override
    public String serviceDate() {
        return serviceDate;
    }

    @Override
    public Integer weekDay() {
        return weekDay;
    }

    @Override
    public String sessionName() {
        return sessionName;
    }

    @Override
    public String startTime() {
        return startTime;
    }

    @Override
    public String endTime() {
        return endTime;
    }

    @Override
    public int departmentCode() {
        return departmentCode;
    }

    @Override
    public String departmentName() {
        return departmentName;
    }

    @Override
    public Integer doctorCode() {
        return doctorCode;
    }

    @Override
    public String doctorName() {
        return doctorName;
    }

    @Override
    public Integer doctorTitleCode() {
        return doctorTitleCode;
    }

    @Override
    public String doctorTitle() {
        return doctorTitle;
    }

    @Override
    public Integer doctorSessTypeCode() {
        return doctorSessTypeCode;
    }

    @Override
    public String doctorSessType() {
        return doctorSessType;
    }

    @Override
    public String fee() {
        return fee;
    }

    @Override
    public Integer availableNum() {
        return availableNum;
    }

    @Override
    public String admitTimeRange() {
        return admitTimeRange;
    }

    @Override
    public Integer lockQueueNo() {
        return lockQueueNo;
    }

    @Override
    public String doctorSpec() {
        return doctorSpec;
    }

    @Override
    public String admitAddress() {
        return admitAddress;
    }
}
