package com.bookbuf.api.responses.impl.integral;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/6/12.
 */

public interface IntegralResponse extends Response{
    //店铺名称
    String vendorName();

    //logo
    String logo();

    //会员卡号
    String cardNumber();

    //积分
    String integral();

    //单位
    String integral_unit();
}
