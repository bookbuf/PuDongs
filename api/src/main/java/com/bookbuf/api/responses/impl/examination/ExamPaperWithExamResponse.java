package com.bookbuf.api.responses.impl.examination;

import com.bookbuf.api.responses.Response;

/**
 * 组合试卷、与上一次的考试记录
 * author: robert.
 * date :  16/9/23.
 */
public interface ExamPaperWithExamResponse extends Response {

	/**
	 * 该试卷相关联的上一次考试记录，允许为null表示从未参加过此类考试。
	 *
	 * @return latestExamRecord
	 */
	ExamResponse latestExamRecord ();

	ExamPaperResponse paper ();
}
