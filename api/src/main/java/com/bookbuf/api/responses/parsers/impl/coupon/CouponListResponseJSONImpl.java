package com.bookbuf.api.responses.parsers.impl.coupon;

import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public class CouponListResponseJSONImpl extends PuDongParserImpl implements CouponListResponse, Serializable {

    @Implementation(CouponResponseJSONImpl.class)
    private List<CouponResponse> coupons;

    public CouponListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<CouponResponse> coupons() {
        return coupons;
    }
}
