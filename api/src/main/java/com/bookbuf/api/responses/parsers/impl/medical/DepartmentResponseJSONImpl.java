package com.bookbuf.api.responses.parsers.impl.medical;

import com.bookbuf.api.responses.impl.medical.DepartmentResponse;
import com.bookbuf.api.responses.impl.medical.SectionResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class DepartmentResponseJSONImpl extends PuDongParserImpl implements DepartmentResponse, Serializable {

    @Key("DepartmentCode")
    private int code;
    @Key("DepartmentName")
    private String name;
    @Key("DepartmentGroup")
    private boolean isGroup;
    @Key("cDepartment")
    @Implementation(SectionResponseJSONImpl.class)
    private List<SectionResponse> sections;

    public DepartmentResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public boolean isGroup() {
        return isGroup;
    }

    @Override
    public List<SectionResponse> sections() {
        return sections;
    }
}
