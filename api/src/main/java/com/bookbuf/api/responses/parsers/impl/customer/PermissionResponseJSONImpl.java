package com.bookbuf.api.responses.parsers.impl.customer;

import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/7/19.
 */

public class PermissionResponseJSONImpl extends PuDongParserImpl implements PermissionResponse, Serializable {

    @Key("gene")
    private boolean gene;

    @Key("withdraw")
    private boolean withdraw;

    @Key("hospital")
    private boolean hospital;

    public PermissionResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public boolean geneEnable() {
        return gene;
    }

    @Override
    public boolean withdrawEnable() {
        return withdraw;
    }

    @Override
    public boolean hospitalEnable() {
        return hospital;
    }
}
