package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSiteListResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSiteResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/12/1.
 */

public class MonitorSiteListResponseJSONImpl extends PuDongParserImpl implements MonitorSiteListResponse,Serializable {

    @Key("code")
    int code;
    @Key("data")
    @Implementation(MonitorSiteResponseJSONImpl.class)
    List<MonitorSiteResponse> data;
    @Key("message")
    String message;

    public MonitorSiteListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public List<MonitorSiteResponse> list() {
        return data;
    }
}
