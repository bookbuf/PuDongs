package com.bookbuf.api.responses.impl.order;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;

/**
 * Created by bo.wei on 2017/5/3.
 */

public interface OrderResultResponse extends Response {
    String isPaySuccess();

    String orderId();

    String error();

    CouponResponse detail();
}
