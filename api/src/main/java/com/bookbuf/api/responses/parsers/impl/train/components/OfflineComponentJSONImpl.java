package com.bookbuf.api.responses.parsers.impl.train.components;

/**
 * author: robert.
 * date :  2016/12/22.
 */

import com.bookbuf.api.responses.impl.train.components.OfflineComponent;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

public class OfflineComponentJSONImpl extends PuDongParserImpl implements OfflineComponent, Serializable {

    @Key("teacher")
    private String teacher;
    @Key("time")
    private String time;
    @Key("address")
    private String address;
    @Key("period")
    private String period;
    @Key("content")
    private String content;

    public OfflineComponentJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String teacher() {
        return teacher;
    }

    @Override
    public String time() {
        return time;
    }

    @Override
    public String address() {
        return address;
    }

    @Override
    public String period() {
        return period;
    }

    @Override
    public String content() {
        return content;
    }
}