package com.bookbuf.api.responses.parsers.impl.train.components;

import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

public final class CourseComponentJSONImpl extends PuDongParserImpl implements CourseComponent, Serializable {

    @Key("id")
    private long id;
    @Key("title")
    private String title;
    @Key("status")
    private int status;
    @Key("online")
    private boolean online;
    @Key("description")
    private String description;
    @Key("passedCount")
    private int passedCount;
    @Key("totalCount")
    private int totalCount;

    public CourseComponentJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String title() {
        return title;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public boolean online() {
        return online;
    }

    @Override
    public int passedCount() {
        return passedCount;
    }

    @Override
    public int totalCount() {
        return totalCount;
    }


}
