package com.bookbuf.api.responses.parsers.impl.measure.latest;

import com.bookbuf.api.responses.impl.measure.latest.MeasureFieldResponse;
import com.bookbuf.api.responses.impl.measure.latest.MeasureRecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public final class MeasureRecordResponseJSONImpl extends PuDongParserImpl implements MeasureRecordResponse, Serializable {


    @Key("id")
    private String id;
    @Key("group_name")
    private String groupName;
    @Key("indicator_name")
    private String indicatorName;
    @Key("unit_name")
    private String unit;
    @Key("take_time")
    private long takeTime;
    @Key("time")
    private String timeFormat;
    @Key("status")
    private int status;
    @Key("indicator_fields")
    @Implementation(MeasureFieldResponseJSONImpl.class)
    private List<MeasureFieldResponse> fields;

    public MeasureRecordResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String groupName() {
        return groupName;
    }

    @Override
    public String indicatorName() {
        return indicatorName;
    }

    @Override
    public String unit() {
        return unit;
    }

    @Override
    public long takeTime() {
        return takeTime;
    }

    @Override
    public String timeFormat() {
        return timeFormat;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public List<MeasureFieldResponse> fields() {
        return fields;
    }
}
