package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  2017/1/3.
 */

public interface FetchCouponStatusResponse extends Response {

    String title();

    int type();

    boolean isUsed();

    int timeLimit();

    boolean isOutService();

    String description();

    String beginDate();

    String endDate();

    List<String> goods();

    class TimeLimit {

        public static final int NORMAL = 0;
        public static final int OVERDUE = 1;
        public static final int WAIT = 2;

    }

}