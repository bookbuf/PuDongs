package com.bookbuf.api.responses.parsers;

import com.bookbuf.api.responses.impl.BannerResponse;
import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.impl.achievement.FetchAchievementDetailResponse;
import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceContextResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceDayResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceMonthResponse;
import com.bookbuf.api.responses.impl.attendance.AttendanceStatusResponse;
import com.bookbuf.api.responses.impl.call.CallCancelResponse;
import com.bookbuf.api.responses.impl.call.CallResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponAliasResponse;
import com.bookbuf.api.responses.impl.coupon.CouponListResponse;
import com.bookbuf.api.responses.impl.coupon.CouponNewResponse;
import com.bookbuf.api.responses.impl.coupon.CouponProductResponse;
import com.bookbuf.api.responses.impl.coupon.CouponResponse;
import com.bookbuf.api.responses.impl.coupon.CouponTypeListResponse;
import com.bookbuf.api.responses.impl.coupon.FetchCouponStatusResponse;
import com.bookbuf.api.responses.impl.coupon.VerifyCouponResponse;
import com.bookbuf.api.responses.impl.customer.BindCardListResponse;
import com.bookbuf.api.responses.impl.customer.CreateUserResponse;
import com.bookbuf.api.responses.impl.customer.CustomerLoginResponse;
import com.bookbuf.api.responses.impl.customer.DetectionListResponse;
import com.bookbuf.api.responses.impl.customer.PermissionResponse;
import com.bookbuf.api.responses.impl.customer.ProfileResponse;
import com.bookbuf.api.responses.impl.customer.UserDocumentResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperResponse;
import com.bookbuf.api.responses.impl.examination.ExamPaperWithExamResponse;
import com.bookbuf.api.responses.impl.examination.ExamResponse;
import com.bookbuf.api.responses.impl.examination.FetchDiscoverQuestionResponse;
import com.bookbuf.api.responses.impl.global.AppUpgradeResponse;
import com.bookbuf.api.responses.impl.global.RoleResponse;
import com.bookbuf.api.responses.impl.global.SplashScreenResponse;
import com.bookbuf.api.responses.impl.healthlive.CategoryListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.FetchRelatedHealthLiveListResponse;
import com.bookbuf.api.responses.impl.healthlive.HealthLiveResponse;
import com.bookbuf.api.responses.impl.integral.IntegralListResponse;
import com.bookbuf.api.responses.impl.measure.add.AddRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.LatestMeasureResponse;
import com.bookbuf.api.responses.impl.measure.list.ListRecordResponse;
import com.bookbuf.api.responses.impl.medical.DepartmentResponse;
import com.bookbuf.api.responses.impl.medical.DoctorResponse;
import com.bookbuf.api.responses.impl.medical.DoctorScheduleResponse;
import com.bookbuf.api.responses.impl.medical.HospitalResponse;
import com.bookbuf.api.responses.impl.medical.MedicalRegisterResponse;
import com.bookbuf.api.responses.impl.message.MessageResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.bookbuf.api.responses.impl.order.OrderDetailResponse;
import com.bookbuf.api.responses.impl.order.OrderGenerateResponse;
import com.bookbuf.api.responses.impl.order.OrderPayResponse;
import com.bookbuf.api.responses.impl.order.OrderResponse;
import com.bookbuf.api.responses.impl.order.OrderResultResponse;
import com.bookbuf.api.responses.impl.order.PayResultResponse;
import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.impl.task.TaskContextResponse;
import com.bookbuf.api.responses.impl.train.FetchLessonResponse;
import com.bookbuf.api.responses.impl.train.FetchSignUpCourseResponse;
import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.bookbuf.api.responses.impl.user.ClerkResponse;
import com.bookbuf.api.responses.impl.user.CredentialResponse;
import com.bookbuf.api.responses.impl.user.CustomerResponse;
import com.bookbuf.api.responses.impl.user.FetchClerkProfileResponse;
import com.bookbuf.api.responses.impl.weather.WeatherResponse;
import com.ipudong.core.Result;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface InternalFactory extends Serializable {

    // -----------------------------------------
    // 扑咚服务 —— 万能适配器
    // -----------------------------------------

    Result<BooleanResponse> createBooleanResponse(JSONObject json);

    Result<CreateUserResponse> createUserResponse(JSONObject json);

    Result<LongResponse> createLongResponse(JSONObject json);

    Result<MonitorCountResponse> createMonitorResponse(JSONObject json);


    // -----------------------------------------
    // 扑咚服务 —— 基础服务
    // -----------------------------------------

    Result<SplashScreenResponse> createSplashScreenResponse(JSONObject json);

    Result<AppUpgradeResponse> createAppUpgradeResponse(JSONObject json);

    Result<RoleResponse> createRoleResponse(JSONObject json);

    Result<CredentialResponse> createCredentialResponse(JSONObject json);

    Result<ClerkResponse> createClerkResponse(JSONObject json);

    Result<FetchClerkProfileResponse> createFetchClerkProfileResponse(JSONObject json);

    Result<CustomerResponse> createCustomerResponse(JSONObject json);

    Result<LongResponse> createCheckUserByQueryResponse(JSONObject json);

    Result<BannerResponse> createBannerResponse(JSONObject json);

    // -----------------------------------------
    // 扑咚服务 —— 检测
    // -----------------------------------------

    Result<AddRecordResponse> createAddRecordResponse(JSONObject json);

    Result<LatestMeasureResponse> createLatestMeasureResponse(JSONObject json);

    Result<ListRecordResponse> createListRecordResponse(JSONObject json);

    // -----------------------------------------
    // 扑咚服务 —— 发现
    // -----------------------------------------

    Result<FetchDiscoverQuestionResponse> createFetchDiscoverResponse(JSONObject json);

    // -----------------------------------------
    // 扑咚服务 —— 任务
    // -----------------------------------------

    Result<TaskContextResponse> createTaskResponseList(JSONObject json);

    // -----------------------------------------
    // 扑咚服务 —— 考试
    // -----------------------------------------

    Result<QuestionResponse> createQuestionResponse(JSONObject json);

    Result<ExamPaperResponse> createExamPaperResponse(JSONObject json);

    Result<ExamResponse> createExamResponse(JSONObject json);

    Result<ExamResponse> createExamHistoryResponse(JSONObject json);

    Result<List<ExamPaperWithExamResponse>> createExamPaperWithExamResponseList(JSONObject json);

    Result<BooleanResponse> createQuestionAnswerBooleanResponse(JSONObject json);

    Result<BooleanResponse> createExamTerminateBooleanResponse(JSONObject json);

    Result<BooleanResponse> createCreateExaminationRecordResponse(JSONObject json);


    // -----------------------------------------
    // 扑咚服务 —— 系统消息&推送
    // -----------------------------------------

    Result<MessageResponse> createMessageResponse(JSONObject json);

    Result<List<MessageResponse>> createMessageListResponse(JSONObject json);

    Result<LongResponse> createFetchNewsMessageResponse(JSONObject json);

//    Result<LongResponse> createMessageStatResponse(JSONObject json) ;

    // -----------------------------------------
    // 扑咚服务 —— 贵州挂号服务
    // -----------------------------------------
    Result<MedicalRegisterResponse> createMedicalRegisterResponse(JSONObject json);

    Result<List<MedicalRegisterResponse>> createMedicalRegisterResponseList(JSONObject json);

    Result<List<HospitalResponse>> createHospitalResponseList(JSONObject json);

    Result<List<DepartmentResponse>> createDepartmentResponseList(JSONObject json);

    Result<List<DoctorResponse>> createDoctorResponseList(JSONObject json);

    Result<List<DoctorScheduleResponse>> createDoctorScheduleResponseList(JSONObject json);

    // -----------------------------------------
    // 天气 —— 和风天气
    // -----------------------------------------

    Result<WeatherResponse> createWeatherResponse(JSONObject json);

    // -----------------------------------------
    // 网络电话 —— 接口服务商已停止服务
    // -----------------------------------------

    Result<CallCancelResponse> createCallCancelResponse(JSONObject json);

    Result<CallResponse> createCallResponse(JSONObject json);

    // -----------------------------------------
    // 扑咚服务 —— 考勤
    // -----------------------------------------

    Result<AttendanceStatusResponse> createAttendanceStatusResponse(JSONObject json);

    Result<AttendanceMonthResponse> createAttendanceMonthResponse(JSONObject json);

    Result<AttendanceDayResponse> createAttendanceDayResponse(JSONObject json);

    Result<AttendanceContextResponse> createAttendanceContextResponse(JSONObject json);

    // -----------------------------------------
    // 扑咚服务 —— 成就
    // -----------------------------------------

    Result<FetchCareerMapResponse> createCareerMapResponse(JSONObject json);

    Result<FetchAchievementDetailResponse> createAchievementResponse(JSONObject json);

    Result<FetchSignUpCourseResponse> createApplyCourseResponse(JSONObject json);

    Result<FetchLessonResponse> createLessonResponse(JSONObject json);

    Result<FetchTrainResponse> createFetchTrainResponse(JSONObject json);

    /*优惠券*/

    Result<CouponResponse> createCouponResponse(JSONObject json);

    Result<CouponNewResponse> createCouponNewResponse(JSONObject json);

    Result<VerifyCouponResponse> createVerifyCouponResponse(JSONObject json);

    Result<CouponListResponse> createCouponListResponse(JSONObject json);

    Result<CouponAliasListResponse> createCouponAliasListResponse(JSONObject json);

    Result<CouponAliasResponse> createCouponAliasResponse(JSONObject json);

    Result<FetchCouponStatusResponse> createFetchCouponStatusResponse(JSONObject json);

    Result<CouponProductResponse> createFetchProductResponse(JSONObject json);

    Result<BindCardListResponse> createBindCardListResponse(JSONObject json);

    /*C端登录*/
    Result<CustomerLoginResponse> createCustomerLoginResponse(JSONObject json);

    Result<ProfileResponse> createProfileResponse(JSONObject json);

    Result<FetchHealthLiveListResponse> createFetchHealthLiveListResponse(JSONObject json);

    Result<CategoryListResponse> createCategoryListResponse(JSONObject json);

    Result<HealthLiveResponse> createHealthLiveResponse(JSONObject json);

    Result<FetchRelatedHealthLiveListResponse> createFetchRelatedHealthLiveListResponse(JSONObject json);

    Result<DetectionListResponse> createDetectionListResponse(JSONObject json);

    Result<UserDocumentResponse> createUserDocumentResponse(JSONObject json);

    Result<OrderPayResponse> createOrderPayResponse(JSONObject json);

    Result<OrderDetailResponse> createOrderDetailResponse(JSONObject json);

    Result<OrderGenerateResponse> createOrderGenerateResponse(JSONObject json);

    Result<OrderResponse> createOrderResponse(JSONObject json);

    Result<OrderResultResponse> createOrderResultResponse(JSONObject json);

    Result<PayResultResponse> createPayResultResponse(JSONObject json);

    Result<IntegralListResponse> createIntergralListResponse(JSONObject json);

    Result<String> createStringResponse(JSONObject json);

    Result<CouponListResponse> createRecomCouponResponse(JSONObject json);

    Result<CouponTypeListResponse> createCouponTypeResponse(JSONObject json);

    Result<PermissionResponse> createPermissionResponse(JSONObject json);
}
