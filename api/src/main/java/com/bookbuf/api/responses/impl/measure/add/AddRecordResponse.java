package com.bookbuf.api.responses.impl.measure.add;

import com.bookbuf.api.responses.Response;
import com.bookbuf.api.responses.impl.measure.latest.MeasureRecordResponse;

/**
 * author: robert.
 * date :  16/11/28.
 */
public interface AddRecordResponse extends Response {

	String suggestion ();

	MeasureRecordResponse record ();
}
