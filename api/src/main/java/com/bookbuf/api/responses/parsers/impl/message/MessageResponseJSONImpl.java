package com.bookbuf.api.responses.parsers.impl.message;

import com.bookbuf.api.responses.impl.message.MessageResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/21.
 */
public final class MessageResponseJSONImpl extends PuDongParserImpl implements MessageResponse, Serializable {


    @Key("id")
    private long id;
    @Key("status")
    private int status;
    @Key("title")
    private String title;
    @Key("content")
    private String content;
    @Key("time")
    private String time;
    @Key("image")
    private String imgUrl;
    @Key("integral")
    private String integral;
    @Key("userName")
    private String userName;
    @Key("posTime")
    private String posTime;

    public MessageResponseJSONImpl(JSONObject wrapperJSONObject) {
        super(wrapperJSONObject);
    }

    @Override
    public long id() {
        return this.id;
    }

    @Override
    public int status() {
        return this.status;
    }

    @Override
    public String title() {
        return this.title;
    }

    @Override
    public String content() {
        return this.content;
    }

    @Override
    public String time() {
        return this.time;
    }

    @Override
    public String imgUrl() {
        return this.imgUrl;
    }

    @Override
    public String integral() {
        return integral;
    }

    @Override
    public String userName() {
        return userName;
    }

    @Override
    public String posTime() {
        return posTime;
    }
}
