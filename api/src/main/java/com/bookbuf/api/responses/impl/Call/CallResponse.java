package com.bookbuf.api.responses.impl.call;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2016/11/23.
 */

public interface CallResponse extends Response{

    String customerSerNum();

    String appId();

    String fromSerNum();

    String dateCreated();

    String callSid();

    String orderId();
}
