package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherDailyResponse extends Response {

    String date();

    String hum();

    String pcpn();

    String pop();

    String pres();

    String vis();

    TmpResponse tmp();

    interface TmpResponse extends Response {
        String max();

        String min();
    }

    WindResponse wind();

    interface WindResponse extends Response {

        String deg();

        String dir();

        String sc();

        String spd();

    }

    AstroResponse astro();

    interface AstroResponse extends Response {
        String mr();

        String ms();

        String sr();

        String ss();
    }

    CondResponse cond();


    interface CondResponse extends Response {
        String codeDaily();

        String codeNight();

        String txtDaily();

        String txtNight();
    }


}
