package com.bookbuf.api.responses.impl.medical;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface MedicalRegisterResponse extends Response {

	long id ();

	String hospitalName ();

	String hospitalDepartment ();

	String hospitalDoctor ();

	String hospitalAddress ();

	String hospitalTel ();

	String clinicalTime ();

	String clinicalCost ();

	String clinicalPerson ();

	String clinicalPersonTel ();

	int remainNum ();

	int remainMaxNum ();
}
