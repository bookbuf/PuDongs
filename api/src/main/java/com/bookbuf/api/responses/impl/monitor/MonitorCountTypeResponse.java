package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/11/29.
 */

public interface MonitorCountTypeResponse extends Response {

    String alarmType();

    String count();
}
