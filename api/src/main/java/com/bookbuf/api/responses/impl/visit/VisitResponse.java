package com.bookbuf.api.responses.impl.visit;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface VisitResponse extends Response {

//			"customerSerNum": "4000287750",
//			"fromSerNum": "4000287750",
//			"appId": "aaf98f8950d585f90150d65a2e01030a",
//			"dateCreated": "2016-09-14 15:34:16",
//			"callSid": "160914153416362400010185004eff7f",
//			"orderId": "CM1018520160914153416199785"

	String displayNum ();

	String fromDisplayNum ();

	String appId ();

	String dateCreated ();

	String callSid ();

	String orderId ();
}
