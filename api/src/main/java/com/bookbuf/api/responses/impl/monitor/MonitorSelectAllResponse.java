package com.bookbuf.api.responses.impl.monitor;

import com.bookbuf.api.responses.Response;

/**
 * Created by bo.wei on 2017/12/6.
 */

public interface MonitorSelectAllResponse extends Response {
    int code();

    MonitorSelectDataResponse data();

    String message();
}
