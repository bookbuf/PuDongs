package com.bookbuf.api.responses.parsers.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * author: robert.
 * date :  2016/12/19.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Implementation {

    /**
     * @return the desired name of the field when it is serialized
     */
    Class<?> value();
}