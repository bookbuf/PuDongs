package com.bookbuf.api.responses.impl.customer;

/**
 * Created by bo.wei on 2017/11/15.
 */

public interface BindCardResponse {
    String card();
}
