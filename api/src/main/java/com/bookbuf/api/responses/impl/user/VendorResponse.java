package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/8/1.
 */
public interface VendorResponse extends Response {

    /**
     * @return 合作商id
     */
    long id();

    /**
     * @return 店铺id
     */
    long shopId();

    /**
     * @return 店铺编号
     */
    String code();

    /**
     * @return 店员编号
     */
    String num();

    /**
     * @return 店铺 logo
     */
    String logo();

    /**
     * @return 店铺
     */
    String name();

    /**
     * @return 部门
     */
    String department();

    /**
     * @return 角色(店长/店员/管理员等等)
     */
    String role();

    /**
     * @return 店铺排名
     */
    int position();

    /**
     * 店铺编号
     * @return
     */
    String storeNumber();
}
