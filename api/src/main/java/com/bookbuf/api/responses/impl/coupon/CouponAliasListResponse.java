package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * Created by bo.wei on 2017/4/12.
 */

public interface CouponAliasListResponse extends Response {
    List<CouponAliasResponse> couponAliases();
}
