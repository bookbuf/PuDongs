package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherSuggestionResponse extends Response {

	WeatherMapResponse comf ();

	WeatherMapResponse cw ();

	WeatherMapResponse drsg ();

	WeatherMapResponse flu ();

	WeatherMapResponse sport ();

	WeatherMapResponse trav ();

	WeatherMapResponse uv ();
}
