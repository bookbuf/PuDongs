package com.bookbuf.api.responses.parsers.impl.achievement;

import com.bookbuf.api.responses.impl.achievement.FetchCareerMapResponse;
import com.bookbuf.api.responses.impl.achievement.components.AchievementComponent;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.achievement.components.AchievementComponentJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public final class FetchCareerMapResponseJSONImpl extends PuDongParserImpl implements FetchCareerMapResponse, Serializable {

    @Key("type")
    private int type;
    @Key("isHot")
    private boolean isHot;
    @Key("achievements")
    @Implementation(AchievementComponentJSONImpl.class)
    private List<AchievementComponent> achievements;

    public FetchCareerMapResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public int type() {
        return type;
    }

    @Override
    public boolean isHot() {
        return isHot;
    }

    @Override
    public List<AchievementComponent> achievements() {
        return achievements;
    }
}
