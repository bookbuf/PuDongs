package com.bookbuf.api.responses.parsers.impl.medical;

import com.bookbuf.api.responses.impl.medical.HospitalResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class HospitalResponseJSONImpl extends PuDongParserImpl implements HospitalResponse, Serializable {


    @Key("hospital_id")
    private long id;
    @Key("hospital_name")
    private String name;
    @Key("hospital_fullname")
    private String fullName;
    @Key("hospital_description")
    private String description;
    @Key("hospital_addr")
    private String address;
    @Key("hospital_tp")
    private String tel;
    @Key("hospital_logo")
    private String logo;

    public HospitalResponseJSONImpl(JSONObject o) {
        super(o);
    }

    private String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\\t|\\r|\\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String fullName() {
        return fullName;
    }

    @Override
    public String description() {
        return replaceBlank(description);
    }

    @Override
    public String address() {
        return address;
    }

    @Override
    public String tel() {
        return tel;
    }

    @Override
    public String logo() {
        return logo;
    }
}
