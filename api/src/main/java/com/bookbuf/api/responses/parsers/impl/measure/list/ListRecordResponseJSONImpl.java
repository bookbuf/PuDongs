package com.bookbuf.api.responses.parsers.impl.measure.list;

import com.bookbuf.api.responses.impl.measure.list.ListRecordResponse;
import com.bookbuf.api.responses.impl.measure.list.RecordResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.measure.add.RecordResponseJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public final class ListRecordResponseJSONImpl extends PuDongParserImpl implements ListRecordResponse, Serializable {

    @Key("name")
    private String name;
    @Key("value_list")
    @Implementation(RecordResponseJSONImpl.class)
    private List<RecordResponse> records;

    public ListRecordResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public List<RecordResponse> records() {
        return records;
    }
}
