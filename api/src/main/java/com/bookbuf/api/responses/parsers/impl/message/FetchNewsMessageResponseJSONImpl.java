package com.bookbuf.api.responses.parsers.impl.message;

import com.bookbuf.api.responses.impl.LongResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2016/12/19.
 */

public class FetchNewsMessageResponseJSONImpl extends PuDongParserImpl implements LongResponse, Serializable {

    @Key("news")
    private long id;

    public FetchNewsMessageResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long get() {
        return id;
    }
}
