package com.bookbuf.api.responses.impl.visit;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface CancelVisitResponse extends Response {

	int type ();

	String sid ();

	String originCode ();

}