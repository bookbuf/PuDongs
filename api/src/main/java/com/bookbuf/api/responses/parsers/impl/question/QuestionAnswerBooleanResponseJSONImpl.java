package com.bookbuf.api.responses.parsers.impl.question;

import com.bookbuf.api.responses.impl.BooleanResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class QuestionAnswerBooleanResponseJSONImpl extends PuDongParserImpl implements BooleanResponse, Serializable {

    @Key("isCorrected")
    private boolean bool;

    public QuestionAnswerBooleanResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public boolean get() {
        return bool;
    }
}
