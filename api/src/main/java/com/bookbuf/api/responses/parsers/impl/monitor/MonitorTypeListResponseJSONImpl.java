package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSiteListResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorSiteResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorTypeListResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/12/1.
 */

public class MonitorTypeListResponseJSONImpl extends PuDongParserImpl implements MonitorTypeListResponse,Serializable {

    @Key("list")
    @Implementation(MonitorTypeResponseJSONImpl.class)
    List<MonitorTypeResponse> list;
    @Key("pageNum")
    int pageNum;
    @Key("pageSize")
    int pageSize;
    @Key("totalPages")
    int totalPages;
    @Key("totalRows")
    int totalRows;

    public MonitorTypeListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<MonitorTypeResponse> list() {
        return list;
    }

    @Override
    public int pageNum() {
        return pageNum;
    }

    @Override
    public int pageSize() {
        return pageSize;
    }

    @Override
    public int totalPages() {
        return totalPages;
    }

    @Override
    public int totalRows() {
        return totalRows;
    }
}
