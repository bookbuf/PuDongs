//package com.bookbuf.api.responses.parsers.impl.task;
//
//import com.bookbuf.api.responses.PuDongResponseImpl;
//import com.bookbuf.api.responses.impl.task.TaskExtrasResponse;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.io.Serializable;
//
///**
// * author: robert.
// * date :  16/9/18.
// */
//public final class TaskExtrasResponseJSONImpl extends PuDongResponseImpl implements TaskExtrasResponse, Serializable {
//
//    private String extras;
//
//    protected TaskExtrasResponseJSONImpl(Object o) {
//        super(o);
//    }
//
//    @Override
//    protected void init(Object object) {
//        if (object instanceof JSONObject) {
//            this.extras = object.toString();
//        } else if (object instanceof JSONArray) {
//            this.extras = object.toString();
//        } else {
//            this.extras = null;
//        }
//    }
//
//    @Override
//    public String extra() {
//        return this.extras;
//    }
//
//    @Override
//    public String toString() {
//        return "TaskExtrasResponseJSONImpl{" +
//                "extras=" + extra() +
//                '}';
//    }
//}
