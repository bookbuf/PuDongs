package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherBasicResponse extends Response {

    String city();

    String cnty();

    String id();

    String lat();

    String lon();

    String prov();

    TimeAreaResponse timeArea();

    interface TimeAreaResponse extends Response {

        String loc();

        String utc();
    }
}
