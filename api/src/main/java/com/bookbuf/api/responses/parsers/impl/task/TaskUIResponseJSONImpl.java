package com.bookbuf.api.responses.parsers.impl.task;

import com.bookbuf.api.responses.impl.task.TaskUIResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/18.
 */
public final class TaskUIResponseJSONImpl extends PuDongParserImpl implements TaskUIResponse, Serializable {
    @Key("background")
    private String background;

    public TaskUIResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String background() {
        return this.background;
    }

}
