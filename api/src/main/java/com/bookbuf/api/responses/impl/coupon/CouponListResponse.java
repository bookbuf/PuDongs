package com.bookbuf.api.responses.impl.coupon;

import com.bookbuf.api.responses.Response;

import java.util.List;

public interface CouponListResponse extends Response {

    List<CouponResponse> coupons();
}
