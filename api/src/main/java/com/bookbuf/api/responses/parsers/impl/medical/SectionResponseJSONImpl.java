package com.bookbuf.api.responses.parsers.impl.medical;

import com.bookbuf.api.responses.impl.medical.SectionResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class SectionResponseJSONImpl extends PuDongParserImpl implements SectionResponse, Serializable {

    @Key("DepartmentCode")
    private int code;
    @Key("DepartmentName")
    private String name;
    @Key("ResRowIdFlag")
    private boolean isResRowIdFlag;
    @Key("HospitalCode")
    private String hospitalCode;

    public SectionResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public boolean isResRowIdFlag() {
        return isResRowIdFlag;
    }

    @Override
    public String hospitalCode() {
        return hospitalCode;
    }

}