package com.bookbuf.api.responses.impl.question;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * author: robert.
 * date :  16/9/14.
 */
public interface QuestionResponse extends Response {

	/**
	 * @return 题目编号
	 */
	long id ();

	/**
	 * @return 题目标题
	 */
	String title ();

	/**
	 * @return 正确答案的数量
	 */
	int choicesNum ();

	/**
	 * {"key":"A","value":"大于7.0、再次检测空腹血糖、三多一少"},
	 * {"key":"B","value":"大于5.7、再次检测空腹血糖、三多一少"},
	 * {"key":"C","value":"大于7.0、再次检测餐后2小时血糖、三多一少"},
	 * {"key":"D","value":"大于5.7、再次检测餐后2小时血糖、三多一少"}
	 *
	 * @return 选项字典
	 */
	List<ChoiceResponse> choices ();

	interface ChoiceResponse extends Response{

		String option ();

		String description ();

	}
}
