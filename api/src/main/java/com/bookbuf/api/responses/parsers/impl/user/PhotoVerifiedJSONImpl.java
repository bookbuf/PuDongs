package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.PhotoVerifiedResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2018/1/18.
 */

public class PhotoVerifiedJSONImpl extends PuDongParserImpl implements PhotoVerifiedResponse, Serializable {

    @Key("verified")
    boolean verified;
    @Key("verified_url")
    String verifiedUrl;

    public PhotoVerifiedJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public boolean verified() {
        return verified;
    }

    @Override
    public String verifiedUrl() {
        return verifiedUrl;
    }
}
