package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeDataResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeListResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bo.wei on 2017/11/28.
 */

public class MonitorCountTypeListResponseJSONImpl extends PuDongParserImpl implements MonitorCountTypeListResponse,Serializable{

    @Key("code")
    int code;
    @Key("data")
    @Implementation(MonitorCountTypeDataResponseJSONImpl.class)
    MonitorCountTypeDataResponse data;
    @Key("message")
    String message;

    public MonitorCountTypeListResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public MonitorCountTypeDataResponse data() {
        return data;
    }

    @Override
    public String message() {
        return message;
    }
}
