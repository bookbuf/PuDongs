package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorSelectResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/12/6.
 */

public class MonitorSelectResponseJSONImpl extends PuDongParserImpl implements MonitorSelectResponse,Serializable {

    @Key("alarmRecordid")
    long alarmRecordid;
    @Key("alarmSite")
    String alarmSite;
    @Key("alarmType")
    String alarmType;
    @Key("imagePath")
    String imagePath;
    @Key("alarmdescribe")
    String alarmdescription;
    @Key("alarmDate")
    long alarmDate;

    public MonitorSelectResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public long alarmRecordid() {
        return alarmRecordid;
    }

    @Override
    public String alarmSite() {
        return alarmSite;
    }

    @Override
    public String alarmType() {
        return alarmType;
    }

    @Override
    public String imagePath() {
        return imagePath;
    }

    @Override
    public String alarmdescription() {
        return alarmdescription;
    }

    @Override
    public long alarmDate() {
        return alarmDate;
    }
}
