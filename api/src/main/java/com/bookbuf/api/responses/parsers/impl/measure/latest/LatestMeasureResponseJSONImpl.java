package com.bookbuf.api.responses.parsers.impl.measure.latest;

import com.bookbuf.api.responses.impl.measure.latest.LatestMeasureResponse;
import com.bookbuf.api.responses.impl.measure.latest.MeasureRecordResponse;
import com.bookbuf.api.responses.impl.measure.latest.MeasureTaskResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public final class LatestMeasureResponseJSONImpl extends PuDongParserImpl implements LatestMeasureResponse, Serializable {

    @Key("key1")
    @Implementation(MeasureTaskResponseJSONImpl.class)
    private List<MeasureTaskResponse> tasks;
    @Key("key2")
    @Implementation(MeasureRecordResponseJSONImpl.class)
    private List<MeasureRecordResponse> records;

    public LatestMeasureResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public List<MeasureTaskResponse> tasks() {
        return tasks;
    }

    @Override
    public List<MeasureRecordResponse> records() {
        return records;
    }
}
