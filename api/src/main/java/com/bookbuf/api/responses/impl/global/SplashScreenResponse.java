package com.bookbuf.api.responses.impl.global;

import com.bookbuf.api.responses.Response;

import org.json.JSONObject;

/**
 * author: robert.
 * date :  17/1/18.
 */
public interface SplashScreenResponse extends Response {


    /**
     * 合作商编号
     *
     * @return vendor
     */
    long vendor();

    String type();

    long id();

    UI ui();

    Target target();

    JSONObject extras();

    interface UI extends Response {
        /**
         * 闪页背景图片的地址
         *
         * @return 图片地址
         */
        String background();

        /**
         * 何时呈现的时间戳（秒级）
         *
         * @return 秒
         */
        long start();

        /**
         * 结束呈现的时间戳（秒级）
         *
         * @return 秒
         */
        long end();
    }

    interface Target extends Response {
        /**
         * 行为类型
         *
         * @return type
         */
        String type();
    }

}
