package com.bookbuf.api.responses.parsers.impl.question;

import com.bookbuf.api.responses.impl.question.QuestionResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/21.
 */
public final class ChoiceResponseJSONImpl extends PuDongParserImpl implements QuestionResponse.ChoiceResponse, Serializable {

    @Key("key")
    private String option;
    @Key("value")
    private String description;

    public ChoiceResponseJSONImpl(JSONObject object) {
        super(object);
    }

    @Override
    public String option() {
        return option;
    }

    @Override
    public String description() {
        return description;
    }
}
