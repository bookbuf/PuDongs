package com.bookbuf.api.responses.impl.user;

import com.bookbuf.api.responses.Response;

/**
 * author: robert.
 * date :  16/8/2.
 */
public interface CredentialResponse extends Response {

	long loginId ();

	String sessionId ();

	int status ();

	String username ();
}
