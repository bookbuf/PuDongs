//package com.bookbuf.api.responses.parsers.impl.customer.detections;
//
//import com.bookbuf.api.responses.impl.customer.CustomerDetectionIndexResponse;
//import com.bookbuf.api.responses.parsers.annotations.Key;
//import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
//
//import org.json.JSONObject;
//
//import java.io.Serializable;
//
///**
// * author: robert.
// * date :  2017/3/20.
// */
//
//public class FieldResponseJSONImpl extends PuDongParserImpl implements CustomerDetectionIndexResponse.Field, Serializable {
//
//    @Key("normalMin")
//    private double normalMin;
//    @Key("normalMax")
//    private double normalMax;
//    @Key("hasNormalMin")
//    private int hasNormalMin;
//    @Key("hasNormalMax")
//    private int hasNormalMax;
//    @Key("name")
//    private String name;
//    @Key("indicatorCode")
//    private String indicatorCode;
//    @Key("unitName")
//    private String unitName;
//    @Key("value")
//    private String value;
//    @Key("status")
//    private int status;
//
//    public FieldResponseJSONImpl(JSONObject jsonObject) {
//        super(jsonObject);
//    }
//
//    @Override
//    public double normalMin() {
//        return normalMin;
//    }
//
//    @Override
//    public double normalMax() {
//        return normalMax;
//    }
//
//    @Override
//    public int hasNormalMin() {
//        return hasNormalMin;
//    }
//
//    @Override
//    public int hasNormalMax() {
//        return hasNormalMax;
//    }
//
//    @Override
//    public String name() {
//        return name;
//    }
//
//    @Override
//    public String indicatorCode() {
//        return indicatorCode;
//    }
//
//    @Override
//    public String unitName() {
//        return unitName;
//    }
//
//    @Override
//    public String value() {
//        return value;
//    }
//
//    @Override
//    public int status() {
//        return status;
//    }
//}
