package com.bookbuf.api.responses.parsers.impl.train;

import com.bookbuf.api.responses.impl.train.FetchTrainResponse;
import com.bookbuf.api.responses.impl.train.components.CourseComponent;
import com.bookbuf.api.responses.impl.train.components.OfflineComponent;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;
import com.bookbuf.api.responses.parsers.impl.train.components.CourseComponentJSONImpl;
import com.bookbuf.api.responses.parsers.impl.train.components.OfflineComponentJSONImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  2016/12/16.
 */

public class FetchTrainResponseJSONImpl extends PuDongParserImpl implements FetchTrainResponse, Serializable {

    @Key("isSignUp")
    private boolean isSignUp;
    @Key("course")
    @Implementation(CourseComponentJSONImpl.class)
    private CourseComponent course;
    @Key("offLine")
    @Implementation(OfflineComponentJSONImpl.class)
    private OfflineComponent offline;

    public FetchTrainResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public boolean isSignUp() {
        return isSignUp;
    }

    @Override
    public CourseComponent course() {
        return course;
    }

    @Override
    public OfflineComponent offline() {
        return offline;
    }

}
