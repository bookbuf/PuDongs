package com.bookbuf.api.responses.parsers.impl.monitor;

import com.bookbuf.api.responses.impl.monitor.MonitorCountResponse;
import com.bookbuf.api.responses.impl.monitor.MonitorCountTypeResponse;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by bo.wei on 2017/11/28.
 */

public class MonitorCountTypeResponseJSONImpl extends PuDongParserImpl implements MonitorCountTypeResponse,Serializable{

    @Key("alarmType")
    String alarmType;
    @Key("count")
    String count;

    public MonitorCountTypeResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String alarmType() {
        return alarmType;
    }

    @Override
    public String count() {
        return count;
    }
}
