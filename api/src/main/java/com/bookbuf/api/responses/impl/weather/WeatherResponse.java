package com.bookbuf.api.responses.impl.weather;

import com.bookbuf.api.responses.Response;

import java.util.List;

/**
 * 包括7-10天预报、实况天气、每小时天气、灾害预警、生活指数、空气质量，一次获取足量数据
 * author: robert.
 * date :  16/11/22.
 */

public interface WeatherResponse extends Response {

	List<WeatherAlarmResponse> alarms ();

	WeatherAQIResponse cityAQI ();

	WeatherBasicResponse basic ();

	List<WeatherDailyResponse> daily ();

	List<WeatherHourlyResponse> hourly ();

	WeatherNowResponse now ();

	WeatherSuggestionResponse suggestion ();

	String status ();
}
