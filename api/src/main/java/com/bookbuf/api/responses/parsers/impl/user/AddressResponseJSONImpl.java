package com.bookbuf.api.responses.parsers.impl.user;

import com.bookbuf.api.responses.impl.user.AddressResponse;
import com.bookbuf.api.responses.parsers.InternalParseUtil;
import com.bookbuf.api.responses.parsers.annotations.IgnoreKey;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/9/19.
 */
public final class AddressResponseJSONImpl extends PuDongParserImpl implements AddressResponse, Serializable {

    @Key("province")
    private String province;
    @Key("city")
    private String city;
    @IgnoreKey
    private String district;
    @Key("detail")
    private String detail;
    @Key("fullAddress")
    private String fullAddress;

    public AddressResponseJSONImpl(JSONObject object) {
        super(object);
        JSONObject jsonObject = (JSONObject) getRealityJSONObject(object);
        this.district = InternalParseUtil.getRawString("district", jsonObject);
        if (this.district == null) {
            this.district = InternalParseUtil.getRawString("distinct", jsonObject);
        }
    }

    @Override
    public String province() {
        return this.province;
    }

    @Override
    public String city() {
        return this.city;
    }

    @Override
    public String district() {
        return this.district;
    }

    @Override
    public String detail() {
        return this.detail;
    }

    @Override
    public String fullAddress() {
        return this.fullAddress;
    }
}
