package com.bookbuf.api.responses.parsers.impl.weather;

import com.bookbuf.api.responses.impl.weather.WeatherBasicResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;
import com.bookbuf.api.responses.parsers.impl.PuDongParserImpl;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * author: robert.
 * date :  16/8/2.
 */
public final class WeatherBasicResponseJSONImpl extends PuDongParserImpl implements WeatherBasicResponse, Serializable {

    @Key("city")
    private String city;
    @Key("cnty")
    private String cnty;
    @Key("id")
    private String id;
    @Key("lat")
    private String lat;
    @Key("lon")
    private String lon;
    @Key("prov")
    private String prov;
    @Key("update")
    @Implementation(TimeAreaJSONImpl.class)
    private TimeAreaResponse timeArea;


    public WeatherBasicResponseJSONImpl(JSONObject o) {
        super(o);
    }

    @Override
    public String city() {
        return city;
    }

    @Override
    public String cnty() {
        return cnty;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String lat() {
        return lat;
    }

    @Override
    public String lon() {
        return lon;
    }

    @Override
    public String prov() {
        return prov;
    }

    @Override
    public TimeAreaResponse timeArea() {
        return timeArea;
    }

    public static final class TimeAreaJSONImpl extends PuDongParserImpl implements TimeAreaResponse, Serializable {

        @Key("loc")
        private String loc;
        @Key("utc")
        private String utc;

        public TimeAreaJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String loc() {
            return loc;
        }

        @Override
        public String utc() {
            return utc;
        }
    }


}
