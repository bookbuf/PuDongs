package com.bookbuf.api.responses.parsers.impl;

import com.bookbuf.api.responses.impl.BannerResponse;
import com.bookbuf.api.responses.parsers.annotations.Implementation;
import com.bookbuf.api.responses.parsers.annotations.Key;

import org.json.JSONObject;

import java.util.List;

/**
 * author: robert.
 * date :  2017/3/7.
 */

public class BannerResponseJSONImpl extends PuDongParserImpl implements BannerResponse {

    @Key("content")
    @Implementation(BannerDetailResponseJSONImpl.class)
    private List<BannerDetailResponse> banners;

    public BannerResponseJSONImpl(JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public List<BannerDetailResponse> banners() {
        return banners;
    }

    public static class BannerDetailResponseJSONImpl extends PuDongParserImpl implements BannerDetailResponse {

        @Key("intent")
        private String intent;
        @Key("url")
        private String url;

        public BannerDetailResponseJSONImpl(JSONObject jsonObject) {
            super(jsonObject);
        }

        @Override
        public String intent() {
            return intent;
        }

        @Override
        public String url() {
            return url;
        }
    }

}
