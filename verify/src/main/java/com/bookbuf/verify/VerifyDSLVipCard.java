package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * Created by robert on 15/11/6.
 */
public class VerifyDSLVipCard extends Verify<VerifyDSLVipCard.Callback> {

	private String vipCard;

	public VerifyDSLVipCard (Callback callback, String vipCard) {
		super (callback);
		this.vipCard = vipCard;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyDSLVipCard (vipCard);
	}

	@Override
	protected void onFailedCallback (int code) {
		if (code == VerifyCode.BS_DSL_CARD_FAIL) {
			callback.onIllegal (vipCard);
		}
	}

	public interface Callback extends IVerifyCallback {
		void onIllegal (String value);
	}
}
