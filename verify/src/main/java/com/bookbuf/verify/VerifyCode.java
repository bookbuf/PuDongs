package com.bookbuf.verify;

public class VerifyCode {

	public static final int CODE_SUCCESS = 0;
	public static final int BS_VERIFY_PHONE_ILLEGAL = 1;
	public static final int BS_VERIFY_TELEPHONE_ILLEGAL = 2;
	public static final int BS_VERIFY_PASSWORD_ILLEGAL = 3;
	public static final int BS_VERIFY_CODE_CODE_EMPTY = 4;
	public static final int BS_VERIFY_EMPTY = 4;
	public static final int BS_TERMS_UNREAD = 5;
	public static final int BS_VERIFY_CODE_UNSEND = 6;
	public static final int BS_VERIFY_CODE_BELONG_NOT_USER = 7;
	public static final int BS_BIND_IDCRAD_NULL = 8;
	public static final int BS_BIND_IDCRAD_ILLAGE = 9;
	public static final int BS_CN_ILLAGE = 10;
	public static final int BS_NAME_TOO_LONG = 11;
	public static final int BS_RELATION_EMPTY = 12;
	public static final int BS_NAME_TOO_SHORT = 13;

	public static final int BS_DSL_CARD_FAIL = 15;


}
