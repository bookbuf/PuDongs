package com.bookbuf.verify.base;

import java.util.LinkedList;

/**
 * Created by robert on 16/6/8.
 */
public class VerifyBuilder {

	private LinkedList<Verify> linkedList = new LinkedList<> ();

	public VerifyBuilder append (Verify verify) {
		if (linkedList.isEmpty ()) {
			// ...
		} else {
			linkedList.getLast ().setNextVerify (verify);
		}
		linkedList.add (verify);
		return this;
	}

	public void execute (ISuccessCallback callback) {
		linkedList.getLast ().setSuccessCallback (callback);
		linkedList.getFirst ().execute ();
	}

}
