package com.bookbuf.verify.base;

/**
 * 职责链
 */
public abstract class Chain<Verify> implements IChain<Verify> {

	protected Verify verify;
	protected ISuccessCallback successCallback;

	public void setNextVerify (Verify verify) {
		this.verify = verify;
	}

	protected Verify getNextVerify () {
		return verify;
	}

	public ISuccessCallback getSuccessCallback () {
		return successCallback;
	}

	public void setSuccessCallback (ISuccessCallback successCallback) {
		this.successCallback = successCallback;
	}
}
