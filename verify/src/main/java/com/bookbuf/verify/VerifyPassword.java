package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * Created by robert on 15/11/6.
 */
public class VerifyPassword extends Verify<VerifyPassword.Callback> {

	private String value;

	public VerifyPassword (Callback callback, String value) {
		super (callback);
		this.value = value;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyPassword (value);
	}

	@Override
	protected void onFailedCallback (int code) {
		switch (code) {
			case VerifyCode.BS_VERIFY_PASSWORD_ILLEGAL:
				callback.onIllegal (value);
				break;
			default:
				break;
		}
	}


	public interface Callback extends IVerifyCallback {
		void onIllegal (String value);
	}
}
