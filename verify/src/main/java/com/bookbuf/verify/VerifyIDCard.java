package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * Created by robert on 15/11/6.
 */
public class VerifyIDCard extends Verify<VerifyIDCard.Callback> {

	private String idCard;

	public VerifyIDCard (Callback callback, String idCard) {
		super (callback);
		this.idCard = idCard;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyIDCard (idCard);
	}

	@Override
	protected void onFailedCallback (int code) {

		switch (code) {
			case VerifyCode.BS_BIND_IDCRAD_ILLAGE:
				callback.onIllegal (idCard);
				break;
			case VerifyCode.BS_BIND_IDCRAD_NULL:
				callback.onEmpty (idCard);
				break;
			default:
				break;
		}

	}

	public interface Callback extends IVerifyCallback {

		void onEmpty (String value);

		void onIllegal (String value);
	}
}
