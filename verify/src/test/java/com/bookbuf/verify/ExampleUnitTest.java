package com.bookbuf.verify;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
	@Test
	public void addition_isCorrect () throws Exception {
		assertEquals (4, 2 + 2);
	}

	@Test
	public void test_verifyRealName () throws Exception {
		assertTrue ((VerifyImpl.verifyRealName ("陈") == VerifyCode.BS_NAME_TOO_SHORT));
		assertTrue ((VerifyImpl.verifyRealName ("陈俊") == VerifyCode.CODE_SUCCESS));
		assertTrue ((VerifyImpl.verifyRealName ("陈俊棋") == VerifyCode.CODE_SUCCESS));
		assertTrue ((VerifyImpl.verifyRealName ("陈俊棋啊") == VerifyCode.CODE_SUCCESS));
		assertTrue ((VerifyImpl.verifyRealName ("陈A") == VerifyCode.BS_CN_ILLAGE));
		assertTrue ((VerifyImpl.verifyRealName ("AA") == VerifyCode.BS_CN_ILLAGE));
		assertTrue ((VerifyImpl.verifyRealName ("ABC陈俊棋") == VerifyCode.BS_CN_ILLAGE));
		assertTrue ((VerifyImpl.verifyRealName ("") == VerifyCode.BS_CN_ILLAGE));
		//Assert.assertTrue ((VerifyImpl.verifyRealName (null) == VerifyCode.BS_CN_ILLAGE));
	}

	@Test
	public void test_verifyDSL () throws Exception {
		assertEquals (VerifyCode.CODE_SUCCESS, VerifyImpl.verifyPhone ("13878561150"));
		assertEquals (VerifyCode.CODE_SUCCESS, VerifyImpl.verifyPhone ("13578561150"));
	}
}